/**
 */
window.onload = function () {
    toolsinit((ma)=>{
        // return;
        ma.hideborder()
        ma.setNodename('聽力理解','點擊播放鈕聽全部，點擊句子可單句發音，點擊開啟英中對照鈕看翻譯')
        templete['item']=$('[data-content-inside] [name="item"]')[0].outerHTML
        templete['item_selected']=$('[data-content-inside] [name="item_selected"]')[0].outerHTML
        templete['a']=$('[data-content-inside] [name="a"]')[0].outerHTML
        templete['b']=$('[data-content-inside] [name="b"]')[0].outerHTML
        let lesson_id=data.DocumentElement.Dialogue.lesson_id;
        // if (!Array.isArray(data.DocumentElement.Listen.listening)){
        //     data.DocumentElement.Listen.listening=[data.DocumentElement.Listen.listening];
        // }
        // for (let i=0;i<data.DocumentElement.Listen.listening.length;i++){
        //     if(!Array.isArray(data.DocumentElement.Listen.listening[i].listening_listening)){
        //         data.DocumentElement.Listen.listening[i].listening_listening=[data.DocumentElement.Listen.listening[i].listening_listening]
        //     }
        // }
        showall(data.DocumentElement.Listen.listening[0].listening_listening)
        let btn=$('[data-content-inside] [name="button"]')[0].outerHTML;
        let btn_highlight=$( templete['item_selected']);
        let lastbtn;
        $('[data-content-inside] [name="btn_container"]').empty();
        if(data.DocumentElement.Listen.listening.length<=1){
            $('[data-content-inside] [name="btn_container"]').hide()
        }else{
            $('[data-content-inside] [name="btn_container"]').show()
            for(let i=0;i<data.DocumentElement.Listen.listening.length;i++){
                let pp=$(btn);
                pp.i=i;
                $('[data-content-inside] [name="btn_container"]').append(pp)
                pp.text(i+1)
                pp.on('click',(e)=>{
                    toggleElements(lastbtn,btn_highlight,true,true,0)
                    toggleElements(pp,btn_highlight,true,true,0);
                    lastbtn=pp;
                    showall(data.DocumentElement.Listen.listening[i].listening_listening)
                })
                if(i==0){
                    lastbtn=pp
                    toggleElements(pp,btn_highlight,true,true,0)
                }
            }
        }
        function showall(dt){
            $('[data-content-inside] [name="btn_toggleChinese"]').text('開啟英中對照')


            $('[name="allother"]').remove();
            let astyle=$(templete['a'])
            let bstyle=$(templete['b'])
            let selectedstyle=$(templete['item_selected']);
            let ary=[];
                // '../material/dialogue_201_1_e.mp3'
                // ,'../material/dialogue_201_2_e.mp3'
                // ,'../material/dialogue_201_3_e.mp3'
                // ,'../material/dialogue_201_4_e.mp3'
                // ,'../material/dialogue_201_5_e.mp3'
                // ];

            for (let i=0;i<dt.length;i++){
                ary.push(x_material_path+dt[i].listening_e_snd+'.mp3')
            }
            
            let mplay=new mp3playback(ary)
            MaxMediaPlayer.AllStop();
            $('[data-content-inside]  playback').empty();
            $('[data-content-inside]  playback').append(mplay.view);
            mplay.addEventChangeIndex((newIndex) => {
                newfocus(newIndex)
            })

            // let mediaplayer = new MaxMediaPlayer(ary, $("[name='btn_play']"), $("[name='progressBar']"), () => {
            //     console.log('完成')
            // },$('[name="btn_pause"]'),$('[name="btn_loop"]'),$('[name="btn_speed"]'))

            // mediaplayer.addEventChangeIndex((newIndex) => {
            //     newfocus(newIndex)
            // })

            // let mediaplayer=new MaxMediaPlayer(ary, $("[name='btn_play']"),$("[name='progressBar']"),()=>{
            //     console.log('完成')
            // })
            // mediaplayer.addEventChangeIndex((newIndex)=>{
            //     // console.log(newIndex)
            //     newfocus(newIndex)
            // })
        


            $('[data-content-inside] [name="btn_toggleChinese"]').off('click').on('click',(e)=>{
                if ($('[data-content-inside] [name="cht_name"]').is(':hidden')) {
                    $(e.currentTarget).text('關閉英中對照')
                    $('[data-content-inside] [name="cht_name"]').show()
                    $('[data-content-inside] [name="cht_text"]').show()
                }else{
                    $(e.currentTarget).text('開啟英中對照')
                    $('[data-content-inside] [name="cht_name"]').hide()
                    $('[data-content-inside] [name="cht_text"]').hide()
                }

            })
            $('[data-content-inside] [name="container"]').empty()
            
            start()
            function newfocus(newIndex){
                for(let i=0;i<$('[data-content-inside] [name="container"]').children().length;i++){
                    let p=$($('[data-content-inside] [name="container"]').children()[i])
                    toggleElements(p,selectedstyle,false,false)
                }
                let p=$($('[data-content-inside] [name="container"]').children()[newIndex])
                toggleElements(p,selectedstyle,false,true)
            }
            function start(){
                var maxWidth = 0;       
                for (let i=0;i<dt.length;i++){
                    // console.log('../material/'+dt[i].pic)
                    // Roberto.jpg
                    // console.log('../material/'+dt[i].dialogue_e_snd+'.mp3')
                    let p=$( templete['item'])
                    $('[data-content-inside] [name="container"]').append(p)
                    $('[name="eng_name"]',p).text(dt[i].listening_e.split(':')[0]+':')
                    $('[name="cht_name"]',p).text(dt[i].listening_c.split('：')[0]+':')
                    $('[name="eng_text"]',p).text(dt[i].listening_e.split(':').slice(1).join(':'))
                    $('[name="cht_text"]',p).text(dt[i].listening_c.split('：').slice(1).join('：'))
                    if(i%2==0){
                        toggleElements(p,astyle)
                    }else{
                        toggleElements(p,bstyle)
                    }
                    p.click((e)=>{
                        console.log(i)
                        mplay.mediaplayer.playindex(i,false);
                        // mplay.playindex(i,false);
                        newfocus(i);
                    })
                    // console.log( $('[name="namebox"]',p).width())
                    let bb=$('[name="namebox"]',p);
                    maxWidth = Math.max(maxWidth, bb.width());

                }
                $('[data-content-inside] [name="cht_name"]').hide()
                $('[data-content-inside] [name="cht_text"]').hide()
                $('[data-content-inside] [name="namebox"]').width(maxWidth)
            }

            // 2023.5.29 Tony 播放按鈕圖片切換
            // $('[name="btn_play"]').click(function () {
            //     let img = $('[name="btn_play"] img');
            //     if (img.attr('src') == '../main_book/play.svg') {
            //         img.attr('src', '../main_book/stop.svg');
            //     } else {
            //         img.attr('src', '../main_book/play.svg');
            //     }
            // });
        }
    })

}