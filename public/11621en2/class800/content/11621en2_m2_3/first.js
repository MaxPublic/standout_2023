/**
 * 聽音辨字
 */
// let templete = {}

window.onload = function () {
    // data = reduce(data)
    // word = reduce(word)
    toolsinit((ma)=>{
        // return;
        ma.hideborder()
        ma.setNodename('單字配對','依據題意與選項配對')
        // let QuizUi = $('[name="Quiz"]')
        // $('[name="QuizContainer"]').empty();

        // $('[name="btn_quizStart"]').hide();
        // jsonData = [
        //     {
        //         title: ["中文題目","中文題目","中文題目"],
        //         title_pic: '11544en2.png',
        //         title_mp3: 'vocab_2182_e',
        //         option_title: ['英文拖曳項目', '英文拖曳項目', '英文拖曳項目'],
        //         option_pic: ['11544en2_prereading.jpg', 'Gabriela.jpg', 'Roberto.jpg'],
        //         option_mp3: ['vocab_2183_e', 'vocab_2184_e', 'vocab_2185_e'],
        //         ans: [0,1],
        //         feedback:'feedback內容0',
        //         submit: [1]
        //     },
        //      ...
        // ]
        // jsonData=Quiz.convertDataToExamJson(data,type='listen_words')
        // console.log(jsonData)

        let QuizObj = new Quiz(data, word, {}, 'vocabulary_test_b0_08')
        QuizObj.addEventStatusChange((status = 'testing') => {
            //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
            console.log(status)
        })
        QuizObj.addEventEnd((data) => {
            //data={score:50,right:5,wrong:5}
            console.log(data)
        })
        // $('[name="QuizContainer"]').append(QuizObj.view);
        SpeedTest(()=>{
            QuizObj.showScore();
        })

    })

}