window.onload = function () {
    toolsinit((ma)=>{
        // return;
        ma.hideborder()
        ma.setNodename('全文對照','開啟字義解析可查生字，開啟英中對照可看翻譯')
        // console.log(JSON.stringify(word))/**數據最佳化 */
        let audio = new Audio();
        const wordList = word.DocumentElement.Word;
        let btnstyle = $('[data-content-inside] [name="btnstyle"]')
        let btnSlected = $('[data-content-inside] [name="btn_selected"]');

        // templete['wordSearch'] = $('[name="wordSearch"]')[0].outerHTML;
        templete['entochtContent'] = $('[data-content-inside] [name="entochtContent"]')[0].outerHTML;
        templete['btn'] = $('[data-content-inside] [name="btn"]')[0].outerHTML;
        templete['showDownLeft'] = $('[data-content-inside] [name="showDownLeft"]')[0].outerHTML;
        // $('[name="showDownLeft"]').remove()
        $('[data-content-inside] [name="showLeft"]').hide()
        $('[data-content-inside] [name="blayer"]').hide();

        let m = data.DocumentElement.Read;



        function parseText(input) {
            const buttonRegex = /<u>(.*?)<\/u>/g;
            const lineBreakRegex = /\/n\/n/g;
        
            let parsedText = input.replace(buttonRegex, (match, p1) => {
                let btn= $(templete['btn'])
                btn.html(p1);
            return btn[0].outerHTML;
            //   `<span class="button-text">${p1}</span>`;
            });
        
            parsedText = parsedText.replace(lineBreakRegex, "<br><br>");
            return parsedText;
        }
        if(true){
            let selectedButton = null;
            // let p = $('<div/>')
            // $(templete['wordSearch']);
            // p.html(parseText( m.reading_e));
            $('[data-content-inside] [name="reading_e"]').html(parseText( m.reading_e));
            $('[data-content-inside] [name="reading_c"]').html(m.reading_c.replace(/\/n\/n/g, "<br><br>"));
            $('[data-content-inside] .button-text').each(function() {
                $(this).replaceWith($('<button>', {
                text: $(this).text(),
                click: function() {
                    console.log($(this).text());
                }
                }));
            });
            $('[data-content-inside] [name="en_to_cht"]').click(function () {
                // if (isClick) {
                if ($('[data-content-inside] .EnToCht').is(':visible')) {
                    $(this).text('開啟英中對照');
                    $('[data-content-inside] .EnToCht').hide();
                    toggleElements($(this), btnSlected, true, true);
                }else{
                    // isClick = false;
                // } else {
                    $(this).text('關閉英中對照');
                    $('[data-content-inside] .EnToCht').show();
                    // isClick = true;
                    toggleElements($(this), btnSlected, true, true);
                }
            });
            $('[data-content-inside] [name="word_search"]').click(function () {
                // showOrHide = !showOrHide
                let a = $('[data-content-inside] [name="btn"]')
                if ( $(this).text()=='開啟字義解析') {
                    $('[data-content-inside] [name="showLeft"]').hide()
                    $('[data-content-inside] [name="blayer"]').show();
            
                    $(this).text('關閉字義解析');
                    toggleElements($(this), btnSlected, true, true);
                    for (let i = 0; i < a.length; i++) {
                        toggleElements($(a[i]), btnstyle, false, true);
                    }
        
                } else {
                    $('[data-content-inside] [name="blayer"]').hide();
                    $(this).text('開啟字義解析');
                    toggleElements($(this), btnSlected, true, true);
                    for (let i = 0; i < a.length; i++) {
                        toggleElements($(a[i]), btnstyle, false, false);
                        toggleElements(selectedButton, btnSlected, false, false);
                    }
        
                }
            })
            // $('[name="wordSearch"]').append(p);
            $('btn').unbind('click').bind('click', function (event) {
                if ($('[data-content-inside] [name="word_search"]').text()=='開啟字義解析') {
                    return;
                }

                if (selectedButton !== null) {
                    toggleElements(selectedButton, btnSlected, false, false);
                }
                selectedButton = $(this);
                // $(event.currentTarget)
                toggleElements(selectedButton, btnSlected, false, true);

                // $('[data-content-inside] [name="showLeft"]').empty();
                for (let i = 0; i < wordList.length; i++) {
                    if ((wordList[i].word).toLowerCase() == ($(this).text()).toLowerCase()) {
                        let offset=$('[data-content-inside] [name="blayer"]').offset();
                        // 英文單字
                        // let c = $(templete['showDownLeft']);
                        $('[data-content-inside] [name="showDownLeft"] [name="popword"]').html(wordList[i].meaning);
                        // $('[name="popword"]', c).html(wordList[i].meaning);
                        // 詞性
                        $('[data-content-inside] [name="showDownLeft"] [name="part_of_speech"]').text(wordList[i].part_of_speech);

                        $('[data-content-inside] [name="showLeft"]').css({
                            "left": event.pageX-offset.left,
                            // ,

                            // event.pageX, - offset.left,
                            // 20,
                            "top":event.pageY-offset.top+20
                            // 
                            // event.pageY -offset.top
                            // + 30
                            // "position": "fixed"
                        });
                        $('[data-content-inside] [name="showLeft"]').show()
                        // $('[data-content-inside] [name="showLeft"]').addClass('showList');
                        // $('[data-content-inside] [name="showLeft"]').append(c);

                        let mp3 = '../vocabWizard/' + wordList[i].voice + '.mp3';
                        play(mp3);
                        return;
                    }
                }

            });
            // if (window.parent && window !== window.parent) {
            //     try {
            //         // 嘗試訪問 parent 的某個屬性來檢查同源政策
            //         var parentUrl = window.parent.location.href;
            //         console.log('可以訪問 window.parent:', parentUrl);
            //     } catch (e) {
            //         console.log('不能訪問 window.parent，可能因為同源政策的限制:', e);
            //     }
            // } else {
            //     console.log('頁面不在 iframe 中運行。');
            // }
            
            
            // window.addEventListener('scroll', function() {
            //     console.log('頁面被捲動了!');
            // });
            
            // $(window).on('scroll', function() {
            //     console.log('頁面被捲動了!');
            
            //     // 如果想知道捲動的具體位置，可以使用以下方法
            //     // let scrollTop = $(this).scrollTop();
            //     // console.log('捲動位置:',
            // })
            // $('body').on('scroll', function() {
            //     console.log('div 被捲動了!');
        
            //     // 如果想知道捲動的具體位置，可以使用以下方法
            //     // let scrollTop = $(this).scrollTop();
            //     // console.log('捲動位置:', scrollTop);
            // });
        }





    // return;

    //     // 開啟字義解析
    //     $('[name="reading_e"]').empty();
    //     let p = $(templete['wordSearch']);
    //     m.reading_e=m.reading_e.replace(/\/n\/n/g, "<br><br>")
    //     // console.log(m.reading_e)
    //     $('[name="reading_e"]', p).text(m.reading_e);
    //     $('[name="wordSearch"]').append(p);
    // // return;

    //     // 開啟英中對照內容
    //     $('[name="entochtContent"]').empty();
    //     let s = $(templete['entochtContent']);
    //     m.reading_c=m.reading_c.replace(/\/n\/n/g, "<br><br>")
    //     $('[name="reading_c"]', s).html(m.reading_c);
    //     $('[name="EnToCht"]').append(s);

        
    //     // const resultList = $.map(wordList, function (item) {
    //     //     return item.word;
    //     // });
    //     const resultList = wordList.map(item => Object.values(item)[1]);
    //     const escapedWordList = resultList.map(w => w.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'));
    //     // console.log(escapedWordList);

    //     // for (let i = 0; i < wordList.length; i++) {
    //     //     // const words = wordList[i].word;
    //     //     // console.log(words);
    //     //     const escapedWord = resultList.map(w => w.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'));
    //     //     o.push(escapedWord);
    //     // }
    //     const articleElement = document.querySelector(".wordSearch");
    //     const articleContent = articleElement.textContent;

    //     const regex = new RegExp(`\\b(${escapedWordList.join("|")})\\b`, "gi");

    //     const highlightedContent = articleContent.replace(regex, templete['btn']);
    //     console.log(highlightedContent)
    //     articleElement.innerHTML = highlightedContent;



    //     let isClick = false;
    //     $('[name="en_to_cht"]').click(function () {
    //         if (isClick) {
    //             $(this).text('開啟英中對照');
    //             $('.EnToCht').hide();
    //             isClick = false;
    //         } else {
    //             $(this).text('關閉英中對照');
    //             $('.EnToCht').show();
    //             isClick = true;
    //         }
    //     });


    //     // 字義解析顏色
    //     let isBtnClick = false;
    //     let selectedButton = null;
    //     let showOrHide = false;
    //     $('[name="word_search"]').click(function () {
    //         showOrHide = !showOrHide
    //         let a = $('[name="btn"]')
    //         if (showOrHide) {
    //             $(this).text('關閉字義解析');
    //             for (let i = 0; i < a.length; i++) {
    //                 toggleElements($(a[i]), btnstyle, false, true);
    //             }

    //         } else {
    //             $(this).text('開啟字義解析');             
    //             for (let i = 0; i < a.length; i++) {
    //                 toggleElements($(a[i]), btnstyle, false, false);
    //                 toggleElements(selectedButton, btnSlected, false, false);
    //             }

    //         }

    //         $('[name="showLeft"]').empty();

    //         if (isBtnClick) {
    //             isBtnClick = false;
    //         } else {
    //             isBtnClick = true;
    //             $('btn').unbind('click').bind('click', function (event) {
    //                 if (!isBtnClick) {
    //                     return;
    //                 }

    //                 if (selectedButton !== null) {
    //                     toggleElements(selectedButton, btnSlected, false, false);
    //                 }
    //                 selectedButton = $(this);
    //                 // $(event.currentTarget)
    //                 toggleElements(selectedButton, btnSlected, false, true);

    //                 $('[name="showLeft"]').empty();
    //                 for (let i = 0; i < wordList.length; i++) {
    //                     if ((wordList[i].word).toLowerCase() == ($(this).text()).toLowerCase()) {

    //                         // 英文單字
    //                         let c = $(templete['showDownLeft']);
    //                         $('[name="popword"]', c).html(wordList[i].meaning);
    //                         // 詞性
    //                         $('[name="part_of_speech"]', c).text(wordList[i].part_of_speech);

    //                         $('[name="showLeft"]').css({
    //                             "left": event.pageX - 20,
    //                             "top": event.pageY + 30
    //                         });
    //                         $('[name="showLeft"]').addClass('showList');
    //                         $('[name="showLeft"]').append(c);

    //                         let mp3 = '../vocabWizard/' + wordList[i].voice + '.mp3';
    //                         play(mp3);
    //                         return;
    //                     }
    //                 }

    //             });
    //         }
    //     });

        function play(mp3) {
            audio.src = mp3;
            audio.play();
        }
    });
}