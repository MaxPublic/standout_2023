
window.onload = function () {

    toolsinit((ma) => {
        // return;
        ma.hideborder()
        ma.setNodename('會話文法摘要', '觀看下列摘要文字，回顧本課重點')
        // templete['group'] = $('[name="group"]')[0].outerHTML;
        templete['conversation'] = $('[data-content-inside] [name="conversation"]')[0].outerHTML;
        templete['example'] = $('[data-content-inside] [name="example"]')[0].outerHTML;
        templete['content'] = $('[data-content-inside] [name="content"]')[0].outerHTML;
        templete['group1'] = $('[data-content-inside] [name="group1"]')[0].outerHTML;
        templete['content1'] = $('[data-content-inside] [name="content1"]')[0].outerHTML;
        templete['group2'] = $('[data-content-inside] [name="group2"]')[0].outerHTML;
        templete['content2'] = $('[data-content-inside] [name="content2"]')[0].outerHTML;
        go()
    });
    function go() {



        let m = data.DocumentElement.Dialogue.dailogue;
        let a = data.DocumentElement.Dialogue.pattern;
        console.log(a);

        // 會話複習
        $('[data-content-inside] [name="group"]').first().empty();
        for (let i = 0; i < m.length; i++) {
            let c = $(templete['content']);
            $('[name="dialogue_e"]', c).text(m[i]['dialogue_e']);
            $('[name="dialogue_c"]', c).text(m[i]['dialogue_c']);
            $('[name="group"]').first().append(c);
        }
        // 例句
        let gg = data.DocumentElement.Dialogue
        if (Array.isArray(gg.pattern)) {
        } else if (typeof gg.pattern === 'object' && gg.pattern !== null) {
            gg.pattern = [gg.pattern]
            console.log('做成陣列')
        }
        $('[data-content-inside] [name="group1"]').empty();
        for (let i = 0; i < gg.pattern.length; i++) {
            let a = gg.pattern[i]
            let aa = a.pattern.split('/n')
            let bb = a.pattern_c.split('/n')

            for (j = 0; j < aa.length; j++) {
                let p = $(templete['content1']);
                $('[name="pattern"]', p).html(aa[j]);
                $('[name="pattern_c"]', p).html(bb[j]);
                $('[name="group1"]').append(p);
            }

        }
        //     let newPattern=aa.replace(/\/n/g, "<br>");
        //     let newPattern_c=bb.replace(/\/n/g, "<br>");
        //     $('[name="group1"]').empty();
        //     let p = $(templete['content1']);
        //     $('[name="pattern"]', p).html(newPattern);
        //     $('[name="pattern_c"]', p).html(newPattern_c);
        //     $('[name="group1"]').append(p);
        // }
        // let newPattern=a.pattern.replace(/\/n/g, "<br>");
        // let newPattern_c=a.pattern_c.replace(/\/n/g, "<br>");
        // $('[name="group1"]').empty();
        // let p = $(templete['content1']);
        // $('[name="pattern"]', p).html(newPattern);
        // $('[name="pattern_c"]', p).html(newPattern_c);
        // $('[name="group1"]').append(p);

        // 句型解說
        $('[data-content-inside] [name="group2"]').empty();
        for (let i = 0; i < gg.pattern.length; i++) {
            let a = gg.pattern[i]
            let aa = a.pattern_explain.split('/n')

            for (j = 0; j < aa.length; j++) {
                let p = $(templete['content2']);
                $('[name="pattern_explain"]', p).html(aa[j]);
                // $('[name="pattern_c"]', p).html(bb[j]);
                $('[name="group2"]').append(p);
            }

        }
        // let newExamplain=a.pattern_explain.replace(/\/n/g, "<br>");
        // $('[name="group2"]').empty();
        // let b = $(templete['content2']);
        // $('[name="pattern_explain"]', b).html(newExamplain);
        // $('[name="group2"]').append(b);

        // 文法表格
        // let form = '../material/' + data.DocumentElement.Dialogue.lesson_id + '_grammar chart.png';
        // $('[name="lesson_id"]').attr('src', form);
        $('[data-content-inside] [name="content3"]').empty()
        // data = reduce(data)
        // console.log(JSON.stringify(data))/**數據最佳化 */
        let filename = data.DocumentElement.Info.course_id + '_grammar.html'
        //'./11544en2_grammar.html'
        function extractBodyContent(html) {
            var bodyTagStart = "<body>";
            var bodyTagEnd = "</body>";
            var bodyStartIndex = html.indexOf(bodyTagStart);
            var bodyEndIndex = html.indexOf(bodyTagEnd);

            if (bodyStartIndex === -1 || bodyEndIndex === -1) {
                return ""; // 無法找到完整的<body>...</body>，直接返回空字符串
            }

            var bodyContent = html.slice(bodyStartIndex + bodyTagStart.length, bodyEndIndex);

            // 移除所有的<script>標籤
            bodyContent = bodyContent.replace(/<script[\s\S]*?<\/script>/gi, '');

            // 將<body>和</body>替換為<div>和</div>
            bodyContent = "<div>" + bodyContent + "</div>";

            return bodyContent;
        }

        // $.get('../material/'+filename, function(data) {
        //     var bodyContent = extractBodyContent(data);
        //     $('body').append($(bodyContent))
        //     console.log(bodyContent);

        // });
        //表格路徑不能直接取用舊的，因為舊版沒有這個東西
        // $.get(x_grammar_path + filename, function (data) {
        //     var bodyContent = extractBodyContent(data);
        //     $('[name="content3"]').append($(bodyContent))
        //     // console.log(bodyContent);

        // });
        $.get(x_grammar_path + filename)
        .done(function(data) {
            var bodyContent = extractBodyContent(data);
            $('[data-content-inside] [name="content3"]').append($(bodyContent))
            // console.log(bodyContent);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // 請求失敗時執行這裡的代碼
            $("[data-content-inside] [name='grammar']").remove()
            
        });
    }
}