/**
 * 課後挑戰（包含前段的選單）
 */
 window.onload = function () {
    let all;
    let titledict={
        "vocabulary_test":{"title":"單字配對","info":"依據題意與選項配對"},
        "cloze_test":{"title":"克漏字","info":"依照題意，在作答區中選擇正確答案，按下確定鈕送出答案"},
        "listening_test":{"title":"聽力填空","info":"點擊喇叭圖示聆聽發音，接著在作答區進行填空，按下確定鈕送出答案"},
        "cloze_vocabulary":{"title":"單字克漏字","info":"依據題意選擇正確答案"},
        "listening_test_sentence":{"title":"聽力填空","info":"點擊喇叭圖示聆聽發音，接著在作答區進行填空，按下確定鈕送出答案"},
        "writing_test":{"title":"短文填空","info":"依序點擊選項完成句子"}
    }
    let pox={
        'G0_01':'vocabulary_test',
        'G0_02':'cloze_test',
        'G0_03':'listening_test',
        'G0_04':'cloze_vocabulary',
        'G0_05':'listening_test_sentence',
        'G0_06':'writing_test'
    }
    let _i_info=[];
    toolsinit((ma)=>{
        ma.hideborder()
        ma.setNodename('課後挑戰','')
        let pageKey = window.location.pathname.replace(/\//g, '_');
        let cid=data.DocumentElement.Info.course_id;//11621en2
        //從總表內讀取課後挑戰的三個節點
        
        $.getJSON('../main/standout_classlist.json', function(__data) {
            for(let i=0;i<__data.length;i++){
                let p=__data[i]
                if(p['教材編號']==cid){
                    // console.log(p['節點名稱'],p['節點名稱'].split('_')[1].trim()=='課後挑戰')
                    if(p['節點名稱'].split('_')[1].trim()=='課後挑戰'){
                        // console.log('找到了',p['模組名稱'].substr(0,5))
                        //找到三個互動內容，不需要再依靠產生器
                        _i_info.push(pox[p['模組名稱'].substr(0,5)]);
                    }
                }else{
                    continue
                }
            }
            console.log(_i_info);
            allreset()
        })
        


        // all=$("[data-content-inside]")
        // console.log(all[0].outerHTML)
        // let all=ma.getContent();
        // $("[data-content-inside]")
        // let _i_info=['vocabulary_test','cloze_test','listening_test']//會有這個變數存在
        // all=$('[name="all"]')[0].outerHTML
        

        function allreset(){
            if(true){
                let types=_i_info;
                SpeedTest(()=>{
                    //只要某種題型0,1,2
                    // types=_i_info[2]
                })
                let QuizObj=new Quiz(data,word,{},types,0)
                QuizObj.RemoveWrong(true);//沒有錯題重答
                QuizObj.addEventStatusChange((status='testing',index,_data)=>{
                    //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
                    // console.log(_data,status)
                    if(status=='測驗進行中'){
                        ma.setNodename('','')
                        // console.log(_data)
                        // $(`[name="${_i_info[i]}"]`).show()
                        // ma.setNodename(titledict[_data.subtype].title,titledict[_data.subtype].info)
                        if(_data.subtype){
                            ma.setNodename(titledict[_data.subtype].title,titledict[_data.subtype].info)
                        }
                    }else if (status=='呈現成績'){
                        localStorage.setItem(pageKey, "true");
                        ma.setNodename('課後挑戰','');
                    }else{
                        ma.setNodename('','');
                    }
                })
                QuizObj.addEventEnd((_data)=>{
                    //_data={score:50,right:5,wrong:5}
                    console.log(_data)
                })
                QuizObj.addResetCallback((goNext)=>{
                //已經完全結束，如果需要重新下一題，就呼叫這個函數
                //或自行定義其他行為。
                    console.log('全部結束，重新開始',all)
                    allreset();
                    // goNext()
                })
                SpeedTest(()=>{
                    //直接秀結果畫面
                    // QuizObj.showScore()
                })
                return;
            }
            if(true){
                all=ma.goFirstContent();
                $('[name="vocabulary_test"]',all).hide()
                $('[name="cloze_test"]',all).hide()
                $('[name="listening_test"]',all).hide()
                $('[name="cloze_vocabulary"]',all).hide()
                $('[name="listening_test_sentence"]',all).hide()
                $('[name="writing_test"]',all).hide()
                for(let i=0;i<_i_info.length;i++){
                    //判斷目前需要的按鈕
                    $(`[name="${_i_info[i]}"]`).show()
                }
                if(localStorage.getItem(pageKey)=="false"){
                    //尚未完成闖關者，必須強制全選
                    for(let i=0;i<radio.list.length;i++){
                        let p=radio.list[i];
                        p.setcheck(true);
                        p.disabled();
                    }                    
                }
                
            }
            start()
        }
        function st______art(){
            // $('body',all).empty()
            // $('body',all).append($(all))
            // let QuizUi=$('[name="Quiz"]',all)
            $('[name="QuizContainer"]',all).empty();
            $('[name="btn_quizStart"]',all).click((e)=>{
                let types=radio.getvalue()
                if(types.length<=0){
                    alert('至少必須選擇一個項目。')
                    return;
                }
                // $('[name="firstpage_view"]',all).hide()
                //    $('body',all).empty();
                let QuizObj=new Quiz(data,word,{},types,0)
                // QuizObj.setMa(ma);
                QuizObj.addEventStatusChange((status='testing',index,_data)=>{
                    //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
                    console.log(_data,status)
                    if(status=='測驗進行中'){
                        ma.setNodename('','')
                        console.log(_data)
                        // $(`[name="${_i_info[i]}"]`).show()
                        // ma.setNodename(titledict[_data.subtype].title,titledict[_data.subtype].info)
                        if(_data.subtype){
                            ma.setNodename(titledict[_data.subtype].title,titledict[_data.subtype].info)
                        }
                    }else if (status=='呈現成績'){
                        localStorage.setItem(pageKey, "true");
                        ma.setNodename('課後挑戰','');
                    }else{
                        ma.setNodename('','');
                    }
                })
                QuizObj.addEventEnd((_data)=>{
                    //_data={score:50,right:5,wrong:5}
                    console.log(_data)
                })
                QuizObj.addResetCallback((goNext)=>{
                //已經完全結束，如果需要重新下一題，就呼叫這個函數
                //或自行定義其他行為。
                    console.log('全部結束，重新開始',all)
                    // $('[name="firstpage_view"]',all).show()
                    // start();
                    allreset();
                    //    $('body',all).empty()
                    //    $('body',all).append($(all))
                    //    QuizUi=$('[name="Quiz"]',all)
                    //    $('[name="QuizContainer"]',all).empty();
                    //    $('[name="firstpage_view"]',all).show()
                    // goNext()
                })
                // $('body',all).empty()
                // $('[name="QuizContainer"]',all).append(QuizObj.view);
            })
        
        }
    });
 }