/**
 * 單字瀏覽
 */
//  let templete = {}

 window.onload = function () {
    // data = reduce(data)
    // word = reduce(word)
    toolsinit((ma)=>{
        // return;
        ma.hideborder()
        ma.setNodename('單字瀏覽','學習本課單字並點擊圖示聆聽發音')
        let audio = new Audio();
        templete["item"]=$('[data-content-inside] [name="item"]')[0].outerHTML
        let container=$('[data-content-inside] [name="container"]')
        container.empty()
        let vv=data.DocumentElement.Vocabulary
        vv.sort((a, b) => a.order - b.order);
        let prop = ["vocab", "meaning_1", "kk", "part_of_speech_1", "vocab_e_snd", "part_of_speech_1", "meaning_1", "eg_1", "eg_meaning_1", "part_of_speech_2", "meaning_2", "eg_2", "eg_meaning_2"]
        // part_of_speech_2meaning_2
        let firstp;
        for(let i=0;i<vv.length;i++){
            let p=$(templete["item"])
            if(!firstp){//拿取第一個項目，自動播放聲音用的
                firstp=p;
            }
            // $('[name="title"] [name="no"]',p).text(i+1)
            container.append(p)
            p.data=vv[i]
            if (p.data.part_of_speech_2=='' && p.data.meaning_2==''){
                $('[name="part_of_speech_2"]',p).parent().hide()
            }
            $('[name="btn_play"]',p).off('click').on('click',(e)=>{
                play(x_material_path+p.data.vocab_e_snd+'.mp3')
            })
            console.log('===')
            for (let j=0;j<prop.length;j++){
                let c=$('[name="descript"] [name="'+prop[j]+'"]',p)
                if(c.length>=1){
                    console.log(p.data['vocab'])
                    c.text(p.data[prop[j]])
                }
            }

        }
        play(x_material_path+firstp.data.vocab_e_snd+'.mp3')
        function play(mp3){
            audio.src = mp3;
            audio.play();
        }

        // Tony 20230714
        $(window).resize(function(){
            let width= $(window).width();
            // console.log(width);

            // if(width<768){
            //     $('[name="point"]').text('....................................................................');
            // }else{
            //     $('[name="point"]').text('.................................................................................................................................................................');
            // }
        });
    })
 }