/**2023/2/10 Max */

window.onload=function(){
    toolsinit((ma)=>{
        // return;
        ma.hideborder()
        ma.setNodename('會話理解','點擊播放按鈕聽全部，點擊句子可單句播放')
        let lesson_id=data.DocumentElement.Dialogue.lesson_id;
        let dt=data.DocumentElement.Dialogue.dailogue;
        // console.log(lesson_id,dt);
        // console.log(JSON.stringify(data))/**數據最佳化 */
        templete['item']=$('[name="item"]')[0].outerHTML
        templete['item_selected']=$('[name="item_selected"]')[0].outerHTML
        templete['a']=$('[name="a"]')[0].outerHTML
        templete['b']=$('[name="b"]')[0].outerHTML

        $('[name="allother"]').remove();
        let astyle=$(templete['a'])
        let bstyle=$(templete['b'])
        let selectedstyle=$(templete['item_selected']);
        let ary=[];
            // '../material/dialogue_201_1_e.mp3'
            // ,'../material/dialogue_201_2_e.mp3'
            // ,'../material/dialogue_201_3_e.mp3'
            // ,'../material/dialogue_201_4_e.mp3'
            // ,'../material/dialogue_201_5_e.mp3'
            // ];

        for (let i=0;i<dt.length;i++){
            ary.push(x_material_path+dt[i].dialogue_e_snd+'.mp3')
        }
        
        let a=new mp3playback(ary)
        // $('body').empty()
        $('playback').append(a.view);
        a.addEventChangeIndex((newIndex) => {
            newfocus(newIndex)
        })

        // let mediaplayer = new MaxMediaPlayer(ary, $("[name='btn_play']"), $("[name='progressBar']"), () => {
        //     console.log('完成')
        // },$('[name="btn_pause"]'),$('[name="btn_loop"]'),$('[name="btn_speed"]'))

        // mediaplayer.addEventChangeIndex((newIndex) => {
        //     newfocus(newIndex)
        // })

        // let mediaplayer=new MaxMediaPlayer(ary, $("[name='btn_play']"),$("[name='progressBar']"),()=>{
        //     console.log('完成')
        // })
        // mediaplayer.addEventChangeIndex((newIndex)=>{
        //     // console.log(newIndex)
        //     newfocus(newIndex)
        // })
    


        $('[name="btn_toggleChinese"]').click((e)=>{
            if ($('[name="cht_name"]').is(':hidden')) {
                $(e.currentTarget).text('關閉英中對照')
                $('[name="cht_name"]').show()
                $('[name="cht_text"]').show()
            }else{
                $(e.currentTarget).text('開啟英中對照')
                $('[name="cht_name"]').hide()
                $('[name="cht_text"]').hide()
            }

        })
        $('[name="container"]').empty()
        $('[name="btn_container"]').hide()
        start()
        function newfocus(newIndex){
            for(let i=0;i<$('[name="container"]').children().length;i++){
                let p=$($('[name="container"]').children()[i])
                toggleElements(p,selectedstyle,false,false)
            }
            let p=$($('[name="container"]').children()[newIndex])
            toggleElements(p,selectedstyle,false,true)
        }
        function start(){
            var maxWidth = 0;       
            for (let i=0;i<dt.length;i++){
                // console.log('../material/'+dt[i].pic)
                // Roberto.jpg
                // console.log('../material/'+dt[i].dialogue_e_snd+'.mp3')
                let p=$( templete['item'])
                $('[name="container"]').append(p)
                $('[name="eng_name"]',p).text(dt[i].dialogue_e.split(':')[0]+':')
                $('[name="cht_name"]',p).text(dt[i].dialogue_c.split('：')[0]+':')
                $('[name="eng_text"]',p).text(dt[i].dialogue_e.split(':').slice(1).join(':'))
                $('[name="cht_text"]',p).text(dt[i].dialogue_c.split('：').slice(1).join('：'))
                if(i%2==0){
                    toggleElements(p,astyle)
                }else{
                    toggleElements(p,bstyle)
                }
                p.click((e)=>{
                    console.log(i)
                    a.mediaplayer.playindex(i,false);
                    // mediaplayer.playindex(i,false);
                    newfocus(i);
                })
                // console.log( $('[name="namebox"]',p).width())
                let bb=$('[name="namebox"]',p);
                maxWidth = Math.max(maxWidth, bb.width());

            }
            $('[name="cht_name"]').hide()
            $('[name="cht_text"]').hide()
            $('[name="namebox"]').width(maxWidth)
        }

        // 2023.5.29 Tony 播放按鈕圖片切換
        // $('[name="btn_play"]').click(function () {
        //     let img = $('[name="btn_play"] img');
        //     if (img.attr('src') == '../main_book/play.svg') {
        //         img.attr('src', '../main_book/stop.svg');
        //     } else {
        //         img.attr('src', '../main_book/play.svg');
        //     }
        // });
    });
  }