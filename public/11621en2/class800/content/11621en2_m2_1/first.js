/**2023/2/9 Max*/

// let templete={}
window.onload=function(){
    // data=reduce(data)
    toolsinit((ma)=>{
        ma.hideborder()
        ma.setNodename('記憶訓練','設定播放間隔後，觀看內容進行記憶訓練')

        let m=data.DocumentElement.Vocabulary;
        // console.log(JSON.stringify(data))/**數據最佳化 */

        $('[name="btn_start"]')[0].outerHTML
        $('[name="game"]')
        $('[name="radio_seconds"]')[0].outerHTML
        $('[name="pagecount"]')[0].outerHTML
        $('[name="containner"]')[0].outerHTML
        templete['card']=$('[name="card"]')[0].outerHTML
        // $('[name="corner"]')[0].outerHTML
        $('[name="cover"]')[0].outerHTML
        $('[name="text"]')[0].outerHTML
        
        let  containner= $('[name="containner"]')
        containner.empty()  
            
        let index=0;
        let restart=false;
        $('[name="cardview"]').hide()
        showindex(index)
        getvalue()
        let aa=new PausableTimeout((n)=>{
            // console.log('完畢')
            $('[name="cover"]',containner).hide()
            $('[name="text"]',containner).show()
            // console.log(n)
            if(n==0){
                newcard(index);
                showindex(index)
                // aa.start(getvalue()); 
            }else if (n==2){
                // $('[name="cover"]',a).hide()
                // $('[name="cover"]',b).hide()
            // }else{
                index++
                if(index>=m.length){
                    index=0;
                    // aa.start(getvalue());
                    aa.pause();
                    $('[name="btn_start"]').text('重新開始')
                    // $('[name="cardview"]').hide()
                    restart=true;
                    var radios = $('[name="radio_seconds"] input')
                    for (var i = 0, length = radios.length; i < length; i++) {
                        radios.eq(i).prop('disabled', false);
                    }
                    return;
                }else{
                    newcard(index);
                    showindex(index)
                    aa.start(getvalue()); 
                }
            }

            // setTimeout(() => {
            //     newcard(index);
            //     showindex(index)
            //     aa.start(getvalue()); 
            // }, getvalue());
            
        })
        // aa.start(getvalue());
        // aa.pause();
        // newcard(index); 
        $('[name="btn_start"]').click(()=>{
            //開始遊戲
            showindex(index)
            $('[name="cardview"]').show()
            // if(restart){
            //     console.log('重新開始啦')
            //     newcard(index);
            //     restart=false;
            //     aa.resume();
            //     $('[name="btn_start"]').text('暫停')
            //     return;
            // }
            if(aa.status()=='paused'){
                let sec=getvalue()
                // console.log(sec)
                aa.start(sec);
                // aa.pause();
                // newcard(index); 
                $('[name="btn_start"]').text('暫停')
                radio.disabled()
                // var radios = $('[name="radio_seconds"] input')
                // for (var i = 0, length = radios.length; i < length; i++) {
                //     radios.eq(i).prop('disabled', true);
                // }
            }else if(aa.status()=='running'){
                aa.pause()
                $('[name="btn_start"]').text('開始')
                radio.enabled()
                // var radios = $('[name="radio_seconds"] input')
                // for (var i = 0, length = radios.length; i < length; i++) {
                //     radios.eq(i).prop('disabled', false);
                // }
            }
            console.log(aa.status(),'===')
        })


        function showindex(index){
            $('[name="pagecount"]').text((index+1)+'/'+m.length)
        }
        function getvalue(){
            return radio.getvalue()[0]*1000
            return;
            var radios = $('[name="radio_seconds"] input')
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {
                    return radios[i].value*1000;
                }
            }
        }
        function newcard(index){
            containner.empty()
            let a=$(templete['card'])
            $('[name="text"]',a).text(m[index].vocab)
            let b=$(templete['card'])
            $('[name="text"]',b).text(m[index].meaning_1)
            if(Math.random()>0.5){
                $('[name="cover"]',a).hide()
                $('[name="text"]',b).hide()
            }else{
                $('[name="text"]',a).hide()
                $('[name="cover"]',b).hide()
            }
            containner.append(a,b)
        }
        return;
    
        function play(mp3){
            console.log('----',mp3)
            var audio = new Audio(mp3);
            audio.play();

        }

        
        console.log(templete['item'])


   })
}
class PausableTimeout {
    constructor(callback) {
        let _self = this;
        _self.callback = callback;
        _self.delay = 0;
        _self.startTime = 0;
        _self.remaining = _self.delay;
        _self.timerId = null;
        _self.paused = true;


        _self.pause = () => {
            clearTimeout(_self.timerId);
            _self.remaining -= new Date() - _self.startTime;
            _self.paused = true;
        }

        // resume() {
        //     _self.startTime = new Date();
        //     //   console.log(_self.remaining)
        //     _self.timerId = setTimeout(() => {
        //         _self.pause()
        //         _self.callback()
        //     }, _self.remaining);
        //     _self.paused = false;
        // }

        _self.start = (delay) => {
            let ii = -1
            _self.delay = delay;
            _self.remaining = _self.delay;
            _self.startTime = new Date();
            // _self.timerId = setTimeout(() => {
            //     _self.timerId = setTimeout(() => {
            //         _self.pause()
            //         _self.callback(2)
            //     }, _self.delay);
            //     // _self.pause()
            //     _self.callback(1)
            // }, _self.delay);
            _self.paused = false;
            go()

            function go() {
                ii++
                // if (ii) {
                    _self.callback(ii)
                    if (ii == 2) {
                        return;
                    }
                // }
                _self.timerId = setTimeout(() => {
                    go()
                }, _self.delay);
            }
        }

        _self.status = () => {
            return _self.paused ? "paused" : "running";
        }
    }


}
  
  