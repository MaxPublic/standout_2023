/**2023/2/6 Max*/

// let {items,ans}=randomItems()
// console.log(items,ans)
let module_html;//ui讀取後的完整html，是jq狀態。
let templete = {};
let data;
let word;
let x_material_path;
let x_vocabWizard_path;
let x_grammar_path;
let x_gravideo_path;
function toolsinit(callback){
    if(false){
        let dd=[
            {"type": "choice","group": "listening_7","ans": [0],"submit": [1]},
            {"type": "choice","group": "listening_7","ans": [0],"submit": [1]},
            {"type": "choice","group": "listening_7","ans": [0],"submit": [1]},
            {"type": "choice","group": "listening_8","ans": [0],"submit": [1]},
            {"type": "choice","group": "listening_8","ans": [0],"submit": [1]},
            {"type": "choice","group": "listening_8","ans": [0],"submit": [1]},
            {"type": "pair","ans": [0,1,2],"submit": [0,1,3]},
            {"type": "pair","ans": [0,1,2],"submit": [0,1,3]}
        ]
        function sco_______re(jsonData){//計算對錯題、分數、錯題重答對象。
            let delary=[]
            let ooary=[]
            let xxary=[]
            let oo=0;
            let xx=0;
            let all=0;
            for (let i = 0; i < jsonData.length; i++) {
                let submit=jsonData[i].submit;
                let ans=jsonData[i].ans;
                if(jsonData[i].type=='pair'){
                    let c=true;
                    for(let ii=0;ii<ans.length;ii++){
                        if(ans[ii]!=submit[ii]){//拖曳某項目錯誤
                            c=false
                            break;
                        }
                    }
                    if(c){
                        delary.push(true);
                    }else{
                        delary.push(false);
                    }
                    continue;
                }else{
                    if (AnsIsEqual(submit, ans)) {
                        delary.push(true);
                        continue;
                    }else{
                        delary.push(false);
                    }
                    continue;
                }
            }
            //得到delary=[true,false,true,false...]每子題的對錯
            console.log(delary,'-----');
            for(let i=0;i<delary.length;i++){
                let nn=getSameGroup(i);
                let gg=delary.slice(i, i + nn.length)
                if(gg.includes(false)){
                    xx++;
                    for (let j = i; j < i + nn.length; j++) {
                        xxary.push(jsonData[j])
                        // delary[j] = false;
                    }
                }else{
                    oo++;
                    for (let j = i; j < i + nn.length; j++) {
                        ooary.push(jsonData[j])
                        // delary[j] = false;
                    }
                }
                i+=nn.length-1;
            }
            all=oo+xx;
            return {score:Math.floor(100/all*oo),all:all,oo:oo,xx:xx,ooary:ooary,xxary:xxary}
            function getSameGroup(_index){//拿取本次與後續的同群組的項目。回傳陣列
                let _nowgroup;
                let nn=[]
                nn.push(jsonData[_index])
                if(!jsonData[_index].group){//非群組題，只傳回自己
                    return nn;
                }else{
                    _nowgroup=jsonData[_index].group;
                }
                for(let i=_index+1;i<jsonData.length;i++){
                        if(jsonData[i]['group'] && jsonData[i]['group']==_nowgroup){
                            //同組
                            nn.push(jsonData[i])
                        }else{
                            // 不同組或不是題組題;
                            break;
                        }    
    
                }
                return nn;
            }
        }
        console.log(sco_______re(dd))
        return;
    }
    if(false){//試算題組題的問題
        let _group=3;
        let delary=[true,false,false,true,true,true,false,true,true]
        for (let i = 0; i < delary.length; i += _group) {
            // 如果該組中有一個 false
            if (delary.slice(i, i + _group).includes(false)) {
                // 將該組所有元素設為 false
                for (let j = 0; j < _group && i + j < delary.length; j++) {
                delary[i + j] = false;
                }
            }
        }
        console.log(delary);
        // return;
    }
    if(true){//拿取網址參數
        let params = new URLSearchParams(location.search);
        for (let [key, value] of params.entries()) {
            console.log(`${key} = ${value}`);
        }
        let datajspath;
        let wordjspath;
        // let userAge = params.get('age');
        // console.log(datajspath,'10000')
        //如果網址有以下兩個參數，代表是測試模式，會去讀取相關的檔案。若無，則代表正式版，直接使用既定路徑
        if(!params.get('datajspath')){
            datajspath="../material/data.js";
            wordjspath="../vocabWizard/word.js";
            x_material_path='../material/';
            x_vocabWizard_path='../vocabWizard/';
            x_grammar_path='../material/';
            x_gravideo_path='../material/';
        }else{
            console.log('測試開始')
            datajspath = params.get('datajspath');
            wordjspath = params.get('wordjspath');
            x_material_path=params.get('materialpath');
            x_vocabWizard_path=params.get('vocabWizardpath');
            x_grammar_path=params.get('grammarpath');
            x_gravideo_path=params.get('gravideopath');
        }
        getJsFileAndExtractJson(datajspath,(_data)=>{
            console.log('88888')
            // console.log(JSON.stringify(_data))
            data = reduce(_data)
            console.log(JSON.stringify(data));
            getJsFileAndExtractJson(wordjspath,(_data)=>{
                word = reduce(_data)
                // console.log('99999',word)
                go();
              
            })
        })
    }

    function go(){
        $.get('../ui_/ui.htm', function(data) {
            // console.log(data);
            processStyles(data);
            let div=$('div').first();
            let ma=new main_layout();
            ma.hideFeedback()
            ma.hideLearntime()
            // ma.showLearntime()
            $('body').append(ma.view);
            if(true){//更換radio模組
                let inputs=$('input[type="radio"]',div)
                inputs.each(function() {
                    let p = new radio(false);
                    let ischeck=$(this).is(':checked');
                    p.settext($(this).text())
                    p.setvalue($(this).val());
                    $(this).replaceWith(p.view);
                    if(ischeck){
                        p.setcheck(true);
                    }else{
                        p.setcheck(false);
                    }
                });
            }
            if(true){//更換checkbox模組
                let inputs=$('input[type="checkbox"]',div)
                inputs.each(function() {
                    let p = new radio(true);
                    let ischeck=$(this).is(':checked');
                    p.settext($(this).text())
                    p.setvalue($(this).val());
                    $(this).replaceWith(p.view);
                    if(ischeck){
                        p.setcheck(true);
                    }else{
                        p.setcheck(false);
                    }
                });
            }
            ma.appendContent(div);
            $('body').show();
            callback&&callback(ma)
        });
    }
    function processStyles(data) {
        module_html = $("<div>").html(data);
        var $stylesInHead = module_html.find('style');
        $stylesInHead.each(function() {
            var styleContent = $(this).html();
            $('head').append('<style>' + styleContent + '</style>');
        });
        module_html.find('style').remove();
    }
    
}
function getJsFileAndExtractJson(path, callback) {//拿取data.js以及word.js的json
    $.ajax({
        url: path,
        dataType: "text",
        success: function(data) {
            var index = data.indexOf('=');
            
            if (index !== -1) {
                var json_content = data.substring(index + 1).trim();
                
                if (json_content.endsWith(";")) {
                    json_content = json_content.slice(0, -1);
                }
                
                var jsonData = JSON.parse(json_content);
                callback(jsonData);
            } else {
                console.log("No '=' found.");
            }
        },
        error: function(error) {
            console.error("Error:", error);
        }
    });
}
function SpeedTest(callback){//快速測試模式，正式發布時return此函數
    return;
    callback&&callback()
}
function isSorted(arr) {//判斷陣列是否為0,1,2,3,4,...排序
    return arr.every((val, i, array) => !i || (val >= array[i - 1]));
}
function checkFillText(a,b){//判斷兩個字串是否吻合?文字輸入判斷使用。
    let aa=a.replace(/'|＇|‘|’/g, "'");
    let bb=b.replace(/'|＇|‘|’/g, "'");
    return aa==bb
}
class radio{
    static list=[]
    static getvalue(){//傳回陣列
        let value=[]
        let list=radio.list;
        for (let i=0;i<list.length;i++){
            let p=list[i]
            if(p.ischeck()){
                value.push(p.val)
                // return p.val;
            }
        }
        return value;
    }
    static disabled(){
        let list=radio.list;
        for (let i=0;i<list.length;i++){
            let p=list[i]
            p.disabled()
        }
    }
    static enabled(){
        let list=radio.list;
        for (let i=0;i<list.length;i++){
            let p=list[i]
            p.enabled()
        }
    }
    constructor(isCheckbox=false){
        let self=this;
        let list=radio.list;
        self.id=list.length;
        list.push(self);
        let _check=false;
        let typename=isCheckbox?'checkbox':'radio'
        self.view=$($('[idname="'+typename+'"]' ,module_html)[0].outerHTML)
        let selected_style=$($('[idname="'+typename+'"]' ,module_html)[1].outerHTML)
        let disabled_style=$($('[idname="'+typename+'"]' ,module_html)[2].outerHTML)
        // console.log(disabled_style[0].outerHTML)
        // selected_style.remove();
        self.disabled=()=>{
            toggleElements(self.view,disabled_style,false,true,0);
        }
        self.enabled=()=>{
            toggleElements(self.view,disabled_style,false,false,0);
        }
        self.setcheck=(check=false)=>{
            _check=check;
            self.redraw()
        }
        self.settext=(tt='')=>{
            $('[name="text"]',self.view).text(tt);
        }
        self.setvalue=(value)=>{
            self.val=value;
            // console.log(value)
        }
        self.ischeck=()=>{
            return _check;
        }
        self.view.off('click').on('click',(e)=>{
            //radio的特性，全關，只有正在點按的為true
            console.log('11')
            if (self.view.hasClass('disabled')) {
                return;
            }
            e.preventDefault()
            if(isCheckbox){
                self.setcheck(!_check);
            }else{
                for (let i=0;i<list.length;i++){
                    let p=list[i]
                    if(p==self){
                        console.log('遇到自己')
                        continue;
                    }
                    p.setcheck(false);
                    
                    // console.log(i,p.ischeck());
                }
                self.setcheck(true)
            }

           
        })
        self.redraw=()=>{
                if(_check){
                    toggleElements(self.view,selected_style,false,true,0);
                }else{
                    toggleElements(self.view,selected_style,false,false,0);
                }

        }
        self.redraw();
    }
}
class progressbar{
    static div;
    constructor(jq_dom){//播放進度表，不用添加入場景。
        let self=this;
        /* <div class="bar_group">
            <div class="bar_gray"></div>
            <div class="bar_progress"></div>
            <div class="bar_point"></div>
            <div class="time">33:00</div>
        </div> */
        self._max=-1;
        self._val=0;
        self.view=jq_dom;
        let graywidth=300;
        let isDragging=false;
        let onInputCallback=null;
        self.attr=(key,value)=>{
            if (key='max'){
                self._max=value;
                redraw()
            }
        }
        self.val=(val)=>{
            if (!val){
                return self._val;
            }else{
                self._val=val;
                redraw()
            }
        }
        self.on=(key,callback)=>{
            if (key=='input'){
                onInputCallback=callback;
            }
        }
        $('.bar_point',self.view).on('mousedown touchstart', startDragging);
        $(document).on('mousemove touchmove', doDrag);
        $(document).on('mouseup touchend', stopDragging);
        $('.bar_gray', self.view).on('click', function(e) {
            let position = 0;
            let barGrayWidth = graywidth;
        
            position = e.pageX - $(this).offset().left; // 直接取得點擊位置相對於 bar_gray 的偏移
        
            position = Math.max(0, Math.min(position, barGrayWidth)); // 確保 position 不超出範圍
            let currentSecond = (position / barGrayWidth) * self._max; // 計算當前秒數
            self._val = currentSecond;
        
            onInputCallback && onInputCallback();
            redraw();
        });
        redraw()
        function startDragging(e) {
            e.preventDefault();
            isDragging = true;
        }
    
        function doDrag(e) {
            if(!isDragging){
                return;
            }
            let position = 0;
            let barGrayWidth =graywidth
            
            if (e.type === 'mousemove') {
                position = e.pageX - $('.bar_gray', self.view).offset().left;
            } else if (e.type === 'touchmove') {
                position = e.touches[0].pageX - $('.bar_gray', self.view).offset().left;
            }
            position = Math.max(0, Math.min(position, barGrayWidth));
            let currentSecond = (position / barGrayWidth) * self._max;
            self._val=currentSecond
            self._val = Math.min(currentSecond, self._max-0.001);
            onInputCallback&&onInputCallback();
            redraw()
        }
    
        function stopDragging() {
            isDragging = false;
        }
        // function update(){
        //     if(!isDragging){
        //         setTimeout(redraw,1000/30)
        //     }
        // }
        function redraw(){
            let progress=(self._val/self._max)*graywidth;
            progress=Math.min(graywidth,progress)
            progress=Math.max(progress,0)
            $('.bar_point',self.view).css('left',progress)
            $('.bar_progress',self.view).css('width',progress)
            $('.time',self.view).text(formatTime(self._val));
            $('.totalTime',self.view).text(formatTime(self._max));
            
        }
        function formatTime(totalSeconds) {
            totalSeconds = Math.floor(totalSeconds);  // 去除小數點
        
            let hours = Math.floor(totalSeconds / 3600);
            let minutes = Math.floor((totalSeconds % 3600) / 60);
            let secs = totalSeconds % 60;
        
            let result = (hours > 0 ? hours + ':' : '') +
                         (hours > 0 ? String(minutes).padStart(2, '0') : minutes) + ':' +
                         String(secs).padStart(2, '0');
        
            return result;
        }
    }
}
class mp3playback{//直接帶入UI與功能，套用MaxMediaPlayer
    constructor(mp3=[]){
        let self=this;
        // let list=radio.list;
        // self.id=list.length;
        // list.push(self);
        // let _check=false;
        self.view=$($('[idname="playback"]' ,module_html)[0].outerHTML)
        // $("[name='progressBar']")


        self.mediaplayer = new MaxMediaPlayer(mp3, $("[name='btn_play']",self.view),new progressbar($('.bar_group',self.view)) , () => {
            // console.log('完成')
        },$('[name="btn_pause"]',self.view),$('[name="btn_loop"]',self.view),$('[name="btn_speed"]',self.view))
        self.addEventChangeIndex=(_callback)=>{
            self.mediaplayer.addEventChangeIndex((newIndex) => {
                _callback&&_callback(newIndex)
            })
        }

    }
}
class main_layout{
    static ma;
    constructor(){
        let self=this;
        main_layout.ma=self;
        // let contentdom;
        let contentList=[];
        let contentIndex=-1;
        self.view=$($('[idname="main_layout"]' ,module_html)[0].outerHTML)
        // self.quizui=$($('[idname="QuizUI"]' ,module_html)[0].outerHTML)
        $('[name="top"] [name="title"]',self.view).html(
            data.DocumentElement.Info.serial+' '+
            data.DocumentElement.Info.book+'<br/>'+
            data.DocumentElement.Info.unit_no+' '+
            'Lesson'+data.DocumentElement.Info.lesson_no
        )
        $('[name="bottom"] [name="time"]',self.view).first().text('建議學習時間：'+data.DocumentElement.Info.suggested_time)

        self.appendContent = (jq_dom) => {
            // 移除當前顯示的內容，但保存於 contentList 中
            if (contentList[contentIndex]) {
                contentList[contentIndex] = $('[name="content"]', self.view).first().children().detach();
            }

            // 添加新的內容到陣列和畫面
            contentList.push(jq_dom);
            contentIndex++;
            return $('[name="content"]', self.view).first().append(jq_dom);
        };
        self.goFirstContent = () => {
            if (contentIndex > 0) {
                // 保存當前內容
                contentList[contentIndex] = $('[name="content"]', self.view).first().children().detach();
                contentIndex--;

                // 附加上一個內容
                $('[name="content"]', self.view).first().append(contentList[contentIndex]);
            }
        };
        // self.appendContent=(jq_dom)=>{
        //     contentList.push( jq_dom.clone(true))
        //     contentIndex++;
        //     // contentdom=jq_dom;
        //     $('[name="content"]',self.view).first().empty();
        //     return $('[name="content"]',self.view).first().append(jq_dom);
        // }
        self.removeContent=()=>{
            contentList.splice(contentIndex);
            $('[name="content"]',self.view).first().empty();
        }
        self.getContent=()=>{
            return contentList[contentIndex];
        }
        // self.goFirstContent=()=>{
        //     contentIndex=0;
        //     $('[name="content"]',self.view).first().empty();
        //     $('[name="content"]',self.view).first().append(contentList[contentIndex]);
        //     return contentList[contentIndex];
        // }
        self.setContentSpace=(val)=>{//內容的底部留白，課後挑戰由於要與feedback面板靠近，所以這個函數是專門使其使用。
            $('[name="content"]',self.view).css('padding', '0px 0px '+val+'px 0px')
        };
        self.setNodename=(text='',info='')=>{
            $('.nodename [name="nodename_text"]',self.view).first().text(text);
            $('.nodename [name="nodename_info"]',self.view).first().text(info);
        }
        self.setFeedback=(jq_dom)=>{
            $('[name="feedback"] [name="feedback_content"]',self.view).first().empty();
            $('[name="feedback"] [name="feedback_content"]',self.view).first().append(jq_dom);
            // $('[name="feedback"] [name="feedback_content"]',self.view).first().html(text);
        }
        self.setFeedbackTitle=(title='詳解')=>{
            $('[name="feedback"] [name="title"]',self.view).first().text(title);
        }
        self.hideFeedback=()=>{
            $('[name="feedback"]',self.view).first().hide()
            // $('.bottom',self.view).css('background-color','#ffffff');
            $('[name="grow"]',self.view).css('background-color','#ffffff');
        }
        self.showFeedback=()=>{
            $('[name="feedback"]',self.view).first().show()
            // $('.bottom',self.view).css('background-color','#eff0f3');
            $('[name="grow"]',self.view).css('background-color','#eff0f3');
        }
        self.hideborder=()=>{
            //移除預覽的邊線
            $.each(document.styleSheets, function(index, stylesheet) {
                if (stylesheet.cssRules) { // For browsers that support cssRules
                    for (let i = 0; i < stylesheet.cssRules.length; i++) {
                        if (stylesheet.cssRules[i].selectorText === '.b_b') {
                            stylesheet.deleteRule(i);
                            i--; // Because the length of cssRules has changed
                        }
                    }
                }
            });
        }
        self.hideLearntime=()=>{
            $('[name="bottom"] [name="time"]',self.view).first().hide()
        }
        self.showLearntime=()=>{
            $('[name="bottom"] [name="time"]',self.view).first().show()
        }
    }
}

function randomItems(items=[["A","B","C"],["a.mp3","b.mp3","c.mp3"],["a.png","b.png","c.png"]],ans=[0,1]){//陣列混亂，並且正確答案會修正成混亂後的結果
    //題目本身可能具備文字、圖片與聲音，所以要連同那些項目一併洗牌卻不可以使其錯誤
    //可以允許空陣列輸入
    //可允許不限數量的陣列
    if(items.length==0 || ans.length==0){
        return {items:[],ans:[]}
    }
    items=Array.from(new Set(items));
    items=padArrays(...items)
    ans=Array.from(new Set(ans));
    let a=[]
    for(let j=0;j<items[0].length;j++){
        let b=[]
        a.push([b,j])//索引值
        for(let i=0;i<items.length;i++){
            b.push(items[i][j])
        }
    }
    a.sort(() => Math.random() - 0.5);
    let aa=[]
    let newans=[]
    let newitems=[]
    // console.log(a);
    for(let i=0;i<a.length;i++){//找出新正確答案
        // console.log(a[i][1],'=======',ans,ans.indexOf(a[i][1]))
        // if(ans.indexOf(a[i][1])>-1){
        //     newans.push(i)
            // (ans.indexOf(a[i][1]))
        if(ans.includes(a[i][1])){
            newans.push(i)
        }
        aa.push(a[i][0])
    }
    
    for(let j=0;j<aa[0].length;j++){//回復結構
        let bb=[]
        for(let i=0;i<aa.length;i++){
            bb.push(aa[i][j])
        }
        newitems.push(bb)
    }
    // console.log(newitems,newans)
    newitems=removeEmptyArrays(...newitems)
    return {items:newitems,ans:newans};
    function padArrays(...arrays) {//將不足的陣列補齊。padArrays(arr1, arr2, arr3)
        const maxLength = arrays.reduce((max, arr) => Math.max(max, arr.length), 0);
        return arrays.map(arr => [...arr, ...Array(maxLength - arr.length).fill('')]);
    }
    function removeEmptyArrays(...arrays) {//將前面補齊空字串陣列的項目變成空陣列，保持輸出輸入一致
        return arrays.map(arr => arr.filter(item => item !== '') || []);
    }
}
function AnsIsEqual(arr1=[0,1], arr2=[1,0]) {//比對兩陣列(答案)是否一致。
    const sortedArr1 = Array.from(new Set(arr1)).sort();
    const sortedArr2 = Array.from(new Set(arr2)).sort();
    return JSON.stringify(sortedArr1) === JSON.stringify(sortedArr2);
}
function reduce(obj/**json */){//數據最佳化
    // 此函數必須要能同時進行word.js與data.js的最佳化
    let data=go2(obj);
    let hasVocabulary=false
    try {
        if(data.DocumentElement.Vocabulary) {
            hasVocabulary=true;
        } else {
            hasVocabulary=false
        }
    } catch(err) {
        hasVocabulary=false
    }
    if(!hasVocabulary){
        //如果沒有這個節點 data.DocumentElement.Vocabulary，則代表是 word文件
        return data;
    }
    if(data.DocumentElement.Listen){//11566這個節點竟然是null
        if(data.DocumentElement.Listen.lesson_id){//11571這裡是null
            if (!Array.isArray(data.DocumentElement.Listen.listening)){
                data.DocumentElement.Listen.listening=[data.DocumentElement.Listen.listening];
            }
            for (let i=0;i<data.DocumentElement.Listen.listening.length;i++){
                if(!Array.isArray(data.DocumentElement.Listen.listening[i].listening_listening)){
                    data.DocumentElement.Listen.listening[i].listening_listening=[data.DocumentElement.Listen.listening[i].listening_listening]
                }
            }
            for (let i=0;i<data.DocumentElement.Listen.listening_practice.length;i++){
                let p=data.DocumentElement.Listen.listening_practice[i]
                let ary=[]
                for (let j=0;j<6;j++){
                    let _p=p['listening_test_c_item_'+(j+1)]
                    p['listening_test_c_item_'+(j+1)]+='===廢止=='
                    if(_p!='0'){
                        ary.push(_p)
                    }
                }
                let ans=find(ary,p['listening_test_c_ans'])
                p['listening_test_c_item']=ary;
                p['listening_test_c_ans']+='===廢止=='
                p['listening_test_cc_ans']=ans;
    
            }
        }

    }
    for (let i=0;i<data.DocumentElement.Vocabulary.length;i++){
        let p=data.DocumentElement.Vocabulary[i]
        p['word_test_e_item']=[p['word_test_e_item_1'],p['word_test_e_item_2'],p['word_test_e_item_3']]
        p['word_test_e_item_e']=[p['word_test_e_item_1e'],p['word_test_e_item_2e'],p['word_test_e_item_3e']]
        p['word_test_e_ans']=find(p['word_test_e_item'],p['word_test_e_ans'])
        p['word_test_c_item']=[p['word_test_c_item_1'],p['word_test_c_item_2'],p['word_test_c_item_3']]
        p['word_test_c_item_c']=[p['word_test_c_item_1c'],p['word_test_c_item_2c'],p['word_test_c_item_3c']]
        if(p['word_test_c_ans']=='DMV(Department of Motor Vehicles)'){//原始資料錯誤修正
            p['word_test_c_ans']='DMV';
        }
        p['word_test_c_ans']=find(p['word_test_c_item'],p['word_test_c_ans'])
        p['cloze_test_item']=[p['cloze_test_item_1'],p['cloze_test_item_2'],p['cloze_test_item_3']]
        p['cloze_test_ans']=find(p['cloze_test_item'],p['cloze_test_ans'])
        p['word_test_e_item_1']+='===廢止=='
        p['word_test_e_item_2']+='===廢止=='
        p['word_test_e_item_3']+='===廢止=='
        p['word_test_e_item_1e']+='===廢止=='
        p['word_test_e_item_2e']+='===廢止=='
        p['word_test_e_item_3e']+='===廢止=='
        p['word_test_c_item_1']+='===廢止=='
        p['word_test_c_item_3']+='===廢止=='
        p['word_test_c_item_3']+='===廢止=='
        p['word_test_c_item_1c']+='===廢止=='
        p['word_test_c_item_2c']+='===廢止=='
        p['word_test_c_item_3c']+='===廢止=='
        p['cloze_test_item_1']+='===廢止=='
        p['cloze_test_item_2']+='===廢止=='
        p['cloze_test_item_3']+='===廢止=='
        p["part_of_speech_2"]=p["part_of_speech_2"]=='0'?'':p["part_of_speech_2"]
        p["meaning_2"]=p["meaning_2"]=='0'?'':p["meaning_2"]
        p["eg_2"]=p["eg_2"]=='0'?'':p["eg_2"]
        p["eg_meaning_2"]=p["eg_meaning_2"]=='0'?'':p["eg_meaning_2"]
        p["syn"]=p["syn"]=='0'?'':p["syn"]
        p["ant"]=p["ant"]=='0'?'':p["ant"]
        p["n_forms"]=p["n_forms"]=='0'?'':p["n_forms"]
        p["v_forms"]=p["v_forms"]=='0'?'':p["v_forms"]
        p["adj_forms_adv_forms"]=p["adj_forms_adv_forms"]=='0'?'':p["adj_forms_adv_forms"]
    }
    for (let i=0;i<data.DocumentElement.Read.reading_practice.length;i++){
        let p=data.DocumentElement.Read.reading_practice[i]
        let ary=[]
        for (let j=0;j<6;j++){
            let _p=p['reading_test_c_item_'+(j+1)]
            p['reading_test_c_item_'+(j+1)]+='===廢止=='
            if(_p!='0'){
                ary.push(_p)
            }
        }
        let ans=find(ary,p['reading_test_ans'])
        p['reading_test_c_item']=ary;
        p['reading_test_ans']+='===廢止=='
        p['reading_test_cc_ans']=ans;

    }
    for(let i=0;i<data.DocumentElement.Dialogue.clozeTest.length;i++){
        let p=data.DocumentElement.Dialogue.clozeTest[i]
        let ary=[p.cloze_test_item_1,p.cloze_test_item_2,p.cloze_test_item_3];
        let ans=find(ary,p.cloze_test_ans);
        p["cloze_test_item_1"]+='===廢止==';
        p["cloze_test_item_2"]+='===廢止==';
        p["cloze_test_item_3"]+='===廢止==';
        p["cloze_test_ans"]+='===廢止==';
        p['cloze_test_item']=ary;
        p['cloze_test_cc_ans']=ans;
    }
    if(data.DocumentElement.Read.writing_test){
        if (typeof data.DocumentElement.Read.writing_test === 'object' && 
         !(data.DocumentElement.Read.writing_test instanceof Array)) {
            //如果此項目是物件，則將其裝在陣列內
            data.DocumentElement.Read.writing_test = [data.DocumentElement.Read.writing_test];
        }
        for(let ii=0;ii<data.DocumentElement.Read.writing_test.length;ii++){
            let qq=data.DocumentElement.Read.writing_test[ii]
            let ary=[]
            for(let i=0;i<10;i++){
                let p=qq['writing_test_c_item_'+(i+1)]
                if(p){
                    ary.push(p)
                    qq['writing_test_c_item_'+(i+1)]+='===廢止=='
                }
            }
            qq['writing_test_c_item']=ary;
        }
    }
    console.log(JSON.stringify(data));
    return data;
    function find(dt,ans='正解1;正解2'){
        let newary=[]
        let ansary=ans.split(';')
        for (let i=0;i<dt.length;i++){
            if(ansary.includes(dt[i])){
            // if(dt[i]==ans){
                newary.push(i);
            }
        }
        return newary;
    }
    function go2(obj){
        let g={}
        if(Array.isArray(obj)){
            g=[]
        }
        for(let i in obj){
            if(obj[i]==null){
                console.log(i,'===========----');
                continue;
            }
            if (i=='#cdata-section'){
                // console.log(Object.keys(obj).length)
                return obj[i].trim();
            }else{
                if(typeof(obj[i])==='object'){
                    g[i]=go2(obj[i])
                }else{
                    g[i]=obj[i]
                }
            }
        }
        return g;  
    }
}
function toggleElements(element1, element2,toggle=true,add=true,delay=0) {//動態轉場[jq元素1,jq元素2,是否toggle,疊加或刪除,延遲毫秒]
    // 設定參數的預設值，防止參數缺失的情況
    setTimeout(()=>{
        element1 = element1 || $();
        element2 = element2 || $();
        
        // 取得兩個元素的子元素，並進行對應
        if (element1.attr("class") && element2.attr("class")) {
            if(toggle){
                element1.toggleClass(element2.attr("class"));
            }else{
                if(add){
                    element1.addClass(element2.attr("class"));
                }else{
                    element1.removeClass(element2.attr("class"));
                }
            }
        }
        element1.children().each(function(index, elem1) {
            elem1=$(elem1)
            var elem2 = $(element2.children()[index]);
            // 判斷兩個子元素是否都有 class，若都有則進行 toggleClass
            if (elem1.attr("class") && elem2.attr("class")) {
                if(toggle){
                    elem1.toggleClass(elem2.attr("class"));
                }else{
                    if(add){
                        elem1.addClass(elem2.attr("class"));
                    }else{
                        elem1.removeClass(elem2.attr("class"));
                    }
                }
            }
            // 若其中一個沒有 class，則進行遞迴呼叫，對應子元素
            else {
                toggleElements($(elem1), $(elem2),toggle,add);
            }
        });
    },delay);
 
}
class MaxMediaPlayer{
    static MpList=[];
    static AllStop(){
        for(let i=0;i<this.MpList.length;i++){
            let p=this.MpList[i];
            p.stop();
        }
    }
    constructor(mp3files=[],playBtn,progressBar=null,CompletePreparationCallback=null,pauseBtn=null,loopBtn=null,speedBtn=null){
        let _self=this;
        MaxMediaPlayer.MpList.push(_self);
        let audios=[];
        let totalDuration = 0;
        let audioLengths=[0]
        let currentAudio = 0;
        let oldindex=-1
        let ChangeIndexCallback;
        let playing=false
        pauseBtn&&pauseBtn.hide()
        let speedary=[
            ["0.75 x",0.75],
            ["1 x",1],
            ["1.25 x",1.25],
            ["1.5 x",1.5],
            ["2.0 x",2],
            ]
        let speedindex=1
        let loop=false;
        createAudios(mp3files)
        showall()
        
        _self.stop=()=>{
            allpause()
        }
        _self.totalDuration=()=>{
            return totalDuration;
        }
        // _self.play=()=>{
        //     go()
        // }
        _self.playindex=(index,autonext)=>{
            allpause()
            currentAudio=index;
            audios[index].currentTime=0
            audios[index].play()
        }
        _self.addEventChangeIndex=(callback)=>{//當切換了音檔的時候
            ChangeIndexCallback=callback;
        }
        function createAudios(files){
            if(files.length==0){
                console.log('媒體陣列為空')
                return;
            }
            let i=0;
            newa(files[i])
            function newa(file){
                newAudio(file,(data)=>{
                    audios.push(data.audio)
                    totalDuration += data.duration;
                    audioLengths.push(totalDuration)
                    if(i>=files.length-1){
                        CompletePreparationCallback&&CompletePreparationCallback();
                        progressBar.attr("max", totalDuration);
                        playBtn.show();
                        setInterval(function() {
                            if(currentAudio>=audioLengths.length){
                                progressBar.val(progressBar.attr("max"));
                            }else{
                                audios[currentAudio].playbackRate =speedary[speedindex][1]
                                let timecode= audioLengths[currentAudio]+audios[currentAudio].currentTime
                                progressBar.val(timecode);
                                let g=gettimeindex(timecode)
                                // console.log('==',g,oldindex,progressBar.val())
                                if (g!=oldindex){
                                    //索引改變，通知監聽事件
                                    // console.log(g,oldindex)
                                    ChangeIndexCallback&&ChangeIndexCallback(g);
                                    if(g>files.length-1){
                                        //到了最後一段，最後的時間點停止
                                        console.log(loop)
                                        if(loop){
                                            currentAudio=0
                                            g=0
                                            go()
                                            return;
                                        }else{
                                            allpause()
                                            currentAudio=0;
                                            playing=false
                                            playBtn.show();
                                            pauseBtn&&pauseBtn.hide()
                                            // toggleElements(playBtn,pauseBtn,true,true,0)
                                        }
                                    }
                                    oldindex=g;
                                }
                            }
                        }, 50);
                        playBtn.unbind('click').bind('click',(e)=> {
                            playing=true
                            // toggleElements(playBtn,pauseBtn,true,true,0)
                            // setTimeout(()=>{
                            //     var classes = playBtn.attr('class').split(' ');
                            //     var lastClass = classes[classes.length - 1];
                            //     console.log(lastClass);
                                // if(lastClass=='btn_pause'){
                                // if(playing){
                                    //播放中
                                    playBtn.hide()
                                    pauseBtn&&pauseBtn.show()
                                    go();
                                // }else {
                                // // if (lastClass=="btn_play"){
                                //     //暫停中
                                //     playBtn.show()
                                //     pauseBtn.hide()
                                //     audios[currentAudio].pause()
                                //     return;
                                // }
                            // },50)
                        });
                        pauseBtn&&pauseBtn.unbind('click').bind('click',(e)=>{
                            playing=false
                            playBtn.show()
                            pauseBtn.hide()
                            audios[currentAudio].pause()
                        })
                        speedBtn&&speedBtn.unbind('click').bind('click',(e)=>{
                            speedindex=(speedindex+1)%speedary.length
                            audios[currentAudio].playbackRate =speedary[speedindex][1]
                            showall()
                        })
                        // if(loopBtn){
                            loopBtn&&loopBtn.unbind('click').bind('click',(e)=>{
                                loop=!loop
                                showall()
                            })
                        // }
                    }else{
                        i++;
                        newa(files[i])
                    }
                })
            }
        }
        function newAudio(file,callback){
            let aud = new Audio();
            aud.src = file;//audioFiles[currentAudio];
            aud.preload = "auto";
            aud.addEventListener("loadedmetadata", function() {
                callback&&callback({audio:aud,duration:aud.duration})
            });
            aud.addEventListener("ended", function() {
                aud.pause();
                currentAudio++;
                if (currentAudio >= audios.length) {
                    currentAudio--;
                    return;
                }
                audios[currentAudio].play();
            });
        }
        function allpause(){
            console.log('停止',audios.length)
            for(let i=0;i<audios.length;i++){
                audios[i].pause()
                audios[i].currentTime =0
            }
        }
        function gettimeindex(val){//拿取目前的索引位置
            for (let i =0;i<audioLengths.length-1;i++){
                if(val>=audioLengths[i]&& val<audioLengths[i+1]){
                    return i;
                }
            }
            return audioLengths.length-1
        }
        function go(){
            // allpause()
            // currentAudio=0;
            audios[currentAudio].play();


            progressBar.on("input", function() {
                let desiredTime = progressBar.val();
                let index=gettimeindex(desiredTime)
                // if(index>audios.length){
                //     return;
                // }
                allpause()
                currentAudio=index
                audios[index].currentTime = desiredTime-audioLengths[index]
                audios[index].play()
            });
        }
        function showall(){
            if(speedBtn){
                speedBtn.text(speedary[speedindex][0]);
            }
            // console.log(loop)
            if(loopBtn){
                if(loop){
                    loopBtn.css('opacity','1')
                }else{
                    loopBtn.css('opacity','0.4')
                }
            }

        }

    }
}
class Quiz{
    static PairAB(inputText, tip) {//找出配對題A:,B:格式中的u標籤內容與組合過後的字串
        // tip = '<br>(' + tip + ')<br>';
        var processedText = inputText.replace(/\/n/g, '<br>');
        var i = 0; // 初始化流水號
        var wordsList = []; // 保存<u>標籤內的單詞列表
        var $tempDiv = $('<div>').html(processedText);
        var $tempDivFeedback = $('<div>').html(processedText);
        let feedback;
        feedback = generateFeedback(inputText, tip)
        // 處理主要文本與單詞列表
        $tempDiv.find('u').each(function () {
            var words = $(this).text().split(' ');

            // 使用擴展運算子直接將單詞添加到wordsList中
            wordsList = [...wordsList, ...words];

            var newHtml = '';
            $.each(words, function (index, word) {
                var spanReplacement = $('<span name="b' + i + '" style="display: inline-block"></span>');
                newHtml += spanReplacement.prop('outerHTML') + '';
                i++; // 每次使用後，流水號加1
            });

            $(this).html(newHtml);

            var currentSentenceEnd = $(this).parent().html().substring($(this).prop('outerHTML').length).match(/^.*?[.!?]/);

            if (currentSentenceEnd) {
                $(this).closest("div").html(function (index, oldHtml) {
                    return oldHtml.replace(currentSentenceEnd[0], currentSentenceEnd[0] + '<br>(' + tip + ')<br>');
                });
            } else {
                $(this).after('<br>(' + tip + ')<br>');
            }
        });

        return {
            html: $tempDiv.html(),
            words: wordsList,
            feedback: feedback
        };
        function generateFeedback(inputText, tip) {
            // Add the HTML break tag to the tip
            tip = '<br>(' + tip + ')<br>';

            // Replace /n with the HTML break tag
            var processedText = inputText.replace(/\/n/g, '<br>');

            // Convert the processed text into a temporary jQuery object
            var $tempDiv = $('<div>').html(processedText);

            // Find each <u> tag and process it
            $tempDiv.find('u').each(function () {
                // Get content after the <u> tag and find the end of the first sentence
                var contentAfterU = $(this).parent().html().substring($(this).prop('outerHTML').length);
                var sentenceEndMatch = contentAfterU.match(/^[^!?\.]*[!?\.]/);

                if (sentenceEndMatch) {
                    var sentenceEnd = sentenceEndMatch[0];
                    $(this).parent().html(function (_, oldHtml) {
                        return oldHtml.replace(sentenceEnd, sentenceEnd + tip);
                    });
                } else {
                    $(this).after(tip);
                }

                // Remove the <u> and </u> tags by unwrapping the contents
                $(this).contents().unwrap();
            });
            let feedback = $tempDiv.html();

            // Create a temporary jQuery object and unwrap <u> tags
            let $tempContent = $('<div>').html(feedback);
            $tempContent.find('u').contents().unwrap();

            // Get the updated HTML
            feedback = $tempContent.html();

            // Return the updated HTML
            return feedback;
        }
    }
    static PairWriting_test(text, keywords) {//取得配對題短文填空組合過的字串
        var i = 0;
    
        // 使用臨時div來協助轉換
        var $tempDiv = $('<div>').text(text); 
    
        $.each(keywords, function(index, keyword) {
            var regex = new RegExp("\\b" + keyword + "\\b", "i"); // 創建正則表達式，匹配整個單詞，不區分大小寫
    
            if ($tempDiv.text().search(regex) !== -1) { // 如果找到匹配的關鍵字
                $tempDiv.html($tempDiv.html().replace(regex, function(match) {
                    var spanReplacement = '<span name="b' + i + '" style="display: inline-block"></span>';
                    i++;
                    return spanReplacement;
                }));
            }
        });
    
        return $tempDiv.html();
    }
    static convertDataToExamJson(datajs/**必須要已經reduce後的資料 */,wordjs/**必須要已經reduce後的資料 */,type='listen_words'/**測驗的類型，可陣列*/){//轉換測驗資料
        let _a={
            title: "題目title_0",
            title_pic: '11544en2.png',
            title_mp3: 'vocab_2182_e',
            option_title: ['aaa', 'bbb', 'ccc'],
            option_pic: ['11544en2_prereading.jpg', 'Gabriela.jpg', 'Roberto.jpg'],
            option_mp3: ['vocab_2183_e', 'vocab_2184_e', 'vocab_2185_e'],
            ans: [0,1],
            submit: [1]
        }
        let _typeary = typeof type === 'string' ? [type] : Array.isArray(type) ? type : [];
        // console.log(_typeary)
        let data =datajs;
        let word =wordjs;
        let newdata=[];
        
        let ary=[];
        for(let i=0;i<_typeary.length;i++){
            switch(_typeary[i]){
                case 'listen_words':
                    //聽音辨字
                    ary=ary.concat(listen_words())
                    break;
                case "listen_words_b0_12":
                    //取用B0_04，修改成只有英文選項、沒有中文選項
                    ary=ary.concat(listen_words(true))
                    break;
                case 'words_listen':
                    //看字辨音
                    ary=ary.concat(words_listen())
                    break;
                case "spelling_practice":
                    //拼字練習
                    ary=ary.concat(spelling_practice())
                    break;
                case "listening":
                    //聽力練習
                    ary=ary.concat(listening())
                    break;
                case "listening_group":
                    //聽力練習_題組題
                    ary=ary.concat(listening_group())
                    break;
                case "listening_one_and_image":
                    //聽力練習_題組題_單圖
                    ary=ary.concat(listening_one_and_image())
                    break;
                case "reading_comprehension":
                    //讀後答題
                    ary=ary.concat(reading_comprehension())
                    break;
                case "unscramble_sentences":
                    //短句重組
                    ary=ary.concat(unscramble_sentences())
                    break;
                case "vocabulary_test":
                    //單字配對vocabulary_test
                    ary=ary.concat(vocabulary_pair())
                    break;
                case "vocabulary_test_b0_08":
                    ary=ary.concat(vocabulary_pair(true))
                    break;
                case "cloze_test":
                    //克漏字cloze_test
                    ary=ary.concat(cloze_choice())
                    break;
                case "listening_test":
                    //聽力填空listening_test
                    ary=ary.concat(listening_fillin())
                    break;


                case "cloze_vocabulary":
                    //單字克漏字cloze_vocabulary
                    ary=ary.concat(cloze_vocabulary())
                    break;
                case "cloze_vocabulary_d0_05":
                    //單字克漏字-會話
                    ary=ary.concat(cloze_vocabulary(true))
                    break;
                    
                case "listening_test_sentence":
                    //聽力填空_句子listening_test_sentence
                    ary=ary.concat(listening_test_sentence())
                    break;
                case "writing_test":
                    //短文填空
                    ary=ary.concat(writing_test_pair())
                    break;
                default:
                    continue;
            }
        }
        return ary;
        function unscramble_sentences(){//短句重組
            // DocumentElement
            // Dialogue
            // patternPractice
            let allitems = data.DocumentElement.Dialogue.patternPractice;
            let _ss=[]
            
            // jsonData[_index].title.replace(/\/n/g, "<br>");
            for (let i=0;i<allitems.length;i++){
                // console.log(i,_ss.length)
                let _r=Quiz.PairAB(allitems[i].pattern_practice_explain,allitems[i].pattern_practice_ques_c);
                let ans= Array(_r.words.length).fill().map((_, index) => index)
                // let fb=allitems[i].pattern_practice_explain.replace(/\/n/g, "<br>")+"<br><br>"+allitems[i].pattern_practice_ques_c.replace(/\/n/g, "<br>")
                _ss.push(
                    {
                        type:'pair',
                        subtype:'unscramble_sentences',
                        title:_r.html,
                        // allitems[i].pattern_practice_explain,
                        //  tit,
                        tit_c:allitems[i].pattern_practice_ques_c,
                        //.replace(/\/n/g, "<br>"),
                        title_pic: '',
                        title_mp3:'',
                        option_title:_r.words,
                        // tit.split(' '),
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:_r.feedback,
                        // +'<br><br>'+allitems.writing_test_explain_c,
                        submit:[],
                        submit_test:[]
                    }              
                )
            }
            return shuffleArray(_ss);  
        }
        function unscramble__________________sentences(){//短句重組
            // DocumentElement
            // Dialogue
            // patternPractice
            let allitems = data.DocumentElement.Dialogue.patternPractice;
            let _ss=[]
            
            // jsonData[_index].title.replace(/\/n/g, "<br>");
            for (let i=0;i<allitems.length;i++){
                // console.log(i,_ss.length)
                let tit=allitems[i].pattern_practice_explain.replace(/<[^>]+>/g, '')
                if(tit.endsWith('.')) {
                    tit = tit.slice(0, -1);
                }
                console.log(tit)
                let ans= Array(tit.split(' ').length).fill().map((_, index) => index)
                console.log('option title=',tit.split(' '))
                let fb=allitems[i].pattern_practice_explain.replace(/\/n/g, "<br>")+"<br><br>"+allitems[i].pattern_practice_ques_c.replace(/\/n/g, "<br>")
                _ss.push(
                    {
                        type:'pair',
                        subtype:'unscramble_sentences',
                        title:allitems[i].pattern_practice_explain,
                        //  tit,
                        tit_c:allitems[i].pattern_practice_ques_c,
                        //.replace(/\/n/g, "<br>"),
                        title_pic: '',
                        title_mp3:'',
                        option_title:tit.split(' '),
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:fb,
                        // +'<br><br>'+allitems.writing_test_explain_c,
                        submit:[],
                        submit_test:[]
                    }              
                )
            }
            return shuffleArray(_ss);  
        }
        function writing_test_pair(){//短文填空(配對題)
            let _ss=[]
            for(let ii=0;ii<data.DocumentElement.Read.writing_test.length;ii++){
                let allitems = data.DocumentElement.Read.writing_test[ii];
                // console.log(allitems,'allitems');
                
                let ans= Array(allitems.writing_test_c_item.length).fill().map((_, index) => index)
                let fb=allitems.writing_test_ques.replace(/\/n/g, "<br>")+"<br><br>"+allitems.writing_test_explain_c.replace(/\/n/g, "<br>")
                // jsonData[_index].title.replace(/\/n/g, "<br>");
                _ss.push(
                    {
                        type:'pair',
                        subtype:'writing_test',
                        title: allitems.writing_test_ques,
                        title_pic: '',
                        title_mp3:'',
                        option_title:allitems.writing_test_c_item,
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:fb,
                        // +'<br><br>'+allitems.writing_test_explain_c,
                        submit:[],
                        submit_test:Array.from({ length:ans.length }, (_, i) => i).sort(() => 0.5 - Math.random())
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        
        function vocabulary_pair(isb0_08=false){//單字配對(配對題)
            //isb0_08=中文題目、英文選項對調
            //其他則是英文題目、中文選項
            let m = JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary))
            for (let i=0;i<m.length;i++){
                m[i].order=i;
            }
            let _ss=[];
            let examNum=parseInt(m.length/3);//總題數，以總數量/3來決定
            for (let j=0;j<examNum;j++){
                m=shuffleArray(m);
                let feedback=''
                let title=[]
                let option_title=[];
                let mp3=[];
                let _ans=[];
                for(let i=0;i<Math.min(m.length,6);i++){
                    if(i<3){
                        if(isb0_08){
                            title.push(m[i].meaning_1)
                        }else{
                            title.push(m[i].vocab)
                        }

                        _ans.push(i)
                        mp3.push(m[i].vocab_e_snd)
                        if(isb0_08){
                            feedback+=`<div>${m[i].meaning_1}</div><br><div>${m[i].vocab}</div><br>`;
                        }else{
                            feedback+=`<div>${m[i].vocab}</div><br><div>${m[i].meaning_1}</div><br>`;
                        }
                    }
                    if(isb0_08){
                        option_title.push([m[i].vocab,i])
                    }else{
                        option_title.push([m[i].meaning_1,i])
                    }
                    
                    // feedback+=`<div>${m[i].vocab}</div><br><div>${m[i].meaning_1}</div><br>`;
                    // allitems[i].dialogue_e+'<br>'+allitems[i].dialogue_c,
                }
                let ans=[]
                let pary=[]
                option_title=shuffleArray(option_title);
                // console.log(option_title,'pppp')
                for (let i=0;i<title.length;i++){
                    for(let j=0;j<option_title.length;j++){
                        if(option_title[j][1]==i){
                            ans.push(j)
                            break;
                        }
                    }
                }
                for(let j=0;j<option_title.length;j++){
                    pary.push(option_title[j][0])
                }
                // let {items,ans}=randomItems([option_title],_ans)
                _ss.push(
                    {
                        type:'pair',
                        subtype:'vocabulary_test',
                        title: title,
                        title_pic: '',
                        title_mp3:mp3,
                        option_title:pary,
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:feedback,
                        submit:[],
                        submit_test:Array.from({ length:ans.length }, (_, i) => i).sort(() => 0.5 - Math.random())
                    }              
                )
            }
            return shuffleArray(_ss);
            function getRandomArray(length) {
                var randomSet = new Set();
                while (randomSet.size < length) {
                  randomSet.add(Math.floor(Math.random() * length));
                }
                return Array.from(randomSet);
            }
        }
        function listening_fillin(){//聽力填空
            let allitems = data.DocumentElement.Dialogue.dailogue;
            let _ss=[];
            for (let i=0;i<allitems.length;i++){
                _ss.push(
                    {
                        type:'fillin',
                        subtype:'listening_test',
                        title: allitems[i].dialogue_e,
                        title_pic: '',
                        title_mp3:allitems[i].dialogue_e_snd,
                        option_title:[allitems[i].listening_f],
                        option_pic:[],
                        option_mp3:[],
                        ans:[0],
                        feedback:allitems[i].dialogue_e+'<br>'+allitems[i].dialogue_c,
                        // inputtext:'',
                        submit:'',
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function cloze_vocabulary(isd0_05=false){// 單字克漏字
            let allitems;
            if (isd0_05){
                allitems=data.DocumentElement.Dialogue.clozeTest
            }else{
                allitems= data.DocumentElement.Vocabulary;
            }
            //  console.log(allitems== data.DocumentElement.Vocabulary)
            // module.dataSet.DocumentElement.Dialogue.clozeTest
            let _ss=[];
            for (let i=0;i<Math.min(allitems.length,10);i++){
                //克漏字最多10題
                let items,ans
                if(isd0_05){
                    ({items,ans}=randomItems([allitems[i].cloze_test_item],allitems[i].cloze_test_cc_ans))
                }else{
                    ({items,ans}=randomItems([allitems[i].cloze_test_item],allitems[i].cloze_test_ans))
                    // console.log(items,ans,'======')
                }
                // console.log(items.length,'哈囉',allitems[i].meaning_1)
                _ss.push(
                    {
                        type:'choice',
                        subtype:'cloze_vocabulary',
                        title: allitems[i].cloze_test_ques,
                        title_pic: '',
                        title_mp3:'',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:allitems[i].cloze_test_ans_explain_c,
                        submit:[],
                        submit_test:[Math.floor(Math.random()*items[0].length)]

                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function listening_test_sentence(){//聽力填空_句子
            let allitems = data.DocumentElement.Dialogue.patternPractice;
            let _ss=[];
            for (let i=0;i<allitems.length;i++){
                let opttit=allitems[i].pattern_practice_explain.replace(/<\/?[^>]+(>|$)/g, "")
                _ss.push(
                    {
                        type:'fillin',
                        subtype:'listening_test_sentence',
                        title: opttit,
                        title_pic: '',
                        title_mp3:allitems[i].pattern_practice_e_snd,
                        option_title:[opttit],
                        option_pic:[],
                        option_mp3:[],
                        ans:[0],
                        feedback:allitems[i].pattern_practice_explain+'<br>'+allitems[i].pattern_practice_ques_c,
                        // inputtext:'',
                        submit:'',
                        submit_test:Math.random()>0.5?opttit:'test a error ans.'
                                            }              
                )
            }

            return shuffleArray(_ss);
        }
        function cloze_choice(){//克漏字
            let allitems = data.DocumentElement.Dialogue.clozeTest;
            let _ss=[];
            for (let i=0;i<allitems.length;i++){
                let {items,ans}=randomItems([allitems[i].cloze_test_item],allitems[i].cloze_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        subtype:'cloze_test',
                        title: allitems[i].cloze_test_ques,
                        title_pic: '',
                        title_mp3:'',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:allitems[i].cloze_test_ans_explain_c,
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function reading_comprehension(){//讀後答題
            let allitems = data.DocumentElement.Read.reading_practice;
            let _ss=[];
            for (let i=0;i<allitems.length;i++){
                let {items,ans}=randomItems([allitems[i].reading_test_c_item],allitems[i].reading_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        title: allitems[i].reading_test_ques,
                        title_pic: '',
                        title_mp3:'',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:allitems[i].reading_test_explain_c,
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function listening(){//聽力練習
            let listen = data.DocumentElement.Listen.listening;
            let practice = data.DocumentElement.Listen.listening_practice;
            let _ss=[];
            for (let i=0;i<listen.length;i++){
                let {items,ans}=randomItems([practice[i].listening_test_c_item],practice[i].listening_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        title: '',
                        title_pic: '',
                        title_mp3: listen[i].listening_listening.listening_e_snd,
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:'',
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function listening_group(){//聽力練習_題組題
            let listen = data.DocumentElement.Listen.listening;
            let practice = data.DocumentElement.Listen.listening_practice;
            // console.log(practice)
            let _ss=[];
            let listening_id;
            let mp3index=-1;
            let mp3=getMp3list()
            _ss= getGroup();
            // console.log(JSON.stringify( getGroup()));
            return _ss;//注意，此處不得使用shuffleArray，因為getGroup時已經做好隨機。
            for (let i=0;i<practice.length;i++){
                // console.log(i);
                if(practice[i].listening_id!='0' && practice[i].listening_id!=''){
                    //拿到題組代號，例如listening_6
                    listening_id=practice[i].listening_id
                    mp3index++;
                }
                let {items,ans}=randomItems([practice[i].listening_test_c_item],practice[i].listening_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        group:listening_id,
                        // groupmp3:mp3[mp3index],
                        title: practice[i].listening_test_ques,
                        title_pic: '',
                        title_mp3: '',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:'',
                        submit:[],
                        submit_test:[Math.floor(Math.random()*items[0].length)]
                    }              
                )
            }
            return shuffleArray(_ss);
            function getGroup(){
                //拿到題組題的陣列，PS這裡會為了方便將之組裝成[{},{}]型態，但是最後進入Quiz時，必須拆成平面
                let mp3=getMp3list()
                let allary=[]
                let _ss=[]
                let final=[]
                for (let i=0;i<practice.length;i++){
                    // console.log(i);
                    if(practice[i].listening_id!='0' && practice[i].listening_id!=''){
                        //拿到題組代號，例如listening_6
                        listening_id=practice[i].listening_id
                        mp3index++;
                        _ss=[]
                        allary.push(_ss);
                    }
                    let {items,ans}=randomItems([practice[i].listening_test_c_item],practice[i].listening_test_cc_ans)
                    _ss.push(
                        {
                            type:'choice',
                            group:listening_id,
                            groupmp3:mp3[mp3index],
                            title: practice[i].listening_test_ques,
                            title_pic: '',
                            title_mp3: '',
                            option_title:items[0],
                            option_pic:[],
                            option_mp3:[],
                            ans:ans,
                            feedback:'',
                            submit:[],
                            submit_test:[Math.floor(Math.random()*items[0].length)]
                        }              
                    )
                }
               
                for(let i=0;i<allary.length;i++){
                    //題組內自己先隨機排列
                    allary[i]=shuffleArray(allary[i])
                }
                allary=shuffleArray(allary);//題組隨機
                for(let i=0;i<allary.length;i++){
                    for(let ii=0;ii<allary[i].length;ii++){
                        final.push(allary[i][ii])
                    }
                }
                return final;

            }
            function getMp3list(){//拿取mp3的路徑以二層陣列回傳
                // return [
                //     [a.mp3,b.mp3],
                //     [aa.mp3,bb.mp3]
                // ]
                let g=[];
                for (let i=0;i<data.DocumentElement.Listen.listening.length;i++){
                    let p=data.DocumentElement.Listen.listening[i]
                    let gg=[]
                    for (let ii=0;ii<p.listening_listening.length;ii++){
                        gg.push(x_material_path+p.listening_listening[ii].listening_e_snd+'.mp3')
                    }
                    g.push(gg);
                }
                return g;
            }
        }
        function listening_one_and_image(){//聽力練習_題組題_單圖
            let listen = data.DocumentElement.Listen.listening;
            let practice = data.DocumentElement.Listen.listening_practice;
            // console.log(practice)
            let _ss=[];
            for (let i=0;i<practice.length;i++){
                // console.log(i);
                let {items,ans}=randomItems([practice[i].listening_test_c_item],practice[i].listening_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        title: practice[i].listening_test_ques,
                        title_pic: practice[i].listening_ep,
                        title_mp3: '',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:'',
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function spelling_practice(){//拼字練習
            let m = JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary)) 
            let _ss=[];
            for (let i=0;i<m.length;i++){
                let mm=JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary)) 
                let _title=mm.splice(i,1)[0]
                _ss.push(
                    {
                        type:'fillin',
                        title: '',
                        title_pic: '',
                        title_mp3: _title.vocab_e_snd,
                        option_title:[_title.vocab],
                        option_pic:[],
                        option_mp3:[],
                        ans:[0],
                        feedback:'<div>'+_title.vocab+' '+_title.meaning_1+'</div>',
                        submit:'',
                        // inputtext:'',
                        tiptext:replaceRandomHalfOfString(_title.vocab)
                    }              
                )
            }
            return shuffleArray(_ss);
            function replaceRandomHalfOfString(str) {//隨機生成提示
                const len = str.length;
                const halfLen = Math.floor(len / 2);
                let count = 0;
                let indices = [];
              
                // 找到字母字符的索引位置
                for (let i = 0; i < len; i++) {
                  if (/[a-zA-Z]/.test(str[i])) {
                    indices.push(i);
                  }
                }
              
                // 隨機選擇要替換的字符
                while (count < halfLen) {
                  const randomIndex = Math.floor(Math.random() * indices.length);
                  const index = indices[randomIndex];
                  str = str.substr(0, index) + '_' + str.substr(index + 1);
                  indices.splice(randomIndex, 1);
                  count++;
                }
              
                return str;
              }
        }
        function words_listen(){//看字辨音
            let m = JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary)) 
            let _ss=[];
            for (let i=0;i<m.length;i++){
                let mm=JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary)) 
                let _title=mm.splice(i,1)[0]
                let _opt = mm.sort(() => Math.random() - 0.5).slice(0, 2);
                let optitem=[_title,_opt[0],_opt[1]]
                let {items,ans}=randomItems([optitem],[0])
                items=items[0]
                let _iseng=Math.random()<0.5
                let f_html=''
                 for(let j=0;j<items.length;j++){
                    if(_iseng){
                        f_html+='<div>'+items[j].vocab+' '+items[j].meaning_1+'</div>'
                    }else{
                        f_html+='<div>'+items[j].meaning_1+' '+items[j].vocab+'</div>'
                    }
                }
                _ss.push(
                    {
                        type:'choice',
                        title: _iseng?_title.vocab:_title.meaning_1,
                        title_pic: '',
                        title_mp3: '',
                        option_title:[],
                        option_pic:[],
                        option_mp3:[items[0].vocab_e_snd,items[1].vocab_e_snd,items[2].vocab_e_snd],
                        ans:ans,
                        feedback:f_html,
                        submit:[]
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function listen_words(isb0_12=false){//聽音辨字
    
            let m = data.DocumentElement.Vocabulary
            let _ss=[];
            for (let i=0;i<m.length;i++){
                let qqary=[]
                let qary;
                if(isb0_12){
                    //only英文題目
                    if(true){
                        let {items,ans}=randomItems([m[i]['word_test_e_item_e'],m[i]['word_test_e_item']],m[i]['word_test_e_ans']);
                        qqary.push([items,ans])
                    }
                    if(true){
                        //英文題目
                        let {items,ans}=randomItems([m[i]['word_test_c_item'],m[i]['word_test_c_item_c']],m[i]['word_test_c_ans'])
                        qqary.push([items,ans])
                    }
                }else{
                    console.log('====','嗨嗨',i)
                    if(true){
                        //中文題目
                        let {items,ans}=randomItems([m[i]['word_test_e_item'],m[i]['word_test_e_item_e']],m[i]['word_test_e_ans'])
                        qqary.push([shuffleArray(items),ans])
                    }
                    if(true){
                        //英文題目
                        let {items,ans}=randomItems([m[i]['word_test_c_item'],m[i]['word_test_c_item_c']],m[i]['word_test_c_ans'])
                        qqary.push([shuffleArray(items),ans])
                    }
                }
                
                let fary=[]
                let f_html=''
                qary = shuffleArray(qqary)[0];
                console.log('====',i,qqary.length)
                console.log(JSON.stringify(qqary))
                for(let j=0;j<qary[0][0].length;j++){
                    f_html+='<div>'+qary[0][0][j]+' '+qary[0][1][j]+'</div>'
                }
               
                    
                
                _ss.push(
                    {
                        type:'choice',
                        title: "",
                        title_pic: '',
                        title_mp3: m[i]['vocab_e_snd'],
                        option_title: qary[0][0],
                        option_pic:[],
                        option_mp3:[],
                        ans:qary[1],
                        feedback:f_html,
                        submit:[]
                    }         
                )
            }
            // console.log(JSON.stringify(_ss))
            return shuffleArray(_ss);
        }
        function shuffleArray(array) {
            return array.sort(() => Math.random() - 0.5);
        }

    
    }
    constructor(_data,_word,jq_dom/**jquery的UI對象 */,_type='listen_words',_again=1/**可再答次數 */,_group=1/**題組每題 -1等於全上 */){
        let self=this;
        let ma=main_layout.ma;
        ma.setContentSpace(0);
        jq_dom=$($('[idname="QuizUI"]' ,module_html)[0].outerHTML)
        // main_layout.ma.quizui;
        self.view=jq_dom;
        // jq_dom=self.view;
        // $('body').empty()
        // $('body').append(jq_dom);
        main_layout.ma.appendContent(jq_dom)
        let jsonData=Quiz.convertDataToExamJson(_data,_word,_type)
        console.log(JSON.stringify(jsonData))
        // SpeedTest(()=>{
        //     //將題目刪減成2題
        //     jsonData.splice(2);
        // })
        // jsonData=[jsonData[0]]
        
        // return;
        _group=_group==-1?jsonData.length:_group
        let StatusChangeCallback;
        let EndCallback;
        let NextCallback;
        let AllresetCallback;//如果有此項目，則按下allreset時，將不會自動進行重新測驗，而是呼叫此函數。
        let bingo = 0;  //答對題數
        let wrong = 0;  //答錯題數
        let score = 0;//分數
        let groupitem=null;//jq的dom物件，可以放在題目的最前面，擁有群組題的最大位置
        let otherButton=null;//添加的按鈕
        let removeWrong=false;//是否移除錯題重答
        let goshowscore=false;//是否直接到最後成績畫面（通常是測試用）
        allViewHide();
        self.RemoveWrong=(val)=>{
            removeWrong=val;
            // console.log('設定好唷',removeWrong)
        }
        self.jsonDataOnlyHead=(n)=>{//只留下幾個題目;
            jsonData=jsonData.slice(0, n);
        }

        self.addEventStatusChange=(callback)=>{
            //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答,重新做答
            StatusChangeCallback=callback;
        }
        self.addEventEnd=(callback)=>{
            EndCallback=callback;
            //data={score:50,right:5,wrong:5}
        }
        self.addResetCallback=(callback)=>{//如果有此項目，則按下allreset時，將不會自動進行重新測驗，而是呼叫此函數。
            AllresetCallback=callback;
        }
        self.addGoNextCallback=(callback)=>{
            NextCallback=callback
        }
        self.addGroupUpItem=(jqitem)=>{//在題目與互動之前疊加項目。例如題組題的播放器。
            groupitem=jqitem;
        }
        self.addOtherButton=(jqitem)=>{//在送出按鈕旁添加一按鈕。
            otherButton=jqitem;
        }
        let audio = new Audio();


        let index_now = 0;
        if($('[name="input_view"]',jq_dom).length>0){
            //有輸入欄位，就先登錄下來
            templete["input_view"] = $('[name="input_view"]',jq_dom)[0].outerHTML;
            // $('[name="input_view"]',jq_dom).remove();
        }
        if($('[name="titlegrid_view"]',jq_dom).length>0){
            //表格化的題目(配對題才會用到)
            templete["titlegrid_view"] = $('[name="titlegrid_view"]',jq_dom)[0].outerHTML;
            $('[name="titlegrid_view"]',jq_dom).remove();
        }
        // let option_btn_focus_style;
        if(true){
            //配對專用
            if($('[name="option_btn"]',jq_dom).length>0){
                templete['option_btn']=$("[name='option_btn']",jq_dom)[0].outerHTML
                // option_btn_focus_style=$($("[name='option_btn_focus']",jq_dom)[0].outerHTML);
            }
            if($('[name="option_select"]',jq_dom).length>0){
                // console.log('cp cp ')
                templete['option_select']=$("[name='option_select']",jq_dom)[0].outerHTML
                templete['option_select_option']=$("[name='option_select_option']",jq_dom)[0].outerHTML
            }
            if($('[name="pair_item"]',jq_dom).length>0){
                templete['pair_item']=$('[name="pair_item"]',jq_dom)[0].outerHTML;
                $('[name="pair_item"]',jq_dom).remove();
            }
            // if($('[name="pair_btn_empty"]',jq_dom).length>0){
            //     templete['pair_btn_empty']=$('[name="pair_btn_empty"]',jq_dom)[0].outerHTML;
            //     $('[name="pair_btn_empty"]',jq_dom).remove();
            // }
            // if($('[name="pair_btn_target"]',jq_dom).length>0){
            //     templete['pair_btn_target']=$('[name="pair_btn_target"]',jq_dom)[0].outerHTML;
            //     $('[name="pair_btn_target"]',jq_dom).remove();
            // }
            if($('[name="pair_btn_play"]',jq_dom).length>0){
                templete['pair_btn_play']=$('[name="pair_btn_play"]',jq_dom)[0].outerHTML;
                $('[name="pair_btn_play"]',jq_dom).remove();
            }
            if($('[name="pair_table_view"]',jq_dom).length>0){
                templete['pair_table_view']=$('[name="pair_table_view"]',jq_dom)[0].outerHTML;
                $('[name="pair_table_view"]',jq_dom).remove();
            }
            if($('[name="pair_feedback_view"]',jq_dom).length>0){
                templete['pair_feedback_view']=$('[name="pair_feedback_view"]',jq_dom)[0].outerHTML;
                $('[name="pair_feedback_view"]',jq_dom).remove();
            }
            if($('[name="pair_options"]',jq_dom).length>0){
                templete['pair_options']=$('[name="pair_options"]',jq_dom)[0].outerHTML;
                $('[name="pair_options"]',jq_dom).remove();
            }
            if($('[name="write_pair"]',jq_dom).length>0){
                templete["write_pair"]=$('[name="write_pair"]',jq_dom)[0].outerHTML;
                $('[name="write_pair"]',jq_dom).remove();
            }

            templete['pair_item_selected']=$('[name="pair_item_selected"]',jq_dom)
            

        }
        let option_selected_style = $('[name="option_selected"]',jq_dom);
        if($('[name="option"]',jq_dom).length>0){
            templete['option'] = $('[name="option"]',jq_dom)[0].outerHTML;
        }
        let option_selectStyle = $('[name="option_selected"]',jq_dom);
        let option_selectBingo = $('[name="option_bingo"]',jq_dom);
        let option_selectWrong = $('[name="option_wrong"]',jq_dom);
        $('[name="option_container"]',jq_dom).empty();
        // if($('[name="ans_o"]',jq_dom).length>0){
        //     templete["ans_o"] = $('[name="ans_o"]',jq_dom)[0].outerHTML;
        //     templete["ans_x"] = $('[name="ans_x"]',jq_dom)[0].outerHTML;
        // }
        // $('[name="ansox"]',jq_dom).empty();
        // if($('[name="question_item"]',jq_dom).length>0){
        templete["question_choice"] = $('[name="question_choice"]',jq_dom)[0].outerHTML;
        templete["question_fillin"] = $('[name="question_fillin"]',jq_dom)[0].outerHTML;
        templete["question_pair"] = $('[name="question_pair"]',jq_dom)[0].outerHTML;
        templete["question_pair_b0_08"] = $('[name="question_pair_b0_08"]',jq_dom)[0].outerHTML;
        // question_choice_readonly
        templete["question_choice_readonly"] = $('[name="question_choice_readonly"]',jq_dom)[0].outerHTML;
        templete["question_fillin_readonly"] = $('[name="question_fillin_readonly"]',jq_dom)[0].outerHTML;
        templete["question_pair_readonly"] = $('[name="question_pair_readonly"]',jq_dom)[0].outerHTML;
        templete["question_pair_b0_08_readonly"] = $('[name="question_pair_b0_08_readonly"]',jq_dom)[0].outerHTML;
        

        templete["question_choice_feedback_container"] = $('[name="question_choice_feedback_container"]',jq_dom)[0].outerHTML;
        templete["question_fillin_feedback_container"] = $('[name="question_fillin_feedback_container"]',jq_dom)[0].outerHTML;
        templete["question_pair_feedback_container"] = $('[name="question_pair_feedback_container"]',jq_dom)[0].outerHTML;
        templete["question_pair_b0_08_feedback_container"] = $('[name="question_pair_b0_08_feedback_container"]',jq_dom)[0].outerHTML;


        
        //配對題跳出視窗
        templete['pair_option_list']=$("[name='pair_option_list']",jq_dom)[0].outerHTML
        templete['option_item']=$("[name='pair_option_list'] [name='option_item']",jq_dom)[0].outerHTML
        templete['option_item_selected']=$("[name='pair_option_list'] [name='option_item_selected']",jq_dom)[0].outerHTML
        templete['option_item_using']=$("[name='pair_option_list'] [name='option_item_using']",jq_dom)[0].outerHTML

        
        // }
        // templete['question_container']=
        $('[name="question_container"]',jq_dom).empty();

        templete['num_no']=$('[name="num_no"]',jq_dom)[0].outerHTML
        templete['num_total']=$('[name="num_total"]',jq_dom)[0].outerHTML
        let numno_view=$('[name="num_no"]',jq_dom).parent();
        // $('[name="num_no"]',jq_dom).remove()
        // $('[name="num_total"]',jq_dom).remove()
        self.showScore=()=>{//測試直接呈現結果頁
            //使用假作答
            jsonDataToDemo();
            goshowscore=true;
            // showRecordList();
        }
        // 作答紀錄呈現
        // console.log($('[name="btn_record"]',jq_dom))
        $('[name="btn_record"]',jq_dom).unbind('click').bind('click', function () {
            // console.log('出現紀錄')
            $('[name="btn_record"]',jq_dom).hide()
            showRecordList();
        });

        //練習錯題
        $('[name="btn_wrong"]',jq_dom).unbind('click').bind('click', function () {
            if(false){
                let delary=[]
                for (let i = 0; i < jsonData.length; i++) {
                    let _groupox;
                    if(i%_group==0){//每個題組開始，先訂為正確
                        _groupox=true;
                    }
                    // console.log(_groupox)
                    if(!_groupox){//題組已經錯誤，同組的直接為錯
                        delary.push(false);
                        continue;
                    }
                    let submit=jsonData[i].submit;
                    let ans=jsonData[i].ans;
                    // console.log('壓哈哈',submit, ans,AnsIsEqual(submit, ans))
                    jsonData[i].submit = []
                    if(jsonData[i].type=='pair'){
                        let c=true;
                        for(let ii=0;ii<ans.length;ii++){
                            if(ans[ii]!=submit[ii]){//拖曳某項目錯誤
                                c=false
                                break;
                            }
                        }
                        if(c){
                            delary.push(true);
                            // jsonData.splice(i, 1);
                            // i--;
                        }else{
                            _groupox=false;
                            delary.push(false);
                        }
                        continue;
                    }else{
                        if (AnsIsEqual(submit, ans)) {
                            // delary.push(i);
                            delary.push(true);
                            // jsonData.splice(i, 1);
                            // i--;
                            continue;
                        }else{
                            _groupox=false;
                            delary.push(false);
                        }
                        continue;
                    }
                }
                console.log(delary,'-----');
                for (let i = 0; i < delary.length; i += _group) {
                    // 如果該組中有一個 false
                    if (delary.slice(i, i + _group).includes(false)) {
                        // 將該組所有元素設為 false
                        for (let j = 0; j < _group && i + j < delary.length; j++) {
                        delary[i + j] = false;
                        }
                    }
                }
                console.log(delary,'-----');
            }
            let o=getScore(jsonData);
            // let newJsonData = jsonData.filter((item, index) => !delary[index]);
            jsonData = o.xxary;
            // newJsonData;
            // delary.sort((a, b) => b - a).forEach(index => {
            //     //刪除掉正確的題組所有正確的題目
            //     jsonData.splice(index, 1);
            // });
            index_now=0
            $('[name="title"]',jq_dom).show();
            $('[name="question_container"]',jq_dom).show();
            $('[name="score_view"]',jq_dom).hide();
            goNext();
            StatusChangeCallback&&StatusChangeCallback('開始錯題重答',0,jsonData)
        });
        //重新作答按鈕
        $('[name="btn_allreset"]',jq_dom).unbind('click').bind('click', function () {
            reset();
        });
        setTimeout(()=>{
            // showScore();
            // showRecordList()
            if(goshowscore){
                showRecordList();
            }else{
                goNext()
            }
            
        },200)
        function getScore(jsonData){//計算對錯題、分數、錯題重答對象。
            let delary=[]
            let ooary=[]
            let xxary=[]
            let oo=0;
            let xx=0;
            let all=0;
            for (let i = 0; i < jsonData.length; i++) {
                let submit=jsonData[i].submit;
                let ans=jsonData[i].ans;
                if(jsonData[i].type=='pair'){
                    let c=true;
                    for(let ii=0;ii<ans.length;ii++){
                        if(ans[ii]!=submit[ii]){//拖曳某項目錯誤
                            c=false
                            break;
                        }
                    }
                    if(c){
                        delary.push(true);
                    }else{
                        delary.push(false);
                    }
                    continue;
                }else{
                    if (AnsIsEqual(submit, ans)) {
                        delary.push(true);
                        continue;
                    }else{
                        delary.push(false);
                    }
                    continue;
                }
            }
            //得到delary=[true,false,true,false...]每子題的對錯
            console.log(delary,'-----');
            for(let i=0;i<delary.length;i++){
                let nn=getSameGroup(i);
                let gg=delary.slice(i, i + nn.length)
                if(gg.includes(false)){
                    xx++;
                    for (let j = i; j < i + nn.length; j++) {
                        xxary.push(jsonData[j])
                    }
                }else{
                    oo++;
                    for (let j = i; j < i + nn.length; j++) {
                        ooary.push(jsonData[j])
                    }
                }
                i+=nn.length-1;
            }
            all=oo+xx;
            return {score:Math.floor(100/all*oo),all:all,oo:oo,xx:xx,ooary:ooary,xxary:xxary}
        }
        function getGroupIndexNow(_index){//拿取目前題號，題組算一題，其他算單題
            let ind=0;
            if(_index==0){
                return 0;
            }
            for (let i=0;i<jsonData.length;i++){
                if(_index<=i){
                    return ind;
                }
                let n=getSameGroup(i);
                // console.log(n.length,'UUUUU')
                i+=n.length-1;
                ind++;

            }
            return ind;

        }
        function getGroupCount(){//拿取題組的總題數，若非題組則為1
            let len=0;
            for (let i=0;i<jsonData.length;i++){
                let n=getSameGroup(i);
                len++;
                i+=n.length-1;
            }
            return len;
        }
        function getSameGroup(_index){//拿取本次與後續的同群組的項目。回傳陣列
            let _nowgroup;
            let nn=[]
            nn.push(jsonData[_index])
            if(!jsonData[_index].group){//非群組題，只傳回自己
                return nn;
            }else{
                _nowgroup=jsonData[_index].group;
            }
            for(let i=_index+1;i<jsonData.length;i++){
                    if(jsonData[i]['group'] && jsonData[i]['group']==_nowgroup){
                        //同組
                        nn.push(jsonData[i])
                    }else{
                        // 不同組或不是題組題;
                        break;
                    }    

            }
            return nn;
        }
        function getItemrow(_index,isActive=true,gosubmit=()=>{},callback=()=>{}){//拿取一整個題目內容，不論是單題或題組題。
            let _aa = $('<div/>')
            let _row=[];
            let _q=getSameGroup(_index);
            if (true){
                for (let i=0;i<_q.length;i++){
                    if(i==0){
                        if(jsonData[_index].groupmp3){
                            let pbk=new mp3playback(jsonData[_index].groupmp3);
                            groupitem=pbk.view;
                        }
                    }
                    let p=drawQuestionItem(_index,isActive,gosubmit,callback);
                    _row.push(p)
                    _aa.append(p);
                    _aa.append('<br>');
                    _index++;
                }
            }
            return {"dom":_aa,"itemrow":_row,"newindex":_index}
        }
        function goNext(){
            MaxMediaPlayer.AllStop();
            $('[name="question_container"]',jq_dom).first().empty()
            // index++;
            allViewHide()
            $('.title').show()
            $('.title [name="title_info"]').show()
            $('font[name="num_no"]').parent('span').show();
            // let groupSize=Math.ceil(jsonData.length/_group)/**題組題的總題數 */
            // let groupIndex=Math.ceil((index_now+1)/_group)/**題組題的目前索引 */
            // $('[name="num_no"]',jq_dom).text('Q'+groupIndex)
            $('[name="num_no"]',jq_dom).text('Q'+(getGroupIndexNow(index_now)+1));
            // $('[name="num_total"]',jq_dom).text('/ '+groupSize)
            $('[name="num_total"]',jq_dom).text('/ '+getGroupCount())
            let _gg=getItemrow(index_now,true,gosubmit,(status,item)=>{
                //selected
                if(status=='selected'){
                    //由題目呼叫本事件，判斷是否到了呈現submit的時候了。主要是題組題(選擇題)的應用場景
                    let p=true;
                    for(let i=0;i<itemsrow.length;i++){
                        if(!itemsrow[i].isSelected){
                            p=false;
                        }
                    }
                    if(p){
                        $('[name="btn_submit"]',jq_dom).show();
                    }else{
                        $('[name="btn_submit"]',jq_dom).hide();
                    }
                }
            })
            let _aa =_gg.dom;
            // $('<div/>')
            let itemsrow=_gg.itemrow;
            index_now=_gg.newindex;
            let ox=[]//本次題、題組對錯
            let again_count=0;
            //題組題出題
            // itemsrow=getItemrow(index_now,gosubmit)
if (false){            
            let _q=getSameGroup(index_now);
            
            // console.log('index==',index_now,len)

            // console.log('index==:',index_now)
if (true){
    for (let i=0;i<_q.length;i++){
        if(i==0){
            if(jsonData[index_now].groupmp3){
                let pbk=new mp3playback(jsonData[index_now].groupmp3);
                groupitem=pbk.view;
                console.log('來到這裡')
            }
        }
        let p=drawQuestionItem(index_now,true,gosubmit);
        itemsrow.push(p)
        _aa.append(p);
        _aa.append('<br>');
        index_now++;
    }
}
}
if(false){
    let len=0;//=Math.min(index_now+_group,jsonData.length)
    for(let i=index_now;i<len;i++){
        let p=drawQuestionItem(index_now,true,gosubmit);
       itemsrow.push(p)
       _aa.append(p);
    //    _aa.append('<hr><br>');
       _aa.append('<br>');
       index_now++
    }

}
            if(groupitem){
                //在整體題目最上方放置項目，例如[再次閱讀]
                $('[name="question_container"]',jq_dom).first().append(groupitem);
            }
            $('[name="question_container"]',jq_dom).first().append(_aa);
            if(otherButton){
                $('[name="btn_submit"]',jq_dom).before(otherButton);
            }
            $('[name="btn_submit"]',jq_dom).unbind('click').bind('click',gosubmit);
            $('[name="btn_again"]',jq_dom).unbind('click').bind('click', (e)=> {
                $('[name="btn_submit"]',jq_dom).hide();
                $('[name="btn_next"]',jq_dom).hide();
                $('[name="btn_again"]',jq_dom).hide();
                // $('[name="feedback_view"]',jq_dom).hide();
                ma.hideFeedback();
                $('[name="btn_score"]',jq_dom).hide()
                again_count++;
                for(let i=0;i<itemsrow.length;i++){
                    if( !ox[i]){
                        itemsrow[i].again(e);
                        itemsrow[i].isSelected=false;
                    }
                }
            });
            //下一題按鈕
            $('[name="btn_next"]',jq_dom).unbind('click').bind('click', function () {
                // drawQuestionItem();
                goNext()
            });



            //觀看成績
            $('[name="btn_score"]',jq_dom).unbind('click').bind('click', function () {
                //統計總成績
                showScore()
                // StatusChangeCallback&&StatusChangeCallback('呈現答題記錄')
            });


            NextCallback&&NextCallback(Math.ceil(index_now/_group)-1);//回傳目前出題數字，題組題不管包含幾題都屬1。
            StatusChangeCallback&&StatusChangeCallback('測驗進行中',index_now,jsonData[index_now-1])//題組題則只傳最後題目

            function gosubmit(e){//最上層submit事件
                $('[name="btn_submit"]',jq_dom).hide();
                $('[name="btn_next"]',jq_dom).hide();
                $('[name="btn_again"]',jq_dom).hide();
                // $('[name="feedback_view"]',jq_dom).hide();
                ma.hideFeedback();
                $('[name="btn_score"]',jq_dom).hide()
                let isallpase=true;
                for(let i=0;i<itemsrow.length;i++){
                    let pass=itemsrow[i].submit(e)
                    ox.push(pass);
                    if(!pass){
                        isallpase=false;
                        if(itemsrow[i].type=='input'){
                            itemsrow[i].showtips()
                            // $('[name="tips_view"]',jq_dom).show()
                        }
                    }
                }
                // console.log('99999',again_count,_again)
                if (!isallpase && again_count < _again) {
                    $('[name="btn_again"]',jq_dom).show()
                    // if(itemsrow[i].type=='input'){
                    // //if( jsonData[i].type=='input'){
                    //     $('[name="tips_view"]',jq_dom).show()
                    // }
                } else {
                    for(let i=0;i<itemsrow.length;i++){
                        itemsrow[i].showNextAndFeedback()
                    }
                    if(index_now<=jsonData.length-1){
                        $('[name="btn_next"]',jq_dom).show()
                    }else{
                        $('[name="btn_score"]',jq_dom).show()
                    }
                }
            }
        }
        function jsonDataToDemo(){//用測試資料作為答題紀錄
            for (let i=0;i<jsonData.length;i++){
                if(jsonData[i].submit_test){
                    jsonData[i].submit=jsonData[i].submit_test;
                }
            }
        }
        function showRecordList() {//列出作答紀錄
            allViewHide()
            $('[name="num_no"]',jq_dom).text('')
            $('[name="num_total"]',jq_dom).text('')
            // $('[name="btn_submit"]',jq_dom).hide();
            // $('[name="btn_next"]',jq_dom).hide();
            // $('[name="btn_again"]',jq_dom).hide();
            // $('[name="feedback_view"]',jq_dom).hide();
            // $('[name="btn_score"]',jq_dom).hide()
            // $('[name="answer_record_view"]',jq_dom).show();
            $('[name="score_view"]',jq_dom).show();
            $('[name="btn_allreset"]',jq_dom).show()
            if(score<100){
                $('[name="btn_wrong"]',jq_dom).show()
            }
            $('[name="btn_record_view_close"]').unbind('click').bind('click',(e)=>{
                // $('[name="answer_record_view"]',jq_dom).hide();
                ma.hideFeedback()
                $('[name="btn_record"]',jq_dom).show()
            })
            // $('[name="listening"]',jq_dom).hide();
            // $('[name="answer_record_view"]',jq_dom).show();
            let aa = $('[name="answer_record_view"]',jq_dom);
            $('[name="container"]', aa).empty();

            // if(true){
            //     let index_now=0;
            //     let groupSize=Math.ceil(jsonData.length/_group)/**題組題的總題數 */
            //     let groupIndex=Math.ceil((index_now+1)/_group)/**題組題的目前索引 */
            //     // $('[name="num_no"]',jq_dom).text(groupIndex)
            //     // $('[name="num_total"]',jq_dom).text('/ '+groupSize)
            //     let _aa = $('<div/>')
            //     let itemsrow=[]
            //     let ox=[]
            //     let again_count=0;
            //     //題組題出題
            //     let len=Math.min(index_now+_group,jsonData.length)
            //     // console.log('index==',index_now,len)
            //     for(let i=index_now;i<len;i++){
            //         let p=drawQuestionItem(index_now,true);
            //         itemsrow.push(p)
            //         _aa.append(p);
            //         index_now++
            //     }
            // }

            // if(false){
            // console.log(JSON.stringify( jsonData))
            let g=$('<div>')//準備裝載容器，每一種互動的回饋容器都不同。
            let _typ;
            let _ct;//feedback的容器對象
            // question_choice_feedback_container
            for (let i = 0; i < jsonData.length; i++) {
                let no=getGroupIndexNow(i)+1
                if(jsonData[i].type!=_typ){
                    _typ=jsonData[i].type;
                    // _ct=$(templete["question_"+_typ+"_feedback_container"])
                    _ct=$(getPairFeedbackContainerHTML(jsonData[i]))
                    _ct.empty();
                    g.append(_ct);
                }

                let _gg=getItemrow(i,false)
                let _aa =_gg.dom;
                // $('<div/>')
                let itemsrow=_gg.itemrow;
                i=_gg.newindex-1



                // let _aa = drawQuestionItem(i,false);
                let num_no=$(templete['num_no'])
                // num_no.text('Q'+(i+1))
                num_no.text('Q'+(no))
                let _cc=$('<div></div>');
                _cc.append(num_no);
                _cc.append(_aa);
                _ct.append(_cc);
            }
            // }
            ma.setFeedback(g);
            ma.setFeedbackTitle('作答紀錄');
            ma.showFeedback();
            StatusChangeCallback&&StatusChangeCallback('呈現答題記錄')
        }
        function getPairFeedbackContainerHTML(_jsdata){//拿取符合模組需求的介面HTML對象
            switch(_jsdata.type){
                case "pair":
                    if(_jsdata.subtype=='vocabulary_test'){
                        return templete["question_pair_b0_08_feedback_container"];
                    }else{
                        return templete["question_pair_feedback_container"];
                    }
                default:
                    return templete["question_"+_jsdata.type+"_feedback_container"]
            }
        }
          
        function drawQuestionItem(_index,isActive=true/**是否互動 */,callsubmit=()=>{},callbackevent=()=>{}) {
            // if(!isActive){
            //     numno_view.empty()
            //     let num_no=$(templete['num_no'])
            //     let num_total=$(templete['num_total'])
            //     let groupSize=Math.ceil(jsonData.length/_group)/**題組題的總題數 */
            //     let groupIndex=Math.ceil((index_now+1)/_group)/**題組題的目前索引 */
            //     num_no.text('Q'+groupIndex)
            //     num_total.text('/ '+groupSize)
                        
            //     numno_view.append(num_no)
            //     numno_view.append(num_total)
            //     question_container
            // }
            let itemary = [];
            let question_item;
            $('[name="question_container"]',jq_dom).empty();

            function getPairHTML(_jsdata){//拿取符合模組需求的介面HTML對象
                switch(_jsdata.type){
                    case "pair":
                        if(_jsdata.subtype=='vocabulary_test'){
                            if(isActive){
                                question_item=$(templete["question_pair_b0_08"]);
                            }else{
                                question_item=$(templete["question_pair_b0_08_readonly"]);
                            }

                        }else{
                            if(isActive){
                                question_item=$(templete["question_pair"]);
                            }else{
                                question_item=$(templete["question_pair_readonly"]);
                            }
                        }
                                //配對題互動項目
                        templete['option_title_focus']=$("[name='option_btn'] [name='title_focus']",question_item)[0].outerHTML
                        templete['option_title_error']=$("[name='option_btn'] [name='title_error']",question_item)[0].outerHTML
                        $("[name='option_btn'] [name='title_focus']",question_item).remove()
                        $("[name='option_btn'] [name='title_error']",question_item).remove()
                        templete['option_btn']=$("[name='option_btn']",question_item)[0].outerHTML
                        break;
                }
                return question_item;
            }
            switch(jsonData[_index].type){
                case "pair":
                    question_item=getPairHTML(jsonData[_index])
                    // if(isActive){
                    //     question_item=$(templete["question_pair"]);
                    // }else{
                    //     question_item=$(templete["question_pair_readonly"]);
                    // }
                    //         //配對題互動項目
                    // templete['option_title_focus']=$("[name='option_btn'] [name='title_focus']",question_item)[0].outerHTML
                    // templete['option_title_error']=$("[name='option_btn'] [name='title_error']",question_item)[0].outerHTML
                    // $("[name='option_btn'] [name='title_focus']",question_item).remove()
                    // $("[name='option_btn'] [name='title_error']",question_item).remove()
                    // templete['option_btn']=$("[name='option_btn']",question_item)[0].outerHTML
                    break;
                case "choice":
                    if(isActive){
                        question_item=$(templete["question_choice"]);
                    }else{
                        question_item=$(templete["question_choice_readonly"]);
                    }
                    $('[name="option_container"]', question_item).empty();
                    $('[name="ansox"]', question_item).empty();
                    if(jsonData[_index].title_pic==''){
                        $('[name="pic"]',question_item).hide();
                    }else{
                        $('[name="pic"] img',question_item).first().attr('src',x_material_path+jsonData[_index].title_pic)
                    }
                    if(jsonData[_index].title==''){
                        $('[name="title"]',question_item).hide();
                    }else{
                        $('[name="title"]',question_item).first().text(jsonData[_index].title)
                    }
        
                    break;
                case "fillin":
                    if(isActive){
                        question_item=$(templete["question_fillin"]);
                    }else{
                        question_item=$(templete["question_fillin_readonly"]);
                    }
                    let tt= $('[name="input"]',question_item).first();
                    $('[name="ansox"] [name="ans_o"]', question_item).hide()
                    $('[name="ansox"] [name="ans_x"]', question_item).hide()
                    if(!isActive){
                        let title=$('[name="title"]',question_item).first();
                        title.text(jsonData[_index].title);
                        tt.prop('disabled', true);
                        tt.val(jsonData[_index].submit);
                        // tt.attr('value',jsonData[_index].submit);
                        if(checkFillText(jsonData[_index].submit,jsonData[_index].option_title[0])){
                        // if(jsonData[_index].submit[0]==0){
                        // if($('[name="input_view"] input',jq_dom).first().val()==jsonData[_index].option_title[0]){
                            $('[name="ansox"] [name="ans_o"]', question_item).show()
                            $('[name="ansox"] [name="ans_x"]', question_item).hide()
                            // showOX('O')
                        }else{
                            $('[name="ansox"] [name="ans_o"]', question_item).hide()
                            $('[name="ansox"] [name="ans_x"]', question_item).show()
                            // showOX('X')
                        }
                    }else{
                        tt.off('keyup').on('keyup', (e)=>{
                            let p=tt;
                            if(e.keyCode == 13){
                                // console.log('這裡啦')
                                callsubmit()
                                // question_item.submit();
                                // submit(e)
                            }else{
                                if($('[name="input_view"] input',question_item).first().val().length > 0){
                                // if(p.val().length > 0){
                                    console.log('有長度')
                                    $('[name="btn_submit"]',jq_dom).show();
                                }else{
                                    $('[name="btn_submit"]',jq_dom).hide();
                                }
                            }
                        })
                    }
                    break;
            }
            
            $('[name="question_container"]',jq_dom).append(question_item);
            question_item.isSelected=false;//是否已經做好選擇(給上層做submit按鈕是否出現的參考依據)
            question_item.type=jsonData[_index].type;
            question_item.submit=()=>{
                // console.log('999===',question_item.showNextAndFeedback,question_item.type)
                if(question_item.type=='input' || question_item.type=='fillin'){
                    $('[name="input_view"] [name="input"]',question_item).prop('disabled', true);
                    jsonData[_index].submit=$('[name="input_view"] input',question_item).first().val()
                    // console.log($('[name="input_view"] input',question_item).first().val(),'++++')
                    // let aa=jsonData[_index].submit.replace(/'|＇|‘|’/g, "'");
                    // let bb=jsonData[_index].option_title[0].replace(/'|＇|‘|’/g, "'");
                    // if(aa==bb){
                    if(checkFillText(jsonData[_index].submit,jsonData[_index].option_title[0])){
                    //if($('[name="input_view"] input',question_item).first().val()==jsonData[_index].option_title[0]){
                        showOX('O')
                        $('[name="ansox"] [name="ans_o"]', question_item).show()
                        $('[name="ansox"] [name="ans_x"]', question_item).hide()

                        // jsonData[_index].submit=[0]
                        // jsonData[_index].submit=
                        question_item.showNextAndFeedback()
                        return true;
                    }else{
                        // jsonData[_index].submit=[1]
                        $('[name="ansox"] [name="ans_o"]', question_item).hide()
                        $('[name="ansox"] [name="ans_x"]', question_item).show()

                        showOX('X')
                        // $('[name="ansox"]', question_item).empty();
                        // let ox=$(templete["ans_x"])
                        // $('[name="ansox"]', question_item).append(ox);
                    }
                }else{
                    // console.log('999===',question_item.type)
                    btnIsOpen = false;
                    jsonData[_index].submit = []
                    for (let i = 0; i < itemary.length; i++) {
                        let p = itemary[i];
                        if (p.selected) {
                            jsonData[_index].submit.push(p.i);
                        }
                    }
                    if (AnsIsEqual(jsonData[_index].ans, jsonData[_index].submit)) {
                        showOX('O')
                        // showNextAndFeedback()
                        question_item.showNextAndFeedback()
                        return true;
                    } else {
                        showOX('X')
                        // $('[name="ansox"]', question_item).empty();
                        // let ox=$(templete["ans_x"])
                        // $('[name="ansox"]', question_item).append(ox);
                    }
                }
                return false;
            };
            question_item.again=()=>{
                btnIsOpen = true;
                if( jsonData[_index].type=='input' || jsonData[_index].type=='fillin'){
                    //輸入聊天
                    $('[name="input_view"] input',question_item).first().val('')
                    $('[name="input_view"] input',question_item).prop('disabled', false);
                    $('[name="btn_submit"]',question_item).hide();
                }else{
                    for (let i = 0; i < itemary.length; i++) {
                        let p = itemary[i];
                        p.selected == false;
                        toggleElements(p, option_selectStyle, false, false);
                        toggleElements(p, option_selectBingo, false, false);
                        toggleElements(p, option_selectWrong, false, false);
                    }
                }
                $('[name="ansox"]',question_item).first().empty();
                return;

            };

            question_item.showtips=()=>{
                $('[name="tips_view"]',question_item).show()
            }
            question_item.showNextAndFeedback=()=>{
                // console.log('來呼叫這裡了',jsonData[_index].feedback)
                for (let i = 0; i < itemary.length; i++) {
                    let p = itemary[i]
                    checkItemStatus(_index, p)
                }
                if (_index > jsonData.length - 1) {
                    // $('[name="btn_score"]',jq_dom).show();
                } else {
                    if(jsonData[_index].feedback!=''){
                        // $('[name="feedback_view"]',jq_dom).show();
                        ma.setFeedbackTitle('詳解');
                        ma.showFeedback();
                    }
                    // $('[name="btn_next"]',jq_dom).show();
                }
            }
            // if(question_item.type=='input' || question_item.type=='fillin'){
            //     $('input').keyup(function(event) {
            //         if (event.which === 13) { // 13是Enter鍵的鍵碼
            //             console.log('這裡')
            //             question_item.submit()
            //         }
            //       });
            // }
            let btnIsOpen = true;

            
            // $('[name="btn_again"]',jq_dom).unbind('click').bind('click', function () {
            //     //再一次按鈕
            //     again(e)


            // });


            // 作答紀錄呈現圈叉
            // $('[name="btn_submit"]',jq_dom).unbind('click').bind('click', (e)=> {
            //     submit(e);
            // });

            if(jsonData[_index].type=='pair'){
                console.log('配對題')

                question_item.empty();
                let options = jsonData[_index].option_title.map((item, index) => ({id: index, v: item}));
                if(jsonData[_index].submit.length>0){

                }else{
                    jsonData[_index].submit=jsonData[_index].ans.map(() => -1);
                }
                
                options.sort(() => Math.random() - 0.5);
                let content;
                // templete['option_select']=$("[name='option_select']")[0].outerHTML
                // `
                //         <div class="select-container-justpair">
                //             <select style="min-width: 100px;width: 1px;transition: width 0.5s;"></select>
                //             <span name="oo" class="select-icon-justpair o">O</span>
                //             <span name="xx" class="select-icon-justpair x">X</span>
                //         </div>`;
                // templete['option_select_option']=$("[name='option_select_option']")[0].outerHTML
                // '<option value="-1"></option>';
                // jsonData[_index].submit=options.map(item => item.id);
                // console.log('===',options,jsonData[_index].submit,jsonData[_index].ans)
                
                // let optionview=$(templete['pair_options']);


                let btnio=true;

                //除標題之外其他清空
                let titary=[]
                let select1=null;
                let btn_ary=[]
                let tag_count=0;
                let runing=true;//流程行進中(遇到再作答一次之類的行為可以短暫暫停為false)
                // $('[name="feedback_view"] [name="feedback_content"]',jq_dom).html(jsonData[_index].feedback)
                ma.setFeedback($('<div>'+jsonData[_index].feedback+'</div>'))
                // $("[name='btn_next']",jq_dom).unbind('click').bind('click',(e)=>{
                //     goNext()
                // })
                // $("[name='btn_score']",jq_dom).unbind('click').bind('click',(e)=>{
                //     showScore()
                // })
                if(jsonData[_index].subtype=='unscramble_sentences'){//短句重組
                    let p=$(templete['write_pair']);
                    tag_count=jsonData[_index].ans.length;
                    question_item.append(p);
                    content=$('[name="content"]',p)
                    content.html(jsonData[_index].title)
                }else if(jsonData[_index].subtype=='unscramble________sentences'){//短句重組
                    let p=$(templete['write_pair']);
                    question_item.append(p);
                    let txt = ''
                    for (var i = 0; i < jsonData[_index].option_title.length; i++) {
                        var option_title = jsonData[_index].option_title[i];
                        tag_count++;
                        txt = txt + '<span name="b'+i+'" style="display: inline-block"></span>';
                    }
                    content=$('[name="content"]',p)
                    content.html(txt+'<br><br>'+jsonData[_index].tit_c);
                }else if(jsonData[_index].subtype=='vocabulary_test'){//表格配對
                    let tbview=$('<div></div>');
                    content=tbview
                    question_item.append(tbview);
                    for (let i=0;i<jsonData[_index].title.length;i++){
                        let gg=$('<span name="b'+i+'"></span>')
                        tbview.append(gg);
                        // $('tbody tr:first td:eq('+(i+1)+')',tbview).first().text(jsonData[_index].title[i])
                        // let pp=newItem('target');
                        
                        // pp.showempty()
                        // pp.settitle('')
                        // titary.push(pp);
                        // '<span name="b'+i+'" style="display: inline-block"></span>'
                        tag_count++;
                        // $('tbody tr:eq(1) td:eq('+(i+1)+')',tbview).first().append($('<span name="b'+i+'"></span>'));
                        // // $('tbody tr:eq(1) td:eq('+(i+1)+')',tbview).first().append(pp.view);
                        // let playbtn=$(templete['pair_btn_play']);
                        // playbtn.mp3=jsonData[_index].title_mp3[i];
                        // $('tbody tr:eq(2) td:eq('+(i+1)+')',tbview).first().append(playbtn);
                        // // if(isActive){
                        // playbtn.unbind('click').bind('click',(e)=>{
                        //     play('../material/'+playbtn.mp3+'.mp3')
                        // })
                        // // }
                    }

                }else if(jsonData[_index].subtype=='vocabulary_____test'){//表格配對
                    let tbview=$(templete['pair_table_view']);
                    content=tbview
                    question_item.append(tbview);
                    $('tbody tr td:not(:first-child)',tbview).empty()
                    for (let i=0;i<jsonData[_index].title.length;i++){
                        
                        $('tbody tr:first td:eq('+(i+1)+')',tbview).first().text(jsonData[_index].title[i])
                        // let pp=newItem('target');
                        
                        // pp.showempty()
                        // pp.settitle('')
                        // titary.push(pp);
                        // '<span name="b'+i+'" style="display: inline-block"></span>'
                        tag_count++;
                        $('tbody tr:eq(1) td:eq('+(i+1)+')',tbview).first().append($('<span name="b'+i+'"></span>'));
                        // $('tbody tr:eq(1) td:eq('+(i+1)+')',tbview).first().append(pp.view);
                        let playbtn=$(templete['pair_btn_play']);
                        playbtn.mp3=jsonData[_index].title_mp3[i];
                        $('tbody tr:eq(2) td:eq('+(i+1)+')',tbview).first().append(playbtn);
                        // if(isActive){
                        playbtn.unbind('click').bind('click',(e)=>{
                            play(x_material_path+playbtn.mp3+'.mp3')
                        })
                        // }
                    }
                }else if(jsonData[_index].subtype=='writing_test'){//文章配對(短文填空)
                    let p=$(templete['write_pair']);
                    question_item.append(p);
                    // let options=jsonData[_index].option_title;

                    // console.log(jsonData[_index].submit,'====')
                    // console.log(jsonData[_index].ans,'====')
                    // console.log(jsonData[_index].title,'====')
                    let txt = jsonData[_index].title.replace(/\/n/g, "<br>");
                    // 遍历option_title数组中的所有字符串


                    for (var i = 0; i < jsonData[_index].option_title.length; i++) {
                        // 找到第一个匹配的单词并将其替换为<button>元素\

                        var option_title = jsonData[_index].option_title[i];
                        var pattern = new RegExp("\\b" + option_title + "\\b");
                        var match = pattern.exec(txt);
                        if (match) {
                            var index = match.index;
                            //   title = title.substring(0, index) + '<gg index='+i+'>' + match[0] + '</gg>' + title.substring(index + match[0].length);
                            tag_count++;
                            txt = txt.substring(0, index) + '<span name="b'+i+'" style="display: inline-block"></span>' + txt.substring(index + match[0].length);
                            continue;
                        }
                    }
                    content=$('[name="content"]',p)
                    content.html(txt)
                }
                if(true){
                    $('[name="btn_submit"]',jq_dom).hide();
                    // templete['select-icon-justpair-o']='<span class="select-icon-justpair-o">O</span>'
                    // templete['select-icon-justpair-x']='<span class="select-icon-justpair-x">X</span>'
                    // selectview.append($('<option value="" hidden>請選取...</option>'))
                    // let selectview=$($(templete['option_select']))
                    // let opt_please_sel=$(templete['option_select_option'])
                    // opt_please_sel.text('請選取...')
                    // opt_please_sel.attr('hidden',true)
                    // $('select',selectview).append(opt_please_sel);
                    // $('[name="oo"]',selectview).hide()
                    // $('[name="xx"]',selectview).hide()
                    // templete['option_select']=selectview[0].outerHTML
                    //製作option以及預設的[請選取]項目
                    // for(let i=0;i<options.length;i++){
                    for(let i=0;i<tag_count;i++){
                        // let pp=newItem('target');
                        // pp.showempty()
                        // pp.settitle(options[i])
                        // titary.push(pp);
                        // console.log(pp.title)
                        // $('[name="b'+i+'"]',content).append(pp.view)
                        let _p=$(templete['option_btn'])
                        if(Array.isArray(jsonData[_index].title) && jsonData[_index].title.length>0){
                            //顯示標題，例如「牛肉」，項目則要選牛肉的對應英文
                            $('[name="tit"]',_p).show()
                            $('[name="tit"]',_p).text(jsonData[_index].title[i])
                        }else{
                            $('[name="tit"]',_p).hide()
                        }

                        if(Array.isArray(jsonData[_index].title_mp3) && jsonData[_index].title_mp3.length>0){
                            //顯示播放鈕
                            $('[name="mp3"]',_p).show()
                            $('[name="mp3"]',_p).on('click',(e)=>{
                                play(x_material_path+jsonData[_index].title_mp3[i]+'.mp3');
                            })
                        }else{
                            $('[name="mp3"]',_p).hide()
                        }
                        _p.i=i;
                        _p._val=-1;
                        btn_ary.push(_p)
                        $('[name="b'+i+'"]',content).append(_p);
                        $('[name="option_ox"]',_p).hide()
                        if(isActive){
                            // console.log('@_@')
                            
                            
                            
                            $('[name="title"]',_p).text('請選取…');//請輸入答案
                            
                            $('[name="title"]',_p).off('click').on('click',(e)=>{
                            // _p.off('click').on('click',(e)=>{
                                if(!runing){
                                    return;
                                }
                                let gg=$(templete['option_title_focus'])
                                toggleElements($('[name="title"]',_p),gg,true,true,0);
                                // toggleElements(_p,option_btn_focus_style,true,true,0);
                                showoption_list(_p);
                            })
                        }else{
                            const item = options.find(obj => obj.id === jsonData[_index].submit[i]);
                            if(item){
                                $('[name="title"]',_p).text(item.v);
                            }else{
                                //測試用，沒有這個v資訊，就僅呈現正確結果
                                // $('[name="title"]',_p).text( jsonData[_index].submit[i]);
                            }
                            // let gg=jsonData[_index].option_title
                            // let _p=$(templete['option_btn'])
                            // $('[name="b'+i+'"]',content).append(_p);
                            // console.log(jsonData[_index].submit[i],'=====')
                            // $('[name="title"]',_p).text(options[jsonData[_index].submit[i]].v);
                            // $('[name="b'+i+'"]',content).append('<span style="color:red">'+gg[jsonData[_index].submit[i]]+'</span>');
                            
                        }
                    }
                }
                $("select",content).on('focus', function() {
                    // console.log('按下後出現選單')
                    let ary=[]
                    $('select').each(function() {
                        var value = $(this).val(); // 获取当前选中的 option 的 value
                        // console.log(value); // 打印 value
                        ary.push(parseInt(value));
                    });
                    // console.log(ary);
                    $(this).empty()
                    // let opt_please_sel=$(templete['option_select_option'])
                    // opt_please_sel.text('請選取...')
                    // opt_please_sel.attr('hidden',true)
                    // $(this).append(opt_please_sel);
                    for (let i=0;i<options.length;i++){
                        let op=$(templete['option_select_option'])
                        if(ary.includes(options[i].id)){
                            op.text(options[i].v)
                            op.css('background','gray')
                        }else{
                            op.text(options[i].v)
                        }
                        op.attr('value',options[i].id)
                        $(this).append(op)
                    }

                });
                $("select",content).change(function() {
                    //選了項目之後，欄位的寬度要改變
                    let value=$(this).find('option:selected').val()
                    // console.log('到了',value)
                    $('select').not(this).each(function() {
                        if ($(this).val() === value) {
                            $(this).val("-1");
                        }
                    });
                    var length = $(this).find('option:selected').text().length;
                    $(this).css('width', (length+2) + 'ch');
                    //如果全部都選了，則出現送出按鈕
                    if(true){
                        jsonData[_index].submit=[]
                        var allSet =$('select').children().length>0? true:false;
                        //select一開始是沒有長度的，所以直接判斷為false
                        $('select').each(function() {
                            // console.log($(this).val())
                            if ($(this).val() === "-1") {
                                allSet = false;
                                return;  // 終止.each()迴圈
                            }else{
                                jsonData[_index].submit.push(parseInt( $(this).val()))
                            }
                        });
                        if(allSet){
                            $('[name="btn_submit"]',jq_dom).show();
                            question_item.submit=()=>{
                                // console.log('====')
                                //鎖住互動
                                //呈現對錯
                            // $('[name="btn_submit"]',jq_dom).unbind('click').bind('click',(e)=>{
                                return checkSubmitAndShowOX()
                                // console.log(question_item.showNextAndFeedback)
                                // question_item.showNextAndFeedback()
                            }
                            question_item.again=()=>{
                                //解鎖互動
                                //移除對錯
                                $('select').prop('disabled', false);
                                let ans=jsonData[_index].ans;
                                for(let i=0;i<ans.length;i++){
                                    $("[name='b"+i+"'] [name='oo']",content).hide()
                                    $("[name='b"+i+"'] [name='xx']",content).hide()
                                }
                            };
                        }else{
                            $('[name="btn_submit"]',jq_dom).hide();
                        }
                      
                    }
                }).change();
                // console.log()
                // titary=titary.sort(() => Math.random() - 0.5);
                function showoption_list(sleect_dom){
                    let div=$(templete['pair_option_list']);
                    div.empty();
                    for (let i=0;i<options.length;i++){
                        let op_=$(templete['option_item'])
                        op_.i=i;
                        if(true){
                            for (let ii=0;ii<btn_ary.length;ii++){
                                let p=btn_ary[ii];
                                if(p._val==options[i].id){
                                    let g=$(templete['option_item_using'])
                                    toggleElements(op_,g,true,true,0);
                                    console.log('拿去變色吧')
                                    break;
                                }
                            }
                        }
                        $('[name="title"]',op_).text(options[i].v)
                        div.append(op_)
                        op_.off('click').on('click',(e)=>{
                            //先把之前佔有的吐出來
                            jsonData[_index].submit =  jsonData[_index].submit.map(value => value === options[op_.i].id ? -1 : value);
                            for (let i=0;i<btn_ary.length;i++){
                                let p=btn_ary[i]
                                if(p._val== options[op_.i].id){
                                    p._val=-1;
                                }
                            }
                            jsonData[_index].submit[sleect_dom.i]=options[op_.i].id
                            sleect_dom._val=options[op_.i].id
                            console.log(jsonData[_index].submit)
                            MadleView.Main.close();
                            redrawOptionTitle()
                            // toggleElements(sleect_dom,option_btn_focus_style,true,true,0);
                            
                        })
                    }
                    MadleView.modalview_bottom(div,(e)=>{
                        // console.log('關閉')
                        let gg=$(templete['option_title_focus'])
                        toggleElements($('[name="title"]',sleect_dom),gg,true,true,0);
                                
                        // toggleElements(sleect_dom,option_btn_focus_style,true,true,0);
                    })
                }
                function redrawOptionTitle(){
                    let allcheck=true;
                    for (let i=0;i<btn_ary.length;i++){
                        let p=btn_ary[i];
                        // console.log('======')
                        // console.log(p._val)
                        if(p._val>=0){
                            const item = options.find(obj => obj.id === p._val);
                            $('[name="title"]',p).text(item.v)
                        }else{
                            $('[name="title"]',p).text('請選取…')//請輸入答案
                            allcheck=false;
                        }
                    }
                    if (allcheck){
                        $('[name="btn_submit"]',jq_dom).show();
                    }
                    
                }
                function checkSubmitAndShowOX(){
                    $('[name="btn_submit"]',jq_dom).hide();
                    let submit=jsonData[_index].submit;
                    let ans=jsonData[_index].ans;
                    // console.log(jsonData[_index].title)
                    // console.log(jsonData[_index].option_title)
                    // console.log(submit,ans)
                    let c=true;
                    $('select').prop('disabled', true);
                    let showitemOX=Array.isArray(jsonData[_index].title) && jsonData[_index].title.length>0;//有這個特性的才需要秀出ox
                    for(let i=0;i<ans.length;i++){
                        // titary[i].setFill(optary[submit[i]])
                        // optary[submit[i]].setEmpty()
                        // console.log(ans[i],submit[i])
                        // btn_ary[i].off('click');
                        if(showitemOX){
                            $('[name="option_ox"]',btn_ary[i]).show()
                            $('[name="ans_o"]',btn_ary[i]).hide();
                            $('[name="ans_x"]',btn_ary[i]).hide();
                        }
                        if(ans[i]==submit[i]){
                            $('[name="ans_o"]',btn_ary[i]).show();
                            // $('[name="ans_o"]',btn_ary[i]).show()
                            // $("[name='b"+i+"'] [name='oo']",content).show()
                            // titary[i].showOX('o')
                        }else{
                            $('[name="ans_x"]',btn_ary[i]).show();
                            let g=$(templete['option_title_error']);
                            toggleElements($('[name="title"]',btn_ary[i]),g,true,true,0);

                            // $('[name="ans_x"]',btn_ary[i]).show()
                            // $("[name='b"+i+"'] [name='xx']",content).show()
                            c=false;
                            // titary[i].showOX('x')
                        }
                    }

                    if(c){
                        showOX('O')
                    }else{
                        showOX('X')
                    }
                    // if(_index>=jsonData.length-1){
                    //     $('[name="btn_score"]',jq_dom).show()
                    // }else{
                    //     $("[name='btn_next']",jq_dom).show()
                    // }
                    // question_item.showNextAndFeedback()
                    return c;

                }
                question_item.submit=()=>{
                    runing=false;
                    return checkSubmitAndShowOX()
                }
                question_item.again=()=>{
                    //解鎖互動
                    //移除對錯
                    // $('select').prop('disabled', false);
                    runing=true;
                    // let ans=jsonData[_index].ans;
                    // for (let i=0;i<btn_ary.length;i++){
                    //     $('[name="option_ox"]',btn_ary[i]).hide()
                    //     let g=$(templete['option_btn_error']);
                    //     toggleElements(btn_ary[i],g,true,false,0);

                    // }
                    let submit=jsonData[_index].submit;
                    let ans=jsonData[_index].ans;
                    for(let i=0;i<ans.length;i++){
                        if(ans[i]==submit[i]){
                        }else{
                            // let g=$(templete['option_btn_error']);
                            // toggleElements(btn_ary[i],g,true,true,0);
                            let g=$(templete['option_title_error']);
                            toggleElements($('[name="title"]',btn_ary[i]),g,true,true,0);

                        }
                    } 
                                               // for(let i=0;i<ans.length;i++){
                    //     $("[name='b"+i+"'] [name='oo']",content).hide()
                    //     $("[name='b"+i+"'] [name='xx']",content).hide()
                    // }
                };
                function checkSu____bmitAndShowOX(){
                    $('[name="btn_submit"]',jq_dom).hide();
                    let submit=jsonData[_index].submit;
                    let ans=jsonData[_index].ans;
                    // console.log(jsonData[_index].title)
                    // console.log(jsonData[_index].option_title)
                    // console.log(submit,ans)
                    let c=true;
                    $('select').prop('disabled', true);
                    for(let i=0;i<ans.length;i++){
                        // titary[i].setFill(optary[submit[i]])
                        // optary[submit[i]].setEmpty()
                        // console.log(ans[i],submit[i])
                        

                        if(ans[i]==submit[i]){
                            $("[name='b"+i+"'] [name='oo']",content).show()
                            // titary[i].showOX('o')
                        }else{
                            $("[name='b"+i+"'] [name='xx']",content).show()
                            c=false;
                            // titary[i].showOX('x')
                        }
                    }

                    if(c){
                        showOX('O')
                    }else{
                        showOX('X')
                    }
                    // if(_index>=jsonData.length-1){
                    //     $('[name="btn_score"]',jq_dom).show()
                    // }else{
                    //     $("[name='btn_next']",jq_dom).show()
                    // }
                    // question_item.showNextAndFeedback()
                    return c;

                }
                // }
                // question_item.append(optionview);
                // let optary=[]
                // for(let i=0;i<jsonData[_index].option_title.length;i++){
                //     let pp=newItem('item');
                //     pp.showtarget()
                //     pp.index=i;
                //     pp.settitle(jsonData[_index].option_title[i])
                //     // console.log(jsonData[_index].option_title[i])
                //     optary.push(pp);
                //     if(isActive){
                //         //如果作答狀態，才會有配對項目，否則不要列出配對項目(在)
                //         optionview.append(pp.view);
                //     }
                // }
                // if (true){//混亂DIV排序
                //     var parent = optionview;
                //     var divs = parent.children();
                //     while (divs.length) {
                //         parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
                //     }
                // }



                // question_item.submit=()=>{
                //     //pair
                //     $('[name="btn_submit"]',jq_dom).hide();
                //     btnio=false;
                //     let g=[];
                //     for(let i=0;i<titary.length;i++){
                //         g.push(titary[i].fillitem.index)
                //     }
                //     jsonData[_index].submit=g;
                //     checkSubmitAndShowOX()
                //     // console.log('999===',question_item.showNextAndFeedback)
                //     question_item.showNextAndFeedback()
                //     //不論對錯都是出現回饋
                //     // $('[name="feedback_view"]',jq_dom).show();
                //     // if(_index>=jsonData.length-1){
                //     //     $('[name="btn_score"]',jq_dom).show()
                //     // }else{
                //     //     $("[name='btn_next']",jq_dom).show()
                //     // }
                // };
                // $('[name="btn_submit"]',jq_dom).unbind('click').bind('click',(e)=>{
                //     $('[name="btn_submit"]',jq_dom).hide();
                //     btnio=false;
                //     let g=[];
                //     for(let i=0;i<titary.length;i++){
                //         g.push(titary[i].fillitem.index)
                //     }
                //     jsonData[_index].submit=g;
                //     checkSubmitAndShowOX()
                //     $('[name="feedback_view"]',jq_dom).show();
                //     if(_index>=jsonData.length-1){
                //         $('[name="btn_score"]',jq_dom).show()
                //     }else{
                //         $("[name='btn_next']",jq_dom).show()
                //     }

                // })
                // console.log(JSON.stringify(jsonData[_index]))
                // jsonData[_index].submit=[0,1,2,3,4,5]
                if (!isActive) {
                    //非互動狀態，列出正確答案英文全文與中文全文
                    checkSubmitAndShowOX()
                    let gg=$('<br><br><div><p>正解</p><br><br>'+jsonData[_index].feedback+'</div>')
                    question_item.append(gg)
                }
                // $('[name="btn_submit"]',jq_dom).hide();
                return question_item;
                // function checkSubmitAndShowOX(){
                    
                //     let submit=jsonData[_index].submit;
                //     let ans=jsonData[_index].ans;
                //     // console.log(jsonData[_index].title)
                //     // console.log(jsonData[_index].option_title)
                //     // console.log(submit,ans)
                //     let c=true;
                //     for(let i=0;i<ans.length;i++){
                //         titary[i].setFill(optary[submit[i]])
                //         optary[submit[i]].setEmpty()
                //         // console.log(ans[i],submit[i])
                //         if(ans[i]==submit[i]){
                //             titary[i].showOX('o')
                //         }else{
                //             c=false;
                //             titary[i].showOX('x')
                //         }
                //     }

                // }

            }
            // pair結束
            // if(jsonData[_index].type=='input'){
            //     $(templete["input_view"])
            // }




            // console.log( 'iiii',$('[name="feedback_view"] [name="feedback_content"]',question_item))
            ma.setFeedback($('<div>'+jsonData[_index].feedback+'</div>'));
            // ma.showFeedback()
            // $('[name="feedback_view"] [name="feedback_content"]',jq_dom).html(jsonData[_index].feedback)
    
            // $('[name="question_container"]',jq_dom).empty();
            // $('[name="question_container"]',jq_dom).append(question_item);
            // $('[name="ansox"]', question_item).empty();

            // if(jsonData[_index].title==''){
            //     $('[name="title"]',question_item).hide();
            // }else{
                
                // if(jsonData[_index].type=='fillin'){
                //     // var newText = jsonData[_index].title.replace( jsonData[_index].option_title[0], templete["input_view"]); 
                //     // $('[name="title"]',question_item).first().append(newText);
                //     let tt= $('[name="input"]',question_item).first();
                //     // $($('input',question_item)).first();
                //     if(!isActive){
                //         // console.log(jsonData[_index].submit,'sss')
                //         // console.log(tt);
                //         // (jsonData[_index].submit)
                //         tt.prop('disabled', true);
                //         tt.val(jsonData[_index].submit);
                //         // tt.attr('value',jsonData[_index].submit);
                //         if($('[name="input_view"] input',jq_dom).first().val()==jsonData[_index].option_title[0]){
                //             showOX('O')
                //         }else{
                //             showOX('X')
                //         }
                //         // submit(null)
                //     }else{
                //         tt.off('keyup').on('keyup', (e)=>{
                //             let p=tt;
                //             if(e.keyCode == 13){
                //                 // console.log('這裡啦')
                //                 callsubmit()
                //                 // question_item.submit();
                //                 // submit(e)
                //             }else{
                //                 if($('[name="input_view"] input',question_item).first().val().length > 0){
                //                 // if(p.val().length > 0){
                //                     console.log('有長度')
                //                     $('[name="btn_submit"]',jq_dom).show();
                //                 }else{
                //                     $('[name="btn_submit"]',jq_dom).hide();
                //                 }
                //             }
                //         })
                //     }
                // }
                // else{
                //     $('[name="title"]',question_item).first().text(jsonData[_index].title)
                // }
            // }
            if(jsonData[_index].title_mp3==''){
                $('[name="mp3"]',question_item).hide();
            }else{
                $('[name="mp3"]',question_item).off('click').on('click',(e)=>{
                    play(x_material_path+jsonData[_index].title_mp3+'.mp3')
                })
                //自動撥放
                if(isActive){
                    play(x_material_path+jsonData[_index].title_mp3+'.mp3')
                }
            }
            
            // console.log('88888',jsonData[_index].feedback)

 
            if(jsonData[_index].type!='fillin'){//填空題不需要選項  
                redrawAllItem(_index, isActive);
            }

            return question_item;
    
            function redrawAllItem(_index, isActive) {
                
    
                let option_title_ary = jsonData[_index].option_title;
                let option_pic_ary = jsonData[_index].option_pic;
                let option_mp3_ary = jsonData[_index].option_mp3;
                let ans = jsonData[_index].ans;

                //陣列中比較長度，最長的長度保留下來 
                const longestLength = [option_title_ary, option_pic_ary, option_mp3_ary].reduce((acc, curr) => {
                    if (curr.length > acc) {
                        return curr.length;
                    } else {
                        return acc;
                    }
                }, 0);
    
              
                
    
                // 答案選項呈現
                if( jsonData[_index].type=='input'){
                    // $('[name="tips_view"]',jq_dom).text(jsonData[_index].tiptext)
                    // $('[name="tips_view"]',jq_dom).hide()
                    // $('[name="input_view"] input',jq_dom).first().val(jsonData[_index].submit)
                    // if (!isActive) {
                    //     $('[name="input_view"] input',jq_dom).prop('disabled', true);
                    // }
                    // $('[name="input_view"] input',jq_dom).unbind('keyup').bind('keyup', (e)=>{
                    //     if(e.keyCode == 13){
                    //         submit(e)
                    //     }else{
                    //         if($('[name="input_view"] input',jq_dom).val().length > 0){
                    //             $('[name="btn_submit"]',jq_dom).show();
                    //         }else{
                    //             $('[name="btn_submit"]',jq_dom).hide();
                    //         }
                    //     }
                    // });
                }else{
                    for (let i = 0; i < longestLength; i++) {
                        let p = $(templete['option']);
                        $('[name="option_ox"]',p).hide();
                        $('[name="option_container"]', question_item).append(p);
                        p.i = i;
                        if (option_title_ary.length == 0) {
                            $('[name="title"]', p).hide();
                        }else{
                            $('[name="title"]', p).text(option_title_ary[i]);
                        }
        
                        if (option_pic_ary.length == 0) {
                            $('[name="image_title"]', p).hide();
                        }else{
                            $('[name="image_title"] img', p).first().attr("src", x_material_path+option_pic_ary[i]);
                        }
        
                        if (option_mp3_ary.length == 0) {
                            $('[name="voice_title"]', p).hide();
                        }else{
                            $('[name=voice_title]', p).click(function () {
                                play(x_material_path+option_mp3_ary[i]+'.mp3');
                            });
                        }
        
                        itemary.push(p);
                        //判斷單複選擇題
                        if (isActive) {
                            p.click((e) => {
                                if (!btnIsOpen) {
                                    return;
                                }
                                if (ans.length > 1) {
                                    //複選題
                                    p.selected = !p.selected;
                                    toggleElements(p, option_selectStyle, true, true);
                                    question_item.isSelected=true;
                                    callbackevent&&callbackevent('selected',question_item);
                                    // $('[name="btn_submit"]',jq_dom).show();
                                    if (p.selected) {
                                        toggleElements(p, option_selected_style, false, true);
                                    } else {
                                        toggleElements(p, option_selected_style, false, false);
                                    }
                                } else {
                                    //單選題
                                    toggleback();
                                    p.selected = true;
                                    toggleElements(p, option_selectStyle, false, true);
                                    question_item.isSelected=true
                                    callbackevent&&callbackevent('selected',question_item);
                                    // $('[name="btn_submit"]',jq_dom).show();
                                    if (jsonData[_index].submit.length > 0) {
                                        if (jsonData[_index].submit[0] != p.i) {
                                            let old_p = itemary[jsonData[_index].submit[0]]
                                            toggleElements(old_p, option_selected_style, false, false);
                                            jsonData[_index].submit[0] = p.i
                                            toggleElements(p, option_selected_style, false, true);
        
                                        }
                                    }
                                }
        
                            });
                        } else {
                            checkItemStatus(_index, p);
                        }
        
        
                    }
                }
    
                if (!isActive) {
                    if (AnsIsEqual(jsonData[_index].ans, jsonData[_index].submit)) {
                        showOX('O')
                    } else {
                        showOX('X')
                    }
                }
                return question_item;  

    
    
            }

            function showOX(OX='O'){//題目的整體對錯呈現，目前美術只要選項對錯呈現，故此項目先停用
                return;
                $('[name="ansox"]', question_item).empty();
                let ox;
                if (OX=='O') {
                    ox=$(templete["ans_o"])
                } else {
                    ox=$(templete["ans_x"])
                }
                // $('[name="ansox"]', question_item).css({background:'red',width:'100px'})
                // console.log('0000',ox[0].outerHTML,$('[name="ansox"]', question_item))
                // console.log($('[name="ansox"]', question_item).length)
                $('[name="ansox"]', question_item).append(ox);
            }
            function checkItemStatus(_index, p) {//單一選項呈現正確與錯誤項目的提示
                $('[name="option_ox"]',p).show();
                $('[name="option_ox"] [name="ans_o"]',p).hide();        
                $('[name="option_ox"] [name="ans_x"]',p).hide();
                if (jsonData[_index].submit.includes(p.i)) {
                    toggleElements(p, option_selected_style, false, true);
                    if (jsonData[_index].ans.includes(p.i)) {
                        $('[name="option_ox"] [name="ans_o"]',p).show();
                        toggleElements(p, option_selectBingo, false, true);
                    } else {
                        $('[name="option_ox"] [name="ans_x"]',p).show();
                        toggleElements(p, option_selectWrong, false, true);
                    }
                } else {
                    if (jsonData[_index].ans.includes(p.i)) {
                        $('[name="option_ox"] [name="ans_o"]',p).show();
                        toggleElements(p, option_selectBingo, false, true);
                    }
                }


            }
            //未被選取選項的樣式
            function toggleback() {
                for (let i = 0; i < itemary.length; i++) {
                    let p = itemary[i];
                    p.selected = false;
                    toggleElements(p, option_selectStyle, false, false);
                }
            }
    
        }
        function allViewHide(){
            $('.title').hide()
            $('font[name="num_no"]').parent('span').hide();
            $('[name="btn_allreset"]',jq_dom).hide()
            $('[name="btn_submit"]',jq_dom).hide()
            $('[name="btn_again"]',jq_dom).hide()
            $('[name="btn_next"]',jq_dom).hide()
            $('[name="btn_score"]',jq_dom).hide()
            $('[name="btn_wrong"]',jq_dom).hide()
            $('[name="btn_record"]',jq_dom).hide()
            // $('[name="feedback_view"]',jq_dom).hide();
            $('[name="answer_record_view"]',jq_dom).hide();
            $('[name="score_view"]',jq_dom).hide();
            ma.hideFeedback();
        }
        function showScore____(){
            //以群組來進行判讀，如果群組內容為1，可以理解為每題就是算一個對錯
            //如果>1，就代表是題組題。例如10題，每三題一組。最後一組只有一題。
            //題組狀態下判斷對錯的方式，在同一題組內，例如0,1,2這三題是同一組，其中一個錯，就是全錯。
            //必須全對，才能從錯題中移除，移除也是一次移除三題。
            let len=Math.floor(jsonData/_group);

            for (let i = 0; i < jsonData.length; i++) {
                let p = jsonData[i]
                // console.log(p.type,p.ans, p.submit)
                if(p.type=='pair'){
                    let submit=p.submit;
                    let ans=p.ans;
                    let c=true;
                    for(let ii=0;ii<ans.length;ii++){
                        if(ans[ii]!=submit[ii]){
                            c=false
                            break;
                        }
                    }
                    if(c){
                        bingo++;
                    }else{
                        wrong++;
                    }
                }else{
                    if (AnsIsEqual(p.ans, p.submit)) {
                        bingo++;
                    } else {
                        wrong++;
                    }
                }
            }
        }
        function showScore(){
            MaxMediaPlayer.AllStop();
            allViewHide()
            $('.title').show()
            $('.title [name="title_info"]').hide()
            bingo=0;
            wrong=0;
            // $('[name="feedback_view"]',jq_dom).hide();
            $('[name="question_container"]',jq_dom).hide();
            // $('[name="title"]',jq_dom).hide();

            $('[name="btn_allreset"]',jq_dom).show();
            $('[name="btn_record"]',jq_dom).show()
            $('[name="btn_score"]',jq_dom).hide();
            let total = jsonData.length;  //總題數
            $('[name="btn_wrong"]',jq_dom).hide();
            if(false){
                for (let i = 0; i < jsonData.length; i++) {
                    let p = jsonData[i]
                    // console.log(p.type,p.ans, p.submit)
                    if(p.type=='pair'){
                        let submit=p.submit;
                        let ans=p.ans;
                        let c=true;
                        for(let ii=0;ii<ans.length;ii++){
                            if(ans[ii]!=submit[ii]){
                                c=false
                                break;
                            }
                        }
                        if(c){
                            bingo++;
                        }else{
                            wrong++;
                        }
                    }else{
                        if (AnsIsEqual(p.ans, p.submit)) {
                            bingo++;
                        } else {
                            wrong++;
                        }
                    }
                }
            }
            let o=getScore(jsonData);
            score = o.score;
            // Math.floor(100 / jsonData.length * bingo); //計算分數
            let score_view= $('[name="score_view"]',jq_dom);
            score_view.show();
            $('[name="num"]',score_view).text(o.all);
            $('[name="bingo"]',score_view).text(o.oo);
            $('[name="wrong"]',score_view).text(o.xx);
            // $('[name="num"]',score_view).text(total);
            // $('[name="bingo"]',score_view).text(bingo);
            // $('[name="wrong"]',score_view).text(wrong);
            // $('[name="result"]',score_view).text(score);
            if (score < 100) {
                $('[name="btn_wrong"]',jq_dom).show();
            }
            if(removeWrong){
                $('[name="btn_wrong"]',jq_dom).hide();
            }
            // console.log('消失了吧!!!',removeWrong)
            let a=new PieChart(score);
            let pv=$('[name="PieChart"]',score_view)
            $(pv).empty()
            $(pv).append(a.view);
            ////data={score:50,right:5,wrong:5}
            EndCallback&&EndCallback({score:score,right:bingo,wrong:wrong})
            StatusChangeCallback&&StatusChangeCallback('呈現成績')
            
        }
    
        //播放音樂mp3函式
        function play(mp3) {
            audio.src = mp3;
            audio.play();
        }
    
        //全部題目做完重置，重新作答
        function reset() {
            index_now = 0;
            jsonData=Quiz.convertDataToExamJson(_data,_word,_type)
            // jsonData=JSON.parse(resetJsonData);
            $('[name="question_container"]',jq_dom).show();
            StatusChangeCallback&&StatusChangeCallback('重新做答')
            if(AllresetCallback){
                allViewHide()
                AllresetCallback&&AllresetCallback(()=>{
                    goNext();
                })
            }else{
                goNext();
            }
            
        }
    
    }
}

class PieChart{
    constructor(score,progressColor='#7752D8',totalColor='#ccc',bgColor='#eee'){
        let self=this;
        self.view=$($('[idname="PieChart"]',module_html)[0].outerHTML);
        perCirc($('#sellPerCirc',self.view), score);
        function perCirc($el, end, i) {
            if (end < 0)
                end = 0;
            else if (end > 100)
                end = 100;
            if (typeof i === 'undefined')
                i = 0;
            var curr = (100 * i) / 360;
            $el.find(".perCircStat").html(Math.round(curr));
            if (i <= 180) {
                $el.css('background-image', 'linear-gradient(' + (90 + i) + 'deg, transparent 50%, var(--linegray) 50%),linear-gradient(90deg, var(--linegray) 50%, transparent 50%)');
            } else {
                $el.css('background-image', 'linear-gradient(' + (i - 90) + 'deg, transparent 50%, var(--linecolor) 50%),linear-gradient(90deg, var(--linegray) 50%, transparent 50%)');
            }
            if (curr < end) {
                setTimeout(function () {
                    perCirc($el, end, ++i);
                }, 1);
            }else{
                // changeClass("is-hide", "is-show", document.querySelector(".end"));
            }
        }
    }
}
class MadleView{//彈跳視窗，分成中間與底部兩種。
    static Main;
    static modalview_middle=(jq_dom,closeCallback)=>{
        let self=new MadleView('modalview_middle',jq_dom,closeCallback);
        return self;
    }
    static modalview_bottom=(jq_dom,closeCallback)=>{
        let self=new MadleView('modalview_bottom',jq_dom,closeCallback);
        return self;
    }
    constructor(type='modalview_middle',jq_dom,closeCallback){
        let self=this;
        MadleView.Main=self;
        self.view=$($('[idname="'+type+'"]',module_html)[0].outerHTML);
        $('[name="content"]',self.view).empty()
        $('[name="content"]',self.view).append(jq_dom)
        $('body').append(self.view);
        self.view.fadeIn(300);
        $(".close",self.view).click(function () {
            self.close()
        })
        $(window).on('click',(e)=>{
            if ($(e.target).is( self.view)) {
                $(window).off('click',close);
                self.close()
            }
        })
        self.close=()=>{
            self.view.fadeOut(300);
            closeCallback&&closeCallback()
            MadleView.Main=null;
        }

    }
}