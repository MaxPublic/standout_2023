/**2023/2/10 Max */

// let templete = {}
window.onload = function () {
    // data = reduce(data)
    // word = reduce(word)
    toolsinit((ma)=>{
        // return;
        ma.hideborder()
        ma.setNodename('會話學習','點擊播放鈕或開啟會話內容進行學習')
        // $(".avatar_1 img").attr('src',"../material/Carla.jpg")
        // $(".avatar_2 img").attr('src',"../material/Fred.jpg")

        let lesson_id = data.DocumentElement.Dialogue.lesson_id;
        let dt = data.DocumentElement.Dialogue.dailogue;
        let namelist = [];
        for (let i = 0; i < dt.length; i++) {//檢查有多少人名
            if (!namelist.includes(dt[i].name)) {
                namelist.push(dt[i].name);
            }
        }
        console.log(namelist.length,'參與人數')
        if(namelist.length==1){
            // $(".avatar_"+dt[1].serial+" img").attr('src',x_material_path+dt[1].pic)
            $(".avatar_2").hide();
            $('[name="role2"]').hide()
        }else{
            $(".avatar_2 img").attr('src',x_material_path+dt[1].pic)
            $('[name="role2"]').text(dt[1].name)
        }
        console.log(lesson_id, dt);
        $(".avatar_1 img").attr('src',x_material_path+dt[0].pic)

        $('[name="role1"]').text(dt[0].name)

        let box1 = $('[name="box1"]')
        let box2style = $($('[name="box2"]')[0].outerHTML)
        $('[name="allother"]').remove();
        let index = 0;
        // $('[name="role1"]').text(dt[index].dialogue_e.split(':')[0])
            
        
        let ary = [];

        for (let i = 0; i < dt.length; i++) {
            ary.push(x_material_path + dt[i].dialogue_e_snd + '.mp3')
        }
        let a=new mp3playback(ary)
        // $('body').empty()
        $('playback').append(a.view);
        a.addEventChangeIndex((newIndex) => {
            newfocus(newIndex)
        })
// return ;

        // let mediaplayer = new MaxMediaPlayer(ary, $("[name='btn_play']"), $("[name='progressBar']"), () => {
        //     console.log('完成')
        // },$('[name="btn_pause"]'),$('[name="btn_loop"]'),$('[name="btn_speed"]'))

        // mediaplayer.addEventChangeIndex((newIndex) => {
        //     newfocus(newIndex)
        // })


        $('[name="btn_toggleChinese"]').unbind('click').bind('click',(e) => {
            if (box1.is(':hidden')) {
                $(e.currentTarget).text('關閉會話內容')
                box1.show()
            } else {
                $(e.currentTarget).text('開啟會話內容')
                box1.hide();
            }
        })

        $('[name="container"]').empty()
        function newfocus(newIndex) {
            index = newIndex;
            if(index>dt.length-1){
                return;
            }
            if(namelist.length==1){
                $('[name="texteng"]').text(dt[index].dialogue_e)
                $("[data-content-inside] .dialogue-arrow-int ").css('left','50%');
                $("[data-content-inside] .dialogue-arrow").css('left','50%');
            }else{
                $('[name="texteng"]').text(dt[index].dialogue_e.split(':').slice(1).join(':'))
                if (index % 2 == 1) {
                    toggleElements(box1, box2style, false, true);
                } else {
                    toggleElements(box1, box2style, false, false);
                }
            }
            
            // $(".avatar_"+dt[index].serial+" img").attr('src',x_material_path+dt[index].pic)
            


        }
    });

}