// Max
window.onload = function () {
    toolsinit((ma)=>{
        ma.hideborder()
        // ma.showLearntime();
        ma.setNodename('閱讀訓練','針對內容進行閱讀，建議閱讀時間截止後，選擇進行讀後答題，或是再閱讀')
        // let mainview_=$("[data-content-inside]").first()[0].outerHTML;
        // let mainview;
        ma.goFirstContent();
        let m = data.DocumentElement.Read;
        let timenumber= parseInt(m.time.match(/\d+/)[0], 10);
        let newMQ1 = m.prereading_q.split('/n')[0];
        let newMC1 = m.prereading_c.split('/n')[0];
        let newMQ2 = m.prereading_q.split('/n')[1];
        let newMC2 = m.prereading_c.split('/n')[1];
        let ep = m.reading_ep;
        
        let newRE = m.reading_e.replace(/<\/?u>/g, '').replace(/\/n\/n/g, "<br><br>");
        let readtimeup_html=$('[data-content-inside] [name="readtimeup"]')[0].outerHTML;
        $('[data-content-inside] [name="readtimeup"]').remove();
        let textview_html=$('[data-content-inside] [name="txt"]')[0].outerHTML;
        let readimg_html=$("[data-content-inside] [name='readimg']")[0].outerHTML;
        $('[data-content-inside] [name="txt"]').remove();
        let btn_readAgain_html=$('[data-content-inside] [name="btn_readagain"]')[0].outerHTML;

        // console.log($('[data-content-inside] [name="btn_readagain"]')[0].outerHTML)
        $('[data-content-inside] [name="btn_readagain"]').remove();
        SpeedTest(()=>{
            timenumber=timenumber/(timenumber*60)*3
            //閱讀時間縮短到只剩三秒
        })
        go()
        function go(){
            // mainview=$(mainview_);
            // main_layout.ma.appendContent(mainview);
            ma.setNodename('閱讀訓練','針對內容進行閱讀，建議閱讀時間截止後，選擇進行讀後答題，或是再閱讀')
            ma.goFirstContent();

            let textview=$(textview_html)
            
            $('[data-content-inside] .pic_m img,[data-content-inside] .pic img').attr('src',x_material_path+data.DocumentElement.Read.prereading_p);
            //"../material/11621en2_prereading.jpg"

            

            $('[data-content-inside] [name="readTime"]').text('00:00');
            if(ep=='0'){
                $('[data-content-inside] [name="sentence"]').html(newRE);
                textview.html(newRE);
            }else{
                let _img=$(readimg_html);
                $('img',_img).attr('src',x_material_path+ep);
                console.log(_img[0].outerHTML,'======')
                $('[data-content-inside] [name="sentence"]').html(_img[0].outerHTML);
                textview.html(_img[0].outerHTML);
            }
            $('[data-content-inside] [name="timing"]').text(timenumber+'分鐘');
            $('[data-content-inside] [name="eng1"]').html(newMQ1);
            $('[data-content-inside] [name="cn1"]').html(newMC1);
            $('[data-content-inside] [name="eng2"]').html(newMQ2);
            $('[data-content-inside] [name="cn2"]').html(newMC2);
    
            $('[data-content-inside] [name="firstview"]').first().show()
            $('[data-content-inside] [name="readandstart"]').first().hide()
            $('[data-content-inside] [name="modalview"]').first().hide()

            $('[data-content-inside] [name="btn_start2"]').off('click').on('click',()=> {
                $('[data-content-inside] [name="firstview"]').first().hide();
                $('[data-content-inside] [name="readandstart"]').first().show();
                let count = 0;
                countdown = setInterval(() => {
                    count++;
                    let minutes = Math.floor(count / 60);
                    let seconds = count % 60;
                    minutes = (minutes < 10 ? '0' : '') + minutes;
                    seconds = (seconds < 10 ? '0' : '') + seconds;
                    let time = minutes + ':' + seconds;
                    $('[data-content-inside] [name="readTime"]').text(time);
                    if(count>=timenumber*60){//閱讀時間到
                    // if (minutes >= timenumber) {
                        clearInterval(countdown);
                        let g=$(readtimeup_html);
                        $('[name="btn_readagain_"]',g).off('click').on('click',(e)=>{
                            MadleView.Main.close();
                            go()
                        })
                        $('[name="btn_readTest_"]',g).off('click').on('click',(e)=>{
                            MadleView.Main.close();
                            readTest()
                        })
                        MadleView.modalview_middle(g,()=>{
                            // $('[data-content-inside] [name="firstview"]').first().show();
                            // $('[data-content-inside] [name="readandstart"]').first().hide();

                            
                        })
                    }
                }, 1000);
                // }
                $('[data-content-inside] [name="btn_readTest"]').first().off('click').on('click',()=> {
                    readTest()
                })
                function readTest(){
                    $('[data-content-inside] [name="readandstart"]').first().hide();
                    clearInterval(countdown);

                    let QuizObj = new Quiz(data, word, {}, 'reading_comprehension')
                    ma.setNodename('讀後答題','依照題意在作答區選擇正確答案，按下確定鈕送出，作答前可點擊再次閱讀鈕回顧內容')
                    QuizObj.addGroupUpItem(newbtn_readAgain())
                    QuizObj.addEventStatusChange((status = 'testing') => {
                        //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
                        if(status=='測驗進行中'){
                            QuizObj.addGroupUpItem(newbtn_readAgain())
                        }else if (status=='呈現成績'){
                            ma.setNodename('讀後答題','')
                            // btn_readAgain.remove();
                        }else{
                            // ma.setNodename('','');
                        }
                    })
                    QuizObj.addEventEnd((data) => {
                        //data={score:50,right:5,wrong:5}
                        console.log(data)
                    })
                    QuizObj.addResetCallback((againcall)=>{
                        console.log('全部重來')
                        go()
                        againcall()
                    })
                    function newbtn_readAgain(){
                        let btn_readAgain=$(btn_readAgain_html);
                        btn_readAgain.off('click').on('click',(e)=>{
                            console.log('啊')
                            let g=$(textview);
                            MadleView.modalview_middle(g);
                        })
                        return btn_readAgain;
                    }
                }
            });
        }
  
    });

};