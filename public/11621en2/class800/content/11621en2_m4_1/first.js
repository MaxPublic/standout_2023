/**2023/5/30 Max*/

window.onload = function () {
    toolsinit((ma)=>{
        // return;
        ma.hideborder()
        ma.setNodename('文法表格','瀏覽以下文法表格，複習文法句型解析中的概念')
        // console.log(JSON.stringify(data))/**數據最佳化 */
        let filename=data.DocumentElement.Info.course_id+'_grammar.html'
        //'./11544en2_grammar.html'
        function extractBodyContent(html) {
            var bodyTagStart = "<body>";
            var bodyTagEnd = "</body>";
            var bodyStartIndex = html.indexOf(bodyTagStart);
            var bodyEndIndex = html.indexOf(bodyTagEnd);
        
            if (bodyStartIndex === -1 || bodyEndIndex === -1) {
                return ""; // 無法找到完整的<body>...</body>，直接返回空字符串
            }
        
            var bodyContent = html.slice(bodyStartIndex + bodyTagStart.length, bodyEndIndex);
        
            // 移除所有的<script>標籤
            bodyContent = bodyContent.replace(/<script[\s\S]*?<\/script>/gi, '');
        
            // 將<body>和</body>替換為<div>和</div>
            bodyContent = "<div>" + bodyContent + "</div>";
        
            return bodyContent;
        }
        
        // $.get('../material/'+filename, function(data) {
        //     var bodyContent = extractBodyContent(data);
        //     $('body').append($(bodyContent))
        //     console.log(bodyContent);
            
        // });
        //表格是重新放置在特殊路徑下的，並不在舊版路徑上。這邊要思考要怎麼換掉路徑。
        // x_material_path
        $.get(x_grammar_path+filename, function(data) {
            var bodyContent = extractBodyContent(data);
            $('.grammar').append($(bodyContent))
            // console.log(bodyContent);
            
        });
    });
    
}