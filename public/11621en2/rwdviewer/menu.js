﻿//v2.0.0 20191003
var level = 0; //樹層
var vid = 0;
var HTML = "";
var levelspace = "";
var linknodes = 0;
var dirArray = new Array(); //記錄目錄節點
var fileArray = new Array(); //記錄檔案節點
var nodeRecord = new Array(); //記錄所有節點
var nodeNum = 0; //節點數目
var dirNum = 0; //目錄數目
var fileNum = 0; //檔案數目
var recordNowNode = ""; //記錄目前所待節點
function getSubject() {
    var xmlDoc;
    var strFile = "imsmanifest.xml";
    if (window.ActiveXObject) {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM"); // Support IE 
        xmlDoc.async = false; // 關閉異步加載 
        xmlDoc.load(strFile);
    } else {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", strFile, false);
        xhr.send(null);
        var parseXml = new DOMParser();
        xmlDoc = parseXml.parseFromString(xhr.responseText, "text/xml"); //將文字轉為xml         
    }
    var nodes = xmlDoc.documentElement;
    itemnodes = firefoxReChildnodes(nodes.getElementsByTagName("organization")[0].childNodes);
    linknodes = firefoxReChildnodes(nodes.getElementsByTagName("resources")[0].childNodes);
    var div_title = document.createElement("div");
    div_title.setAttribute("class", "title");
    div_title.innerHTML = itemnodes[0].firstChild.nodeValue;
    document.getElementById("menu").appendChild(div_title);
    //HTML += itemnodes[0].firstChild.nodeValue + "<p>";
    //frames[0].document.getElementById("title2").firstChild.nodeValue=itemnodes[0].firstChild.nodeValue;
    //alert(HTML);
    for (var i = 0; i < itemnodes.length; i++) { //問題
        levelspace = "";
        if (itemnodes[i].nodeName == "item") {
            vid++;
            nodeRecord[i - 1] = vid;
            readTree(itemnodes[i]);
            nodeNum2 = 0;
            nodeNum = nodeNum + 1;
        }
    }

    function readTree(nownode) {
        //nodeRecord[nodeNum][nodeNum2]=vid;
        //alert(nodeRecord.length);
        var templink = "";
        var tempid = "";
        var media_link = "";
        var nodes = firefoxReChildnodes(nownode.childNodes);
        //document.write(nownode.getAttribute("identifier"));
        //var menuHTML = "";
        //var menuHTML = levelspace;
        if (nodeRecord[nodeNum] !== vid) {
            nodeRecord[nodeNum] = nodeRecord[nodeNum] + "&" + vid;
            nodeRecord[nodeNum] = nodeRecord[nodeNum] + "&" + vid;
        }
        for (var i = 0; i < linknodes.length; i++) {
            if (nownode.getAttribute("identifierref") !== null) {
                if (nownode.getAttribute("identifierref") === linknodes[i].getAttribute("identifier")) {                    
                    if(linknodes[i].getElementsByTagName("media").length > 0) {
                        media_link=linknodes[i].getElementsByTagName("media")[0].getAttribute("href");
                    } else media_link="";
                    templink = linknodes[i].getAttribute("href");
                    tempid = nownode.getAttribute("identifier");
                }
            }
        }
        var nav = document.createElement("nav");
        var ul = document.createElement("ul");
        var li_icon = document.createElement("li");
        li_icon.setAttribute("class", "icon");
        var a = document.createElement("a");
        a.setAttribute("id", "read" + vid);
        a.setAttribute("target", "blank");
        if (levelspace !== "") {
            nav.setAttribute("class", "child");
            //menuHTML += '<nav class="child"><ul><li>';
        } else {
            //menuHTML += '<nav><ul><li>';
        }
        var lock = true;
        if (templink === "") {
            if (nodes.length === 1) {
                templink = "javascript: void(0)";
                a.setAttribute("class", "lock");
                var li_lock = document.createElement("li");
                var img_lock = document.createElement("img");
                img_lock.setAttribute("src", "rwdviewer/lock_2.png");
                li_lock.appendChild(img_lock);
                ul.appendChild(li_lock);
            } else {
                for (var i = 0; i < nodes.length; i++) {
                    if (nodes[i].nodeName == "item") {
                        if (nodes[i].getAttributeNode("identifierref")) {
                            lock = false;
                            break;
                        }
                    }
                }
                if (lock == true) {
                    a.setAttribute("class", "lesson");
                    var li_lock = document.createElement("li");
                    var img_lock = document.createElement("img");
                    img_lock.setAttribute("src", "rwdviewer/lock_1.png");
                    li_lock.appendChild(img_lock);
                    ul.appendChild(li_lock);
                } else if (lock == false) {
                    templink = "javascript: void(0)";
                    a.setAttribute("href", templink);
                    a.setAttribute("onclick", "divshow('" + vid + "')");
                    a.setAttribute("class", "lesson");
                    var li_book = document.createElement("li");
                    var img_book = document.createElement("img");
                    img_book.setAttribute("src", "rwdviewer/book.png");
                    li_book.appendChild(img_book);
                    ul.appendChild(li_book);
                }
            }
        } else {
            a.setAttribute("class", "course");
            if (nownode.getElementsByTagName("title")[0].firstChild.nodeValue.indexOf('〈未開放〉') !== -1) {
                a.setAttribute("class", "course demo");
            } else {
                a.setAttribute("class", "course");
            }
            a.setAttribute("href", templink + "?read" + vid);
            a.setAttribute("onclick", "divshow('" + vid + "','read" + vid + "');");
            //閱讀狀態
            var li_read = document.createElement("li");
            var img_read = document.createElement("img");
            img_read.setAttribute("src", "rwdviewer/unread.png");
            li_read.appendChild(img_read);
            ul.appendChild(li_read);
        }
        //節點名稱
        var li_node = document.createElement("li");
        li_node.setAttribute("class", "node");
        li_node.innerHTML = nownode.getElementsByTagName("title")[0].firstChild.nodeValue;
        ul.appendChild(li_node);
        //箭頭
        if (a.className === "lesson") {
            var div_arrow = document.createElement("div");
            div_arrow.setAttribute("class", "arrow right");
            li_icon.appendChild(div_arrow);
            ul.appendChild(li_icon);
        }
        nav.appendChild(ul);
        a.appendChild(nav);
        HTML += a.outerHTML;
        if (nodes.length === 1) {
            fileArray[fileNum] = "read" + vid;
            fileNum = fileNum + 1;
        } else {
            dirArray[dirNum] = "read" + vid;
            dirNum = dirNum + 1;
        }
        if (a.className === "lesson") {
            HTML += "<div width='100%' id='div" + vid + "' style='display:none;'>\n";
            //templink="";
            for (var i = 0; i < nodes.length; i++) {
                level++;
                var tempImg = 0;
                for (var j = 0; j < level; j++) {
                    tempImg = tempImg + 20;
                }
                levelspace = tempImg;
                if (nodes[i].nodeName == "item") {
                    vid++;
                    if (lock == false) {
                        readTree(nodes[i], true);
                    }
                }
                level--;
            }
            HTML += "</div>\n";
        }
    }
    delete(xmlDoc);
    containerinit();
}

function menuinit() {
    document.cookie = "menuEnd=1";
    //document.getElementById("box").className = "box-show";
    //document.getElementById("tip").className = "tip-hide";
    //document.getElementById("box").setAttribute("onClick", "menuHide();");
    //document.getElementById("hamburger").setAttribute("onClick", "menuToggle();");
}

function containerinit() {
    var div_container = document.createElement("div");
    div_container.setAttribute("class", "container");
    div_container.innerHTML += HTML;
    document.getElementById("menu").appendChild(div_container);
    var div_bottom = document.createElement("div");
    div_bottom.setAttribute("class", "bottom");
    document.getElementById("menu").appendChild(div_bottom);
    disableSelection(document.body);
    var t = "no-touch";
    if (isTouchDevice()) {
        t = "touch";
    }
    document.getElementsByTagName("div")[0].className = t;
    if (readCookie("menuEnd") == "1") {
        menuinit();
    } else {
        //document.getElementById("tip").className = "tip-show";
    }
}

function divshow(vid, read) {
    if (document.getElementById("div" + vid) && document.getElementById("div" + vid).style.display === "none") {
        opendiv(vid);
    } else {
        closediv(vid);
    }
    if (typeof read !== 'undefined') {
        if (recordNowNode) {
            $('#' + recordNowNode).removeClass("active");
        }
        var cookieValue = readCookie("endNode");
        sss = cookieValue.split("&");
        if (sss[1] === undefined) {
            writeCookie('endNode', read);
            var cookieValue = readCookie("endNode");
            sss = cookieValue.split("&");
            recordNowNode = sss[1];
        } else {
            recordNowNode = read;
            document.cookie = "endNode=";
            writeCookie('endNode', read);
        }
        if (recordNowNode) {
            $('#' + recordNowNode).addClass("active");
        }
        goCookies(read);
        if (getWH()[0] < 768) {
            menuHide();
        }
    }
}

function goCookies(read) {
    writeCookie('hasRead', read);
    document.cookie = "nowNode=";
    writeCookie('nowNode', read);
    checkCookie('hasRead');
    checkCookie('readEnd');
}

function opendiv(vid) {
    if (document.getElementById("div" + vid)) {
        document.getElementById("div" + vid).style.display = "";
        for (var i = 0; i < dirArray.length; i++) {
            //alert(read+"  "+dirArray[i]);
            if ("read" + vid === dirArray[i]) {
                profile = document.getElementById(dirArray[i]);
                profile.scrollIntoView();
                changeClass("right", "down", profile.getElementsByTagName("li")[2].getElementsByTagName("div")[0]);
            }
        }
    }
}

function closediv(vid) {
    if (document.getElementById("div" + vid)) {
        document.getElementById("div" + vid).style.display = "none";
        for (var i = 0; i < dirArray.length; i++) {
            if ("read" + vid === dirArray[i]) {
                profile = document.getElementById(dirArray[i]);
                changeClass("down", "right", profile.getElementsByTagName("li")[2].getElementsByTagName("div")[0]);
            }
        }
    }
}

function writeCookie(name, ID) {
    //var date = new Date(); //獲取當前時間
    //var expireDays = 30 * 24 * 3600 * 1000; //將date設置為n天以後的時間
    //date.setTime(date.getTime() + expireDays);
    var test = false;
    var values = "&" + ID;
    var cookieValue = readCookie(name);
    //var the_date = new Date("December 31, 2020");
    var now = new Date();
    now.setTime(now.getTime() + 1000 * 60 * 60 * 24);
    sss = cookieValue.split("&")
    for (var i = 0; i < sss.length; i++) {
        if (sss[i] === ID) {
            test = true;
            break;
        }
    }
    if (test) {
        //document.cookie = name + "=" + cookieValue;
        document.cookie = name + "=" + cookieValue + ";expires=" + now.toGMTString();
        test = false;
    } else {
        //document.cookie = name + "=" + cookieValue + values ; expires=date.toGMTString();
        document.cookie = name + "=" + cookieValue + values + ";expires=" + now.toGMTString();
    }
    //document.cookie = name + "=;" + escape(psd) + "; expire=" + date.toGMTString();
    //alert("設置成功!時間為：" + expireDays + "秒（" + 10 + "天）"); 
}

function readCookie(name) {
    var cookieValue = "";
    var search = name + "=";
    if (document.cookie.length > 0) {
        offset = document.cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = document.cookie.indexOf(";", offset);
            if (end == -1) end = document.cookie.length;
            cookieValue = document.cookie.substring(offset, end)
        }
    }
    return cookieValue;
}
var str0 = "";

function checkCookie(name) {
    var cookieValue = readCookie(name);
    sss = cookieValue.split("&");
    if (name !== "readEnd") {
        for (var i = 1; i < sss.length; i++) {
            if (sss[i].indexOf("_") === -1) {
                profile = document.getElementById(sss[i]);
                if (profile) {
                    incompleted(profile.getElementsByTagName("li")[0]);
                }
            }
        }
    } else {
        for (var i = 1; i < sss.length; i++) {
            profile = document.getElementById(sss[i]);
            if (profile) {
                completed(profile.getElementsByTagName("li")[0]);
            }
        }

        function checkSelfNode(selfNode) { //檢查有多少子節點
            if (selfNode.nextSibling) {
                if (selfNode.nodeName === "A") {
                    str0 += selfNode.getAttributeNode("id").nodeValue + "&";
                }
                checkSelfNode(selfNode.nextSibling);
            }
        }
    }
}

function incompleted(node) {
    node.getElementsByTagName('img')[0].src = "rwdviewer/incompleted.png";
}

function completed(node) {
    node.getElementsByTagName('img')[0].src = "rwdviewer/completed.png";
}

function firefoxReChildnodes(nodes) {
    var g2 = 0;
    var itemnodes_renew = new Array();
    for (var i = 0; i < nodes.length; i++) { //重新寫入陣列，因firefox的childnodes計算的length會不對
        if (nodes[i].nodeType != 3 && nodes[i].nodeType != 8) {
            itemnodes_renew[g2] = nodes[i];
            g2++;
        }
    }
    return itemnodes_renew;
}

function loadInfo() {
    var tmpArr, QueryString;
    var URL = document.location.toString();
    if (URL.lastIndexOf("?") != -1) {
        QueryString = URL.substring(URL.lastIndexOf("?") + 1, URL.length);
        tmpArr = QueryString.split("&");
        for (var i = 0; i <= tmpArr.length; i++) {
            try {
                eval(tmpArr[i]);
            } catch (e) {
                var re = new RegExp("(.*)=(.*)", "ig");
                re.exec(tmpArr[i]);
                try {
                    eval(RegExp.$1 + "=" + "\"" + RegExp.$2 + "\"");
                } catch (e) {}
            }
        }
    } else {
        var nodeBack = new Array();
        var temp = "";
        var cookieValue = readCookie("nowNode");
        var jumpNode;
        if (cookieValue) {
            sss = cookieValue.split("&");
            jumpNode = sss[1];
        } else {
            loop: for (var i = 0; i < nodeRecord.length; i = i + 1) {
                if (String(nodeRecord[i]).indexOf("&") === -1) {
                    jumpNode = 'read' + nodeRecord[i];
                    break loop;
                } else {
                    var nodes = nodeRecord[i].split("&");
                    for (var j = 0; j < nodes.length; j = j + 1) {
                        var className = $('#read' + nodes[j]).attr('class');
                        if (className.indexOf('lesson') == -1) {
                            jumpNode = 'read' + nodes[j];
                            console.log(nodes[j])
                            break loop;
                        }
                    }
                }
            }
            function getNodeName(node) {}
        }
        for (var i = 0; i < nodeRecord.length; i = i + 1) {
            if (String(nodeRecord[i]).indexOf("&") === -1) {
                if (String(nodeRecord[i]) === jumpNode.replace("read", "")) {
                    divshow(String(nodeRecord[i]), "read" + String(nodeRecord[i]));
                    goSrc(jumpNode);
                }
            } else {
                var nodes = nodeRecord[i].split("&");
                for (var j = 0; j < nodes.length; j = j + 1) {
                    nodeBack[j] = nodes[j];
                    if (nodes[j] === jumpNode.replace("read", "")) {
                        for (var k = 0; k < nodeBack.length; k = k + 1) {
                            //alert(nodeBack[k]);
                            divshow(nodeBack[k], jumpNode);
                            goSrc(jumpNode);
                            return;
                        }
                    }
                }
            }
        }
        QueryString = "";
    }
    if (QueryString !== "") {
        writeCookie('hasRead', QueryString + "_");
    }
    //checkCookie('hasRead'); //檢查已閱讀的節點
    //checkCookie('readEnd'); //檢查已閱讀完畢的節點
}

function goSrc(jumpNode) {
    var loc = document.getElementById(jumpNode).getAttributeNode("href").nodeValue.replace("../", "");
    document.getElementById("blank").src = loc;
}

function changeState(totalTime, scoTime, nowPage) { //接收右框架傳回來的值，時間大於設定的總時間便節點閱讀完畢
    //alert(totalTime+"   "+scoTime);
    //alert(URL.substring(URL.lastIndexOf("?")+1,URL.length));
    if (totalTime > scoTime && scoTime !== -1) {
        writeCookie('readEnd', nowPage);
        //alert("閱讀完畢");
        checkCookie('readEnd');
    }
}

function nodeComplete(nowPage) { //接收右框架傳回來的值，節點閱讀完畢
    //alert(totalTime+"   "+scoTime);
    //alert(URL.substring(URL.lastIndexOf("?")+1,URL.length));
    writeCookie('readEnd', nowPage);
    //alert("閱讀完畢");
    checkCookie('readEnd');
}

function clears() {
    document.cookie = "hasRead=";
    document.cookie = "readEnd=";
    document.cookie = "readRead=";
    document.cookie = "nowNode=";
    document.cookie = "endNode=";
    document.cookie = "menuEnd=";
    //window.location.href='cookie_show.asp';
} //清除COOKIE
function opencookies() {
    alert(document.cookie);
    //window.location.href='cookie_show.asp';
} //清除COOKIE
function changeClass(oldname, newname, obj) {
    if (obj !== null) {
        obj.className = obj.className.replace(oldname, newname);
    }
}

function disableSelection(target) { //無法選取反白
    //For IE This code will work
    if (typeof target.onselectstart != "undefined") target.onselectstart = function() {
        return false
    }
    //For Firefox This code will work
    else if (typeof target.style.MozUserSelect != "undefined") target.style.MozUserSelect = "none"
    //All other  (ie: Opera) This code will work
    else target.onmousedown = function() {
        return false
    }
    target.style.cursor = "default"
}

function isTouchDevice() {
    return !!('ontouchstart' in window) || (!!('onmsgesturechange' in window) && !!window.navigator.maxTouchPoints);
}