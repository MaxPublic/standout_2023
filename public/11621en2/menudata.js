var memuXml=`<manifest xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2" xmlns:imsmd="http://www.imsglobal.org/xsd/imsmd_rootv1p2p1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2" identifier="manifest_11621en2" xsi:schemaLocation="http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd">
  <organizations default="org_11621en2">
    <organization identifier="org_11621en2" structure="hierarchical">
    <title><![CDATA[Unit 7 Lesson 4 Job interviews]]></title>
    <item identifier="item_11621en2_m1" isvisible="true" parameters="?time=60"><title><![CDATA[課程資訊]]></title><item identifier="item_11621en2_m1_0" isvisible="true" identifierref="res_11621en2_m1_0" parameters="?time=60"><title><![CDATA[學習資訊]]></title></item></item><item identifier="item_11621en2_m2" isvisible="true" parameters="?time=60"><title><![CDATA[單字學習]]></title><item identifier="item_11621en2_m2_0" isvisible="true" identifierref="res_11621en2_m2_0" parameters="?time=60"><title><![CDATA[單字瀏覽]]></title></item><item identifier="item_11621en2_m2_1" isvisible="true" identifierref="res_11621en2_m2_1" parameters="?time=60"><title><![CDATA[記憶訓練]]></title></item><item identifier="item_11621en2_m2_2" isvisible="true" identifierref="res_11621en2_m2_2" parameters="?time=60"><title><![CDATA[聽音辨字]]></title></item><item identifier="item_11621en2_m2_3" isvisible="true" identifierref="res_11621en2_m2_3" parameters="?time=60"><title><![CDATA[單字配對]]></title></item></item><item identifier="item_11621en2_m3" isvisible="true" parameters="?time=60"><title><![CDATA[會話學習]]></title><item identifier="item_11621en2_m3_0" isvisible="true" identifierref="res_11621en2_m3_0" parameters="?time=60"><title><![CDATA[會話學習]]></title></item><item identifier="item_11621en2_m3_1" isvisible="true" identifierref="res_11621en2_m3_1" parameters="?time=60"><title><![CDATA[會話理解]]></title></item></item><item identifier="item_11621en2_m4" isvisible="true" parameters="?time=60"><title><![CDATA[文法學習]]></title><item identifier="item_11621en2_m4_0" isvisible="true" identifierref="res_11621en2_m4_0" parameters="?time=60"><title><![CDATA[文法句型解析]]></title></item><item identifier="item_11621en2_m4_1" isvisible="true" identifierref="res_11621en2_m4_1" parameters="?time=60"><title><![CDATA[文法表格]]></title></item><item identifier="item_11621en2_m4_2" isvisible="true" identifierref="res_11621en2_m4_2" parameters="?time=60"><title><![CDATA[短句重組]]></title></item><item identifier="item_11621en2_m4_3" isvisible="true" identifierref="res_11621en2_m4_3" parameters="?time=60"><title><![CDATA[文法克漏字]]></title></item></item><item identifier="item_11621en2_m5" isvisible="true" parameters="?time=60"><title><![CDATA[聽力訓練]]></title><item identifier="item_11621en2_m5_0" isvisible="true" identifierref="res_11621en2_m5_0" parameters="?time=60"><title><![CDATA[聽力訓練]]></title></item><item identifier="item_11621en2_m5_1" isvisible="true" identifierref="res_11621en2_m5_1" parameters="?time=60"><title><![CDATA[聽力理解]]></title></item></item><item identifier="item_11621en2_m6" isvisible="true" parameters="?time=60"><title><![CDATA[閱讀訓練]]></title><item identifier="item_11621en2_m6_0" isvisible="true" identifierref="res_11621en2_m6_0" parameters="?time=60"><title><![CDATA[閱讀訓練]]></title></item><item identifier="item_11621en2_m6_1" isvisible="true" identifierref="res_11621en2_m6_1" parameters="?time=60"><title><![CDATA[全文對照]]></title></item></item><item identifier="item_11621en2_m7" isvisible="true" parameters="?time=60"><title><![CDATA[課後挑戰]]></title><item identifier="item_11621en2_m7_0" isvisible="true" identifierref="res_11621en2_m7_0" parameters="?time=60"><title><![CDATA[單字克漏字]]></title></item></item><item identifier="item_11621en2_m8" isvisible="true" parameters="?time=60"><title><![CDATA[重點整理]]></title><item identifier="item_11621en2_m8_0" isvisible="true" identifierref="res_11621en2_m8_0" parameters="?time=60"><title><![CDATA[單字列表]]></title></item><item identifier="item_11621en2_m8_1" isvisible="true" identifierref="res_11621en2_m8_1" parameters="?time=60"><title><![CDATA[會話文法摘要]]></title></item><item identifier="item_11621en2_m8_2" isvisible="true" identifierref="res_11621en2_m8_2" parameters="?time=60"><title><![CDATA[講義下載]]></title></item></item></organization>
    </organizations>
  <resources><resource identifier="res_11621en2_m1_0" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m1_0/index_sco.htm">
         <file href="class800/content/11621en2_m1_0/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m2_0" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m2_0/index_sco.htm">
         <file href="class800/content/11621en2_m2_0/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m2_1" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m2_1/index_sco.htm">
         <file href="class800/content/11621en2_m2_1/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m2_2" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m2_2/index_sco.htm">
         <file href="class800/content/11621en2_m2_2/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m2_3" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m2_3/index_sco.htm">
         <file href="class800/content/11621en2_m2_3/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m3_0" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m3_0/index_sco.htm">
         <file href="class800/content/11621en2_m3_0/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m3_1" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m3_1/index_sco.htm">
         <file href="class800/content/11621en2_m3_1/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m4_0" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m4_0/index_sco.htm">
         <file href="class800/content/11621en2_m4_0/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m4_1" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m4_1/index_sco.htm">
         <file href="class800/content/11621en2_m4_1/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m4_2" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m4_2/index_sco.htm">
         <file href="class800/content/11621en2_m4_2/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m4_3" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m4_3/index_sco.htm">
         <file href="class800/content/11621en2_m4_3/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m5_0" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m5_0/index_sco.htm">
         <file href="class800/content/11621en2_m5_0/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m5_1" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m5_1/index_sco.htm">
         <file href="class800/content/11621en2_m5_1/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m6_0" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m6_0/index_sco.htm">
         <file href="class800/content/11621en2_m6_0/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m6_1" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m6_1/index_sco.htm">
         <file href="class800/content/11621en2_m6_1/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m7_0" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m7_0/index_sco.htm">
         <file href="class800/content/11621en2_m7_0/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m8_0" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m8_0/index_sco.htm">
         <file href="class800/content/11621en2_m8_0/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m8_1" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m8_1/index_sco.htm">
         <file href="class800/content/11621en2_m8_1/index_sco.htm"/>
        </resource>
        <resource identifier="res_11621en2_m8_2" type="webcontent" adlcp:scormtype="sco" href="class800/content/11621en2_m8_2/index_sco.htm">
         <file href="class800/content/11621en2_m8_2/index_sco.htm"/>
        </resource>
        </resources>
</manifest>
`