﻿$(document).ready(function() {
	validity();
});

function validity(){
    if(!document.querySelector('script[src="main/validity.js"]')){
         _importJS("main/validity.js");
    }
}
function _importJS(url){
    var html_doc = document.getElementsByTagName('head')[0];
    var js = document.createElement('script');
    js.setAttribute('type', 'text/javascript'); 
    js.setAttribute('src', url); 
    html_doc.appendChild(js);  
}