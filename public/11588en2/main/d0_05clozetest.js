﻿/**短文填空 */


let templete = {}

window.onload = function () {
    data = reduce(data)
    //  console.log(JSON.stringify(data))
    //  return;
    word = reduce(word)

    // if(true){
    //     $('[name="vocabulary_test"]').hide()
    //     $('[name="cloze_test"]').hide()
    //     $('[name="listening_test"]').hide()
    //     $('[name="cloze_vocabulary"]').hide()
    //     $('[name="listening_test_sentence"]').hide()
    //     $('[name="writing_test"]').hide()
    //     //說明文字
    //     $('[name="vocabulary_test_title"]').hide()
    //     $('[name="cloze_test_title"]').hide()
    //     $('[name="listening_test_title"]').hide()
    //     $('[name="cloze_vocabulary_title"]').hide()
    //     $('[name="listening_test_sentence_title"]').hide()
    //     $('[name="writing_test_title"]').hide()
    //     for(let i=0;i<_i_info.length;i++){
    //         //判斷目前需要的按鈕
    //         $(`[name="${_i_info[i]}"]`).show()
    //     }
    // }
    // let _i_info=['vocabulary_test','cloze_test','listening_test']//會有這個變數存在
    let all = $('[name="all"]')[0].outerHTML
    start()
    function start() {
        $('body').empty()
        $('body').append($(all))
        let QuizUi = $('[name="Quiz"]')
        $('[name="QuizContainer"]').empty();

        //     var checkboxes = $('[name="firstpage_view"] [name="exam"]')
        //     var types = $('input[type=checkbox]:checked').map(function() {
        //        return $(this).val();
        //      }).get();
        //    if(types.length<=0){
        //        return;
        //    }
        $('[name="firstpage_view"]').hide()
        let QuizObj = new Quiz(data, word, QuizUi, 'cloze_vocabulary', 0)
        QuizObj.addEventStatusChange((status = 'testing', index, data) => {
            //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
            console.log(status)
            if (status == '測驗進行中') {
                console.log(index, data.subtype);
                showtitle()
            }
            function showtitle() {
                $('[name="vocabulary_test_title"]').hide()
                $('[name="cloze_test_title"]').hide()
                $('[name="listening_test_title"]').hide()
                $('[name="cloze_vocabulary_title"]').hide()
                $('[name="listening_test_sentence_title"]').hide()
                $('[name="writing_test_title"]').hide()
                if (data.subtype) {
                    $('[name="' + data.subtype + '_title"]').show()
                }
            }
        })
        QuizObj.addEventEnd((data) => {
            //data={score:50,right:5,wrong:5}
            console.log(data)
        })
        QuizObj.addResetCallback((goNext) => {
            //已經完全結束，如果需要重新下一題，就呼叫這個函數
            //或自行定義其他行為。
            console.log('全部結束，重新開始')
            start();
            //    $('body').empty()
            //    $('body').append($(all))
            //    QuizUi=$('[name="Quiz"]')
            //    $('[name="QuizContainer"]').empty();
            //    $('[name="firstpage_view"]').show()
            // goNext()
        })
        $('[name="QuizContainer"]').append(QuizObj.view);
        
        $('[name="btn_quizStart"]').click((e) => {
        })

    }

}