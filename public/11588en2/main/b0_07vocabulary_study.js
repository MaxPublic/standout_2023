/**
 * 讀後答題
 */
 let templete = {}

 window.onload = function () {
     data = reduce(data)
    //  console.log(JSON.stringify(data))
    //  return;
    word=reduce(word)
    let audio = new Audio();
    templete["item"]=$('[name="item"]')[0].outerHTML
    let container=$('[name="container"]')
    container.empty()
    let vv=data.DocumentElement.Vocabulary
    let prop = ["vocab", "meaning_1", "kk", "part_of_speech_1", "vocab_e_snd", "part_of_speech_1", "meaning_1", "eg_1", "eg_meaning_1", "part_of_speech_2", "meaning_2", "eg_2", "eg_meaning_2"]
    for(let i=0;i<vv.length;i++){
        let p=$(templete["item"])
        $('[name="title"] [name="no"]',p).text(i+1)
        container.append(p)
        p.data=vv[i]
        p.unbind('click').bind('click',(e)=>{
            play('../material/'+p.data.vocab_e_snd+'.mp3')
        })
        for (let j=0;j<prop.length;j++){
            let c=$('[name="descript"] [name="'+prop[j]+'"]',p)
            if(c.length>=1){
                console.log(p.data['vocab'])
                c.text(p.data[prop[j]])
            }
        }

    }
    function play(mp3){
        audio.src = mp3;
        audio.play();
    }

    // Tony 20230714
    $(window).resize(function(){
        let width= $(window).width();
        console.log(width);

        if(width<768){
            $('[name="point"]').text('....................................................................');
        }else{
            $('[name="point"]').text('.................................................................................................................................................................');
        }
    });
 }