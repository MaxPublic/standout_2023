﻿var data={
    "DocumentElement": {
        "Info": {
            "course_id": {
                "#cdata-section": "11561en2"
            },
            "serial": {
                "#cdata-section": "StandOut"
            },
            "book": {
                "#cdata-section": "Book 1"
            },
            "unit_no": {
                "#cdata-section": "Unit 3"
            },
            "lesson_no": {
                "#cdata-section": "5"
            },
            "course_name": {
                "#cdata-section": "Buying lunch"
            },
            "introduction": {
                "#cdata-section": "相信每個人都有到外面用餐的經驗！那你知道該怎麼用美語說出要點什麼東西嗎?本課要教你用美語來點餐，讓你不用再和服務生比手劃腳卻點不到想吃的東西！文法解析裡則介紹常用的Yes/No問句，請大家好好學習喔！"
            },
            "learning_objectives_e": {
                "#cdata-section": "1. Learn to place orders/n2. Learn to answer Yes/No questions"
            },
            "learning_objectives_c": {
                "#cdata-section": "學會點餐/n學習如何回答Yes/No 問句"
            },
            "suggested_time": {
                "#cdata-section": "60分鐘"
            }
        },
        "Vocabulary": [
            {
                "order": {
                    "#cdata-section": "4"
                },
                "vocabulary_id": {
                    "#cdata-section": "580"
                },
                "lesson_id": {
                    "#cdata-section": "11561en2"
                },
                "vocab": {
                    "#cdata-section": "cheeseburger"
                },
                "kk": {
                    "#cdata-section": "[ˋtʃiz͵bɝgɚ] "
                },
                "part_of_speech_1": {
                    "#cdata-section": "n. 名詞"
                },
                "meaning_1": {
                    "#cdata-section": "起士漢堡"
                },
                "eg_1": {
                    "#cdata-section": "I want a cheeseburger."
                },
                "eg_meaning_1": {
                    "#cdata-section": "我要1個起士漢堡。"
                },
                "part_of_speech_2": {
                    "#cdata-section": "0"
                },
                "meaning_2": {
                    "#cdata-section": "0"
                },
                "eg_2": {
                    "#cdata-section": "0"
                },
                "eg_meaning_2": {
                    "#cdata-section": "0"
                },
                "syn": {
                    "#cdata-section": "0"
                },
                "ant": {
                    "#cdata-section": "0"
                },
                "n_forms": {
                    "#cdata-section": "cheeseburgers"
                },
                "v_forms": {
                    "#cdata-section": "0"
                },
                "adj_forms_adv_forms": {
                    "#cdata-section": "0"
                },
                "word_usage": {
                    "#cdata-section": "veggie burger 素食漢堡"
                },
                "word_test_e_ques": {
                    "#cdata-section": "cheeseburger"
                },
                "word_test_e_item_1": {
                    "#cdata-section": "起士漢堡"
                },
                "word_test_e_item_1e": {
                    "#cdata-section": "cheeseburger"
                },
                "word_test_e_item_2": {
                    "#cdata-section": "漢堡"
                },
                "word_test_e_item_2e": {
                    "#cdata-section": "hamburger"
                },
                "word_test_e_item_3": {
                    "#cdata-section": "熱狗"
                },
                "word_test_e_item_3e": {
                    "#cdata-section": "hot dog"
                },
                "word_test_e_ans": {
                    "#cdata-section": "起士漢堡"
                },
                "word_test_c_ques": {
                    "#cdata-section": "起士漢堡"
                },
                "word_test_c_item_1": {
                    "#cdata-section": "hamburger"
                },
                "word_test_c_item_1c": {
                    "#cdata-section": "漢堡"
                },
                "word_test_c_item_2": {
                    "#cdata-section": "sandwich"
                },
                "word_test_c_item_2c": {
                    "#cdata-section": "三明治"
                },
                "word_test_c_item_3": {
                    "#cdata-section": "cheeseburger"
                },
                "word_test_c_item_3c": {
                    "#cdata-section": "起士漢堡"
                },
                "word_test_c_ans": {
                    "#cdata-section": "cheeseburger"
                },
                "cloze_test_ques": {
                    "#cdata-section": "I want a ___."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "cheesburger"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "chisburger"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "cheeseburger"
                },
                "cloze_test_ans": {
                    "#cdata-section": "cheeseburger"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "I want a <u>cheeseburger</u>."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "我要1個起士漢堡。"
                },
                "vocab_e_snd": {
                    "#cdata-section": "vocab_580_e"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "vocab_c_snd": {
                    "#cdata-section": "0"
                }
            },
            {
                "order": {
                    "#cdata-section": "3"
                },
                "vocabulary_id": {
                    "#cdata-section": "581"
                },
                "lesson_id": {
                    "#cdata-section": "11561en2"
                },
                "vocab": {
                    "#cdata-section": "hot dog"
                },
                "kk": {
                    "#cdata-section": "[hɑt] [dɔg] "
                },
                "part_of_speech_1": {
                    "#cdata-section": "n. phr. 名詞片語"
                },
                "meaning_1": {
                    "#cdata-section": "熱狗"
                },
                "eg_1": {
                    "#cdata-section": "I don't want to have hot dogs for dinner."
                },
                "eg_meaning_1": {
                    "#cdata-section": "我不想吃熱狗當晚餐。"
                },
                "part_of_speech_2": {
                    "#cdata-section": "0"
                },
                "meaning_2": {
                    "#cdata-section": "0"
                },
                "eg_2": {
                    "#cdata-section": "0"
                },
                "eg_meaning_2": {
                    "#cdata-section": "0"
                },
                "syn": {
                    "#cdata-section": "0"
                },
                "ant": {
                    "#cdata-section": "0"
                },
                "n_forms": {
                    "#cdata-section": "hot dogs"
                },
                "v_forms": {
                    "#cdata-section": "0"
                },
                "adj_forms_adv_forms": {
                    "#cdata-section": "0"
                },
                "word_usage": {
                    "#cdata-section": "hot pot 火鍋"
                },
                "word_test_e_ques": {
                    "#cdata-section": "hot dog"
                },
                "word_test_e_item_1": {
                    "#cdata-section": "起士漢堡"
                },
                "word_test_e_item_1e": {
                    "#cdata-section": "cheeseburger"
                },
                "word_test_e_item_2": {
                    "#cdata-section": "熱狗"
                },
                "word_test_e_item_2e": {
                    "#cdata-section": "hot dog"
                },
                "word_test_e_item_3": {
                    "#cdata-section": "火腿"
                },
                "word_test_e_item_3e": {
                    "#cdata-section": "ham"
                },
                "word_test_e_ans": {
                    "#cdata-section": "熱狗"
                },
                "word_test_c_ques": {
                    "#cdata-section": "熱狗"
                },
                "word_test_c_item_1": {
                    "#cdata-section": "ham"
                },
                "word_test_c_item_1c": {
                    "#cdata-section": "火腿"
                },
                "word_test_c_item_2": {
                    "#cdata-section": "hot dog"
                },
                "word_test_c_item_2c": {
                    "#cdata-section": "熱狗"
                },
                "word_test_c_item_3": {
                    "#cdata-section": "cheeseburger"
                },
                "word_test_c_item_3c": {
                    "#cdata-section": "起士漢堡"
                },
                "word_test_c_ans": {
                    "#cdata-section": "hot dog"
                },
                "cloze_test_ques": {
                    "#cdata-section": "I don't want to have ___ for dinner."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "hat dogs"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "hot dogs"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "hut dogs"
                },
                "cloze_test_ans": {
                    "#cdata-section": "hot dogs"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "I don't want to have <u>hot dogs</u> for dinner."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "我不想吃熱狗當晚餐。"
                },
                "vocab_e_snd": {
                    "#cdata-section": "vocab_581_e"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "vocab_c_snd": {
                    "#cdata-section": "0"
                }
            },
            {
                "order": {
                    "#cdata-section": "6"
                },
                "vocabulary_id": {
                    "#cdata-section": "582"
                },
                "lesson_id": {
                    "#cdata-section": "11561en2"
                },
                "vocab": {
                    "#cdata-section": "salad"
                },
                "kk": {
                    "#cdata-section": "[ˋsæləd] "
                },
                "part_of_speech_1": {
                    "#cdata-section": "n. 名詞"
                },
                "meaning_1": {
                    "#cdata-section": "沙拉"
                },
                "eg_1": {
                    "#cdata-section": "Jasmine always has a bowl of fruit salad every morning."
                },
                "eg_meaning_1": {
                    "#cdata-section": "潔思敏每天早上總是會吃1碗水果沙拉。"
                },
                "part_of_speech_2": {
                    "#cdata-section": "0"
                },
                "meaning_2": {
                    "#cdata-section": "0"
                },
                "eg_2": {
                    "#cdata-section": "0"
                },
                "eg_meaning_2": {
                    "#cdata-section": "0"
                },
                "syn": {
                    "#cdata-section": "0"
                },
                "ant": {
                    "#cdata-section": "0"
                },
                "n_forms": {
                    "#cdata-section": "salads"
                },
                "v_forms": {
                    "#cdata-section": "0"
                },
                "adj_forms_adv_forms": {
                    "#cdata-section": "0"
                },
                "word_usage": {
                    "#cdata-section": "salad bowl 沙拉碗"
                },
                "word_test_e_ques": {
                    "#cdata-section": "salad"
                },
                "word_test_e_item_1": {
                    "#cdata-section": "沙拉"
                },
                "word_test_e_item_1e": {
                    "#cdata-section": "salad"
                },
                "word_test_e_item_2": {
                    "#cdata-section": "水果"
                },
                "word_test_e_item_2e": {
                    "#cdata-section": "fruit"
                },
                "word_test_e_item_3": {
                    "#cdata-section": "飲料"
                },
                "word_test_e_item_3e": {
                    "#cdata-section": "beverage"
                },
                "word_test_e_ans": {
                    "#cdata-section": "沙拉"
                },
                "word_test_c_ques": {
                    "#cdata-section": "沙拉"
                },
                "word_test_c_item_1": {
                    "#cdata-section": "salad"
                },
                "word_test_c_item_1c": {
                    "#cdata-section": "沙拉"
                },
                "word_test_c_item_2": {
                    "#cdata-section": "side order"
                },
                "word_test_c_item_2c": {
                    "#cdata-section": "附餐"
                },
                "word_test_c_item_3": {
                    "#cdata-section": "beverage"
                },
                "word_test_c_item_3c": {
                    "#cdata-section": "飲料"
                },
                "word_test_c_ans": {
                    "#cdata-section": "salad"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Jasmine always has a bowl of fruit ___ every morning."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "saled"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "salary"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "salad"
                },
                "cloze_test_ans": {
                    "#cdata-section": "salad"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Jasmine always has a bowl of fruit <u>salad</u> every morning."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "潔思敏每天早上總是會吃1碗水果沙拉。"
                },
                "vocab_e_snd": {
                    "#cdata-section": "vocab_582_e"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "vocab_c_snd": {
                    "#cdata-section": "0"
                }
            },
            {
                "order": {
                    "#cdata-section": "2"
                },
                "vocabulary_id": {
                    "#cdata-section": "583"
                },
                "lesson_id": {
                    "#cdata-section": "11561en2"
                },
                "vocab": {
                    "#cdata-section": "side order"
                },
                "kk": {
                    "#cdata-section": "[saɪd]  [ˋɔrdɚ] "
                },
                "part_of_speech_1": {
                    "#cdata-section": "n. phr. 名詞片語"
                },
                "meaning_1": {
                    "#cdata-section": "附餐"
                },
                "eg_1": {
                    "#cdata-section": "What would you like for side order?"
                },
                "eg_meaning_1": {
                    "#cdata-section": "你附餐要點什麼呢？"
                },
                "part_of_speech_2": {
                    "#cdata-section": "0"
                },
                "meaning_2": {
                    "#cdata-section": "0"
                },
                "eg_2": {
                    "#cdata-section": "0"
                },
                "eg_meaning_2": {
                    "#cdata-section": "0"
                },
                "syn": {
                    "#cdata-section": "side dish"
                },
                "ant": {
                    "#cdata-section": "main course"
                },
                "n_forms": {
                    "#cdata-section": "side orders"
                },
                "v_forms": {
                    "#cdata-section": "0"
                },
                "adj_forms_adv_forms": {
                    "#cdata-section": "0"
                },
                "word_usage": {
                    "#cdata-section": "appetizer 開胃菜"
                },
                "word_test_e_ques": {
                    "#cdata-section": "side order"
                },
                "word_test_e_item_1": {
                    "#cdata-section": "沙拉"
                },
                "word_test_e_item_1e": {
                    "#cdata-section": "salad"
                },
                "word_test_e_item_2": {
                    "#cdata-section": "飲料"
                },
                "word_test_e_item_2e": {
                    "#cdata-section": "beverage"
                },
                "word_test_e_item_3": {
                    "#cdata-section": "附餐"
                },
                "word_test_e_item_3e": {
                    "#cdata-section": "side order"
                },
                "word_test_e_ans": {
                    "#cdata-section": "附餐"
                },
                "word_test_c_ques": {
                    "#cdata-section": "附餐"
                },
                "word_test_c_item_1": {
                    "#cdata-section": "beverage"
                },
                "word_test_c_item_1c": {
                    "#cdata-section": "飲料"
                },
                "word_test_c_item_2": {
                    "#cdata-section": "salad"
                },
                "word_test_c_item_2c": {
                    "#cdata-section": "沙拉"
                },
                "word_test_c_item_3": {
                    "#cdata-section": "side order"
                },
                "word_test_c_item_3c": {
                    "#cdata-section": "附餐"
                },
                "word_test_c_ans": {
                    "#cdata-section": "side order"
                },
                "cloze_test_ques": {
                    "#cdata-section": "What would you like for ___?"
                },
                "cloze_test_item_1": {
                    "#cdata-section": "saide order"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "side oder"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "side order"
                },
                "cloze_test_ans": {
                    "#cdata-section": "side order"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "What would you like for <u>side order</u>?"
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "你附餐要點什麼呢？"
                },
                "vocab_e_snd": {
                    "#cdata-section": "vocab_583_e"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "vocab_c_snd": {
                    "#cdata-section": "0"
                }
            },
            {
                "order": {
                    "#cdata-section": "7"
                },
                "vocabulary_id": {
                    "#cdata-section": "584"
                },
                "lesson_id": {
                    "#cdata-section": "11561en2"
                },
                "vocab": {
                    "#cdata-section": "ounce"
                },
                "kk": {
                    "#cdata-section": "[aʊns]"
                },
                "part_of_speech_1": {
                    "#cdata-section": "n. 名詞"
                },
                "meaning_1": {
                    "#cdata-section": "盎司"
                },
                "eg_1": {
                    "#cdata-section": "One pound equals sixteen ounces."
                },
                "eg_meaning_1": {
                    "#cdata-section": "1磅等於16盎司。"
                },
                "part_of_speech_2": {
                    "#cdata-section": "0"
                },
                "meaning_2": {
                    "#cdata-section": "0"
                },
                "eg_2": {
                    "#cdata-section": "0"
                },
                "eg_meaning_2": {
                    "#cdata-section": "0"
                },
                "syn": {
                    "#cdata-section": "0"
                },
                "ant": {
                    "#cdata-section": "0"
                },
                "n_forms": {
                    "#cdata-section": "ounces"
                },
                "v_forms": {
                    "#cdata-section": "0"
                },
                "adj_forms_adv_forms": {
                    "#cdata-section": "0"
                },
                "word_usage": {
                    "#cdata-section": "ounce 的縮寫為 oz."
                },
                "word_test_e_ques": {
                    "#cdata-section": "ounce"
                },
                "word_test_e_item_1": {
                    "#cdata-section": "盎司"
                },
                "word_test_e_item_1e": {
                    "#cdata-section": "ounce"
                },
                "word_test_e_item_2": {
                    "#cdata-section": "熱狗"
                },
                "word_test_e_item_2e": {
                    "#cdata-section": "hot dog"
                },
                "word_test_e_item_3": {
                    "#cdata-section": "附餐"
                },
                "word_test_e_item_3e": {
                    "#cdata-section": "side order"
                },
                "word_test_e_ans": {
                    "#cdata-section": "盎司"
                },
                "word_test_c_ques": {
                    "#cdata-section": "盎司"
                },
                "word_test_c_item_1": {
                    "#cdata-section": "pound"
                },
                "word_test_c_item_1c": {
                    "#cdata-section": "磅"
                },
                "word_test_c_item_2": {
                    "#cdata-section": "gallon"
                },
                "word_test_c_item_2c": {
                    "#cdata-section": "加侖"
                },
                "word_test_c_item_3": {
                    "#cdata-section": "ounce"
                },
                "word_test_c_item_3c": {
                    "#cdata-section": "盎司"
                },
                "word_test_c_ans": {
                    "#cdata-section": "ounce"
                },
                "cloze_test_ques": {
                    "#cdata-section": "One pound equals sixteen ___."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "ounces"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "once"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "unce"
                },
                "cloze_test_ans": {
                    "#cdata-section": "ounces"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "One pound equals sixteen <u>ounces</u>."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "1磅等於16盎司。"
                },
                "vocab_e_snd": {
                    "#cdata-section": "vocab_584_e"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "vocab_c_snd": {
                    "#cdata-section": "0"
                }
            },
            {
                "order": {
                    "#cdata-section": "5"
                },
                "vocabulary_id": {
                    "#cdata-section": "585"
                },
                "lesson_id": {
                    "#cdata-section": "11561en2"
                },
                "vocab": {
                    "#cdata-section": "ham"
                },
                "kk": {
                    "#cdata-section": "[hæm] "
                },
                "part_of_speech_1": {
                    "#cdata-section": "n. 名詞"
                },
                "meaning_1": {
                    "#cdata-section": "火腿"
                },
                "eg_1": {
                    "#cdata-section": "All ham sandwiches are sold out."
                },
                "eg_meaning_1": {
                    "#cdata-section": "所有的火腿三明治都賣完了。"
                },
                "part_of_speech_2": {
                    "#cdata-section": "0"
                },
                "meaning_2": {
                    "#cdata-section": "0"
                },
                "eg_2": {
                    "#cdata-section": "0"
                },
                "eg_meaning_2": {
                    "#cdata-section": "0"
                },
                "syn": {
                    "#cdata-section": "0"
                },
                "ant": {
                    "#cdata-section": "0"
                },
                "n_forms": {
                    "#cdata-section": "0"
                },
                "v_forms": {
                    "#cdata-section": "0"
                },
                "adj_forms_adv_forms": {
                    "#cdata-section": "0"
                },
                "word_usage": {
                    "#cdata-section": "ham-handed 笨手笨腳的"
                },
                "word_test_e_ques": {
                    "#cdata-section": "ham"
                },
                "word_test_e_item_1": {
                    "#cdata-section": "起士漢堡"
                },
                "word_test_e_item_1e": {
                    "#cdata-section": "cheeseburger"
                },
                "word_test_e_item_2": {
                    "#cdata-section": "火腿"
                },
                "word_test_e_item_2e": {
                    "#cdata-section": "ham"
                },
                "word_test_e_item_3": {
                    "#cdata-section": "熱狗"
                },
                "word_test_e_item_3e": {
                    "#cdata-section": "hot dog"
                },
                "word_test_e_ans": {
                    "#cdata-section": "火腿"
                },
                "word_test_c_ques": {
                    "#cdata-section": "火腿"
                },
                "word_test_c_item_1": {
                    "#cdata-section": "hamburger"
                },
                "word_test_c_item_1c": {
                    "#cdata-section": "漢堡"
                },
                "word_test_c_item_2": {
                    "#cdata-section": "ham"
                },
                "word_test_c_item_2c": {
                    "#cdata-section": "火腿"
                },
                "word_test_c_item_3": {
                    "#cdata-section": "hot dog"
                },
                "word_test_c_item_3c": {
                    "#cdata-section": "熱狗"
                },
                "word_test_c_ans": {
                    "#cdata-section": "ham"
                },
                "cloze_test_ques": {
                    "#cdata-section": "All ___ sandwiches are sold out."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "hand"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "ham"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "dam"
                },
                "cloze_test_ans": {
                    "#cdata-section": "ham"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "All <u>ham</u> sandwiches are sold out."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "所有的火腿三明治都賣完了。"
                },
                "vocab_e_snd": {
                    "#cdata-section": "vocab_585_e"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "vocab_c_snd": {
                    "#cdata-section": "0"
                }
            },
            {
                "order": {
                    "#cdata-section": "1"
                },
                "vocabulary_id": {
                    "#cdata-section": "586"
                },
                "lesson_id": {
                    "#cdata-section": "11561en2"
                },
                "vocab": {
                    "#cdata-section": "beverage"
                },
                "kk": {
                    "#cdata-section": "[ˋbɛvərɪdʒ] "
                },
                "part_of_speech_1": {
                    "#cdata-section": "n. 名詞"
                },
                "meaning_1": {
                    "#cdata-section": "飲料"
                },
                "eg_1": {
                    "#cdata-section": "Here is the beverage menu."
                },
                "eg_meaning_1": {
                    "#cdata-section": "這是飲料單。"
                },
                "part_of_speech_2": {
                    "#cdata-section": "0"
                },
                "meaning_2": {
                    "#cdata-section": "0"
                },
                "eg_2": {
                    "#cdata-section": "0"
                },
                "eg_meaning_2": {
                    "#cdata-section": "0"
                },
                "syn": {
                    "#cdata-section": "drink"
                },
                "ant": {
                    "#cdata-section": "0"
                },
                "n_forms": {
                    "#cdata-section": "beverages"
                },
                "v_forms": {
                    "#cdata-section": "0"
                },
                "adj_forms_adv_forms": {
                    "#cdata-section": "0"
                },
                "word_usage": {
                    "#cdata-section": "liquor  酒、含酒精飲料"
                },
                "word_test_e_ques": {
                    "#cdata-section": "beverage"
                },
                "word_test_e_item_1": {
                    "#cdata-section": "沙拉"
                },
                "word_test_e_item_1e": {
                    "#cdata-section": "salad"
                },
                "word_test_e_item_2": {
                    "#cdata-section": "飲料"
                },
                "word_test_e_item_2e": {
                    "#cdata-section": "beverage"
                },
                "word_test_e_item_3": {
                    "#cdata-section": "袋"
                },
                "word_test_e_item_3e": {
                    "#cdata-section": "bag"
                },
                "word_test_e_ans": {
                    "#cdata-section": "飲料"
                },
                "word_test_c_ques": {
                    "#cdata-section": "飲料"
                },
                "word_test_c_item_1": {
                    "#cdata-section": "beverage"
                },
                "word_test_c_item_1c": {
                    "#cdata-section": "飲料"
                },
                "word_test_c_item_2": {
                    "#cdata-section": "fruit"
                },
                "word_test_c_item_2c": {
                    "#cdata-section": "水果"
                },
                "word_test_c_item_3": {
                    "#cdata-section": "vegetable"
                },
                "word_test_c_item_3c": {
                    "#cdata-section": "蔬菜"
                },
                "word_test_c_ans": {
                    "#cdata-section": "beverage"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Here is the ___ menu."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "biverage"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "beveradge"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "beverage"
                },
                "cloze_test_ans": {
                    "#cdata-section": "beverage"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Here is the <u>beverage</u> menu."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "這是飲料單。"
                },
                "vocab_e_snd": {
                    "#cdata-section": "vocab_586_e"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "vocab_c_snd": {
                    "#cdata-section": "0"
                }
            }
        ],
        "Dialogue": {
            "lesson_id": {
                "#cdata-section": "11561en2"
            },
            "lesson_title": {
                "#cdata-section": "Buying lunch"
            },
            "dialogue_id": {
                "#cdata-section": "dialogue_54"
            },
            "dailogue": [
                {
                    "sentence_id": {
                        "#cdata-section": "dialogue_54_1"
                    },
                    "dialogue_e": {
                        "#cdata-section": "Customer: Hi, I want a ham sandwich, please."
                    },
                    "dialogue_c": {
                        "#cdata-section": "顧客：嗨，請給我1份火腿三明治。"
                    },
                    "sex": {
                        "#cdata-section": "男"
                    },
                    "dialogue_e_snd": {
                        "#cdata-section": "dialogue_54_1_e"
                    },
                    "dialogue_c_snd": {
                        "#cdata-section": "0"
                    },
                    "listening_f": {
                        "#cdata-section": "ham sandwich"
                    },
                    "name": {
                        "#cdata-section": "Customer"
                    },
                    "pic": {
                        "#cdata-section": "Customer_b.jpg"
                    },
                    "serial": {
                        "#cdata-section": "1"
                    }
                },
                {
                    "sentence_id": {
                        "#cdata-section": "dialogue_54_2"
                    },
                    "dialogue_e": {
                        "#cdata-section": "Server: Do you want a side order?"
                    },
                    "dialogue_c": {
                        "#cdata-section": "服務生：你要點附餐嗎？"
                    },
                    "sex": {
                        "#cdata-section": "女"
                    },
                    "dialogue_e_snd": {
                        "#cdata-section": "dialogue_54_2_e"
                    },
                    "dialogue_c_snd": {
                        "#cdata-section": "0"
                    },
                    "listening_f": {
                        "#cdata-section": "side order"
                    },
                    "name": {
                        "#cdata-section": "Server"
                    },
                    "pic": {
                        "#cdata-section": "Server_f.jpg"
                    },
                    "serial": {
                        "#cdata-section": "2"
                    }
                },
                {
                    "sentence_id": {
                        "#cdata-section": "dialogue_54_3"
                    },
                    "dialogue_e": {
                        "#cdata-section": "Customer: Yes, a green salad."
                    },
                    "dialogue_c": {
                        "#cdata-section": "顧客：好的，1份蔬菜沙拉。"
                    },
                    "sex": {
                        "#cdata-section": "男"
                    },
                    "dialogue_e_snd": {
                        "#cdata-section": "dialogue_54_3_e"
                    },
                    "dialogue_c_snd": {
                        "#cdata-section": "0"
                    },
                    "listening_f": {
                        "#cdata-section": "salad"
                    },
                    "name": {
                        "#cdata-section": "Customer"
                    },
                    "pic": {
                        "#cdata-section": "Customer_b.jpg"
                    },
                    "serial": {
                        "#cdata-section": "1"
                    }
                },
                {
                    "sentence_id": {
                        "#cdata-section": "dialogue_54_4"
                    },
                    "dialogue_e": {
                        "#cdata-section": "Server: Sure, do you want a drink?"
                    },
                    "dialogue_c": {
                        "#cdata-section": "服務生：你要飲料嗎？"
                    },
                    "sex": {
                        "#cdata-section": "女"
                    },
                    "dialogue_e_snd": {
                        "#cdata-section": "dialogue_54_4_e"
                    },
                    "dialogue_c_snd": {
                        "#cdata-section": "0"
                    },
                    "listening_f": {
                        "#cdata-section": "drink"
                    },
                    "name": {
                        "#cdata-section": "Server"
                    },
                    "pic": {
                        "#cdata-section": "Server_f.jpg"
                    },
                    "serial": {
                        "#cdata-section": "2"
                    }
                },
                {
                    "sentence_id": {
                        "#cdata-section": "dialogue_54_5"
                    },
                    "dialogue_e": {
                        "#cdata-section": "Customer: Orange juice, thanks."
                    },
                    "dialogue_c": {
                        "#cdata-section": "顧客：柳橙汁，謝謝。"
                    },
                    "sex": {
                        "#cdata-section": "男"
                    },
                    "dialogue_e_snd": {
                        "#cdata-section": "dialogue_54_5_e"
                    },
                    "dialogue_c_snd": {
                        "#cdata-section": "0"
                    },
                    "listening_f": {
                        "#cdata-section": "Orange juice"
                    },
                    "name": {
                        "#cdata-section": "Customer"
                    },
                    "pic": {
                        "#cdata-section": "Customer_b.jpg"
                    },
                    "serial": {
                        "#cdata-section": "1"
                    }
                }
            ],
            "pattern": {
                "pattern_id": {
                    "#cdata-section": "pattern_82"
                },
                "pattern": {
                    "#cdata-section": "A: Do you want a hot dog?/nB: Yes, I do."
                },
                "pattern_c": {
                    "#cdata-section": "A: 你想要熱狗嗎？/nB: 好，我要。"
                },
                "pattern_explain": {
                    "#cdata-section": "一般動詞要造問句時，我們需使用助動詞。簡答時，請使用助動詞來回答，而非一般動詞。Do的否定為don't：而does的否定為doesn't。"
                },
                "pattern_e_snd": {
                    "#cdata-section": "pattern_82_e"
                },
                "pattern_c_snd": {
                    "#cdata-section": "0"
                },
                "pattern_explain_snd": {
                    "#cdata-section": "pattern_82_explain"
                }
            },
            "patternPractice": [
                {
                    "pattern_id": {
                        "#cdata-section": "pattern_82"
                    },
                    "pattern_practice_id": {
                        "#cdata-section": "pattern_practice_82_1"
                    },
                    "pattern_practice_ques": {
                        "#cdata-section": "A: _______________?/nB: Yes, I do."
                    },
                    "pattern_practice_ques_c": {
                        "#cdata-section": "你想要附餐嗎？"
                    },
                    "pattern_practice_explain": {
                        "#cdata-section": "A: <u>Do you want a side order</u>?/nB: Yes, I do."
                    },
                    "pattern_practice_e_snd": {
                        "#cdata-section": "pattern_practice_82_1_e"
                    }
                },
                {
                    "pattern_id": {
                        "#cdata-section": "pattern_82"
                    },
                    "pattern_practice_id": {
                        "#cdata-section": "pattern_practice_82_2"
                    },
                    "pattern_practice_ques": {
                        "#cdata-section": "A: _______________?/nB: No, she doesn't."
                    },
                    "pattern_practice_ques_c": {
                        "#cdata-section": "她想要熱狗嗎？"
                    },
                    "pattern_practice_explain": {
                        "#cdata-section": "A: <u>Does she want a hot dog</u>?/nB: No, she doesn't."
                    },
                    "pattern_practice_e_snd": {
                        "#cdata-section": "pattern_practice_82_2_e"
                    }
                },
                {
                    "pattern_id": {
                        "#cdata-section": "pattern_82"
                    },
                    "pattern_practice_id": {
                        "#cdata-section": "pattern_practice_82_3"
                    },
                    "pattern_practice_ques": {
                        "#cdata-section": "A: _______________?/nB: No, they don't."
                    },
                    "pattern_practice_ques_c": {
                        "#cdata-section": "強尼和珍妮想要柳橙汁嗎？"
                    },
                    "pattern_practice_explain": {
                        "#cdata-section": "A: <u>Do Johnny and Jenny want orange juice</u>?/nB: No, they don't."
                    },
                    "pattern_practice_e_snd": {
                        "#cdata-section": "pattern_practice_82_3_e"
                    }
                },
                {
                    "pattern_id": {
                        "#cdata-section": "pattern_82"
                    },
                    "pattern_practice_id": {
                        "#cdata-section": "pattern_practice_82_4"
                    },
                    "pattern_practice_ques": {
                        "#cdata-section": "A: Does he want cheese?/nB: _______________."
                    },
                    "pattern_practice_ques_c": {
                        "#cdata-section": "不，他不要。"
                    },
                    "pattern_practice_explain": {
                        "#cdata-section": "A: Does he want cheese?/nB: <u>No, he doesn't</u>."
                    },
                    "pattern_practice_e_snd": {
                        "#cdata-section": "pattern_practice_82_4_e"
                    }
                },
                {
                    "pattern_id": {
                        "#cdata-section": "pattern_82"
                    },
                    "pattern_practice_id": {
                        "#cdata-section": "pattern_practice_82_5"
                    },
                    "pattern_practice_ques": {
                        "#cdata-section": "A: Do you want french fries?/nB: _______________."
                    },
                    "pattern_practice_ques_c": {
                        "#cdata-section": "好，我要。"
                    },
                    "pattern_practice_explain": {
                        "#cdata-section": "A: Do you want french fries?/nB: <u>Yes, I do</u>."
                    },
                    "pattern_practice_e_snd": {
                        "#cdata-section": "pattern_practice_82_5_e"
                    }
                }
            ],
            "clozeTest": [
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_1"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "___ he want a side order?"
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "Do"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "Is"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "Does"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "Does"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "Does he want a side order?"
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "他想要附餐嗎？"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_2"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "___ they want drinks?"
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "Do"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "Does"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "Are"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "Do"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "Do they want drinks?"
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "他們想要飲料嗎？"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_3"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "Does ___ want a green salad?"
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "Chris"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "you"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "Hank and Frank"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "Chris"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "Does Chris want a green salad?"
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "克里斯想要1份蔬菜沙拉嗎？"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_4"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "Do ___ want sandwiches?"
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "Abby"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "they"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "your sister"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "they"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "Do they want sandwiches?"
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "他們想要三明治嗎？"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_5"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "Do you ___ a hot dog?"
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "wants"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "is wanting"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "want"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "want"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "Do you want a hot dog?"
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "你想要1份熱狗嗎？"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_6"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "Does Jane ___ orange juice?"
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "wants"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "want"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "wanted"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "want"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "Does Jane want orange juice?"
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "珍想要柳橙汁嗎？"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_7"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "A: Does he want milk?/nB: No, he ___."
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "does"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "doesn't"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "do"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "doesn't"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "A: Does he want milk?/nB: No, he doesn't."
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "A: 他想要牛奶？/nB: 不，他不要。"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_8"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "A: Do they want cheese?/nB: No, they ___."
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "doesn't"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "do"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "don't"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "don't"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "A: Do they want cheese?/nB: No, they don't."
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "A: 他們想要乳酪嗎？/nB: 不，他們不要。"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_9"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "A: Do they want salad?/nB: ___, they do."
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "Yes"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "No"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "Are"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "Yes"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "A: Do they want salad?/nB: Yes, they do."
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "A: 他們想要沙拉嗎？/nB: 好，他們要。"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_10"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "___ James ___ yogurt?"
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "Does, want"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "Do, wants"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "Does, wants"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "Does, want"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "Does James want yogurt?"
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "詹姆士想要優格嗎？"
                    }
                },
                {
                    "clozed_test_id": {
                        "#cdata-section": "clozed_test_54_11"
                    },
                    "cloze_test_ques": {
                        "#cdata-section": "A: Do you want a cheeseburger?/nB: No, I ___."
                    },
                    "cloze_test_item_1": {
                        "#cdata-section": "doesn't"
                    },
                    "cloze_test_item_2": {
                        "#cdata-section": "don't"
                    },
                    "cloze_test_item_3": {
                        "#cdata-section": "didn't"
                    },
                    "cloze_test_ans": {
                        "#cdata-section": "don't"
                    },
                    "cloze_test_ans_explain": {
                        "#cdata-section": "A: Do you want a cheeseburger?/nB: No, I don't."
                    },
                    "cloze_test_ans_explain_c": {
                        "#cdata-section": "A: 你想要1個起士漢堡嗎？/nB: 不，我不要。"
                    }
                }
            ]
        },
        "Read": {
            "lesson_id": {
                "#cdata-section": "11561en2"
            },
            "lesson_title": {
                "#cdata-section": "Buying lunch"
            },
            "reading_id": {
                "#cdata-section": "reading_17"
            },
            "prereading_q": {
                "#cdata-section": "Q1. What do you normally order in a fast food restaurant?/nQ2. Do you order side dishes?"
            },
            "prereading_c": {
                "#cdata-section": "你通常在速食餐廳會點什麼？ /n你會點附餐嗎？"
            },
            "prereading_p": {
                "#cdata-section": "11561en2_prereading.jpg"
            },
            "time": {
                "#cdata-section": "1分鐘"
            },
            "reading_e": {
                "#cdata-section": "Main:/nhamburger --- ＄2.50/nsandwich --- ＄2.00/nchicken --- ＄6.99/n/nSide Orders:/nFrench fries --- ＄1.50/nsalad --- ＄0.99/n/nDesserts/npie --- ＄1.69/nice cream --- ＄1.75/n/nDrinks/nCoke --- ＄0.80/ncoffee --- ＄1.10"
            },
            "reading_c": {
                "#cdata-section": "主食/n漢堡 --- 美金2塊50分/n三明治 --- 美金2塊/n烤雞 --- 美金6塊99分/n/n附餐/n薯條 --- 美金1塊50分/n沙拉 --- 美金99分/n/n甜點/n派 --- 美金1塊69分/n冰淇淋 --- 美金1塊75分/n/n飲料/n可樂 --- 美金80分/n咖啡 --- 美金1塊10分"
            },
            "reading_ep": {
                "#cdata-section": "11561en2_reading_e.jpg"
            },
            "reading_cp": {
                "#cdata-section": "11561en2_reading_c.jpg"
            },
            "reading_practice": [
                {
                    "lesson_id": {
                        "#cdata-section": "11561en2"
                    },
                    "lesson_title": {
                        "#cdata-section": "Buying lunch"
                    },
                    "reading_id": {
                        "#cdata-section": "reading_17"
                    },
                    "reading_test_id": {
                        "#cdata-section": "reading_17_1"
                    },
                    "reading_test_ques": {
                        "#cdata-section": "How much is an order of french fries?"
                    },
                    "reading_test_c_item_1": {
                        "#cdata-section": "$1.50"
                    },
                    "reading_test_c_item_2": {
                        "#cdata-section": "$1.75"
                    },
                    "reading_test_c_item_3": {
                        "#cdata-section": "$1.69"
                    },
                    "reading_test_c_item_4": {
                        "#cdata-section": "0"
                    },
                    "reading_test_c_item_5": {
                        "#cdata-section": "0"
                    },
                    "reading_test_c_item_6": {
                        "#cdata-section": "0"
                    },
                    "reading_test_ans": {
                        "#cdata-section": "$1.50"
                    },
                    "reading_test_explain_c": {
                        "#cdata-section": "french fries --- ＄1.50/n薯條 --- 美金1塊50分"
                    }
                },
                {
                    "lesson_id": {
                        "#cdata-section": "0"
                    },
                    "lesson_title": {
                        "#cdata-section": "0"
                    },
                    "reading_id": {
                        "#cdata-section": "0"
                    },
                    "reading_test_id": {
                        "#cdata-section": "reading_17_2"
                    },
                    "reading_test_ques": {
                        "#cdata-section": "How much is a salad?"
                    },
                    "reading_test_c_item_1": {
                        "#cdata-section": "$0.80"
                    },
                    "reading_test_c_item_2": {
                        "#cdata-section": "$0.99"
                    },
                    "reading_test_c_item_3": {
                        "#cdata-section": "$1.10"
                    },
                    "reading_test_c_item_4": {
                        "#cdata-section": "0"
                    },
                    "reading_test_c_item_5": {
                        "#cdata-section": "0"
                    },
                    "reading_test_c_item_6": {
                        "#cdata-section": "0"
                    },
                    "reading_test_ans": {
                        "#cdata-section": "$0.99"
                    },
                    "reading_test_explain_c": {
                        "#cdata-section": "salad --- ＄0.99/n沙拉 --- 美金99分"
                    }
                },
                {
                    "lesson_id": {
                        "#cdata-section": "0"
                    },
                    "lesson_title": {
                        "#cdata-section": "0"
                    },
                    "reading_id": {
                        "#cdata-section": "0"
                    },
                    "reading_test_id": {
                        "#cdata-section": "reading_17_3"
                    },
                    "reading_test_ques": {
                        "#cdata-section": "How much are a hamburger and a Coke?"
                    },
                    "reading_test_c_item_1": {
                        "#cdata-section": "$3.30"
                    },
                    "reading_test_c_item_2": {
                        "#cdata-section": "$3.50"
                    },
                    "reading_test_c_item_3": {
                        "#cdata-section": "$3.25"
                    },
                    "reading_test_c_item_4": {
                        "#cdata-section": "0"
                    },
                    "reading_test_c_item_5": {
                        "#cdata-section": "0"
                    },
                    "reading_test_c_item_6": {
                        "#cdata-section": "0"
                    },
                    "reading_test_ans": {
                        "#cdata-section": "$3.30"
                    },
                    "reading_test_explain_c": {
                        "#cdata-section": "hamburger --- ＄2.50 /Coke --- ＄0.80/n漢堡 --- 美金2塊50分/可樂 --- 美金80分"
                    }
                }
            ]
        },
        "Listen": {
            "lesson_id": {
                "#cdata-section": "11561en2"
            },
            "lesson_title": {
                "#cdata-section": "Buying lunch"
            },
            "listening": [
                {
                    "listening_id": {
                        "#cdata-section": "listening_27"
                    },
                    "listening_listening": [
                        {
                            "listening_p": {
                                "#cdata-section": "listening_27_1"
                            },
                            "listening_e": {
                                "#cdata-section": "Manny: I want a cheeseburger, a green salad, and an orange juice please. "
                            },
                            "listening_c": {
                                "#cdata-section": "曼尼：我要1個起士漢堡、1份蔬菜沙拉、和1杯柳橙汁。"
                            },
                            "sex": {
                                "#cdata-section": "男"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_27_1_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_27_2"
                            },
                            "listening_e": {
                                "#cdata-section": "Server: No orange juice today. Would you like milk or soda? "
                            },
                            "listening_c": {
                                "#cdata-section": "服務生：今天沒有柳橙汁。你要喝牛奶或汽水嗎？"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_27_2_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_27_3"
                            },
                            "listening_e": {
                                "#cdata-section": "Manny: A soda please."
                            },
                            "listening_c": {
                                "#cdata-section": "曼尼：1杯汽水。"
                            },
                            "sex": {
                                "#cdata-section": "男"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_27_3_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_27_4"
                            },
                            "listening_e": {
                                "#cdata-section": "Server: OK, two minutes. Next."
                            },
                            "listening_c": {
                                "#cdata-section": "服務生：好的，請等2分鐘。下一位。"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_27_4_e"
                            }
                        }
                    ]
                },
                {
                    "listening_id": {
                        "#cdata-section": "listening_28"
                    },
                    "listening_listening": [
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_1"
                            },
                            "listening_e": {
                                "#cdata-section": "Tran: What sandwiches do you have?"
                            },
                            "listening_c": {
                                "#cdata-section": "川恩：有什麼口味的三明治？"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_1_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_2"
                            },
                            "listening_e": {
                                "#cdata-section": "Server: Ham or cheese."
                            },
                            "listening_c": {
                                "#cdata-section": "服務生：火腿或起士。"
                            },
                            "sex": {
                                "#cdata-section": "男"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_2_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_3"
                            },
                            "listening_e": {
                                "#cdata-section": "Tran: I'll have a ham sandwich."
                            },
                            "listening_c": {
                                "#cdata-section": "川恩：我要1個火腿三明治。"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_3_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_4"
                            },
                            "listening_e": {
                                "#cdata-section": "Server: OK."
                            },
                            "listening_c": {
                                "#cdata-section": "服務生：好的。"
                            },
                            "sex": {
                                "#cdata-section": "男"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_4_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_5"
                            },
                            "listening_e": {
                                "#cdata-section": "Tran: And a green salad, too."
                            },
                            "listening_c": {
                                "#cdata-section": "川恩：再1份蔬菜沙拉。"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_5_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_6"
                            },
                            "listening_e": {
                                "#cdata-section": "Server: Of course. What about a drink?"
                            },
                            "listening_c": {
                                "#cdata-section": "服務生：沒問題。那飲料呢？"
                            },
                            "sex": {
                                "#cdata-section": "男"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_6_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_7"
                            },
                            "listening_e": {
                                "#cdata-section": "Tran: No, thanks."
                            },
                            "listening_c": {
                                "#cdata-section": "川恩：不需要，謝謝。"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_7_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_8"
                            },
                            "listening_e": {
                                "#cdata-section": "Server: OK. That's a ham sandwich and a green salad, right?"
                            },
                            "listening_c": {
                                "#cdata-section": "服務生：OK。1份火腿三明治和1份蔬菜沙拉，對吧？"
                            },
                            "sex": {
                                "#cdata-section": "男"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_8_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_28_9"
                            },
                            "listening_e": {
                                "#cdata-section": "Tran: That's right. Thanks."
                            },
                            "listening_c": {
                                "#cdata-section": "川恩：沒錯，謝謝。"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_28_9_e"
                            }
                        }
                    ]
                },
                {
                    "listening_id": {
                        "#cdata-section": "listening_29"
                    },
                    "listening_listening": [
                        {
                            "listening_p": {
                                "#cdata-section": "listening_29_1"
                            },
                            "listening_e": {
                                "#cdata-section": "Miyuki: I want some milk, please, and a hot dog."
                            },
                            "listening_c": {
                                "#cdata-section": "美雪：我要一些牛奶和1份熱狗。"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_29_1_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_29_2"
                            },
                            "listening_e": {
                                "#cdata-section": "Server: Do you want mustard?"
                            },
                            "listening_c": {
                                "#cdata-section": "服務生：你要芥末嗎？"
                            },
                            "sex": {
                                "#cdata-section": "男"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_29_2_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_29_3"
                            },
                            "listening_e": {
                                "#cdata-section": "Miyuki: No, thanks. Just french fries."
                            },
                            "listening_c": {
                                "#cdata-section": "美雪：不，謝了。還要薯條。"
                            },
                            "sex": {
                                "#cdata-section": "女"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_29_3_e"
                            }
                        },
                        {
                            "listening_p": {
                                "#cdata-section": "listening_29_4"
                            },
                            "listening_e": {
                                "#cdata-section": "Server: That's fine. Hot dog, no mustard, french fries, and a milk coming up."
                            },
                            "listening_c": {
                                "#cdata-section": "服務生：沒問題。不加芥末的熱狗、薯條、和牛奶馬上來。"
                            },
                            "sex": {
                                "#cdata-section": "男"
                            },
                            "listening_e_snd": {
                                "#cdata-section": "listening_29_4_e"
                            }
                        }
                    ]
                }
            ],
            "listening_practice": [
                {
                    "lesson_id": {
                        "#cdata-section": "11561en2"
                    },
                    "lesson_title": {
                        "#cdata-section": "Buying lunch"
                    },
                    "listening_id": {
                        "#cdata-section": "listening_27"
                    },
                    "listening_test_id": {
                        "#cdata-section": "listening_27_q1"
                    },
                    "listening_test_ques": {
                        "#cdata-section": "What does Manny order?"
                    },
                    "listening_test_c_item_1": {
                        "#cdata-section": "a ham sandwich,  a green salad, and water"
                    },
                    "listening_test_c_item_2": {
                        "#cdata-section": "a cheeseburger, a green salad, and a soda"
                    },
                    "listening_test_c_item_3": {
                        "#cdata-section": "a cheeseburger, a fruit salad, and an orange juice"
                    },
                    "listening_test_c_item_4": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_item_5": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_item_6": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_ans": {
                        "#cdata-section": "a cheeseburger, a green salad, and a soda"
                    }
                },
                {
                    "lesson_id": {
                        "#cdata-section": "0"
                    },
                    "lesson_title": {
                        "#cdata-section": "0"
                    },
                    "listening_id": {
                        "#cdata-section": "listening_28"
                    },
                    "listening_test_id": {
                        "#cdata-section": "listening_28_q1"
                    },
                    "listening_test_ques": {
                        "#cdata-section": "What does Tran want?"
                    },
                    "listening_test_c_item_1": {
                        "#cdata-section": "a cheeseburger and a green salad"
                    },
                    "listening_test_c_item_2": {
                        "#cdata-section": "a hamburger and a green salad"
                    },
                    "listening_test_c_item_3": {
                        "#cdata-section": "a ham sandwich and a green salad"
                    },
                    "listening_test_c_item_4": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_item_5": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_item_6": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_ans": {
                        "#cdata-section": "a ham sandwich and a green salad"
                    }
                },
                {
                    "lesson_id": {
                        "#cdata-section": "0"
                    },
                    "lesson_title": {
                        "#cdata-section": "0"
                    },
                    "listening_id": {
                        "#cdata-section": "listening_29"
                    },
                    "listening_test_id": {
                        "#cdata-section": "listening_29_q1"
                    },
                    "listening_test_ques": {
                        "#cdata-section": "What does Miyuki want?"
                    },
                    "listening_test_c_item_1": {
                        "#cdata-section": "hot dog with mustard, french fries, and a milk"
                    },
                    "listening_test_c_item_2": {
                        "#cdata-section": "hot dog without mustard, french fries, and a juice"
                    },
                    "listening_test_c_item_3": {
                        "#cdata-section": "hot dog without mustard, french fries, and a milk"
                    },
                    "listening_test_c_item_4": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_item_5": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_item_6": {
                        "#cdata-section": "0"
                    },
                    "listening_test_c_ans": {
                        "#cdata-section": "hot dog without mustard, french fries, and a milk"
                    }
                }
            ]
        }
    }
}