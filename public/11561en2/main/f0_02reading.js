let templete = {}

window.onload = function () {
    data = reduce(data);
    word = reduce(word);
    let countdown;
    let QuizUi = $('[name="Quiz"]')
    $('[name="QuizContainer"]').empty();
    $('.readTest').hide();
    $('.index').hide();
    $('.read').hide();
    $('.blackbg').hide();

    $('[name="btn_quizStart"]').hide();
    $('.method').hide();
    $('.index').show();
    $('.testMethod').hide();
    $('.blackbg').hide();

    let QuizObj = new Quiz(data, word, QuizUi, 'reading_comprehension')
    QuizObj.addEventStatusChange((status = 'testing') => {
        //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
        console.log(status)
    })
    QuizObj.addEventEnd((data) => {
        //data={score:50,right:5,wrong:5}
        console.log(data)
    })
    $('[name="QuizContainer"]').append(QuizObj.view);

    $('[name="btn_quizStart"]').click((e) => {
    });

    // 2023.4.24 Tony 
    let m = data.DocumentElement.Read;
    let newMQ1 = m.prereading_q.split('/n')[0];
    let newMC1 = m.prereading_c.split('/n')[0];
    let newMQ2 = m.prereading_q.split('/n')[1];
    let newMC2 = m.prereading_c.split('/n')[1];
    let newRE = m.reading_e.replace(/<\/?u>/g, '');

    templete['content'] = $('[name="content"]')[0].outerHTML;
    $('[name="readContent"]').empty();
    let p = $(templete['content']);
    $('[name="pic"] img').attr('src', '../material/' + m.prereading_p);
    $('[name="img"] img', p).attr('src', '../material/' + m.reading_ep);
    $('[name="sentence"]', p).html(newRE);
    $('[name="readContent"]').append(p);
    $('[name="timing"]').text(m.time);
    $('[name="eng1"]').html(newMQ1);
    $('[name="cn1"]').html(newMC1);
    $('[name="eng2"]').html(newMQ2);
    $('[name="cn2"]').html(newMC2);

    $('.btn_start2').click(function () {
        $('.index').hide();
        $('.pic_m').hide();
        $('.read').show();
        if ($('.read').show()) {
            let count = 0;
            countdown = setInterval(() => {
                let minutes = Math.floor(count / 60);
                let seconds = count % 60;
                minutes = (minutes < 10 ? '0' : '') + minutes;
                seconds = (seconds < 10 ? '0' : '') + seconds;
                let time = minutes + ':' + seconds;
                $('.readTime').text(time);
                count++;
                if (minutes == 1) {
                    clearInterval(countdown);
                    $('.read').hide();
                    $('.blackbg').show();
                }
            }, 1000);
        }
    });

    $('.btn_readTest').click(function () {
        $('.readTest').show();
        $('.read').hide();
        $('.blackbg').hide();
        clearInterval(countdown);
    });

    $('.again').click(function () {
        $('.index').show();
        $('.blackbg').hide();
        $('.pic_m').show();
    });
}

