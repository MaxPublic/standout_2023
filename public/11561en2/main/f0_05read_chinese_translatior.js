let templete = {}
window.onload = function () {
    data = reduce(data);
    word = reduce(word);

    let m = data.DocumentElement.Read;

    $('[name="reading_e"] img').attr('src', '../material/' + m.reading_ep);

    // 開啟中英對照功能
    let isClick = false;
    $('[name="en_to_cht"]').unbind('click').bind('click', function () {
        isClick = !isClick;
        if (isClick) {
            $(this).text('開啟英中對照');
            $(this).removeClass('btnClick');
            $('[name="reading_e"] img').attr('src', '../material/' + m.reading_ep);            
        } else {          
            $(this).text('關閉英中對照');
            $(this).addClass('btnClick');
            $('[name="reading_e"] img').attr('src', '../material/' + m.reading_cp);
        }
    })

}