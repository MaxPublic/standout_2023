
// console.log(items,ans)
window.onload = function () {
    QUnit.test('測試randomItems多元陣列隨機', function(assert) {
        let {items,ans}=randomItems()
        var expected = [["C","B","A"],["c.mp3","b.mp3","a.mp3"],["c.png","b.png","a.png"]]
        expected[0]=expected[0].sort()
        expected[1]=expected[1].sort()
        expected[2]=expected[2].sort()
        items[0]=items[0].sort()
        items[1]=items[1].sort()
        items[2]=items[2].sort()
        assert.propEqual(items, expected, '測試多元陣列隨機成功');
    });
    QUnit.test("Testing randomItems function with valid inputs", function(assert) {
        let inputItems = [["A", "B", "C"], ["a.mp3", "b.mp3", "c.mp3"], ["a.png", "b.png", "c.png"]];
        let inputAns = [0, 1];
        let result = randomItems(inputItems, inputAns);
      
        assert.deepEqual(result.items.length, 3, "Number of rows should match");
        assert.deepEqual(result.items[0].length, 3, "Number of columns should match");
        assert.deepEqual(result.ans.length, 2, "Number of correct answers should match");
    });
    
    QUnit.test("Testing randomItems function with empty inputs", function(assert) {
        let inputItems = [];
        let inputAns = [];
        let result = randomItems(inputItems, inputAns);
        
        assert.deepEqual(result.items.length, 0, "Number of rows should be zero");
        assert.deepEqual(result.ans.length, 0, "Number of correct answers should be zero");
    });
    
    QUnit.test("Testing randomItems function with duplicate inputs", function(assert) {
        let inputItems = [["A", "B", "C"], ["A", "B", "C"], ["a.mp3", "b.mp3", "c.mp3"]];
        let inputAns = [0, 1];
        let result = randomItems(inputItems, inputAns);
        
        assert.deepEqual(result.items.length, 3, "Number of rows should match");
        assert.deepEqual(result.items[0].length, 3, "Number of columns should match");
        assert.deepEqual(result.ans.length, 2, "Number of correct answers should match");
    });
    QUnit.test('測試reduce data.js', function(assert) {
        let datacc = reduce(data);
        let arr1=datacc.DocumentElement.Vocabulary[0]
        //.word_test_e_item;
        // let arr2=['疑問、詢問', '問候、招呼', '問題']
        assert.ok(
            typeof(arr1.order)==='string' &&
            typeof(arr1.vocabulary_id)==='string' && 
            typeof(arr1.lesson_id)==='string' && 
            Array.isArray(arr1.word_test_e_item)
            ,'data.js成功拔除cdata，並且正確將欄位改成陣列');
        // assert.deepEqual(arr1, arr2, 'data.js成功拔除cdata');
    })
    QUnit.test('測試reduce word.js', function(assert) {
        let datacc = reduce(word);
        assert.equal(datacc.DocumentElement.Word[0].word, 'answers', 'word.js成功拔除cdata');
    })
    QUnit.test('測試AnsIsEqual 作答比對', function(assert) {
        let datacc = AnsIsEqual([0,1,2],[2,1,0])
        assert.ok(datacc, '[0,1,2],[2,1,0]AnsIsEqual作答比對正確');
    })
    QUnit.test('測試題目轉換器convertDataToExamJson-listen_words', function(assert) {
        let _data=reduce(data)
        let _word=reduce(word)
        let _a=Quiz.convertDataToExamJson(_data,_word,type='listen_words')[0]
        //取第0題做檢查
        // console.log(JSON.stringify(_a.option_title))
        assert.ok(
            typeof(_a.title) === 'string' &&
            typeof(_a.title_pic)==='string' &&
            typeof(_a.title_mp3)==='string' &&
            Array.isArray(_a.option_title) &&
            Array.isArray(_a.option_pic) &&
            Array.isArray(_a.option_mp3) && 
            Array.isArray(_a.ans) &&
            Array.isArray(_a.submit)
            , 'listen_words 題目型態產出正確');
    })

}