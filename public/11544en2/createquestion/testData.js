// var data = {
//     "vocab": [
//         {
//             "word_test_e_item": [
//                 "疑問、詢問",
//                 "問候、招呼",
//                 "問題"
//             ],
//             "word_test_e_item_e": [
//                 "question",
//                 "greet",
//                 "problem"
//             ],
//             "word_test_c_item": [
//                 "greet",
//                 "green",
//                 "green"
//             ],
//             "word_test_c_item_c": [
//                 "問候、招呼",
//                 "極好的",
//                 "綠色"
//             ],
//             "cloze_test_item": [
//                 "great",
//                 "green",
//                 "greet"
//             ]
//         },
//         {
//             "word_test_e_item": [
//                 "朋友",
//                 "隊友",
//                 "校友"
//             ],
//             "word_test_e_item_e": [
//                 "friend",
//                 "teammate",
//                 "alumni"
//             ],
//             "word_test_c_item": [
//                 "fried",
//                 "friend",
//                 "friend"
//             ],
//             "word_test_c_item_c": [
//                 "油炸的",
//                 "燃燒的",
//                 "朋友"
//             ],
//             "cloze_test_item": [
//                 "friemd",
//                 "fired",
//                 "friend"
//             ]
//         },
//         {
//             "word_test_e_item": [
//                 "歡喜",
//                 "歡迎",
//                 "歡樂"
//             ],
//             "word_test_e_item_e": [
//                 "like",
//                 "welcome",
//                 "joy"
//             ],
//             "word_test_c_item": [
//                 "welcome",
//                 "well-born",
//                 "well-born"
//             ],
//             "word_test_c_item_c": [
//                 "歡迎",
//                 "做得好的",
//                 "出身名門的"
//             ],
//             "cloze_test_item": [
//                 "Become",
//                 "Income",
//                 "Welcome"
//             ]
//         }
//     ]
// };


// let m = data.vocab;

// function getrandom() {
//     let ary=[];
//     let aary=[];
//     for (let i = 0; i < m.length; i++) {
//         ary.push(m[i]);
//         console.log(ary);
//     }
//     for(let i = 0; i < m.length; i++){
//         aary.push(ary.splice(Math.random()*ary.length));
//         console.log(aary);
//     }
// }
// getrandom();

// let jsonData =
//     'question' = [
//         {
//             'title': "",
//             'title_pic': "xxx.jpg",
//             'title_mp3': "xxx.mp3",
//             'ans_list': []
//         }
//     ]


// 定义一个包含所有答案的数组
const answerArray = ['A', 'B', 'C', 'D', 'E', 'F'];

// 从数组中抽出正确答案
const correctAnswer = answerArray.splice(2, 1)[0];

// 打乱剩余答案的顺序
const shuffledArray = answerArray.sort(() => Math.random() - 0.5);

// 随机生成一个插入位置的索引
const insertIndex = Math.floor(Math.random() * (shuffledArray.length + 1));

// 在指定位置插入正确答案
shuffledArray.splice(insertIndex, 0, correctAnswer);

console.log('正确答案是：', correctAnswer);
console.log('混乱后答案数组为：', shuffledArray);
console.log('正确答案的位置为：', insertIndex);








