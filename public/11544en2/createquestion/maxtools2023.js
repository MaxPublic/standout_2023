/**2023/2/6 Max*/


function reduce(obj/**json */){//數據最佳化
    let data=go2(obj);
    for (let i=0;i<data.DocumentElement.Vocabulary.length;i++){
        let p=data.DocumentElement.Vocabulary[i]
        p['word_test_e_item']=[p['word_test_e_item_1'],p['word_test_e_item_2'],p['word_test_e_item_3']]
        p['word_test_e_item_e']=[p['word_test_e_item_1e'],p['word_test_e_item_2e'],p['word_test_e_item_3e']]
        p['word_test_e_ans']=find(p['word_test_e_item'],p['word_test_e_ans'])
        p['word_test_c_item']=[p['word_test_c_item_1'],p['word_test_c_item_3'],p['word_test_c_item_3']]
        p['word_test_c_item_c']=[p['word_test_c_item_1c'],p['word_test_c_item_2c'],p['word_test_c_item_3c']]
        p['word_test_c_ans']=find(p['word_test_c_item'],p['word_test_c_ans'])
        p['cloze_test_item']=[p['cloze_test_item_1'],p['cloze_test_item_2'],p['cloze_test_item_3']]
        p['cloze_test_ans']=find(p['cloze_test_item'],p['cloze_test_ans'])
        disabledVocabularyProp(p)
    }
    return data;
    function find(dt,ans){
        for (let i=0;i<dt.length;i++){
            if(dt[i]==ans){
                return i;
            }
        }
        return -1;
    }
    function disabledVocabularyProp(p){
        p['word_test_e_item_1']+='===廢止=='
        p['word_test_e_item_2']+='===廢止=='
        p['word_test_e_item_3']+='===廢止=='
        p['word_test_e_item_1e']+='===廢止=='
        p['word_test_e_item_2e']+='===廢止=='
        p['word_test_e_item_3e']+='===廢止=='
        p['word_test_c_item_1']+='===廢止=='
        p['word_test_c_item_3']+='===廢止=='
        p['word_test_c_item_3']+='===廢止=='
        p['word_test_c_item_1c']+='===廢止=='
        p['word_test_c_item_2c']+='===廢止=='
        p['word_test_c_item_3c']+='===廢止=='
        p['cloze_test_item_1']+='===廢止=='
        p['cloze_test_item_2']+='===廢止=='
        p['cloze_test_item_3']+='===廢止=='
    }
    function go2(obj){
        let g={}
        if(Array.isArray(obj)){
            g=[]
        }
        for(let i in obj){
            if (i=='#cdata-section'){
                // console.log(Object.keys(obj).length)
                return obj[i];
            }else{
                if(typeof(obj[i])==='object'){
                    g[i]=go2(obj[i])
                }else{
                    g[i]=obj[i]
                }
            }
        }
        return g;  
    }
}
function toggleElements(element1, element2,toggle=true,add=true) {
    // 設定參數的預設值，防止參數缺失的情況
    element1 = element1 || $();
    element2 = element2 || $();
    
    // 取得兩個元素的子元素，並進行對應
    if (element1.attr("class") && element2.attr("class")) {
        if(toggle){
            element1.toggleClass(element2.attr("class"));
        }else{
            if(add){
                element1.addClass(element2.attr("class"));
            }else{
                element1.removeClass(element2.attr("class"));
            }
        }
    }
    element1.children().each(function(index, elem1) {
        elem1=$(elem1)
        var elem2 = $(element2.children()[index]);
        // 判斷兩個子元素是否都有 class，若都有則進行 toggleClass
        if (elem1.attr("class") && elem2.attr("class")) {
            if(toggle){
                elem1.toggleClass(elem2.attr("class"));
            }else{
                if(add){
                    elem1.addClass(elem2.attr("class"));
                }else{
                    elem1.removeClass(elem2.attr("class"));
                }
            }
        }
        // 若其中一個沒有 class，則進行遞迴呼叫，對應子元素
        else {
            toggleElements($(elem1), $(elem2),toggle,add);
        }
    });
}
class MaxMediaPlayer{
    constructor(mp3files=[],playBtn,progressBar,CompletePreparationCallback){
        let _self=this;
        let audios=[];
        let totalDuration = 0;
        let audioLengths=[0]
        let currentAudio = 0;
        let oldindex=-1
        let ChangeIndexCallback;
        createAudios(mp3files)


        _self.totalDuration=()=>{
            return totalDuration;
        }
        // _self.play=()=>{
        //     go()
        // }
        _self.playindex=(index,autonext)=>{
            allpause()
            currentAudio=index;
            audios[index].currentTime=0
            audios[index].play()
        }
        _self.addEventChangeIndex=(callback)=>{//當切換了音檔的時候
            ChangeIndexCallback=callback;
        }
        function createAudios(files){
            if(files.length==0){
                console.log('媒體陣列為空')
                return;
            }
            let i=0;
            newa(files[i])
            function newa(file){
                newAudio(file,(data)=>{
                    audios.push(data.audio)
                    totalDuration += data.duration;
                    audioLengths.push(totalDuration)
                    if(i>=files.length-1){
                        CompletePreparationCallback&&CompletePreparationCallback();
                        progressBar.attr("max", totalDuration);
                        playBtn.show();
                        setInterval(function() {
                            if(currentAudio>=audioLengths.length){
                                progressBar.val(progressBar.attr("max"));
                            }else{
                                
                                
                                progressBar.val( audioLengths[currentAudio]+audios[currentAudio].currentTime);
                                let g=gettimeindex(progressBar.val())
                                // console.log('==',g,oldindex,progressBar.val())
                                if (g!=oldindex){
                                    //索引改變，通知監聽事件
                                    ChangeIndexCallback&&ChangeIndexCallback(g);
                                    oldindex=g;
                                }
                            }
                        }, 50);
                        playBtn.click(function() {
                            go();
                        });
                    }else{
                        i++;
                        newa(files[i])
                    }
                })
            }
        }
        function newAudio(file,callback){
            let aud = new Audio();
            aud.src = file;//audioFiles[currentAudio];
            aud.preload = "auto";
            aud.addEventListener("loadedmetadata", function() {
                callback&&callback({audio:aud,duration:aud.duration})
            });
            aud.addEventListener("ended", function() {
                aud.pause();
                currentAudio++;
                if (currentAudio >= audios.length) {
                    currentAudio--;
                    return;
                }
                audios[currentAudio].play();
            });
        }
        function allpause(){
            for(let i=0;i<audios.length;i++){
                audios[i].pause()
                audios[i].currentTime =0
            }
        }
        function gettimeindex(val){//拿取目前的索引位置
            for (let i =0;i<audioLengths.length-1;i++){
                if(val>=audioLengths[i]&& val<audioLengths[i+1]){
                    return i;
                }
            }
            return audioLengths.length-1
        }
        function go(){
            allpause()
            currentAudio=0;
            audios[currentAudio].play();


            progressBar.on("input", function() {
                let desiredTime = progressBar.val();
                let index=gettimeindex(desiredTime)
                allpause()
                currentAudio=index
                audios[index].currentTime = desiredTime-audioLengths[index]
                audios[index].play()
            });
        }

    }
}