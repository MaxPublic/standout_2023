﻿var data = {
    "DocumentElement": {
        "Info": {
            "course_id": {
                "#cdata-section": "11544en2"
            },
            "serial": {
                "#cdata-section": "StandOut"
            },
            "book": {
                "#cdata-section": "Book 1"
            },
            "unit_no": {
                "#cdata-section": "Pre-Unit"
            },
            "lesson_no": {
                "#cdata-section": "1"
            },
            "course_name": {
                "#cdata-section": "Hello!"
            },
            "introduction": {
                "#cdata-section": "見到朋友時，你都是如何打招呼呢？在國外，除了握手，有時還會擁抱，甚至於會互相親吻對方臉頰，在本課程同學們將學習如何用美語與朋友打招呼，文法句型解析裡會教同學be動詞縮寫的用法。"
            },
            "learning_objectives_e": {
                "#cdata-section": "1. Greet with friends/n2. Learn be verb and its contractions"
            },
            "learning_objectives_c": {
                "#cdata-section": "與朋友打招呼/n學習be動詞和be動詞的縮寫"
            },
            "suggested_time": {
                "#cdata-section": "60分鐘"
            }
        },
        "Vocabulary": [{
            "order": {
                "#cdata-section": "1"
            },
            "vocabulary_id": {
                "#cdata-section": "2182"
            },
            "lesson_id": {
                "#cdata-section": "11544en8"
            },
            "vocab": {
                "#cdata-section": "greet"
            },
            "kk": {
                "#cdata-section": "[grit]"
            },
            "part_of_speech_1": {
                "#cdata-section": "v. 動詞"
            },
            "meaning_1": {
                "#cdata-section": "問候、招呼"
            },
            "eg_1": {
                "#cdata-section": "Bill greets his friends at the door."
            },
            "eg_meaning_1": {
                "#cdata-section": "比爾在門口問候他的朋友。"
            },
            "part_of_speech_2": {
                "#cdata-section": "0"
            },
            "meaning_2": {
                "#cdata-section": "0"
            },
            "eg_2": {
                "#cdata-section": "0"
            },
            "eg_meaning_2": {
                "#cdata-section": "0"
            },
            "syn": {
                "#cdata-section": "meet, address, salute"
            },
            "ant": {
                "#cdata-section": "say goodbye"
            },
            "n_forms": {
                "#cdata-section": "0"
            },
            "v_forms": {
                "#cdata-section": "greeted, greeted, greeting"
            },
            "adj_forms_adv_forms": {
                "#cdata-section": "0"
            },
            "word_usage": {
                "#cdata-section": "greeting card 賀卡"
            },
            "word_test_e_ques": {
                "#cdata-section": "greet"
            },
            "word_test_e_item_1": {
                "#cdata-section": "疑問、詢問"
            },
            "word_test_e_item_1e": {
                "#cdata-section": "question"
            },
            "word_test_e_item_2": {
                "#cdata-section": "問候、招呼"
            },
            "word_test_e_item_2e": {
                "#cdata-section": "greet"
            },
            "word_test_e_item_3": {
                "#cdata-section": "問題"
            },
            "word_test_e_item_3e": {
                "#cdata-section": "problem"
            },
            "word_test_e_ans": {
                "#cdata-section": "問候、招呼"
            },
            "word_test_c_ques": {
                "#cdata-section": "問候、招呼"
            },
            "word_test_c_item_1": {
                "#cdata-section": "greet"
            },
            "word_test_c_item_1c": {
                "#cdata-section": "問候、招呼"
            },
            "word_test_c_item_2": {
                "#cdata-section": "great"
            },
            "word_test_c_item_2c": {
                "#cdata-section": "極好的"
            },
            "word_test_c_item_3": {
                "#cdata-section": "green"
            },
            "word_test_c_item_3c": {
                "#cdata-section": "綠色"
            },
            "word_test_c_ans": {
                "#cdata-section": "greet"
            },
            "cloze_test_ques": {
                "#cdata-section": "Bill ___s his friends at the door."
            },
            "cloze_test_item_1": {
                "#cdata-section": "great"
            },
            "cloze_test_item_2": {
                "#cdata-section": "green"
            },
            "cloze_test_item_3": {
                "#cdata-section": "greet"
            },
            "cloze_test_ans": {
                "#cdata-section": "greet"
            },
            "cloze_test_ans_explain": {
                "#cdata-section": "Bill <u>greet</u>s his friends at the door."
            },
            "cloze_test_ans_explain_c": {
                "#cdata-section": "比爾在門口問候他的朋友。"
            },
            "vocab_e_snd": {
                "#cdata-section": "vocab_2182_e"
            },
            "sex": {
                "#cdata-section": "男"
            },
            "vocab_c_snd": {
                "#cdata-section": "0"
            }
        }, {
            "order": {
                "#cdata-section": "2"
            },
            "vocabulary_id": {
                "#cdata-section": "2183"
            },
            "lesson_id": {
                "#cdata-section": "11544en8"
            },
            "vocab": {
                "#cdata-section": "friend"
            },
            "kk": {
                "#cdata-section": "[frɛnd]"
            },
            "part_of_speech_1": {
                "#cdata-section": "n. 名詞"
            },
            "meaning_1": {
                "#cdata-section": "朋友"
            },
            "eg_1": {
                "#cdata-section": "Mandy is my best friend."
            },
            "eg_meaning_1": {
                "#cdata-section": "曼蒂是我最要好的朋友。"
            },
            "part_of_speech_2": {
                "#cdata-section": "0"
            },
            "meaning_2": {
                "#cdata-section": "0"
            },
            "eg_2": {
                "#cdata-section": "0"
            },
            "eg_meaning_2": {
                "#cdata-section": "0"
            },
            "syn": {
                "#cdata-section": "companion, pal, mate, buddy "
            },
            "ant": {
                "#cdata-section": "enemy, foe"
            },
            "n_forms": {
                "#cdata-section": "friends"
            },
            "v_forms": {
                "#cdata-section": "0"
            },
            "adj_forms_adv_forms": {
                "#cdata-section": "0"
            },
            "word_usage": {
                "#cdata-section": "make friends 結交朋友"
            },
            "word_test_e_ques": {
                "#cdata-section": "friend"
            },
            "word_test_e_item_1": {
                "#cdata-section": "朋友"
            },
            "word_test_e_item_1e": {
                "#cdata-section": "friend"
            },
            "word_test_e_item_2": {
                "#cdata-section": "隊友"
            },
            "word_test_e_item_2e": {
                "#cdata-section": "teammate"
            },
            "word_test_e_item_3": {
                "#cdata-section": "校友"
            },
            "word_test_e_item_3e": {
                "#cdata-section": "alumni"
            },
            "word_test_e_ans": {
                "#cdata-section": "朋友"
            },
            "word_test_c_ques": {
                "#cdata-section": "朋友"
            },
            "word_test_c_item_1": {
                "#cdata-section": "fried"
            },
            "word_test_c_item_1c": {
                "#cdata-section": "油炸的"
            },
            "word_test_c_item_2": {
                "#cdata-section": "fired"
            },
            "word_test_c_item_2c": {
                "#cdata-section": "燃燒的"
            },
            "word_test_c_item_3": {
                "#cdata-section": "friend"
            },
            "word_test_c_item_3c": {
                "#cdata-section": "朋友"
            },
            "word_test_c_ans": {
                "#cdata-section": "friend"
            },
            "cloze_test_ques": {
                "#cdata-section": "Mandy is my best ___."
            },
            "cloze_test_item_1": {
                "#cdata-section": "friemd"
            },
            "cloze_test_item_2": {
                "#cdata-section": "fired"
            },
            "cloze_test_item_3": {
                "#cdata-section": "friend"
            },
            "cloze_test_ans": {
                "#cdata-section": "friend"
            },
            "cloze_test_ans_explain": {
                "#cdata-section": "Mandy is my best <u>friend</u>."
            },
            "cloze_test_ans_explain_c": {
                "#cdata-section": "曼蒂是我最要好的朋友。"
            },
            "vocab_e_snd": {
                "#cdata-section": "vocab_2183_e"
            },
            "sex": {
                "#cdata-section": "男"
            },
            "vocab_c_snd": {
                "#cdata-section": "0"
            }
        }, {
            "order": {
                "#cdata-section": "3"
            },
            "vocabulary_id": {
                "#cdata-section": "2184"
            },
            "lesson_id": {
                "#cdata-section": "11544en8"
            },
            "vocab": {
                "#cdata-section": "welcome"
            },
            "kk": {
                "#cdata-section": "[ˋwɛlkəm]"
            },
            "part_of_speech_1": {
                "#cdata-section": "int. 感嘆詞"
            },
            "meaning_1": {
                "#cdata-section": "歡迎"
            },
            "eg_1": {
                "#cdata-section": "Welcome to our party!"
            },
            "eg_meaning_1": {
                "#cdata-section": "歡迎來到我們的派對！"
            },
            "part_of_speech_2": {
                "#cdata-section": "adj. 形容詞"
            },
            "meaning_2": {
                "#cdata-section": "受歡迎的"
            },
            "eg_2": {
                "#cdata-section": "Darcy is a welcome guest."
            },
            "eg_meaning_2": {
                "#cdata-section": "達西是位受歡迎的客人。"
            },
            "syn": {
                "#cdata-section": "0"
            },
            "ant": {
                "#cdata-section": "0"
            },
            "n_forms": {
                "#cdata-section": "0"
            },
            "v_forms": {
                "#cdata-section": "welcomed, welcomed, welcoming"
            },
            "adj_forms_adv_forms": {
                "#cdata-section": "more welcome, most welcome"
            },
            "word_usage": {
                "#cdata-section": "welcoming party 迎新派對"
            },
            "word_test_e_ques": {
                "#cdata-section": "welcome"
            },
            "word_test_e_item_1": {
                "#cdata-section": "歡喜"
            },
            "word_test_e_item_1e": {
                "#cdata-section": "like"
            },
            "word_test_e_item_2": {
                "#cdata-section": "歡迎"
            },
            "word_test_e_item_2e": {
                "#cdata-section": "welcome"
            },
            "word_test_e_item_3": {
                "#cdata-section": "歡樂"
            },
            "word_test_e_item_3e": {
                "#cdata-section": "joy"
            },
            "word_test_e_ans": {
                "#cdata-section": "歡迎"
            },
            "word_test_c_ques": {
                "#cdata-section": "歡迎"
            },
            "word_test_c_item_1": {
                "#cdata-section": "welcome"
            },
            "word_test_c_item_1c": {
                "#cdata-section": "歡迎"
            },
            "word_test_c_item_2": {
                "#cdata-section": "well-done"
            },
            "word_test_c_item_2c": {
                "#cdata-section": "做得好的"
            },
            "word_test_c_item_3": {
                "#cdata-section": "well-born"
            },
            "word_test_c_item_3c": {
                "#cdata-section": "出身名門的"
            },
            "word_test_c_ans": {
                "#cdata-section": "welcome"
            },
            "cloze_test_ques": {
                "#cdata-section": "___ to our party!"
            },
            "cloze_test_item_1": {
                "#cdata-section": "Become"
            },
            "cloze_test_item_2": {
                "#cdata-section": "Income"
            },
            "cloze_test_item_3": {
                "#cdata-section": "Welcome"
            },
            "cloze_test_ans": {
                "#cdata-section": "Welcome"
            },
            "cloze_test_ans_explain": {
                "#cdata-section": "<u>Welcome</u> to our party!"
            },
            "cloze_test_ans_explain_c": {
                "#cdata-section": "歡迎來到我們的派對！"
            },
            "vocab_e_snd": {
                "#cdata-section": "vocab_2184_e"
            },
            "sex": {
                "#cdata-section": "男"
            },
            "vocab_c_snd": {
                "#cdata-section": "0"
            }
        }, {
            "order": {
                "#cdata-section": "4"
            },
            "vocabulary_id": {
                "#cdata-section": "2185"
            },
            "lesson_id": {
                "#cdata-section": "11544en8"
            },
            "vocab": {
                "#cdata-section": "conversation"
            },
            "kk": {
                "#cdata-section": "[͵kɑnvɚˋseʃən]"
            },
            "part_of_speech_1": {
                "#cdata-section": "n. 名詞"
            },
            "meaning_1": {
                "#cdata-section": "會話、談話"
            },
            "eg_1": {
                "#cdata-section": "Harry is having a conversation with his English teacher."
            },
            "eg_meaning_1": {
                "#cdata-section": "哈利正在和他的英文老師談話。"
            },
            "part_of_speech_2": {
                "#cdata-section": "0"
            },
            "meaning_2": {
                "#cdata-section": "0"
            },
            "eg_2": {
                "#cdata-section": "0"
            },
            "eg_meaning_2": {
                "#cdata-section": "0"
            },
            "syn": {
                "#cdata-section": "chat, discussion, dialogue"
            },
            "ant": {
                "#cdata-section": "0"
            },
            "n_forms": {
                "#cdata-section": "conversations"
            },
            "v_forms": {
                "#cdata-section": "0"
            },
            "adj_forms_adv_forms": {
                "#cdata-section": "0"
            },
            "word_usage": {
                "#cdata-section": "conversation piece 易引起話題的東西 "
            },
            "word_test_e_ques": {
                "#cdata-section": "conversation"
            },
            "word_test_e_item_1": {
                "#cdata-section": "會話、談話"
            },
            "word_test_e_item_1e": {
                "#cdata-section": "conversation"
            },
            "word_test_e_item_2": {
                "#cdata-section": "說話"
            },
            "word_test_e_item_2e": {
                "#cdata-section": "talk"
            },
            "word_test_e_item_3": {
                "#cdata-section": "閒話"
            },
            "word_test_e_item_3e": {
                "#cdata-section": "gossip"
            },
            "word_test_e_ans": {
                "#cdata-section": "會話、談話"
            },
            "word_test_c_ques": {
                "#cdata-section": "會話、談話"
            },
            "word_test_c_item_1": {
                "#cdata-section": "conversation"
            },
            "word_test_c_item_1c": {
                "#cdata-section": "會話、談話"
            },
            "word_test_c_item_2": {
                "#cdata-section": "conversion"
            },
            "word_test_c_item_2c": {
                "#cdata-section": "改變、轉變"
            },
            "word_test_c_item_3": {
                "#cdata-section": "conservation"
            },
            "word_test_c_item_3c": {
                "#cdata-section": "保護、管理"
            },
            "word_test_c_ans": {
                "#cdata-section": "conversation"
            },
            "cloze_test_ques": {
                "#cdata-section": "Harry is having a ___ with his English teacher."
            },
            "cloze_test_item_1": {
                "#cdata-section": "conversation"
            },
            "cloze_test_item_2": {
                "#cdata-section": "conversion"
            },
            "cloze_test_item_3": {
                "#cdata-section": "conversition"
            },
            "cloze_test_ans": {
                "#cdata-section": "conversation"
            },
            "cloze_test_ans_explain": {
                "#cdata-section": "Harry is having a <u>conversation</u> with his English teacher."
            },
            "cloze_test_ans_explain_c": {
                "#cdata-section": "哈利正在和他的英文老師談話。"
            },
            "vocab_e_snd": {
                "#cdata-section": "vocab_2185_e"
            },
            "sex": {
                "#cdata-section": "男"
            },
            "vocab_c_snd": {
                "#cdata-section": "0"
            }
        }, {
            "order": {
                "#cdata-section": "5"
            },
            "vocabulary_id": {
                "#cdata-section": "2186"
            },
            "lesson_id": {
                "#cdata-section": "11544en8"
            },
            "vocab": {
                "#cdata-section": "partner"
            },
            "kk": {
                "#cdata-section": "[ˋpɑrtnɚ]"
            },
            "part_of_speech_1": {
                "#cdata-section": "n. 名詞"
            },
            "meaning_1": {
                "#cdata-section": "夥伴、搭檔"
            },
            "eg_1": {
                "#cdata-section": "Andy is my partner in the English play."
            },
            "eg_meaning_1": {
                "#cdata-section": "安迪是我英文話劇的搭檔。"
            },
            "part_of_speech_2": {
                "#cdata-section": "v. 動詞"
            },
            "meaning_2": {
                "#cdata-section": "合夥、合作"
            },
            "eg_2": {
                "#cdata-section": "Annie used to partner Colby in ballroom dancing."
            },
            "eg_meaning_2": {
                "#cdata-section": "安妮曾經與寇比搭檔跳國標舞。"
            },
            "syn": {
                "#cdata-section": "companion, mate"
            },
            "ant": {
                "#cdata-section": "0"
            },
            "n_forms": {
                "#cdata-section": "partners"
            },
            "v_forms": {
                "#cdata-section": "partnered, partnered, partnering"
            },
            "adj_forms_adv_forms": {
                "#cdata-section": "0"
            },
            "word_usage": {
                "#cdata-section": "partnership 合夥關係"
            },
            "word_test_e_ques": {
                "#cdata-section": "partner"
            },
            "word_test_e_item_1": {
                "#cdata-section": "夥計"
            },
            "word_test_e_item_1e": {
                "#cdata-section": "clerk"
            },
            "word_test_e_item_2": {
                "#cdata-section": "搭救"
            },
            "word_test_e_item_2e": {
                "#cdata-section": "rescue"
            },
            "word_test_e_item_3": {
                "#cdata-section": "夥伴、搭檔"
            },
            "word_test_e_item_3e": {
                "#cdata-section": "partner"
            },
            "word_test_e_ans": {
                "#cdata-section": "夥伴、搭檔"
            },
            "word_test_c_ques": {
                "#cdata-section": "夥伴、搭檔"
            },
            "word_test_c_item_1": {
                "#cdata-section": "partner"
            },
            "word_test_c_item_1c": {
                "#cdata-section": "夥伴、搭擋"
            },
            "word_test_c_item_2": {
                "#cdata-section": "partaker"
            },
            "word_test_c_item_2c": {
                "#cdata-section": "關係者、分擔者"
            },
            "word_test_c_item_3": {
                "#cdata-section": "part age"
            },
            "word_test_c_item_3c": {
                "#cdata-section": "部份、分配"
            },
            "word_test_c_ans": {
                "#cdata-section": "partner"
            },
            "cloze_test_ques": {
                "#cdata-section": "Andy is my English play ___."
            },
            "cloze_test_item_1": {
                "#cdata-section": "partner"
            },
            "cloze_test_item_2": {
                "#cdata-section": "partaker"
            },
            "cloze_test_item_3": {
                "#cdata-section": "partier"
            },
            "cloze_test_ans": {
                "#cdata-section": "partner"
            },
            "cloze_test_ans_explain": {
                "#cdata-section": "Andy is my English play <u>partner</u>."
            },
            "cloze_test_ans_explain_c": {
                "#cdata-section": "安迪是我英文話劇的搭檔。"
            },
            "vocab_e_snd": {
                "#cdata-section": "vocab_2186_e"
            },
            "sex": {
                "#cdata-section": "男"
            },
            "vocab_c_snd": {
                "#cdata-section": "0"
            }
        }],
        "Dialogue": {
            "lesson_id": {
                "#cdata-section": "11544en2"
            },
            "lesson_title": {
                "#cdata-section": "Hello!"
            },
            "dialogue_id": {
                "#cdata-section": "dialogue_201"
            },
            "dailogue": [{
                "sentence_id": {
                    "#cdata-section": "dialogue_201_1"
                },
                "dialogue_e": {
                    "#cdata-section": "Roberto: Hi. I’m Roberto. How are you?"
                },
                "dialogue_c": {
                    "#cdata-section": "羅伯特：嗨，我是羅伯特。你好嗎？"
                },
                "sex": {
                    "#cdata-section": "男"
                },
                "dialogue_e_snd": {
                    "#cdata-section": "dialogue_201_1_e"
                },
                "dialogue_c_snd": {
                    "#cdata-section": "0"
                },
                "listening_f": {
                    "#cdata-section": "Hi"
                },
                "name": {
                    "#cdata-section": "Roberto"
                },
                "pic": {
                    "#cdata-section": "Roberto.jpg"
                },
                "serial": {
                    "#cdata-section": "1"
                }
            }, {
                "sentence_id": {
                    "#cdata-section": "dialogue_201_2"
                },
                "dialogue_e": {
                    "#cdata-section": "Gabriela: Hello. My name is Gabriela. I’m fine, thanks."
                },
                "dialogue_c": {
                    "#cdata-section": "蓋比艾拉：哈囉，我的名字是蓋比艾拉，我很好，謝謝。"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "dialogue_e_snd": {
                    "#cdata-section": "dialogue_201_2_e"
                },
                "dialogue_c_snd": {
                    "#cdata-section": "0"
                },
                "listening_f": {
                    "#cdata-section": "Hello"
                },
                "name": {
                    "#cdata-section": "Gabriela"
                },
                "pic": {
                    "#cdata-section": "Gabriela.jpg"
                },
                "serial": {
                    "#cdata-section": "2"
                }
            }, {
                "sentence_id": {
                    "#cdata-section": "dialogue_201_3"
                },
                "dialogue_e": {
                    "#cdata-section": "Roberto: Welcome to our class."
                },
                "dialogue_c": {
                    "#cdata-section": "羅伯特：歡迎你來我們班。"
                },
                "sex": {
                    "#cdata-section": "男"
                },
                "dialogue_e_snd": {
                    "#cdata-section": "dialogue_201_3_e"
                },
                "dialogue_c_snd": {
                    "#cdata-section": "0"
                },
                "listening_f": {
                    "#cdata-section": "Welcome"
                },
                "name": {
                    "#cdata-section": "Roberto"
                },
                "pic": {
                    "#cdata-section": "Roberto.jpg"
                },
                "serial": {
                    "#cdata-section": "1"
                }
            }, {
                "sentence_id": {
                    "#cdata-section": "dialogue_201_4"
                },
                "dialogue_e": {
                    "#cdata-section": "Gabriela: Thank you."
                },
                "dialogue_c": {
                    "#cdata-section": "蓋比艾拉：謝謝。"
                },
                "sex": {
                    "#cdata-section": "女"
                },
                "dialogue_e_snd": {
                    "#cdata-section": "dialogue_201_4_e"
                },
                "dialogue_c_snd": {
                    "#cdata-section": "0"
                },
                "listening_f": {
                    "#cdata-section": "Thank you"
                },
                "name": {
                    "#cdata-section": "Gabriela"
                },
                "pic": {
                    "#cdata-section": "Gabriela.jpg"
                },
                "serial": {
                    "#cdata-section": "2"
                }
            }, {
                "sentence_id": {
                    "#cdata-section": "dialogue_201_5"
                },
                "dialogue_e": {
                    "#cdata-section": "Roberto: Our teacher is Miss Smith."
                },
                "dialogue_c": {
                    "#cdata-section": "羅伯特：我們的老師是史密斯小姐。"
                },
                "sex": {
                    "#cdata-section": "男"
                },
                "dialogue_e_snd": {
                    "#cdata-section": "dialogue_201_5_e"
                },
                "dialogue_c_snd": {
                    "#cdata-section": "0"
                },
                "listening_f": {
                    "#cdata-section": "teacher"
                },
                "name": {
                    "#cdata-section": "Roberto"
                },
                "pic": {
                    "#cdata-section": "Roberto.jpg"
                },
                "serial": {
                    "#cdata-section": "1"
                }
            }],
            "pattern": {
                "pattern_id": {
                    "#cdata-section": "pattern_248"
                },
                "pattern": {
                    "#cdata-section": "Hi. I am Hans. / Hi. I’m Hans./nHello. My name is Maria. / Hello. My name’s Maria."
                },
                "pattern_c": {
                    "#cdata-section": "嗨，我是漢斯。/n哈囉，我的名字是瑪利亞。"
                },
                "pattern_explain": {
                    "#cdata-section": "與人初次見面，介紹自己的句型：/n1. 「I am + 人名」/n2. 「My name is + 人名」/nI後面使用的be動詞為am；而He、She、It及單數主詞使用的be動詞為is。/nI am = I’m/nMy name is = My name’s"
                },
                "pattern_e_snd": {
                    "#cdata-section": "pattern_248_e"
                },
                "pattern_c_snd": {
                    "#cdata-section": "0"
                },
                "pattern_explain_snd": {
                    "#cdata-section": "pattern_248_explain"
                }
            },
            "patternPractice": [{
                "pattern_id": {
                    "#cdata-section": "pattern_248"
                },
                "pattern_practice_id": {
                    "#cdata-section": "pattern_practice_248_1"
                },
                "pattern_practice_ques": {
                    "#cdata-section": "_______________."
                },
                "pattern_practice_ques_c": {
                    "#cdata-section": "嗨，我的名字是麗莎。"
                },
                "pattern_practice_explain": {
                    "#cdata-section": "<u>Hi. My name is Lisa.</u> / <u>Hi. My name’s Lisa.</u>"
                },
                "pattern_practice_e_snd": {
                    "#cdata-section": "pattern_practice_248_1_e"
                }
            }, {
                "pattern_id": {
                    "#cdata-section": "pattern_248"
                },
                "pattern_practice_id": {
                    "#cdata-section": "pattern_practice_248_2"
                },
                "pattern_practice_ques": {
                    "#cdata-section": "_______________."
                },
                "pattern_practice_ques_c": {
                    "#cdata-section": "哈囉，我是傑克。"
                },
                "pattern_practice_explain": {
                    "#cdata-section": "<u>Hello. I am Jack.</u> / <u>Hello. I’m Jack.</u>"
                },
                "pattern_practice_e_snd": {
                    "#cdata-section": "pattern_practice_248_2_e"
                }
            }, {
                "pattern_id": {
                    "#cdata-section": "pattern_248"
                },
                "pattern_practice_id": {
                    "#cdata-section": "pattern_practice_248_3"
                },
                "pattern_practice_ques": {
                    "#cdata-section": "_______________."
                },
                "pattern_practice_ques_c": {
                    "#cdata-section": "哈囉，我的名字是蒂娜。"
                },
                "pattern_practice_explain": {
                    "#cdata-section": "<u>Hello. My name is Tina.</u> / <u>Hello. My name’s Tina.</u>"
                },
                "pattern_practice_e_snd": {
                    "#cdata-section": "pattern_practice_248_3_e"
                }
            }, {
                "pattern_id": {
                    "#cdata-section": "pattern_248"
                },
                "pattern_practice_id": {
                    "#cdata-section": "pattern_practice_248_4"
                },
                "pattern_practice_ques": {
                    "#cdata-section": "_______________."
                },
                "pattern_practice_ques_c": {
                    "#cdata-section": "嗨，我是約翰。"
                },
                "pattern_practice_explain": {
                    "#cdata-section": "<u>Hi. I am John.</u> / <u>Hi. I’m John.</u>"
                },
                "pattern_practice_e_snd": {
                    "#cdata-section": "pattern_practice_248_4_e"
                }
            }, {
                "pattern_id": {
                    "#cdata-section": "pattern_248"
                },
                "pattern_practice_id": {
                    "#cdata-section": "pattern_practice_248_5"
                },
                "pattern_practice_ques": {
                    "#cdata-section": "_______________."
                },
                "pattern_practice_ques_c": {
                    "#cdata-section": "嗨，我的名字是瑪莉。"
                },
                "pattern_practice_explain": {
                    "#cdata-section": "<u>Hi. My name is Mary.</u> / <u>Hi. My name’s Mary.</u>"
                },
                "pattern_practice_e_snd": {
                    "#cdata-section": "pattern_practice_248_5_e"
                }
            }],
            "clozeTest": [{
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_1"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hi. I ___ Chris."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "is"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "am"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "are"
                },
                "cloze_test_ans": {
                    "#cdata-section": "am"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hi. I <u>am</u> Chris."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "嗨，我是克里斯。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_2"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hi. My name ___ Joey."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "am"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "is"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "are"
                },
                "cloze_test_ans": {
                    "#cdata-section": "is"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hi. My name <u>is</u> Joey."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "嗨，我的名字是喬伊。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_3"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hello. ___ am Jessica."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "I"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "She"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "My name"
                },
                "cloze_test_ans": {
                    "#cdata-section": "I"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hello. <u>I</u> am Jessica."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "哈囉，我是潔西卡。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_4"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hi. ___ is Yvonne."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "My name"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "I"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "I’m"
                },
                "cloze_test_ans": {
                    "#cdata-section": "My name"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hi. <u>My name</u> is Yvonne."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "嗨，我的名字是依芳。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_5"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hello. ___ Linda."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "My name"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "I’s"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "I am"
                },
                "cloze_test_ans": {
                    "#cdata-section": "I am"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hello. <u>I am</u> Linda."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "哈囉，我是琳達。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_6"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hi. ___ Zack."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "My name’s"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "I"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "My name"
                },
                "cloze_test_ans": {
                    "#cdata-section": "My name’s"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hi. <u>My name’s</u> Zack."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "嗨，我的名字是柴克。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_7"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hello. I ___ Ben."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "is"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "am"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "name is"
                },
                "cloze_test_ans": {
                    "#cdata-section": "am"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hello. I <u>am</u> Ben."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "哈囉，我是班。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_8"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hello. My name ___ Tracy."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "was"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "am"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "is"
                },
                "cloze_test_ans": {
                    "#cdata-section": "is"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hello. My name <u>is</u> Tracy."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "哈囉，我的名字是翠西。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_9"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hi. ___ Valerie."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "I is"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "My name’m"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "I’m"
                },
                "cloze_test_ans": {
                    "#cdata-section": "I’m"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hi. <u>I’m</u> Valerie."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "嗨，我是瓦樂莉。"
                }
            }, {
                "clozed_test_id": {
                    "#cdata-section": "clozed_test_201_10"
                },
                "cloze_test_ques": {
                    "#cdata-section": "Hello. ___ Peter."
                },
                "cloze_test_item_1": {
                    "#cdata-section": "My name’s"
                },
                "cloze_test_item_2": {
                    "#cdata-section": "I’d"
                },
                "cloze_test_item_3": {
                    "#cdata-section": "I name’s"
                },
                "cloze_test_ans": {
                    "#cdata-section": "My name’s"
                },
                "cloze_test_ans_explain": {
                    "#cdata-section": "Hello. <u>My name’s</u> Peter."
                },
                "cloze_test_ans_explain_c": {
                    "#cdata-section": "哈囉，我的名字是彼德。"
                }
            }]
        },
        "Read": {
            "lesson_id": {
                "#cdata-section": "11544en2"
            },
            "lesson_title": {
                "#cdata-section": "Hello!"
            },
            "reading_id": {
                "#cdata-section": "reading_164"
            },
            "prereading_q": {
                "#cdata-section": "Q1. Where are they?/nQ2. What are they doing?"
            },
            "prereading_c": {
                "#cdata-section": "他們在哪裡？/n他們在做什麼呢？"
            },
            "prereading_p": {
                "#cdata-section": "11544en2_prereading.jpg"
            },
            "time": {
                "#cdata-section": "2分鐘"
            },
            "reading_e": {
                "#cdata-section": "Lien is a <u>student</u> at Johnson <u>Adult School</u>. She is a good <u>student</u>.  Mario is her <u>friend</u>.  He is a <u>student</u> too.  Lien and Mario go to <u>school</u> at <u>night</u>.  Mr. Serna is a <u>teacher</u> in the <u>class</u>."
            },
            "reading_c": {
                "#cdata-section": "蓮是強生成人學校的學生，她是個好學生。馬力歐是她的朋友，他也是名學生。蓮和馬力歐在晚上的時候去上學，塞爾納先生是這班的老師。"
            },
            "reading_ep": {
                "#cdata-section": "0"
            },
            "reading_cp": {
                "#cdata-section": "0"
            },
            "reading_practice": [{
                "lesson_id": {
                    "#cdata-section": "11544en2"
                },
                "lesson_title": {
                    "#cdata-section": "Hello!"
                },
                "reading_id": {
                    "#cdata-section": "reading_164"
                },
                "reading_test_id": {
                    "#cdata-section": "reading_164_1"
                },
                "reading_test_ques": {
                    "#cdata-section": "Who is a student at Johnson Adult School?"
                },
                "reading_test_c_item_1": {
                    "#cdata-section": "Lien"
                },
                "reading_test_c_item_2": {
                    "#cdata-section": "Maria"
                },
                "reading_test_c_item_3": {
                    "#cdata-section": "Mr. Serna"
                },
                "reading_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "reading_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "reading_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "reading_test_ans": {
                    "#cdata-section": "Lien"
                },
                "reading_test_explain_c": {
                    "#cdata-section": "Lien is a student at Johnson Adult School. 蓮是強生成人學校的學生。"
                }
            }, {
                "lesson_id": {
                    "#cdata-section": "0"
                },
                "lesson_title": {
                    "#cdata-section": "0"
                },
                "reading_id": {
                    "#cdata-section": "0"
                },
                "reading_test_id": {
                    "#cdata-section": "reading_164_2"
                },
                "reading_test_ques": {
                    "#cdata-section": "Who is Lien’s friend?"
                },
                "reading_test_c_item_1": {
                    "#cdata-section": "Mr. Serna"
                },
                "reading_test_c_item_2": {
                    "#cdata-section": "Maria"
                },
                "reading_test_c_item_3": {
                    "#cdata-section": "Mario"
                },
                "reading_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "reading_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "reading_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "reading_test_ans": {
                    "#cdata-section": "Mario"
                },
                "reading_test_explain_c": {
                    "#cdata-section": "Mario is her friend. 馬力歐是她的朋友。"
                }
            }, {
                "lesson_id": {
                    "#cdata-section": "0"
                },
                "lesson_title": {
                    "#cdata-section": "0"
                },
                "reading_id": {
                    "#cdata-section": "0"
                },
                "reading_test_id": {
                    "#cdata-section": "reading_164_3"
                },
                "reading_test_ques": {
                    "#cdata-section": "Who is NOT a student?"
                },
                "reading_test_c_item_1": {
                    "#cdata-section": "Lien"
                },
                "reading_test_c_item_2": {
                    "#cdata-section": "Mario"
                },
                "reading_test_c_item_3": {
                    "#cdata-section": "Mr. Serna"
                },
                "reading_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "reading_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "reading_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "reading_test_ans": {
                    "#cdata-section": "Mr. Serna"
                },
                "reading_test_explain_c": {
                    "#cdata-section": "Mr. Serna is a teacher in the class. 塞爾納先生是這班的老師。"
                }
            }]
        },
        "Listen": {
            "lesson_id": {
                "#cdata-section": "11544en2"
            },
            "lesson_title": {
                "#cdata-section": "Hello!"
            },
            "listening": [{
                "listening_id": {
                    "#cdata-section": "listening_289"
                },
                "listening_listening": {
                    "listening_p": {
                        "#cdata-section": "listening_289_1"
                    },
                    "listening_e": {
                        "#cdata-section": "Hi! I’m Gabriela. G-A-B-R-I-E-L-A."
                    },
                    "listening_c": {
                        "#cdata-section": "嗨！我是蓋比艾拉，G-A-B-R-I-E-L-A。"
                    },
                    "sex": {
                        "#cdata-section": "女"
                    },
                    "listening_e_snd": {
                        "#cdata-section": "listening_289_1_e"
                    }
                }
            }, {
                "listening_id": {
                    "#cdata-section": "listening_290"
                },
                "listening_listening": {
                    "listening_p": {
                        "#cdata-section": "listening_290_1"
                    },
                    "listening_e": {
                        "#cdata-section": "Hello. I’m Duong. D-U-O-N-G."
                    },
                    "listening_c": {
                        "#cdata-section": "哈囉，我是都安，D-U-O-N-G。"
                    },
                    "sex": {
                        "#cdata-section": "男"
                    },
                    "listening_e_snd": {
                        "#cdata-section": "listening_290_1_e"
                    }
                }
            }, {
                "listening_id": {
                    "#cdata-section": "listening_291"
                },
                "listening_listening": {
                    "listening_p": {
                        "#cdata-section": "listening_291_1"
                    },
                    "listening_e": {
                        "#cdata-section": "Hi! I’m Susan."
                    },
                    "listening_c": {
                        "#cdata-section": "嗨！我是蘇珊。"
                    },
                    "sex": {
                        "#cdata-section": "女"
                    },
                    "listening_e_snd": {
                        "#cdata-section": "listening_291_1_e"
                    }
                }
            }, {
                "listening_id": {
                    "#cdata-section": "listening_292"
                },
                "listening_listening": {
                    "listening_p": {
                        "#cdata-section": "listening_292_1"
                    },
                    "listening_e": {
                        "#cdata-section": "Hello! My name is Bill."
                    },
                    "listening_c": {
                        "#cdata-section": "哈囉，我的名字是比爾。"
                    },
                    "sex": {
                        "#cdata-section": "男"
                    },
                    "listening_e_snd": {
                        "#cdata-section": "listening_292_1_e"
                    }
                }
            }, {
                "listening_id": {
                    "#cdata-section": "listening_293"
                },
                "listening_listening": {
                    "listening_p": {
                        "#cdata-section": "listening_293_1"
                    },
                    "listening_e": {
                        "#cdata-section": "How are you? I’m Ana."
                    },
                    "listening_c": {
                        "#cdata-section": "你好嗎？我是安娜。"
                    },
                    "sex": {
                        "#cdata-section": "女"
                    },
                    "listening_e_snd": {
                        "#cdata-section": "listening_293_1_e"
                    }
                }
            }, {
                "listening_id": {
                    "#cdata-section": "listening_294"
                },
                "listening_listening": {
                    "listening_p": {
                        "#cdata-section": "listening_294_1"
                    },
                    "listening_e": {
                        "#cdata-section": "Hi! My name is Tony."
                    },
                    "listening_c": {
                        "#cdata-section": "嗨！我的名字是湯尼。"
                    },
                    "sex": {
                        "#cdata-section": "男"
                    },
                    "listening_e_snd": {
                        "#cdata-section": "listening_294_1_e"
                    }
                }
            }],
            "listening_practice": [{
                "lesson_id": {
                    "#cdata-section": "11544en2"
                },
                "lesson_title": {
                    "#cdata-section": "Hello!"
                },
                "listening_id": {
                    "#cdata-section": "listening_289"
                },
                "listening_test_id": {
                    "#cdata-section": "listening_289_q1"
                },
                "listening_test_ques": {
                    "#cdata-section": "What’s her name?"
                },
                "listening_test_c_item_1": {
                    "#cdata-section": "Gabriela."
                },
                "listening_test_c_item_2": {
                    "#cdata-section": "Gabiela."
                },
                "listening_test_c_item_3": {
                    "#cdata-section": "Grabriela."
                },
                "listening_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "listening_test_c_ans": {
                    "#cdata-section": "Gabriela."
                }
            }, {
                "lesson_id": {
                    "#cdata-section": "0"
                },
                "lesson_title": {
                    "#cdata-section": "0"
                },
                "listening_id": {
                    "#cdata-section": "listening_290"
                },
                "listening_test_id": {
                    "#cdata-section": "listening_290_q1"
                },
                "listening_test_ques": {
                    "#cdata-section": "How do you spell his name?"
                },
                "listening_test_c_item_1": {
                    "#cdata-section": "B-U-O-N-G."
                },
                "listening_test_c_item_2": {
                    "#cdata-section": "H-U-O-N-G."
                },
                "listening_test_c_item_3": {
                    "#cdata-section": "D-U-O-N-G."
                },
                "listening_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "listening_test_c_ans": {
                    "#cdata-section": "D-U-O-N-G."
                }
            }, {
                "lesson_id": {
                    "#cdata-section": "0"
                },
                "lesson_title": {
                    "#cdata-section": "0"
                },
                "listening_id": {
                    "#cdata-section": "listening_291"
                },
                "listening_test_id": {
                    "#cdata-section": "listening_291_q1"
                },
                "listening_test_ques": {
                    "#cdata-section": "What’s her name?"
                },
                "listening_test_c_item_1": {
                    "#cdata-section": "Sue."
                },
                "listening_test_c_item_2": {
                    "#cdata-section": "Susan."
                },
                "listening_test_c_item_3": {
                    "#cdata-section": "Sushi."
                },
                "listening_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "listening_test_c_ans": {
                    "#cdata-section": "Susan."
                }
            }, {
                "lesson_id": {
                    "#cdata-section": "0"
                },
                "lesson_title": {
                    "#cdata-section": "0"
                },
                "listening_id": {
                    "#cdata-section": "listening_292"
                },
                "listening_test_id": {
                    "#cdata-section": "listening_292_q1"
                },
                "listening_test_ques": {
                    "#cdata-section": "What’s his name?"
                },
                "listening_test_c_item_1": {
                    "#cdata-section": "Fill."
                },
                "listening_test_c_item_2": {
                    "#cdata-section": "Pill."
                },
                "listening_test_c_item_3": {
                    "#cdata-section": "Bill."
                },
                "listening_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "listening_test_c_ans": {
                    "#cdata-section": "Bill."
                }
            }, {
                "lesson_id": {
                    "#cdata-section": "0"
                },
                "lesson_title": {
                    "#cdata-section": "0"
                },
                "listening_id": {
                    "#cdata-section": "listening_293"
                },
                "listening_test_id": {
                    "#cdata-section": "listening_293_q1"
                },
                "listening_test_ques": {
                    "#cdata-section": "Can you spell her name？"
                },
                "listening_test_c_item_1": {
                    "#cdata-section": "A-N-A."
                },
                "listening_test_c_item_2": {
                    "#cdata-section": "A-M-A."
                },
                "listening_test_c_item_3": {
                    "#cdata-section": "A-H-A."
                },
                "listening_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "listening_test_c_ans": {
                    "#cdata-section": "A-N-A."
                }
            }, {
                "lesson_id": {
                    "#cdata-section": "0"
                },
                "lesson_title": {
                    "#cdata-section": "0"
                },
                "listening_id": {
                    "#cdata-section": "listening_294"
                },
                "listening_test_id": {
                    "#cdata-section": "listening_294_q1"
                },
                "listening_test_ques": {
                    "#cdata-section": "What’s his name?"
                },
                "listening_test_c_item_1": {
                    "#cdata-section": "Sony."
                },
                "listening_test_c_item_2": {
                    "#cdata-section": "Tony."
                },
                "listening_test_c_item_3": {
                    "#cdata-section": "Pony."
                },
                "listening_test_c_item_4": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_5": {
                    "#cdata-section": "0"
                },
                "listening_test_c_item_6": {
                    "#cdata-section": "0"
                },
                "listening_test_c_ans": {
                    "#cdata-section": "Tony."
                }
            }]
        }
    }
}