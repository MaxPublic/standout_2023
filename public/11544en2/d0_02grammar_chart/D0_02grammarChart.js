﻿Module = function(){
	var self = this;
	var dom = document;
	this.init = function(){
		var doc = module.dataSet.DocumentElement.Info;
		this.course_id=getValue(doc.course_id);
	}
	this.start = function(){
		dom.getElementById("grammarImg").innerHTML="";
		checkGrammarChart(self.course_id,self.showImg);
	}
	this.showImg = function(img){
		for(var i=0;i<img.length;i=i+1){
			if(i < img.length-1){
				dom.getElementById("grammarImg").innerHTML+="<img src='"+img[i]+"'/><P>";
			}else{
				dom.getElementById("grammarImg").innerHTML+="<img src='"+img[i]+"'/>";
			}
		}
	}
}; 