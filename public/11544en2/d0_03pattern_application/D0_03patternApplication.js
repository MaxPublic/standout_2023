﻿Module = function(){
	var self=this;
	var dom = document;
	self.dataNum=0;
	this.init = function(){
		var doc = getForObj(module.dataSet.DocumentElement.Dialogue.patternPractice);
		this.ques =new Array();
		this.ques_c =new Array();
		this.explain=new Array();
		this.e_snd = new Array();
		for(var i=0;i<doc.length;i=i+1){
			this.ques[i]=getValue(doc[i].pattern_practice_ques);
			this.ques_c[i]=getValue(doc[i].pattern_practice_ques_c);
			this.explain[i]=getValue(doc[i].pattern_practice_explain);
			this.e_snd[i]=getValue(doc[i].pattern_practice_e_snd);
		}
		this.openAllBut = true;
		//this.originalPatternTable = dom.getElementById("patternTable").cloneNode(true);
		this.mp = new MediaPlayer(dom);
	}
	this.start = function(){
		var patternDiv = dom.getElementById("patternDiv")
		var patternTable = patternDiv.getElementsByTagName("table")[1].cloneNode(true);
		patternDiv.innerHTML="";
		var data=self.ques;
		var ques_c=self.ques_c;
		var explain=self.explain;
		var e_snd=self.e_snd;
		var dataNum=self.dataNum;
		for(var i=0;i<data.length;i=i+1){
			var clonePatternTable = patternTable.cloneNode(true);
			var clonePatternTd = clonePatternTable.getElementsByTagName("td");
			if(data[i]){
				if(i===0){
					clonePatternTd[2].innerHTML='<font class="txta_16_blue">'+self.processLineUtags(data[i],ques_c[i])+'</font>'
					clonePatternTd[5].getElementsByTagName("img")[0].parentNode.removeChild(clonePatternTd[5].getElementsByTagName("img")[0]);
					clonePatternTd[5].getElementsByTagName("div")[0].style.display="";
				}else{
					clonePatternTd[2].getElementsByTagName("font")[0].innerHTML=self.processLineUtags(data[i],ques_c[i]);
				}
				clonePatternTd[3].getElementsByTagName("img")[0].setAttribute("mp3",e_snd[i]);
				clonePatternTd[5].getElementsByTagName("div")[0].innerHTML=self.processLineUtags(explain[i],ques_c[i]);
				patternDiv.appendChild(clonePatternTable);
			}
		}
	}
	this.processLineUtags = function(ques,ques_c){
		var t1=ques;
    	var t2=ques_c;    
    	var t11 = t1.split(ModuleSet.REPLACEBR);
    	var strArray = new Array();
    	for(var i=0;i<t11.length;i=i+1){
    		if(t11[i].indexOf("<u>")!=-1 || t11[i].indexOf("___")!=-1){
				strArray.push(t11[i]+ModuleSet.REPLACEBR+"（"+t2+"）");
    		}else{
				strArray.push(t11[i]);
    		}
    	}
    	var tt="";
    	for(var i=0;i<strArray.length;i=i+1){
    		if(i==strArray.length-1){
    			tt+=strArray[i];
    		}else{
    			tt+=strArray[i]+ModuleSet.REPLACEBR;
    		}
    	}
		return tt;
	}
	this.openAll = function(obj){
		var patternTable = dom.getElementById("patternDiv");
		var table = patternTable.getElementsByTagName("table");
		if(self.openAllBut){
			obj.src="images/btnw_hideanswer.png";
			self.openAllBut=false;
			for(var i=0;i<table.length;i=i+1){
				var img=table[i].getElementsByTagName("img")[1];
				if(img){
					self.openWord(img,"open");
				}
			}	
		}else{
			obj.src="images/btnw_showanswer.png";
			self.openAllBut=true;
			for(var i=0;i<table.length;i=i+1){
				var img=table[i].getElementsByTagName("img")[1];
				if(img){
					self.openWord(img,"cloase");
				}
			}	
		}
	}
	this.openWord = function(obj,act){
		if(act){
			if(act === "open"){
				obj.style.display="";
			}else{
				obj.style.display="none";
			}
		}
		if(obj.style.display!==""){
			prev(obj).style.display="none";
			obj.style.display="";
		}else{
			var img = prev(prev(obj.parentNode)).getElementsByTagName("img")[0];
			if(img.getAttribute("push") === "true" || act){
				prev(obj).style.display="";
				obj.style.display="none";
			}
		}
		var patternDiv = dom.getElementById("patternDiv");
		var tables = patternDiv.getElementsByTagName("table");
		var opened = 0;
		var nowOpen = 0;
		for(var i=0;i<tables.length;i=i+1){
			if(i!==0){
				var img = tables[i].getElementsByTagName("img")[1];
				opened++;
				if(img.style.display){
					nowOpen++;
				}
			}else{
				opened++;
				nowOpen++;
			}
		}
		if(nowOpen === opened){
			dom.getElementById("openAllWord").src="images/btnw_hideanswer.png";
			self.openAllBut=false;
		}
		self.obj = obj;
	}
	this.play = function(but){
		var stickyImg = but.parentNode.parentNode.getElementsByTagName("td")[5].getElementsByTagName("img")[0];
		if(stickyImg){
			stickyImg.setAttribute("onClick","clickBut('chickWord',this);");
		}
		but.setAttribute("push","true");
		but.src="images/btn_sound_04.png";
		self.mp.url(decodeURI(getBaseFile()+but.getAttribute("mp3")+".mp3"));
		self.mp.Play();
	}
}; 

