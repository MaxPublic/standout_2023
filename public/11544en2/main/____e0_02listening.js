/**2023/2/10 Max */

/**2023/2/23 Tony */
let templete = {}
window.onload = function () {
    data = reduce(data)
    console.log(JSON.stringify(data)); //數據最佳化

    // 取得資料
    let listen = data.DocumentElement.Listen.listening;
    let practice = data.DocumentElement.Listen.listening_practice;
    console.log(practice);

    // 按下播放鍵放出聲音
    let ary = [];
    // let au = [];
    for (let i = 0; i < listen.length; i++) {
        ary.push('../material/' + listen[i].listening_listening.listening_e_snd + '.mp3');
    }

    new MaxMediaPlayer(ary, $("[name='btn_play']"), $("[name='progressBar']"), () => {
        console.log('完成');
    })

    // 載入操作說明圖片
    $('[name="start"]').attr('src', '../e0_02listening/images/ex_e0_02.png');

    // 按下開始進入測驗畫面
    $('.startbutton').click(function () {
        $('.start').hide();
        $('.read').show();
    });

    // 樣版帶入
    // templete['plyplace'] = $('[name="plyplace"]')[0].outerHTML;
    // templete['radio'] = $('[name="radio"]')[0].outerHTML;

    // $('[name="content"]').empty();
    // $('[name="answer"]').empty();

    // let p = $(templete['plyplace']);
    // let r = $(templete['radio']);

    // for (let i = 0; i < practice.length; i++) {
    //     $('[name="listening_test_ques"]', p).text(practice[i].listening_test_ques);
    //     $('[name="content"]').append(p);

    //     $('[name="listening_test_c_item_1"]', r).text(practice[i].listening_test_c_item_1);
    //     $('[name="listening_test_c_item_2"]', r).text(practice[i].listening_test_c_item_2);
    //     $('[name="listening_test_c_item_3"]', r).text(practice[i].listening_test_c_item_3);

    //     $('[name="answer"]').append(r);
    // }

    // 單選完後確定按鈕會出現
    $('.confirm').hide();
    $('.radio').click(function () {
        $('.confirm').show();
    });
}