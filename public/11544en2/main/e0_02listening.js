/**
 * 聽音辨字
 */
let templete = {}

window.onload = function () {
    data = reduce(data)
    // console.log(JSON.stringify(data))
    // return;
    word = reduce(word)
    let QuizUi = $('[name="Quiz"]')
    $('[name="QuizContainer"]').empty();

    $('[name="btn_quizStart"]').hide();
    // jsonData = [
    //     {
    //         title: "題目title_0",
    //         title_pic: '11544en2.png',
    //         title_mp3: 'vocab_2182_e',
    //         option_title: ['aaa', 'bbb', 'ccc'],
    //         option_pic: ['11544en2_prereading.jpg', 'Gabriela.jpg', 'Roberto.jpg'],
    //         option_mp3: ['vocab_2183_e', 'vocab_2184_e', 'vocab_2185_e'],
    //         ans: [0,1],
    //         feedback:'feedback內容0',
    //         submit: [1]
    //     },
    //     {
    //         title: "題目title_1",
    //         title_pic: '11544en2.png',
    //         title_mp3: 'vocab_2182_e',
    //         option_title: ['aaa', 'bbb', 'ccc'],
    //         option_pic: ['11544en2_prereading.jpg', 'Gabriela.jpg', 'Roberto.jpg'],
    //         option_mp3: ['vocab_2183_e', 'vocab_2184_e', 'vocab_2185_e'],
    //         ans: [0],
    //         feedback:'feedback內容1',
    //         submit: [0]
    //     },
    //     {
    //         title: "題目title_2",
    //         title_pic: '11544en2.png',
    //         title_mp3: 'vocab_2182_e',
    //         option_title: ['ddd', 'eee', 'fff'],
    //         option_pic: ['11544en2.png', '11544en2_grammar chart.png', 'Roberto.jpg'],
    //         option_mp3: ['vocab_2183_e', 'vocab_2184_e', 'vocab_2185_e'],
    //         ans: [1],
    //         feedback:'feedback內容2',
    //         submit: [2]
    //     },
    //     {
    //         title: "題目title_3",
    //         title_pic: '11544en2.png',
    //         title_mp3: 'vocab_2182_e',
    //         option_title: ['ggg', 'hhh', 'jjj'],
    //         option_pic: ['11544en2.png', '11544en2_grammar chart.png', 'Roberto.jpg'],
    //         option_mp3: ['vocab_2183_e', 'vocab_2184_e', 'vocab_2185_e'],
    //         ans: [2],
    //         feedback:'feedback內容3',
    //         submit: [2]
    //     }
    // ]
    // jsonData=Quiz.convertDataToExamJson(data,type='listen_words')

    // console.log(jsonData)

    let QuizObj = new Quiz(data, word, QuizUi, 'listening')
    QuizObj.addEventStatusChange((status = 'testing') => {
        //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
        console.log(status)
    })
    QuizObj.addEventEnd((data) => {
        //data={score:50,right:5,wrong:5}
        console.log(data)
    })
    $('[name="QuizContainer"]').append(QuizObj.view);
    
    $('[name="btn_quizStart"]').click((e) => {
    })

}