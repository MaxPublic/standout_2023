let templete = {}
window.onload = function () {
    data = reduce(data);
    word = reduce(word);
    // console.log(JSON.stringify(word))/**數據最佳化 */
    let audio = new Audio();
    const wordList = word.DocumentElement.Word;
    // templete['wordSearch'] = $('[name="wordSearch"]')[0].outerHTML;
    templete['entochtContent'] = $('[name="entochtContent"]')[0].outerHTML;
    templete['btn'] = $('[name="btn"]')[0].outerHTML;
    templete['showDownLeft'] = $('[name="showDownLeft"]')[0].outerHTML;
    $('[name="showDownLeft"]').remove()
    let btnstyle = $('[name="btnstyle"]')
    let btnSlected = $('[name="btn_selected"]');
    let m = data.DocumentElement.Read;



    function parseText(input) {
        const buttonRegex = /<u>(.*?)<\/u>/g;
        const lineBreakRegex = /\/n\/n/g;
      
        let parsedText = input.replace(buttonRegex, (match, p1) => {
            let btn= $(templete['btn'])
            btn.html(p1);
          return btn[0].outerHTML;
        //   `<span class="button-text">${p1}</span>`;
        });
      
        parsedText = parsedText.replace(lineBreakRegex, "<br><br>");
        return parsedText;
    }
    if(true){
        let selectedButton = null;
        // let p = $('<div/>')
        // $(templete['wordSearch']);
        // p.html(parseText( m.reading_e));
        $('[name="reading_e"]').html(parseText( m.reading_e));
        $('[name="reading_c"]').html(m.reading_c.replace(/\/n\/n/g, "<br><br>"));
        $('.button-text').each(function() {
            $(this).replaceWith($('<button>', {
            text: $(this).text(),
            click: function() {
                console.log($(this).text());
            }
            }));
        });
        $('[name="en_to_cht"]').click(function () {
            // if (isClick) {
            if ($('.EnToCht').is(':visible')) {
                $(this).text('開啟英中對照');
                $('.EnToCht').hide();
            }else{
                // isClick = false;
            // } else {
                $(this).text('關閉英中對照');
                $('.EnToCht').show();
                // isClick = true;
            }
        });
        $('[name="word_search"]').click(function () {
            // showOrHide = !showOrHide
            let a = $('[name="btn"]')
            if ( $(this).text()=='開啟字義解析') {
                $(this).text('關閉字義解析');
                for (let i = 0; i < a.length; i++) {
                    toggleElements($(a[i]), btnstyle, false, true);
                }
    
            } else {
                $(this).text('開啟字義解析');             
                for (let i = 0; i < a.length; i++) {
                    toggleElements($(a[i]), btnstyle, false, false);
                    toggleElements(selectedButton, btnSlected, false, false);
                }
    
            }
        })
        // $('[name="wordSearch"]').append(p);
        $('btn').unbind('click').bind('click', function (event) {
            if ($('[name="word_search"]').text()=='開啟字義解析') {
                return;
            }

            if (selectedButton !== null) {
                toggleElements(selectedButton, btnSlected, false, false);
            }
            selectedButton = $(this);
            // $(event.currentTarget)
            toggleElements(selectedButton, btnSlected, false, true);

            $('[name="showLeft"]').empty();
            for (let i = 0; i < wordList.length; i++) {
                if ((wordList[i].word).toLowerCase() == ($(this).text()).toLowerCase()) {

                    // 英文單字
                    let c = $(templete['showDownLeft']);
                    $('[name="popword"]', c).html(wordList[i].meaning);
                    // 詞性
                    $('[name="part_of_speech"]', c).text(wordList[i].part_of_speech);

                    $('[name="showLeft"]').css({
                        "left": event.pageX - 20,
                        "top": event.pageY + 30
                    });
                    $('[name="showLeft"]').addClass('showList');
                    $('[name="showLeft"]').append(c);

                    let mp3 = '../vocabWizard/' + wordList[i].voice + '.mp3';
                    play(mp3);
                    return;
                }
            }

        });
    }





// return;

//     // 開啟字義解析
//     $('[name="reading_e"]').empty();
//     let p = $(templete['wordSearch']);
//     m.reading_e=m.reading_e.replace(/\/n\/n/g, "<br><br>")
//     // console.log(m.reading_e)
//     $('[name="reading_e"]', p).text(m.reading_e);
//     $('[name="wordSearch"]').append(p);
// // return;

//     // 開啟英中對照內容
//     $('[name="entochtContent"]').empty();
//     let s = $(templete['entochtContent']);
//     m.reading_c=m.reading_c.replace(/\/n\/n/g, "<br><br>")
//     $('[name="reading_c"]', s).html(m.reading_c);
//     $('[name="EnToCht"]').append(s);

    
//     // const resultList = $.map(wordList, function (item) {
//     //     return item.word;
//     // });
//     const resultList = wordList.map(item => Object.values(item)[1]);
//     const escapedWordList = resultList.map(w => w.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'));
//     // console.log(escapedWordList);

//     // for (let i = 0; i < wordList.length; i++) {
//     //     // const words = wordList[i].word;
//     //     // console.log(words);
//     //     const escapedWord = resultList.map(w => w.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'));
//     //     o.push(escapedWord);
//     // }
//     const articleElement = document.querySelector(".wordSearch");
//     const articleContent = articleElement.textContent;

//     const regex = new RegExp(`\\b(${escapedWordList.join("|")})\\b`, "gi");

//     const highlightedContent = articleContent.replace(regex, templete['btn']);
//     console.log(highlightedContent)
//     articleElement.innerHTML = highlightedContent;



//     let isClick = false;
//     $('[name="en_to_cht"]').click(function () {
//         if (isClick) {
//             $(this).text('開啟英中對照');
//             $('.EnToCht').hide();
//             isClick = false;
//         } else {
//             $(this).text('關閉英中對照');
//             $('.EnToCht').show();
//             isClick = true;
//         }
//     });


//     // 字義解析顏色
//     let isBtnClick = false;
//     let selectedButton = null;
//     let showOrHide = false;
//     $('[name="word_search"]').click(function () {
//         showOrHide = !showOrHide
//         let a = $('[name="btn"]')
//         if (showOrHide) {
//             $(this).text('關閉字義解析');
//             for (let i = 0; i < a.length; i++) {
//                 toggleElements($(a[i]), btnstyle, false, true);
//             }

//         } else {
//             $(this).text('開啟字義解析');             
//             for (let i = 0; i < a.length; i++) {
//                 toggleElements($(a[i]), btnstyle, false, false);
//                 toggleElements(selectedButton, btnSlected, false, false);
//             }

//         }

//         $('[name="showLeft"]').empty();

//         if (isBtnClick) {
//             isBtnClick = false;
//         } else {
//             isBtnClick = true;
//             $('btn').unbind('click').bind('click', function (event) {
//                 if (!isBtnClick) {
//                     return;
//                 }

//                 if (selectedButton !== null) {
//                     toggleElements(selectedButton, btnSlected, false, false);
//                 }
//                 selectedButton = $(this);
//                 // $(event.currentTarget)
//                 toggleElements(selectedButton, btnSlected, false, true);

//                 $('[name="showLeft"]').empty();
//                 for (let i = 0; i < wordList.length; i++) {
//                     if ((wordList[i].word).toLowerCase() == ($(this).text()).toLowerCase()) {

//                         // 英文單字
//                         let c = $(templete['showDownLeft']);
//                         $('[name="popword"]', c).html(wordList[i].meaning);
//                         // 詞性
//                         $('[name="part_of_speech"]', c).text(wordList[i].part_of_speech);

//                         $('[name="showLeft"]').css({
//                             "left": event.pageX - 20,
//                             "top": event.pageY + 30
//                         });
//                         $('[name="showLeft"]').addClass('showList');
//                         $('[name="showLeft"]').append(c);

//                         let mp3 = '../vocabWizard/' + wordList[i].voice + '.mp3';
//                         play(mp3);
//                         return;
//                     }
//                 }

//             });
//         }
//     });

    function play(mp3) {
        audio.src = mp3;
        audio.play();
    }

}