/**2023/2/10 Tony*/

let templete = {}
window.onload = function () {
    data = reduce(data)
    console.log(JSON.stringify(data))/**數據最佳化 */
    let audio = new Audio();

    $('.startbutton').on('click', function () {
        $('.instruction').css('display', 'none');
        $('.application').css('display', 'block');
    });

    templete['list'] = $('[name="list"]')[0].outerHTML;
    
    let m = data.DocumentElement.Dialogue.patternPractice;
    $('[name="list_pattern"]').empty();
    
    let io = []; //宣告空陣列io
    for (let i = 0; i < m.length; i++) {
        io.push(0); //將空陣列塞0進去
        let p = $(templete['list']);
        
        $('[name="question"]', p).html('_____________________________________.'+"<br>"+'(' + m[i]['pattern_practice_ques_c'] + ')');        

        $('[name="content"]', p).html(m[i]['pattern_practice_explain'] );      

        $('[name="list_pattern"]').append(p);        
        let mp3 = '../material/' + m[i].pattern_practice_e_snd + '.mp3';
        $('[name="tip"]', p).click(function () {
            io[i] = 1; // 直接將i帶入1
            play(mp3);
        });       

        $('[name="pic"]:first').hide();
        $('[name="content"]:first').show();       

        $('[name="pic"]', p).on('click', function () {
            if (io[i] == 0) {
                return;
            }
            $(this).css('display', 'none');

            if ($('.content', p).css('display', 'block')) {
                $('[name="closeAllWord"]').show();
                $('[name="openAllWord"]').hide();
            } else {
                $('[name="closeAllWord"]').hide();
                $('[name="openAllWord"]').show();
            }
        });



        $('[name="openAllWord"]').click(function () {
            $(this).hide();
            $('.closeAllWord').show();
            $('[name="pic"]', p).css('display', 'none');
            $('[name="content"]', p).css('display', 'block');
            $('[name="pic"]:first').hide();
            $('[name="content"]:first').show();
        });

        $('[name="closeAllWord"]').click(function () {
            $(this).hide();
            $('.openAllWord').show();
            $('[name="pic"]', p).css('display', 'block');
            $('[name="content"]', p).css('display', 'none');
            $('[name="pic"]:first').hide();
            $('[name="content"]:first').show();
        });

    }
    function play(mp3) {
        audio.src = mp3;
        audio.play();
    }

    // 2023.5.23 Tony 隨視窗換圖片
    $(window).resize(function () {
        let width = $(window).width();
        if (width < 768) {
            $('.tip img').attr('src', '../images_ui/002.svg');
        } else {
            $('.tip img').attr('src', '../images_ui/003.svg');
        }
    });

}