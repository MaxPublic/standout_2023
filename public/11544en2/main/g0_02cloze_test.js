/**
 * 讀後答題
 */
 let templete = {}

 window.onload = function () {
     data = reduce(data)
    //  console.log(JSON.stringify(data))
    //  return;
    word=reduce(word)
    let all=$('[name="all"]')[0].outerHTML
    start()
    function start(){
        $('body').empty()
        $('body').append($(all))
        let QuizUi=$('[name="Quiz"]')
        $('[name="QuizContainer"]').empty();
        $('[name="btn_quizStart"]').click((e)=>{
            var checkboxes = $('[name="firstpage_view"] [name="exam"]')
            var types = $('input[type=checkbox]:checked').map(function() {
               return $(this).val();
             }).get();
           if(types.length<=0){
               return;
           }
           $('[name="firstpage_view"]').hide()
           // console.log(types);
           // return;
           //  var values = [];
           //  // 循环遍历每个复选框，判断它是否被选中
           //  for (var i = 0; i < checkboxes.length; i++) {
           //    if (checkboxes[i].checked) {
           //      // 如果被选中，将其值添加到结果数组中
           //      values.push(checkboxes[i].value);
           //    }
           //  }
           //  // 返回结果数组
           //  console.log(values);
   
   
   
            let QuizObj=new Quiz(data,word,QuizUi,types,0)
            QuizObj.addEventStatusChange((status='testing')=>{
                //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
                console.log(status)
            })
            QuizObj.addEventEnd((data)=>{
                //data={score:50,right:5,wrong:5}
                console.log(data)
            })
            QuizObj.addResetCallback((goNext)=>{
               //已經完全結束，如果需要重新下一題，就呼叫這個函數
               //或自行定義其他行為。
               console.log('全部結束，重新開始')
               start();
            //    $('body').empty()
            //    $('body').append($(all))
            //    QuizUi=$('[name="Quiz"]')
            //    $('[name="QuizContainer"]').empty();
            //    $('[name="firstpage_view"]').show()
               // goNext()
            })
            $('[name="QuizContainer"]').append(QuizObj.view);
        })
    
    }
  
 }