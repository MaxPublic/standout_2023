//2023.4.7 Tony
let templete = {}
window.onload = function () {
    data = reduce(data);
    // word = reduce(word);
    $('.box2').hide();

    let audio = new Audio();

    let btnSlected = $('[name="selected"]');

    let m = data.DocumentElement.Listen.listening;

    templete["button"]=$('[name="button"]')[0].outerHTML;

    $('[name="btn_container"]').empty()
    let lct=$('[name="cn"]');
    let llt=$('[name="eng"]');


    $('[name="cn"]').hide();
    $('.line').hide();
    let isClick = false;
    $('[name="en_to_cht"]').click(function () {
        isClick=!isClick
        if (isClick) {
            $(this).text('關閉英中對照');
            $('[name="cn"]').show();
            $('.line').show();
        } else {
            $(this).text('開啟英中對照');
            $('[name="cn"]').hide();
            $('.line').hide();
        }
    });

    let selectedButton = null;
    for(let i=0;i<m.length;i++){
        let p=$(templete["button"]);
        p.i=i;
        p.text(i+1);
        $('.button:first').click();
        $('[name="btn_container"]').append(p);        
        p.unbind('click').bind('click',(e)=> {
            lct.text(m[p.i].listening_listening.listening_c)
            llt.text(m[p.i].listening_listening.listening_e)
            if (selectedButton !== null) {
                toggleElements(selectedButton, btnSlected, false, false);
            }
            selectedButton = p;
            toggleElements(selectedButton, btnSlected, false, true);

            $('[name="btn_play"]').unbind('click').bind('click',function(){
                let mp3='../material/'+m[i].listening_listening.listening_e_snd+'.mp3';
                play(mp3);
            });   
        });          
        
    }


    function play(mp3) {
        audio.src = mp3;
        audio.play();
    }

     // 2023.5.26 Tony 播放按鈕圖片切換
     $('[name="btn_play"]').click(function () {
        let img = $('[name="btn_play"] img');
        if (img.attr('src') == '../main_book/play.svg') {
            img.attr('src', '../main_book/stop.svg');
        } else {
            img.attr('src', '../main_book/play.svg');
        }
    });

}
