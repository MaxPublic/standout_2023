$(document).ready(function () {
    if ($(window).width() <= 768) {
        $('.mb_title').text('Job interviews');
        $('.title').text(' ');
        $('.down_right').insertBefore('.down_left');
        $('.intro').text('本課將教你如何預備面試，並回答在面試中可能會被問到的問題。文法句型解析則會教你過去簡單式的否定形態，讓你可以說明過去沒有做過甚麼樣的事情。');
    } else {
        $('.title').text('Job interviews');
        $('.mb_title').text(' ');
        $('.down_left').insertBefore('.down_right');
        $('.intro').html('本課將教你如何預備面試，並回答在面試中可能會被問到的問題。<br>文法句型解析則會教你過去簡單式的否定形態，<br>讓你可以說明過去沒有做過甚麼樣的事情。');
    }



    $(window).on('resize', function () {
        if ($(window).width() <= 768) {
            $('.mb_title').text('Job interviews');
            $('.title').text(' ');
            $('.down_right').insertBefore('.down_left');
            $('.intro').text('本課將教你如何預備面試，並回答在面試中可能會被問到的問題。文法句型解析則會教你過去簡單式的否定形態，讓你可以說明過去沒有做過甚麼樣的事情。');
        } else {
            $('.title').text('Job interviews');
            $('.mb_title').text(' ');
            $('.down_left').insertBefore('.down_right');
            $('.intro').html('本課將教你如何預備面試，並回答在面試中可能會被問到的問題。<br>文法句型解析則會教你過去簡單式的否定形態，<br>讓你可以說明過去沒有做過甚麼樣的事情。');
        }
    });

    

});

