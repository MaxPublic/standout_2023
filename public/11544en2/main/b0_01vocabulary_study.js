/**2023/2/6 Max*/

let templete={}
window.onload=function(){
    data=reduce(data)
    console.log(JSON.stringify(data))/**數據最佳化 */
    let audio = new Audio();
    templete['item']=$('[name="item"]')[0].outerHTML
    templete['item_selected']=$('[name="item_selected"]')[0].outerHTML
    let s=$(templete['item_selected'])//第二種狀態
    $('[name="item"]').remove()
    $('[name="item_selected"]').remove()
    $('[name="list_contanin"]').empty()
    let m=data.DocumentElement.Vocabulary
    for(let i=0;i<m.length;i++){
        let p=$(templete['item'])
        $('[name="numindex"]',p).text(i+1)
        p.attr('no',i);
        $('[name="list_contanin"]').append(p)
        let mp3='../material/'+m[i].vocab_e_snd+'.mp3'
        hidedata(p)
        p.click((e)=>{
            e.preventDefault();
            let p=$(e.currentTarget)
            if(p.attr('toggle')=='true'){
                hidedata(p);
                p.removeClass('clicked');               
            }else{
                showdata(p);
                play(mp3)
                p.addClass('clicked');   
            }
            
            
            toggleElements(p,s)
            console.log('no:',p.attr('no'))
        })
    }

    
    $('[name="openall"]').click((e)=>{ 
       $('[name="openall"]').toggleClass("clicked");

        e.preventDefault();
        let mm=$('[name="list_contanin"]')
        console.log(mm.children().length)
        let io=false;
        for(let i=0;i<mm.children().length;i++){
            let p=$(mm.children()[i])
            if(!p.attr('toggle')){
                io=true;
                break;
            }
        }
        for(let i=0;i<mm.children().length;i++){
            let p=$(mm.children()[i])
            if(io){
                if(p.attr('toggle')=='true'){
                    continue;
                }
                showdata(p);
                p.addClass('clicked'); 
            }else{
                if(!p.attr('toggle')){
                    continue;
                }
                hidedata(p);
                p.removeClass('clicked'); 
            }
            toggleElements(p,s,true,true,i*100)
        }        
    })
    function showdata(p){
        let i=p.attr('no');
        p.attr('toggle','true')
        $('[name="corner"]',p).addClass('corner');//2023.5.16 Tony 新增
        $('[name="corner"]',p).html('<div class="v"><img src="../main_book/002.svg" alt=""></div>');//2023.5.16 Tony 新增
        $('[name="vocab"]',p).text(m[i].vocab).addClass('color');//2023.5.26 Tony 新增
        $('[name="meaning"]',p).text(m[i].meaning_1)
        $('[name="kk"]',p).text(m[i].kk)
        $('[name="point"]',p).text('.............................................');
        $('[name="speech"]',p).text('('+m[i].part_of_speech_1+')') 
    }
    function hidedata(p){
        let i=p.attr('no');
        p.attr('toggle',null)
        $('[name="corner"]',p).removeClass('corner');//2023.5.16 Tony 新增
        $('[name="corner"]',p).html('');//2023.5.16 Tony 新增
        $('[name="vocab"]',p).text(m[i].vocab).removeClass('color')//2023.5.26 Tony 新增
        $('[name="meaning"]',p).text('')
        $('[name="kk"]',p).text('')
        $('[name="point"]',p).text('');
        $('[name="speech"]',p).text('') 
    }
    function play(mp3){
        audio.src = mp3;
        audio.play();
    }
    console.log(templete['item'])

    // 2023.5.16 Tony 漢堡選單動畫
    $(".ham").click(function(){
        $(this).toggleClass("active");
      });
}

