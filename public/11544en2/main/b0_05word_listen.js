/**
 * 看字辨音
 */
let templete = {}

window.onload = function () {
    data = reduce(data)
    word = reduce(word)
    let QuizUi = $('[name="Quiz"]')
    $('[name="QuizContainer"]').empty();
    $('[name="btn_quizStart"]').hide();

    let QuizObj = new Quiz(data, word, QuizUi, 'words_listen')
    QuizObj.addEventStatusChange((status = 'testing') => {
        //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
        console.log(status)
    })
    QuizObj.addEventEnd((data) => {
        //data={score:50,right:5,wrong:5}
        console.log(data)
    })
    $('[name="QuizContainer"]').append(QuizObj.view);

    $('[name="btn_quizStart"]').click((e) => {
    })

}