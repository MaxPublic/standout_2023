/**2023/2/10 Max */

let templete = {}
window.onload = function () {
    data = reduce(data)
    let lesson_id = data.DocumentElement.Dialogue.lesson_id;
    let dt = data.DocumentElement.Dialogue.dailogue;
    console.log(lesson_id, dt);
    let box1 = $('[name="box1"]')
    let box2style = $($('[name="box2"]')[0].outerHTML)
    $('[name="allother"]').remove();
    let index = 0;
    $('[name="role1"]').text(dt[index].dialogue_e.split(':')[0])
    $('[name="role2"]').text(dt[index + 1].dialogue_e.split(':')[0])

    let ary = [];

    for (let i = 0; i < dt.length; i++) {
        ary.push('../material/' + dt[i].dialogue_e_snd + '.mp3')
    }
    let mediaplayer = new MaxMediaPlayer(ary, $("[name='btn_play']"), $("[name='progressBar']"), () => {
        console.log('完成')
    })
    mediaplayer.addEventChangeIndex((newIndex) => {
        newfocus(newIndex)
    })



    $('[name="btn_toggleChinese"]').click((e) => {
        if (box1.is(':hidden')) {
            $(e.currentTarget).text('關閉會話內容')
            box1.show()
        } else {
            $(e.currentTarget).text('開啟會話內容')
            box1.hide();
        }
    })

    // 2023.3.22 Tony 播放速度文字切換
    $('[name="btn_speed"]').click(function () {
        if ($('[name="btn_speed"]').text() == "0.75 x") {
            $('[name="btn_speed"]').text("1 x");
        } else if ($('[name="btn_speed"]').text() == "1 x") {
            $('[name="btn_speed"]').text("1.25 x");
        } else if ($('[name="btn_speed"]').text() == "1.25 x") {
            $('[name="btn_speed"]').text("1.5 x");
        }
    });

    // 2023.3.21 Tony 播放按鈕圖片切換
    $('[name="btn_play"]').click(function () {
        let img = $('[name="btn_play"] img');
        if (img.attr('src') == '../main_book/play.svg') {
            img.attr('src', '../main_book/stop.svg');
        } else {
            img.attr('src', '../main_book/play.svg');
        }
    });

    $('[name="container"]').empty()
    function newfocus(newIndex) {
        index = newIndex;
        $('[name="texteng"]').text(dt[index].dialogue_e.split(':')[1])
        if (index % 2 == 1) {
            toggleElements(box1, box2style, false, true);
        } else {
            toggleElements(box1, box2style, false, false);
        }

    }

}