﻿Module = function(){
	var self=this;
	this.moduleSelf = self;
	var dom = document;
	var dataNum=0;
	var error=0;
	var optionOnclick = new Array();
	var wordSearch;
	var EnToCht;
	this.init = function(){
		_importParentClass("StringBuilder");
		var doc = module.dataSet.DocumentElement.Read;
		this.reading_e = getValue(doc.reading_e);
		this.reading_c = getValue(doc.reading_c);
		this.vocabWizard = new VocabWizard(module.wordSet.DocumentElement.Word,self,window);
		this.mp = new MediaPlayer(dom);
	}
	this.start = function(){
		wordSearch = dom.getElementById("wordSearch").getElementsByTagName("td")[5];
		EnToCht = dom.getElementById("EnToCht").getElementsByTagName("td")[5];
		var sb = self.vocabWizard.vocabularyWizardOutput(self.reading_e,new StringBuilder());
		wordSearch.innerHTML=sb.toString();
		EnToCht.innerHTML=sb.toString();
		dom.getElementById("EnToCht").getElementsByTagName("td")[7].innerHTML=self.reading_c;
	}
	this.EnToCht = function(img){
		if(dom.getElementById("EnToCht").style.display===""){
			img.src="images/btnw_s_translation_.png"
			dom.getElementById("wordSearch").style.display="";
			dom.getElementById("EnToCht").style.display="none";
		}else{
			dom.getElementById("EnToCht").style.display="";
			img.src="images/btnw_h_translation_.png"
			var span = wordSearch.getElementsByTagName("span");
			dom.getElementById("wordSearch").style.display="none";
		}
		var wordSearchBut = dom.getElementById("wordSearchBut");
		if(wordSearchBut.getAttribute("push") === "true"){
			wordSearchBut.setAttribute("push","false");
			self.wordSearch(wordSearchBut);
		}else{
			wordSearchBut.setAttribute("push","true");
			self.wordSearch(wordSearchBut);
		}
		self.wordClear();
		dom.getElementById("showLeft").style.display="none";
	}
	this.wordSearch = function(img){
		var span;
		var vocabWizard = this.vocabWizard;
		if(dom.getElementById("EnToCht").style.display===""){
			span = EnToCht.getElementsByTagName("span");
		}else{
			span = wordSearch.getElementsByTagName("span");
		}
		if(img.getAttribute("push")===null || img.getAttribute("push") === "false"){	
			img.setAttribute("push","true");
			for(var i=0;i<span.length;i=i+1){
				span[i].setAttribute("onclick",'module.vocabWizard.wordShow(this);bubbleStop(event);');
			}
			img.src="images/btnw_h_dictionary_.png";
			vocabWizard.spanAddUnderLine(dom);
			//vocabWizard.wordShow(span[0]); //按下字彙精靈後，請將第一個單字預設打開
		}else{
			img.setAttribute("push","false");
			self.wordClear();
			dom.getElementById("showLeft").style.display="none";
			img.src="images/btnw_s_dictionary_.png";
			vocabWizard.spanRemoveUnderLine(dom);
		}
	}
	this.wordClear = function(){
		var span;
		if(dom.getElementById("EnToCht").style.display===""){
			span = EnToCht.getElementsByTagName("span");
		}else{
			span = wordSearch.getElementsByTagName("span");
		}
		for(var i=0;i<span.length;i=i+1){
			span[i].style.color="black";
		}		
	}
}; 