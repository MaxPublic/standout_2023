﻿Module = function(){
	var self = this;
	var dom = document;
	var iframe = new Object();
	this.init = function(){
		var doc = getForObj(module.dataSet.DocumentElement.Vocabulary);
		this.data =new Array();
		this.dataCht=new Array();
		this.wordMp3=new Array();
		this.kk=new Array();
		this.part_of_speech_1 = new Array();
		this.eg_1=new Array();
		this.eg_meaning_1 = new Array();
		for(var i=0;i<doc.length;i=i+1){
			this.data[i]=getValue(doc[i].vocab);
			this.dataCht[i]=getValue(doc[i].meaning_1);
			this.wordMp3[i]=getValue(doc[i].vocab_e_snd);
			this.kk[i]=getValue(doc[i].kk);
			this.part_of_speech_1[i]=getValue(doc[i].part_of_speech_1);
			this.eg_1[i]=getValue(doc[i].eg_1);
			this.eg_meaning_1[i]=getValue(doc[i].eg_meaning_1);
		}
		this.mp = new MediaPlayer(dom);
	}
	this.start = function(){
		var data = self.data;
		var dataCht = self.dataCht;
		var wordMp3 = self.wordMp3;
		var kk = self.kk;
		var part_of_speech_1 = self.part_of_speech_1;
		var list = dom.getElementById("list");
		var table = list.getElementsByTagName("table")[0].cloneNode(true);
		var table1 = list.getElementsByTagName("table")[1].cloneNode(true);
		list.innerHTML="";
		for(var i=0;i<data.length;i=i+1){
			var cloneTable = table.cloneNode(true);
			if(i%2 !== 0){
				cloneTable.style.backgroundColor="#f5f5f5";
			}
			var cloneTable1 = table1.cloneNode(true);
			var cloneTd = cloneTable.getElementsByTagName("td");
			cloneTd[0].innerHTML = (i+1)+".";
			cloneTd[1].getElementsByTagName("img")[0].setAttribute("mp3",wordMp3[i]);
			var font = cloneTd[2].getElementsByTagName("font");
			font[0].innerHTML=data[i]+"&nbsp;";
			font[1].innerHTML=kk[i];
			font[2].innerHTML="&nbsp;("+part_of_speech_1[i]+")&nbsp;"+dataCht[i];
			list.appendChild(cloneTable);
			list.appendChild(cloneTable1);
		}
	}
	this.play = function(but){
		but.src="images/btn_sound_04.png";
		self.mp.Stop();
		self.mp.url(decodeURI(getBaseFile()+but.getAttribute("mp3")+".mp3"));
		self.mp.Play();
	}
}; 