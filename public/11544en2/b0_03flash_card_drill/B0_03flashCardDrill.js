Module = function(){
	var self=this;
	var dom = document;
	var nowAction = true;
	var intervalTime = 3000;
	var dataNum=0;
	this.init = function(){
		var doc = getForObj(module.dataSet.DocumentElement.Vocabulary);
		this.data =new Array();
		this.dataCht=new Array();
		this.wordMp3=new Array();
		for(var i=0;i<doc.length;i=i+1){
			this.data[i]=getValue(doc[i].vocab);
			this.dataCht[i]=getValue(doc[i].meaning_1);
			this.wordMp3[i]=getValue(doc[i].vocab_e_snd);
		}
		if(module.mainMP){
			this.mp = module.mainMP;
		}else{
			this.mp = new MediaPlayer(dom);
		}
		self.controlRadio("disabled",true);
	}
	this.start = function(){
		nowAction=true;
		var data = self.data;
		if(data.length > dataNum){
			dom.getElementById("examNum").innerHTML='<font class="txta_24_orange"> '+(dataNum+1)+'</font><font class="txta_20_black"> / '+data.length+'</font>';
			self.pause();
			self.playWord();
			self.nextPage();
//            clickBut('touch',dom.getElementById("playButton"));
		}else{
			dataNum=0;
			var obj = dom.getElementById("playButton");
			obj.src = "images/btnw_rest.png";
			obj.onclick = function(){
				this.src = "images/btnw_pause.png";			
				this.setAttribute("touch",false);
				this.onclick = function(){
					clickBut('pause',dom.getElementById("playButton"));
				}
				clickBut('start');
			}
			self.pause();
		}
	}
	this.nextPage = function(){
		var data = self.data;
		var dataCht = self.dataCht;
		var upImg = dom.getElementById("up").parentNode.getElementsByTagName("img")[0];
		var downImg = dom.getElementById("down").parentNode.getElementsByTagName("img")[0];
		if (usefloor(0, 10) < 5) {
			upImg.style.display="none";
			dom.getElementById("up").style.display="";
			downImg.style.display="";
			dom.getElementById("down").style.display="none";
		}else{
			upImg.style.display="";
			dom.getElementById("up").style.display="none";
			downImg.style.display="none";
			dom.getElementById("down").style.display="";
		}
		dom.getElementById("up").innerHTML=data[dataNum];
		dom.getElementById("down").innerHTML=dataCht[dataNum++];
	}
	this.openCard = function(){
		var upImg = dom.getElementById("up").parentNode.getElementsByTagName("img")[0];
		var downImg = dom.getElementById("down").parentNode.getElementsByTagName("img")[0];
		if(upImg.style.display === ""){
			upImg.style.display="none";
			dom.getElementById("up").style.display="";
		}else{
			downImg.style.display="none";
			dom.getElementById("down").style.display="";
		}
		self.pause();
		self.waitTime(self.start);
		nowAction=false;
	}
	this.touch = function(obj){
		if (obj.getAttribute("touch") === "true") {
			obj.setAttribute("touch",false);
			obj.src = "images/btnw_pause.png";
			self.controlRadio("disabled",true);
			self.startTime();
		}else{
			obj.setAttribute("touch",true);
			obj.src = "images/btnw_start.png";
			self.controlRadio("disabled",false);
			self.pause();
		}
	}
	this.controlRadio = function(control,disabled,checked){
		var input = dom.getElementById("radio").getElementsByTagName("td");
		switch(control){
			case "disabled":
				for(var i=0;i<input.length;i=i+1){
					var img = input[i].getElementsByTagName("img")[0];
					if(img){
						var font = input[i].getElementsByTagName("font")[0];
						var childFont = font.getElementsByTagName("font")[0];
						if(disabled){
							img.style.cursor='';
							if(img.src.indexOf("checkon") === -1){
								font.setAttribute("class","txt_16_gray");
								childFont.setAttribute("class","txta_16_gray");
								img.src="images/icon_checkoffg.png";
								img.removeAttribute("onclick");
							}else{
								font.setAttribute("class","txt_16_black");
								childFont.setAttribute("class","txta_16_black");
								img.onclick=function(){
									clickBut("changeSec",this);
								}	
							}
						}else{
							img.style.cursor='pointer';
							if(img.src.indexOf("checkon") === -1){
								font.setAttribute("class","txt_16_black");
								childFont.setAttribute("class","txta_16_black");
								img.src="images/icon_checkoff.png";
								img.onclick=function(){
									clickBut("changeSec",this);
								}	
							}
						}
					}
				}
				break;
			case "checked":
				for(var i=0;i<input.length;i=i+1){
					var img = input[i].getElementsByTagName("img")[0];
					if(img){
						var font = input[i].getElementsByTagName("font")[0];
						var childFont = font.getElementsByTagName("font")[0];
						font.setAttribute("class","txt_16_black");
						childFont.setAttribute("class","txta_16_black");
						img.src="images/icon_checkoff.png";
						img.onclick=function(){
							clickBut("changeSec",this);
						}
					}
				}
				break;
		}
	}
	this.startTime = function(){
		if (nowAction) {
			self.waitTime(self.openCard);
		}else{
			self.waitTime(self.start);
		}
	}
	this.playWord = function(){
//		self.mp.setOnPlayEnd(self.playState);
//		self.mp.url(decodeURI(getBaseFile()+self.wordMp3[dataNum]+".mp3"));
//		self.mp.Play();
        self.playState();
	}
	this.pause = function(){
		self.mp.setOnPlayEnd(undefined);
		self.mp.Stop();
		clearTimeout(self.examTimer);
	}
	this.waitTime = function(fn){
		self.examTimer = setTimeout(fn,intervalTime);
	}
	this.playState = function(){
		self.waitTime(self.openCard);
		nowAction=true;
	}
	this.changeWaitTime = function(img){
		self.controlRadio("checked",null,false);
		var input = img.parentNode;
		var font = input.getElementsByTagName("font")[0];
		var childFont = font.getElementsByTagName("font")[0];
		font.setAttribute("class","txt_16_black");
		childFont.setAttribute("class","txta_16_black");
		img.src="images/icon_checkon.png";
		img.onclick=function(){
			clickBut("changeSec",this);
		}
		intervalTime=parseInt(img.getAttribute("value"));
	}
}; 

