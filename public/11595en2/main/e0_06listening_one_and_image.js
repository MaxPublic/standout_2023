/**
 * E0_06 聽力訓練_單題+圖片
 * 
 */
let templete = {}

window.onload = function () {
    data = reduce(data)
    // console.log(JSON.stringify(data))
    // return;
    word = reduce(word)
    // $('[name="btn_play"]').first().hide();
    // $('[name="pic"]').first().hide();
    let audio = new Audio();
    let mp3='listening_listening[k].listening_e_snd'
    // data.DocumentElement.Listen.listening.listening_listening.listening_e_snd;
    //播放音樂mp3函式
    let QuizUi = $('[name="Quiz"]')
    $('[name="QuizContainer"]').empty();

    $('[name="pic"]').first().show();
    $('[name="btn_play"]').first().show();
    $('[name="btn_play"]').unbind('click').bind('click', (e) => {
        play('../material/' + mp3 + '.mp3')
    })

    $('[name="btn_quizStart"]').hide();

    let QuizObj = new Quiz(data, word, QuizUi, 'listening_one_and_image', 1, 1);
    QuizObj.addEventStatusChange((status = 'testing') => {
        //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
        console.log(status)
    })
    QuizObj.addGoNextCallback((index) => {
        // if(_picary.length-1>=index){
        // $('[name="pic"]').first().attr('src','../material/'+data.DocumentElement.Info.course_id+'_listening_1.jpg')
        // }
    })
    QuizObj.addEventEnd((data) => {
        //data={score:50,right:5,wrong:5}
        console.log(data)
    })
    $('[name="QuizContainer"]').append(QuizObj.view);
    $('[name="btn_quizStart"]').click((e) => {
    })
    function play(mp3) {
        audio.src = mp3;
        audio.play();
    }

}