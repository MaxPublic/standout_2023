/**2023/2/6 Max*/

// let {items,ans}=randomItems()
// console.log(items,ans)
function randomItems(items=[["A","B","C"],["a.mp3","b.mp3","c.mp3"],["a.png","b.png","c.png"]],ans=[0,1]){//陣列混亂，並且正確答案會修正成混亂後的結果
    //題目本身可能具備文字、圖片與聲音，所以要連同那些項目一併洗牌卻不可以使其錯誤
    //可以允許空陣列輸入
    //可允許不限數量的陣列
    if(items.length==0 || ans.length==0){
        return {items:[],ans:[]}
    }
    items=Array.from(new Set(items));
    items=padArrays(...items)
    ans=Array.from(new Set(ans));
    let a=[]
    for(let j=0;j<items[0].length;j++){
        let b=[]
        a.push([b,j])//索引值
        for(let i=0;i<items.length;i++){
            b.push(items[i][j])
        }
    }
    a.sort(() => Math.random() - 0.5);
    let aa=[]
    let newans=[]
    let newitems=[]
    // console.log(a);
    for(let i=0;i<a.length;i++){//找出新正確答案
        // console.log(a[i][1],'=======',ans,ans.indexOf(a[i][1]))
        // if(ans.indexOf(a[i][1])>-1){
        //     newans.push(i)
            // (ans.indexOf(a[i][1]))
        if(ans.includes(a[i][1])){
            newans.push(i)
        }
        aa.push(a[i][0])
    }
    
    for(let j=0;j<aa[0].length;j++){//回復結構
        let bb=[]
        for(let i=0;i<aa.length;i++){
            bb.push(aa[i][j])
        }
        newitems.push(bb)
    }
    // console.log(newitems,newans)
    newitems=removeEmptyArrays(...newitems)
    return {items:newitems,ans:newans};
    function padArrays(...arrays) {//將不足的陣列補齊。padArrays(arr1, arr2, arr3)
        const maxLength = arrays.reduce((max, arr) => Math.max(max, arr.length), 0);
        return arrays.map(arr => [...arr, ...Array(maxLength - arr.length).fill('')]);
    }
    function removeEmptyArrays(...arrays) {//將前面補齊空字串陣列的項目變成空陣列，保持輸出輸入一致
        return arrays.map(arr => arr.filter(item => item !== '') || []);
    }
}
function AnsIsEqual(arr1=[0,1], arr2=[1,0]) {//比對兩陣列(答案)是否一致。
    const sortedArr1 = Array.from(new Set(arr1)).sort();
    const sortedArr2 = Array.from(new Set(arr2)).sort();
    return JSON.stringify(sortedArr1) === JSON.stringify(sortedArr2);
}
function reduce(obj/**json */){//數據最佳化
    // 此函數必須要能同時進行word.js與data.js的最佳化
    let data=go2(obj);
    let hasVocabulary=false
    try {
        if(data.DocumentElement.Vocabulary) {
            hasVocabulary=true;
        } else {
            hasVocabulary=false
        }
    } catch(err) {
        hasVocabulary=false
    }
    if(!hasVocabulary){
        //如果沒有這個節點 data.DocumentElement.Vocabulary，則代表是 word文件
        return data;
    }
    for (let i=0;i<data.DocumentElement.Vocabulary.length;i++){
        let p=data.DocumentElement.Vocabulary[i]
        p['word_test_e_item']=[p['word_test_e_item_1'],p['word_test_e_item_2'],p['word_test_e_item_3']]
        p['word_test_e_item_e']=[p['word_test_e_item_1e'],p['word_test_e_item_2e'],p['word_test_e_item_3e']]
        p['word_test_e_ans']=find(p['word_test_e_item'],p['word_test_e_ans'])
        p['word_test_c_item']=[p['word_test_c_item_1'],p['word_test_c_item_2'],p['word_test_c_item_3']]
        p['word_test_c_item_c']=[p['word_test_c_item_1c'],p['word_test_c_item_2c'],p['word_test_c_item_3c']]
        p['word_test_c_ans']=find(p['word_test_c_item'],p['word_test_c_ans'])
        p['cloze_test_item']=[p['cloze_test_item_1'],p['cloze_test_item_2'],p['cloze_test_item_3']]
        p['cloze_test_ans']=find(p['cloze_test_item'],p['cloze_test_ans'])
        p['word_test_e_item_1']+='===廢止=='
        p['word_test_e_item_2']+='===廢止=='
        p['word_test_e_item_3']+='===廢止=='
        p['word_test_e_item_1e']+='===廢止=='
        p['word_test_e_item_2e']+='===廢止=='
        p['word_test_e_item_3e']+='===廢止=='
        p['word_test_c_item_1']+='===廢止=='
        p['word_test_c_item_3']+='===廢止=='
        p['word_test_c_item_3']+='===廢止=='
        p['word_test_c_item_1c']+='===廢止=='
        p['word_test_c_item_2c']+='===廢止=='
        p['word_test_c_item_3c']+='===廢止=='
        p['cloze_test_item_1']+='===廢止=='
        p['cloze_test_item_2']+='===廢止=='
        p['cloze_test_item_3']+='===廢止=='
        p["part_of_speech_2"]=p["part_of_speech_2"]=='0'?'':p["part_of_speech_2"]
        p["meaning_2"]=p["meaning_2"]=='0'?'':p["meaning_2"]
        p["eg_2"]=p["eg_2"]=='0'?'':p["eg_2"]
        p["eg_meaning_2"]=p["eg_meaning_2"]=='0'?'':p["eg_meaning_2"]
        p["syn"]=p["syn"]=='0'?'':p["syn"]
        p["ant"]=p["ant"]=='0'?'':p["ant"]
        p["n_forms"]=p["n_forms"]=='0'?'':p["n_forms"]
        p["v_forms"]=p["v_forms"]=='0'?'':p["v_forms"]
        p["adj_forms_adv_forms"]=p["adj_forms_adv_forms"]=='0'?'':p["adj_forms_adv_forms"]
    }
    for (let i=0;i<data.DocumentElement.Listen.listening_practice.length;i++){
        let p=data.DocumentElement.Listen.listening_practice[i]
        let ary=[]
        for (let j=0;j<6;j++){
            let _p=p['listening_test_c_item_'+(j+1)]
            p['listening_test_c_item_'+(j+1)]+='===廢止=='
            if(_p!='0'){
                ary.push(_p)
            }
        }
        let ans=find(ary,p['listening_test_c_ans'])
        p['listening_test_c_item']=ary;
        p['listening_test_c_ans']+='===廢止=='
        p['listening_test_cc_ans']=ans;

    }
    for (let i=0;i<data.DocumentElement.Read.reading_practice.length;i++){
        let p=data.DocumentElement.Read.reading_practice[i]
        let ary=[]
        for (let j=0;j<6;j++){
            let _p=p['reading_test_c_item_'+(j+1)]
            p['reading_test_c_item_'+(j+1)]+='===廢止=='
            if(_p!='0'){
                ary.push(_p)
            }
        }
        let ans=find(ary,p['reading_test_ans'])
        p['reading_test_c_item']=ary;
        p['reading_test_ans']+='===廢止=='
        p['reading_test_cc_ans']=ans;

    }
    for(let i=0;i<data.DocumentElement.Dialogue.clozeTest.length;i++){
        let p=data.DocumentElement.Dialogue.clozeTest[i]
        let ary=[p.cloze_test_item_1,p.cloze_test_item_2,p.cloze_test_item_3];
        let ans=find(ary,p.cloze_test_ans);
        p["cloze_test_item_1"]+='===廢止==';
        p["cloze_test_item_2"]+='===廢止==';
        p["cloze_test_item_3"]+='===廢止==';
        p["cloze_test_ans"]+='===廢止==';
        p['cloze_test_item']=ary;
        p['cloze_test_cc_ans']=ans;
    }
    if(data.DocumentElement.Read.writing_test){
        let ary=[]
        for(let i=0;i<10;i++){
            let p=data.DocumentElement.Read.writing_test['writing_test_c_item_'+(i+1)]
            if(p){
                ary.push(p)
                data.DocumentElement.Read.writing_test['writing_test_c_item_'+(i+1)]+='===廢止=='
            }
        }
        data.DocumentElement.Read.writing_test['writing_test_c_item']=ary;
    }
    return data;
    function find(dt,ans='正解1;正解2'){
        let newary=[]
        let ansary=ans.split(';')
        for (let i=0;i<dt.length;i++){
            if(ansary.includes(dt[i])){
            // if(dt[i]==ans){
                newary.push(i);
            }
        }
        return newary;
    }
    function go2(obj){
        let g={}
        if(Array.isArray(obj)){
            g=[]
        }
        for(let i in obj){
            if (i=='#cdata-section'){
                // console.log(Object.keys(obj).length)
                return obj[i];
            }else{
                if(typeof(obj[i])==='object'){
                    g[i]=go2(obj[i])
                }else{
                    g[i]=obj[i]
                }
            }
        }
        return g;  
    }
}
function toggleElements(element1, element2,toggle=true,add=true,delay=0) {//動態轉場[jq元素1,jq元素2,是否toggle,疊加或刪除,延遲毫秒]
    // 設定參數的預設值，防止參數缺失的情況
    setTimeout(()=>{
        element1 = element1 || $();
        element2 = element2 || $();
        
        // 取得兩個元素的子元素，並進行對應
        if (element1.attr("class") && element2.attr("class")) {
            if(toggle){
                element1.toggleClass(element2.attr("class"));
            }else{
                if(add){
                    element1.addClass(element2.attr("class"));
                }else{
                    element1.removeClass(element2.attr("class"));
                }
            }
        }
        element1.children().each(function(index, elem1) {
            elem1=$(elem1)
            var elem2 = $(element2.children()[index]);
            // 判斷兩個子元素是否都有 class，若都有則進行 toggleClass
            if (elem1.attr("class") && elem2.attr("class")) {
                if(toggle){
                    elem1.toggleClass(elem2.attr("class"));
                }else{
                    if(add){
                        elem1.addClass(elem2.attr("class"));
                    }else{
                        elem1.removeClass(elem2.attr("class"));
                    }
                }
            }
            // 若其中一個沒有 class，則進行遞迴呼叫，對應子元素
            else {
                toggleElements($(elem1), $(elem2),toggle,add);
            }
        });
    },delay);
 
}
class MaxMediaPlayer{
    constructor(mp3files=[],playBtn,progressBar,CompletePreparationCallback){
        let _self=this;
        let audios=[];
        let totalDuration = 0;
        let audioLengths=[0]
        let currentAudio = 0;
        let oldindex=-1
        let ChangeIndexCallback;
        createAudios(mp3files)


        _self.totalDuration=()=>{
            return totalDuration;
        }
        // _self.play=()=>{
        //     go()
        // }
        _self.playindex=(index,autonext)=>{
            allpause()
            currentAudio=index;
            audios[index].currentTime=0
            audios[index].play()
        }
        _self.addEventChangeIndex=(callback)=>{//當切換了音檔的時候
            ChangeIndexCallback=callback;
        }
        function createAudios(files){
            if(files.length==0){
                console.log('媒體陣列為空')
                return;
            }
            let i=0;
            newa(files[i])
            function newa(file){
                newAudio(file,(data)=>{
                    audios.push(data.audio)
                    totalDuration += data.duration;
                    audioLengths.push(totalDuration)
                    if(i>=files.length-1){
                        CompletePreparationCallback&&CompletePreparationCallback();
                        progressBar.attr("max", totalDuration);
                        playBtn.show();
                        setInterval(function() {
                            if(currentAudio>=audioLengths.length){
                                progressBar.val(progressBar.attr("max"));
                            }else{
                                
                                
                                progressBar.val( audioLengths[currentAudio]+audios[currentAudio].currentTime);
                                let g=gettimeindex(progressBar.val())
                                // console.log('==',g,oldindex,progressBar.val())
                                if (g!=oldindex){
                                    //索引改變，通知監聽事件
                                    ChangeIndexCallback&&ChangeIndexCallback(g);
                                    oldindex=g;
                                }
                            }
                        }, 50);
                        playBtn.click(function() {
                            go();
                        });
                    }else{
                        i++;
                        newa(files[i])
                    }
                })
            }
        }
        function newAudio(file,callback){
            let aud = new Audio();
            aud.src = file;//audioFiles[currentAudio];
            aud.preload = "auto";
            aud.addEventListener("loadedmetadata", function() {
                callback&&callback({audio:aud,duration:aud.duration})
            });
            aud.addEventListener("ended", function() {
                aud.pause();
                currentAudio++;
                if (currentAudio >= audios.length) {
                    currentAudio--;
                    return;
                }
                audios[currentAudio].play();
            });
        }
        function allpause(){
            for(let i=0;i<audios.length;i++){
                audios[i].pause()
                audios[i].currentTime =0
            }
        }
        function gettimeindex(val){//拿取目前的索引位置
            for (let i =0;i<audioLengths.length-1;i++){
                if(val>=audioLengths[i]&& val<audioLengths[i+1]){
                    return i;
                }
            }
            return audioLengths.length-1
        }
        function go(){
            allpause()
            currentAudio=0;
            audios[currentAudio].play();


            progressBar.on("input", function() {
                let desiredTime = progressBar.val();
                let index=gettimeindex(desiredTime)
                allpause()
                currentAudio=index
                audios[index].currentTime = desiredTime-audioLengths[index]
                audios[index].play()
            });
        }

    }
}
class Quiz{
    static convertDataToExamJson(datajs/**必須要已經reduce後的資料 */,wordjs/**必須要已經reduce後的資料 */,type='listen_words'/**測驗的類型，可陣列*/){//轉換測驗資料
        let _a={
            title: "題目title_0",
            title_pic: '11544en2.png',
            title_mp3: 'vocab_2182_e',
            option_title: ['aaa', 'bbb', 'ccc'],
            option_pic: ['11544en2_prereading.jpg', 'Gabriela.jpg', 'Roberto.jpg'],
            option_mp3: ['vocab_2183_e', 'vocab_2184_e', 'vocab_2185_e'],
            ans: [0,1],
            submit: [1]
        }
        let _typeary = typeof type === 'string' ? [type] : Array.isArray(type) ? type : [];
        console.log(_typeary)
        let data =datajs;
        let word =wordjs;
        let newdata=[];
        
        let ary=[];
        for(let i=0;i<_typeary.length;i++){
            switch(_typeary[i]){
                case 'listen_words':
                    //聽音辨字
                    ary=ary.concat(listen_words())
                    break;
                case 'words_listen':
                    //看字辨音
                    ary=ary.concat(words_listen())
                    break;
                case "spelling_practice":
                    //拼字練習
                    ary=ary.concat(spelling_practice())
                    break;
                case "listening":
                    //聽力練習
                    ary=ary.concat(listening())
                    break;
                case "listening_group":
                    //聽力練習_題組題
                    ary=ary.concat(listening_group())
                    break;
                case "reading_comprehension":
                    //讀後答題
                    ary=ary.concat(reading_comprehension())
                    break;
                case "unscramble_sentences":
                    //短句重組
                    ary=ary.concat(unscramble_sentences())
                    break;

                case "vocabulary_test":
                    //單字配對vocabulary_test
                    ary=ary.concat(vocabulary_pair())
                    break;
                case "cloze_test":
                    //克漏字cloze_test
                    ary=ary.concat(cloze_choice())
                    break;
                case "listening_test":
                    //聽力填空listening_test
                    ary=ary.concat(listening_fillin())
                    break;


                case "cloze_vocabulary":
                    //單字克漏字cloze_vocabulary
                    ary=ary.concat(cloze_vocabulary())
                    break;
                case "listening_test_sentence":
                    //聽力填空_句子listening_test_sentence
                    ary=ary.concat(listening_test_sentence())
                    break;
                case "writing_test":
                    //短文填空
                    ary=ary.concat(writing_test_pair())
                    break;
                default:
                    continue;
            }
        }
        return ary;
        function unscramble_sentences(){//短句重組
            // DocumentElement
            // Dialogue
            // patternPractice
            let allitems = data.DocumentElement.Dialogue.patternPractice;
            let _ss=[]
            
            // jsonData[_index].title.replace(/\/n/g, "<br>");
            for (let i=0;i<allitems.length;i++){
                // console.log(i,_ss.length)
                let tit=allitems[i].pattern_practice_explain.replace(/<[^>]+>/g, '')
                if(tit.endsWith('.')) {
                    tit = tit.slice(0, -1);
                }
                console.log(tit)
                let ans= Array(tit.split(' ').length).fill().map((_, index) => index)
                console.log('option title=',tit.split(' '))
                let fb=allitems[i].pattern_practice_explain.replace(/\/n/g, "<br>")+"<br><br>"+allitems[i].pattern_practice_ques_c.replace(/\/n/g, "<br>")
                _ss.push(
                    {
                        type:'pair',
                        subtype:'writing_test',
                        title: tit,
                        title_pic: '',
                        title_mp3:'',
                        option_title:tit.split(' '),
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:fb,
                        // +'<br><br>'+allitems.writing_test_explain_c,
                        submit:[]
                    }              
                )
            }
            return shuffleArray(_ss);  
        }
        function writing_test_pair(){//短文填空(配對題)
            let allitems = data.DocumentElement.Read.writing_test;
            let _ss=[]
            let ans= Array(allitems.writing_test_c_item.length).fill().map((_, index) => index)
            let fb=allitems.writing_test_ques.replace(/\/n/g, "<br>")+"<br><br>"+allitems.writing_test_explain_c.replace(/\/n/g, "<br>")
            // jsonData[_index].title.replace(/\/n/g, "<br>");
            _ss.push(
                {
                    type:'pair',
                    subtype:'writing_test',
                    title: allitems.writing_test_ques,
                    title_pic: '',
                    title_mp3:'',
                    option_title:allitems.writing_test_c_item,
                    option_pic:[],
                    option_mp3:[],
                    ans:ans,
                    feedback:fb,
                    // +'<br><br>'+allitems.writing_test_explain_c,
                    submit:[],
                }              
            )
            return shuffleArray(_ss);
        }
        function vocabulary_pair(){//單字配對(配對題)
            let m = JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary))
            for (let i=0;i<m.length;i++){
                m[i].order=i;
            }
            let _ss=[];
            let examNum=parseInt(m.length/3);//總題數，以總數量/3來決定
            for (let j=0;j<examNum;j++){
                m=shuffleArray(m);
                let feedback=''
                let title=[]
                let option_title=[];
                let mp3=[];
                let _ans=[];
                for(let i=0;i<Math.min(m.length,6);i++){
                    if(i<3){
                        title.push(m[i].vocab)
                        _ans.push(i)
                        mp3.push(m[i].vocab_e_snd)
                    }
                    option_title.push([m[i].meaning_1,i])
                    feedback+=`<div>${m[i].vocab}</div><br><div>${m[i].meaning_1}</div><br>`;
                    // allitems[i].dialogue_e+'<br>'+allitems[i].dialogue_c,
                }
                let ans=[]
                let pary=[]
                option_title=shuffleArray(option_title);
                // console.log(option_title,'pppp')
                for (let i=0;i<title.length;i++){
                    for(let j=0;j<option_title.length;j++){
                        if(option_title[j][1]==i){
                            ans.push(j)
                            break;
                        }
                    }
                }
                for(let j=0;j<option_title.length;j++){
                    pary.push(option_title[j][0])
                }
                // let {items,ans}=randomItems([option_title],_ans)
                _ss.push(
                    {
                        type:'pair',
                        subtype:'vocabulary_test',
                        title: title,
                        title_pic: '',
                        title_mp3:mp3,
                        option_title:pary,
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:feedback,
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
            function getRandomArray(length) {
                var randomSet = new Set();
                while (randomSet.size < length) {
                  randomSet.add(Math.floor(Math.random() * length));
                }
                return Array.from(randomSet);
            }
        }
        function listening_fillin(){//聽力填空
            let allitems = data.DocumentElement.Dialogue.dailogue;
            let _ss=[];
            for (let i=0;i<allitems.length;i++){
                _ss.push(
                    {
                        type:'fillin',
                        subtype:'listening_test',
                        title: allitems[i].dialogue_e,
                        title_pic: '',
                        title_mp3:allitems[i].dialogue_e_snd,
                        option_title:[allitems[i].listening_f],
                        option_pic:[],
                        option_mp3:[],
                        ans:[0],
                        feedback:allitems[i].dialogue_e+'<br>'+allitems[i].dialogue_c,
                        inputtext:'',
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function cloze_vocabulary(){// 單字克漏字
            let allitems = data.DocumentElement.Vocabulary;
            let _ss=[];
            for (let i=0;i<Math.min(allitems.length,10);i++){
                let {items,ans}=randomItems([allitems[i].cloze_test_item],allitems[i].cloze_test_ans)
                _ss.push(
                    {
                        type:'choice',
                        subtype:'cloze_vocabulary',
                        title: allitems[i].cloze_test_ques,
                        title_pic: '',
                        title_mp3:'',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:allitems[i].cloze_test_ans_explain_c,
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function listening_test_sentence(){//聽力填空_句子
            let allitems = data.DocumentElement.Dialogue.patternPractice;
            let _ss=[];
            for (let i=0;i<allitems.length;i++){
                let opttit=allitems[i].pattern_practice_explain.replace(/<\/?[^>]+(>|$)/g, "")
                _ss.push(
                    {
                        type:'fillin',
                        subtype:'listening_test_sentence',
                        title: opttit,
                        title_pic: '',
                        title_mp3:allitems[i].pattern_practice_e_snd,
                        option_title:[opttit],
                        option_pic:[],
                        option_mp3:[],
                        ans:[0],
                        feedback:allitems[i].pattern_practice_explain+'<br>'+allitems[i].pattern_practice_ques_c,
                        inputtext:'',
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function cloze_choice(){//克漏字
            let allitems = data.DocumentElement.Dialogue.clozeTest;
            let _ss=[];
            for (let i=0;i<allitems.length;i++){
                let {items,ans}=randomItems([allitems[i].cloze_test_item],allitems[i].cloze_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        subtype:'cloze_test',
                        title: allitems[i].cloze_test_ques,
                        title_pic: '',
                        title_mp3:'',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:allitems[i].cloze_test_ans_explain_c,
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function reading_comprehension(){//讀後答題
            let allitems = data.DocumentElement.Read.reading_practice;
            let _ss=[];
            for (let i=0;i<allitems.length;i++){
                let {items,ans}=randomItems([allitems[i].reading_test_c_item],allitems[i].reading_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        title: allitems[i].reading_test_ques,
                        title_pic: '',
                        title_mp3:'',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:allitems[i].reading_test_explain_c,
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function listening(){//聽力練習
            let listen = data.DocumentElement.Listen.listening;
            let practice = data.DocumentElement.Listen.listening_practice;
            let _ss=[];
            for (let i=0;i<listen.length;i++){
                let {items,ans}=randomItems([practice[i].listening_test_c_item],practice[i].listening_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        title: '',
                        title_pic: '',
                        title_mp3: listen[i].listening_listening.listening_e_snd,
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:'',
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function listening_group(){//聽力練習_題組題
            let listen = data.DocumentElement.Listen.listening;
            let practice = data.DocumentElement.Listen.listening_practice;
            // console.log(practice)
            let _ss=[];
            for (let i=0;i<practice.length;i++){
                // console.log(i);
                let {items,ans}=randomItems([practice[i].listening_test_c_item],practice[i].listening_test_cc_ans)
                _ss.push(
                    {
                        type:'choice',
                        title: practice[i].listening_test_ques,
                        title_pic: '',
                        title_mp3: '',
                        option_title:items[0],
                        option_pic:[],
                        option_mp3:[],
                        ans:ans,
                        feedback:'',
                        submit:[],
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function spelling_practice(){//拼字練習
            let m = JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary)) 
            let _ss=[];
            for (let i=0;i<m.length;i++){
                let mm=JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary)) 
                let _title=mm.splice(i,1)[0]
                _ss.push(
                    {
                        type:'input',
                        title: '',
                        title_pic: '',
                        title_mp3: _title.vocab_e_snd,
                        option_title:[_title.vocab],
                        option_pic:[],
                        option_mp3:[],
                        ans:[0],
                        feedback:'<div>'+_title.vocab+' '+_title.meaning_1+'</div>',
                        submit:[],
                        inputtext:'',
                        tiptext:replaceRandomHalfOfString(_title.vocab)
                    }              
                )
            }
            return shuffleArray(_ss);
            function replaceRandomHalfOfString(str) {//隨機生成提示
                const len = str.length;
                const halfLen = Math.floor(len / 2);
                let count = 0;
                let indices = [];
              
                // 找到字母字符的索引位置
                for (let i = 0; i < len; i++) {
                  if (/[a-zA-Z]/.test(str[i])) {
                    indices.push(i);
                  }
                }
              
                // 隨機選擇要替換的字符
                while (count < halfLen) {
                  const randomIndex = Math.floor(Math.random() * indices.length);
                  const index = indices[randomIndex];
                  str = str.substr(0, index) + '_' + str.substr(index + 1);
                  indices.splice(randomIndex, 1);
                  count++;
                }
              
                return str;
              }
        }
        function words_listen(){//看字辨音
            let m = JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary)) 
            let _ss=[];
            for (let i=0;i<m.length;i++){
                let mm=JSON.parse(JSON.stringify(data.DocumentElement.Vocabulary)) 
                let _title=mm.splice(i,1)[0]
                let _opt = mm.sort(() => Math.random() - 0.5).slice(0, 2);
                let optitem=[_title,_opt[0],_opt[1]]
                let {items,ans}=randomItems([optitem],[0])
                items=items[0]
                let _iseng=Math.random()<0.5
                let f_html=''
                 for(let j=0;j<items.length;j++){
                    if(_iseng){
                        f_html+='<div>'+items[j].vocab+' '+items[j].meaning_1+'</div>'
                    }else{
                        f_html+='<div>'+items[j].meaning_1+' '+items[j].vocab+'</div>'
                    }
                }
                _ss.push(
                    {
                        type:'choice',
                        title: _iseng?_title.vocab:_title.meaning_1,
                        title_pic: '',
                        title_mp3: '',
                        option_title:[],
                        option_pic:[],
                        option_mp3:[items[0].vocab_e_snd,items[1].vocab_e_snd,items[2].vocab_e_snd],
                        ans:ans,
                        feedback:f_html,
                        submit:[]
                    }              
                )
            }
            return shuffleArray(_ss);
        }
        function listen_words(){//聽音辨字
    
            let m = data.DocumentElement.Vocabulary
            let _ss=[];
            for (let i=0;i<m.length;i++){
                let qqary=[]
                let qary;
                if(true){
                    let {items,ans}=randomItems([m[i]['word_test_e_item'],m[i]['word_test_e_item_e']],m[i]['word_test_e_ans'])
                    qqary.push([shuffleArray(items),ans])
                }
                if(true){
                    let {items,ans}=randomItems([m[i]['word_test_c_item'],m[i]['word_test_c_item_c']],m[i]['word_test_c_ans'])
                    qqary.push([shuffleArray(items),ans])
                }
                let fary=[]
                let f_html=''
                qary = shuffleArray(qqary)[0];
                for(let j=0;j<qary[0][0].length;j++){
                    f_html+='<div>'+qary[0][0][j]+' '+qary[0][1][j]+'</div>'
                }
                _ss.push(
                    {
                        type:'choice',
                        title: "",
                        title_pic: '',
                        title_mp3: m[i]['vocab_e_snd'],
                        option_title: qary[0][0],
                        option_pic:[],
                        option_mp3:[],
                        ans:qary[1],
                        feedback:f_html,
                        submit:[]
                    }         
                )
            }
            return shuffleArray(_ss);
        }
        function shuffleArray(array) {
            return array.sort(() => Math.random() - 0.5);
        }

    
    }
    constructor(_data,_word,jq_dom/**jquery的UI對象 */,_type='listen_words',_again=1/**可再答次數 */,_group=1/**題組每題 */){
        let self=this;
        self.view=$(jq_dom);
        jq_dom=self.view;
        let jsonData=Quiz.convertDataToExamJson(_data,_word,_type)
        console.log(JSON.stringify(jsonData))
        // return;
        let StatusChangeCallback;
        let EndCallback;
        let AllresetCallback;//如果有此項目，則按下allreset時，將不會自動進行重新測驗，而是呼叫此函數。
        let bingo = 0;  //答對題數
        let wrong = 0;  //答錯題數
        let score = 0;//分數
        allViewHide()
        self.addEventStatusChange=(callback)=>{
            //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答,重新做答
            StatusChangeCallback=callback;
        }
        self.addEventEnd=(callback)=>{
            EndCallback=callback;
            //data={score:50,right:5,wrong:5}
        }
        self.addResetCallback=(callback)=>{//如果有此項目，則按下allreset時，將不會自動進行重新測驗，而是呼叫此函數。
            AllresetCallback=callback;
        }
        let audio = new Audio();


        let index_now = 0;
        if($('[name="input_view"]',jq_dom).length>0){
            //有輸入欄位，就先登錄下來
            templete["input_view"] = $('[name="input_view"]',jq_dom)[0].outerHTML;
            // $('[name="input_view"]',jq_dom).remove();
        }
        if($('[name="titlegrid_view"]',jq_dom).length>0){
            //表格化的題目(配對題才會用到)
            templete["titlegrid_view"] = $('[name="titlegrid_view"]',jq_dom)[0].outerHTML;
            $('[name="titlegrid_view"]',jq_dom).remove();
        }
        if(true){
            //配對專用
            
            if($('[name="pair_item"]',jq_dom).length>0){
                templete['pair_item']=$('[name="pair_item"]',jq_dom)[0].outerHTML;
                $('[name="pair_item"]',jq_dom).remove();
            }
            // if($('[name="pair_btn_empty"]',jq_dom).length>0){
            //     templete['pair_btn_empty']=$('[name="pair_btn_empty"]',jq_dom)[0].outerHTML;
            //     $('[name="pair_btn_empty"]',jq_dom).remove();
            // }
            // if($('[name="pair_btn_target"]',jq_dom).length>0){
            //     templete['pair_btn_target']=$('[name="pair_btn_target"]',jq_dom)[0].outerHTML;
            //     $('[name="pair_btn_target"]',jq_dom).remove();
            // }
            if($('[name="pair_btn_play"]',jq_dom).length>0){
                templete['pair_btn_play']=$('[name="pair_btn_play"]',jq_dom)[0].outerHTML;
                $('[name="pair_btn_play"]',jq_dom).remove();
            }
            if($('[name="pair_table_view"]',jq_dom).length>0){
                templete['pair_table_view']=$('[name="pair_table_view"]',jq_dom)[0].outerHTML;
                $('[name="pair_table_view"]',jq_dom).remove();
            }
            if($('[name="pair_feedback_view"]',jq_dom).length>0){
                templete['pair_feedback_view']=$('[name="pair_feedback_view"]',jq_dom)[0].outerHTML;
                $('[name="pair_feedback_view"]',jq_dom).remove();
            }
            if($('[name="pair_options"]',jq_dom).length>0){
                templete['pair_options']=$('[name="pair_options"]',jq_dom)[0].outerHTML;
                $('[name="pair_options"]',jq_dom).remove();
            }
            if($('[name="write_pair"]',jq_dom).length>0){
                templete["write_pair"]=$('[name="write_pair"]',jq_dom)[0].outerHTML;
                $('[name="write_pair"]',jq_dom).remove();
            }

            templete['pair_item_selected']=$('[name="pair_item_selected"]',jq_dom)
            

        }
        let option_selected_style = $('[name="option_selected"]',jq_dom);
        if($('[name="option"]',jq_dom).length>0){
            templete['option'] = $('[name="option"]',jq_dom)[0].outerHTML;
        }
        let option_selectStyle = $('[name="option_selected"]',jq_dom);
        let option_selectBingo = $('[name="option_bingo"]',jq_dom);
        let option_selectWrong = $('[name="option_wrong"]',jq_dom);
        $('[name="option_container"]',jq_dom).empty();
        if($('[name="ans_o"]',jq_dom).length>0){
            templete["ans_o"] = $('[name="ans_o"]',jq_dom)[0].outerHTML;
            templete["ans_x"] = $('[name="ans_x"]',jq_dom)[0].outerHTML;
        }
        $('[name="ansox"]',jq_dom).empty();
        if($('[name="question_item"]',jq_dom).length>0){
            templete["question_item"] = $('[name="question_item"]',jq_dom)[0].outerHTML;
        }
        $('[name="question_container"]',jq_dom).empty();

        setTimeout(()=>{
            // showScore();
            // showRecordList()
            goNext()
        },200)
        function goNext(){
            $('[name="question_container"]',jq_dom).first().empty()
            // index++;
            allViewHide()
            
            $('[name="num_no"]',jq_dom).text(Math.ceil((index_now+1)/_group))
            $('[name="num_total"]',jq_dom).text('/ '+Math.ceil(jsonData.length/_group))
            let _aa = $('<div/>')
            let itemsrow=[]
            let ox=[]
            let again_count=0;
            //題組題出題
            let len=Math.min(index_now+_group,jsonData.length)
            console.log('index==',index_now,len)
            for(let i=index_now;i<len;i++){
                 let p=drawQuestionItem(index_now,true);
                itemsrow.push(p)
                _aa.append(p);
                index_now++
            }
            console.log('index==:',index_now)
            $('[name="question_container"]',jq_dom).first().append(_aa);
            
            $('[name="btn_submit"]',jq_dom).unbind('click').bind('click', (e)=> {
                $('[name="btn_submit"]',jq_dom).hide();
                $('[name="btn_next"]',jq_dom).hide();
                $('[name="btn_again"]',jq_dom).hide();
                $('[name="feedback_view"]',jq_dom).hide();
                $('[name="btn_score"]',jq_dom).hide()
                let isallpase=true;
                for(let i=0;i<itemsrow.length;i++){
                    let pass=itemsrow[i].submit(e)
                    ox.push(pass);
                    if(!pass){
                        isallpase=false;
                        if(itemsrow[i].type=='input'){
                            itemsrow[i].showtips()
                            // $('[name="tips_view"]',jq_dom).show()
                        }
                    }
                }
                if (!isallpase && again_count < _again) {
                    $('[name="btn_again"]',jq_dom).show()
                    // if(itemsrow[i].type=='input'){
                    // //if( jsonData[i].type=='input'){
                    //     $('[name="tips_view"]',jq_dom).show()
                    // }
                } else {
                    for(let i=0;i<itemsrow.length;i++){
                        itemsrow[i].showNextAndFeedback()
                    }
                    if(index_now<=jsonData.length-1){
                        $('[name="btn_next"]',jq_dom).show()
                    }else{
                        $('[name="btn_score"]',jq_dom).show()
                    }
                }
            });
            $('[name="btn_again"]',jq_dom).unbind('click').bind('click', (e)=> {
                $('[name="btn_submit"]',jq_dom).hide();
                $('[name="btn_next"]',jq_dom).hide();
                $('[name="btn_again"]',jq_dom).hide();
                $('[name="feedback_view"]',jq_dom).hide();
                $('[name="btn_score"]',jq_dom).hide()
                again_count++;
                for(let i=0;i<itemsrow.length;i++){
                    if( !ox[i]){
                        itemsrow[i].again(e);
                    }
                }
            });

            StatusChangeCallback&&StatusChangeCallback('測驗進行中',index_now,jsonData[index_now])
        }
        function goNext_______(){
            $('[name="question_container"]',jq_dom).first().empty()
            index_now++;
            allViewHide()
            $('[name="num_no"]',jq_dom).text(index_now+1)
            $('[name="num_total"]',jq_dom).text('/ '+jsonData.length)
            let _aa = drawQuestionItem(index_now,true);
            
            $('[name="question_container"]',jq_dom).first().append(_aa);
            StatusChangeCallback&&StatusChangeCallback('測驗進行中',index_now,jsonData[index_now])
        }
        function showRecordList() {//列出作答紀錄
            allViewHide()
            // $('[name="btn_submit"]',jq_dom).hide();
            // $('[name="btn_next"]',jq_dom).hide();
            // $('[name="btn_again"]',jq_dom).hide();
            // $('[name="feedback_view"]',jq_dom).hide();
            // $('[name="btn_score"]',jq_dom).hide()
            $('[name="answer_record_view"]',jq_dom).show();
            $('[name="score_view"]',jq_dom).show();
            $('[name="btn_allreset"]',jq_dom).show()
            if(score<100){
                $('[name="btn_wrong"]',jq_dom).show()
            }
            $('[name="btn_record_view_close"]').unbind('click').bind('click',(e)=>{
                $('[name="answer_record_view"]',jq_dom).hide();
                $('[name="btn_record"]',jq_dom).show()
            })
            // $('[name="listening"]',jq_dom).hide();
            // $('[name="answer_record_view"]',jq_dom).show();
            let aa = $('[name="answer_record_view"]',jq_dom);
            $('[name="container"]', aa).empty();
            for (let i = 0; i < jsonData.length; i++) {
                let _aa = drawQuestionItem(i,false);
                $('[name="container"]', aa).first().append(_aa);
    
            }
            StatusChangeCallback&&StatusChangeCallback('呈現答題記錄')
        }

          
        function drawQuestionItem(_index,isActive=true/**是否互動 */) {

            let itemary = [];
            let question_item = $(templete["question_item"]);
            question_item.submit=()=>{
                if(question_item.type=='input' || question_item.type=='fillin'){
                    $('[name="input_view"] input',question_item).prop('disabled', true);
                    jsonData[_index].inputtext=$('[name="input_view"] input',question_item).first().val()
                    if($('[name="input_view"] input',question_item).first().val()==jsonData[_index].option_title[0]){
                        showOX('O')
                        jsonData[_index].submit=[0]
                        question_item.showNextAndFeedback()
                        return true;
                    }else{
                        jsonData[_index].submit=[1]
                        showOX('X')
                    }
                }else{
                    btnIsOpen = false;
                    jsonData[_index].submit = []
                    for (let i = 0; i < itemary.length; i++) {
                        let p = itemary[i];
                        if (p.selected) {
                            jsonData[_index].submit.push(p.i);
                        }
                    }
                    if (AnsIsEqual(jsonData[_index].ans, jsonData[_index].submit)) {
                        showOX('O')
                        // showNextAndFeedback()
                        question_item.showNextAndFeedback()
                        return true;
                    } else {
                        showOX('X')
                    }
                }
                return false;
            };
            question_item.again=()=>{
                btnIsOpen = true;
                if( jsonData[_index].type=='input' || jsonData[_index].type=='fillin'){
                    //輸入聊天
                    $('[name="input_view"] input',question_item).first().val('')
                    $('[name="input_view"] input',question_item).prop('disabled', false);
                    $('[name="btn_submit"]',question_item).hide();
                }else{
                    for (let i = 0; i < itemary.length; i++) {
                        let p = itemary[i];
                        p.selected == false;
                        toggleElements(p, option_selectStyle, false, false);
                        toggleElements(p, option_selectBingo, false, false);
                        toggleElements(p, option_selectWrong, false, false);
                    }
                }
                $('[name="ansox"]',question_item).first().empty();
                return;

            };
            question_item.type=jsonData[_index].type;
            question_item.showtips=()=>{
                $('[name="tips_view"]',question_item).show()
            }
            question_item.showNextAndFeedback=()=>{
                for (let i = 0; i < itemary.length; i++) {
                    let p = itemary[i]
                    checkItemStatus(_index, p)
                }
                if (_index >= jsonData.length - 1) {
                    // $('[name="btn_score"]',jq_dom).show();
                } else {
                    if(jsonData[_index].feedback!=''){
                        $('[name="feedback_view"]',jq_dom).show();
                    }
                    // $('[name="btn_next"]',jq_dom).show();
                }
            }
            let btnIsOpen = true;

            $('[name="option_container"]', question_item).empty();
            // $('[name="btn_again"]',jq_dom).unbind('click').bind('click', function () {
            //     //再一次按鈕
            //     again(e)


            // });

            //下一題按鈕
            $('[name="btn_next"]',jq_dom).unbind('click').bind('click', function () {
                // drawQuestionItem();
                goNext()
            });

            //重新作答按鈕
            $('[name="btn_allreset"]',jq_dom).unbind('click').bind('click', function () {
                reset();
            });

            //觀看成績
            $('[name="btn_score"]',jq_dom).unbind('click').bind('click', function () {
                //統計總成績
                showScore()
                // StatusChangeCallback&&StatusChangeCallback('呈現答題記錄')
            });

            // 作答紀錄呈現
            // console.log($('[name="btn_record"]',jq_dom))
            $('[name="btn_record"]',jq_dom).unbind('click').bind('click', function () {
                // console.log('出現紀錄')
                $('[name="btn_record"]',jq_dom).hide()
                showRecordList();
            });

            //練習錯題
            $('[name="btn_wrong"]',jq_dom).unbind('click').bind('click', function () {
                for (let i = 0; i < jsonData.length; i++) {
                    if (AnsIsEqual(jsonData[i].submit, jsonData[i].ans)) {
                        jsonData.splice(i, 1);
                        i--;
                        continue;
                    }
                    jsonData[i].submit = []
                }
                index_now=0
                $('[name="title"]',jq_dom).show();
                $('[name="question_container"]',jq_dom).show();
                $('[name="score_view"]',jq_dom).hide();
                goNext();
                StatusChangeCallback&&StatusChangeCallback('開始錯題重答')
            });
            // 作答紀錄呈現圈叉
            // $('[name="btn_submit"]',jq_dom).unbind('click').bind('click', (e)=> {
            //     submit(e);
            // });

            if(jsonData[_index].type=='pair'){
                console.log('配對題')

                question_item.empty();

                let optionview=$(templete['pair_options']);


                let btnio=true;

                //除標題之外其他清空
                let titary=[]
                let select1=null;
                $('[name="feedback_view"] [name="feedback_content"]',jq_dom).html(jsonData[_index].feedback)
                // $("[name='btn_next']",jq_dom).unbind('click').bind('click',(e)=>{
                //     goNext()
                // })
                // $("[name='btn_score']",jq_dom).unbind('click').bind('click',(e)=>{
                //     showScore()
                // })
                function newItem(type='item or target'){
                    let g={
                        view:$(templete['pair_item']),
                        type:type,
                        title:'',
                        index:-1,
                        settitle:(title)=>{
                            g.title=title;
                            $('[name="pair_btn_target"]',g.view).text(title)
                        },
                        showempty:()=>{
                            $('[name="pair_btn_empty"]',g.view).show();
                            $('[name="pair_btn_target"]',g.view).hide();
                        },
                        showtarget:()=>{
                            $('[name="pair_btn_empty"]',g.view).hide();
                            $('[name="pair_btn_target"]',g.view).show();
                        },
                        showOX:(ox='o')=>{
                            $('[name="pair_o"]',g.view).hide();
                            $('[name="pair_x"]',g.view).hide();
                            if(ox=='o'){
                                $('[name="pair_o"]',g.view).show();
                            }else{
                                $('[name="pair_x"]',g.view).show();
                            }
                        },
                        init:()=>{
                            $('[name="pair_o"]',g.view).hide();
                            $('[name="pair_x"]',g.view).hide();
                            g.view.click(g.click);
                        },
                        fillitem:null,
                        fillto:null,
                        setEmpty:()=>{
                            if(g.fillitem){
                                g.fillitem.showtarget();
                                g.fillitem.fillto=null;
                            }
                            g.fillitem=null;
                            g.settitle('');
                            g.showempty()
                        },
                        setFill:(item)=>{
                            if(g.fillitem){
                                //原有的要彈回
                                g.fillitem.showtarget()
                                g.fillitem.fillto=null;
                            }
                            g.fillitem=item;
                            g.settitle(item.title);
                            g.showtarget();
                            //被甚麼填入了
                        },
                        click:(e)=>{
                            if(!isActive){
                                return;
                            }
                            if(!btnio){
                                return;
                            }
                            if(g.type=='target'){
                                    if(select1==g){
                                        //丟棄原有
                                        g.setEmpty()
                                        select1=null;
                                        toggleElements(g.view, templete['pair_item_selected'],true,true,0)
                                        return;
                                    }
                                    if(select1 && select1!=g){
                                        //之前的要棄選
                                        toggleElements(select1.view, templete['pair_item_selected'],true,true,0)
                                    }
                                    //呈現選取
                                    select1=g;
                                    toggleElements(g.view, templete['pair_item_selected'],true,true,0)
                            }else{
                                //item
                                // console.log(select1.view.parent().index())
                                if(select1){
                                    if(g.fillto && g.fillto!=select1){
                                        // console.log('fillto.index='+g.fillto.view.parent().index())
                                        g.fillto.setEmpty();
                                        g.fillto=null;
                                        // toggleElements(g.fillto.view, templete['pair_item_selected'],true,true,0)
                                    }
                                    
                                    select1.setFill(g);
                                    g.fillto=select1;
                                    g.showempty()
                                    let _a=[]
                                    for(let i=0;i<titary.length;i++){
                                        if(titary[i].fillitem){
                                            _a.push(titary[i].fillitem.index)
                                        }
                                    }
                                    // console.log(_a);
                                    if(_a.length>=titary.length){
                                        $('[name="btn_submit"]',jq_dom).show();
                                    }
                                }
                            }
                            
                        },
                        pairtarget:null
                    }
                    g.init()
                    return g; 
                }
                if(jsonData[_index].subtype=='vocabulary_test'){
                    let tbview=$(templete['pair_table_view']);
                    question_item.append(tbview);
                    $('tbody tr td:not(:first-child)',tbview).empty()
                    for (let i=0;i<jsonData[_index].title.length;i++){
                        
                        $('tbody tr:first td:eq('+(i+1)+')',tbview).first().text(jsonData[_index].title[i])
                        let pp=newItem('target');
                        
                        pp.showempty()
                        pp.settitle('')
                        titary.push(pp);
                        $('tbody tr:eq(1) td:eq('+(i+1)+')',tbview).first().append(pp.view);
                        let playbtn=$(templete['pair_btn_play']);
                        playbtn.mp3=jsonData[_index].title_mp3[i];
                        $('tbody tr:eq(2) td:eq('+(i+1)+')',tbview).first().append(playbtn);
                        // if(isActive){
                        playbtn.unbind('click').bind('click',(e)=>{
                            play('../material/'+playbtn.mp3+'.mp3')
                        })
                        // }
                    }
                }else if(jsonData[_index].subtype=='writing_test'){
                    let p=$(templete['write_pair']);
                    question_item.append(p);
                    var title = jsonData[_index].title.replace(/\/n/g, "<br>");

                    // 遍历option_title数组中的所有字符串
                    for (var i = 0; i < jsonData[_index].option_title.length; i++) {
                        // 找到第一个匹配的单词并将其替换为<button>元素\

                        var option_title = jsonData[_index].option_title[i];
                        var pattern = new RegExp("\\b" + option_title + "\\b");
                        var match = pattern.exec(title);
                        if (match) {
                            var index = match.index;
                            //   title = title.substring(0, index) + '<gg index='+i+'>' + match[0] + '</gg>' + title.substring(index + match[0].length);
                            title = title.substring(0, index) + '<span name="b'+i+'" style="display: inline-block"></span>' + title.substring(index + match[0].length);
                            continue;
                        }
                    }
                    let content=$('[name="content"]',p)
                    content.html(title)
                    for(let i=0;i<jsonData[_index].option_title.length;i++){
                        let pp=newItem('target');
                        pp.showempty()
                        pp.settitle(jsonData[_index].option_title[i])
                        titary.push(pp);
                        // console.log(pp.title)
                        $('[name="b'+i+'"]',content).append(pp.view)
                    }
                    // console.log()
                    // titary=titary.sort(() => Math.random() - 0.5);

                }
                question_item.append(optionview);
                let optary=[]
                for(let i=0;i<jsonData[_index].option_title.length;i++){
                    let pp=newItem('item');
                    pp.showtarget()
                    pp.index=i;
                    pp.settitle(jsonData[_index].option_title[i])
                    optary.push(pp);
                    // optionview.append(pp.view);
                }
                optary.sort(() => Math.random() - 0.5);
                
                 for(let i=0;i<optary.length;i++){
                    //混亂項目
                   optionview.append(optary[i].view);
                }
                $('[name="btn_submit"]',jq_dom).unbind('click').bind('click',(e)=>{
                    $('[name="btn_submit"]',jq_dom).hide();
                    btnio=false;
                    let g=[];
                    for(let i=0;i<titary.length;i++){
                        g.push(titary[i].fillitem.index)
                    }
                    jsonData[_index].submit=g;
                    checkSubmitAndShowOX()
                    $('[name="feedback_view"]',jq_dom).show();
                    if(_index>=jsonData.length-1){
                        $('[name="btn_score"]',jq_dom).show()
                    }else{
                        $("[name='btn_next']",jq_dom).show()
                    }

                })
                if (!isActive) {
                    checkSubmitAndShowOX()
                }
                return question_item;
                function checkSubmitAndShowOX(){
                    
                    let submit=jsonData[_index].submit;
                    let ans=jsonData[_index].ans;
                    // console.log(jsonData[_index].title)
                    // console.log(jsonData[_index].option_title)
                    // console.log(submit,ans)
                    let c=true;
                    for(let i=0;i<ans.length;i++){
                        titary[i].setFill(optary[submit[i]])
                        optary[submit[i]].setEmpty()
                        // console.log(ans[i],submit[i])
                        if(ans[i]==submit[i]){
                            titary[i].showOX('o')
                        }else{
                            c=false;
                            titary[i].showOX('x')
                        }
                    }

                }

            }
            // if(jsonData[_index].type=='input'){
            //     $(templete["input_view"])
            // }




            // console.log( 'iiii',$('[name="feedback_view"] [name="feedback_content"]',question_item))
            $('[name="feedback_view"] [name="feedback_content"]',jq_dom).html(jsonData[_index].feedback)
    
            $('[name="question_container"]',jq_dom).empty();
            $('[name="question_container"]',jq_dom).append(question_item);
            $('[name="ansox"]', question_item).empty();

            if(jsonData[_index].title==''){
                $('[name="title"]',question_item).hide();
            }else{
                
                if(jsonData[_index].type=='fillin'){
                    var newText = jsonData[_index].title.replace( jsonData[_index].option_title[0], templete["input_view"]); 
                    $('[name="title"]',question_item).first().append(newText);
                    let tt= $($('[name="title"] input',question_item)).first();
                    if(!isActive){
                        tt.val(jsonData[_index].inputtext)
                        tt.prop('disabled', true);
                        if($('[name="input_view"] input',jq_dom).first().val()==jsonData[_index].option_title[0]){
                            showOX('O')
                        }else{
                            showOX('X')
                        }
                        // submit(null)
                    }
                    tt.unbind('keyup').bind('keyup', (e)=>{
                        let p=tt;
                        if(e.keyCode == 13){
                            submit(e)
                        }else{
                            if(p.val().length > 0){
                                $('[name="btn_submit"]',jq_dom).show();
                            }else{
                                $('[name="btn_submit"]',jq_dom).hide();
                            }
                        }
                    })
                }else{
                    $('[name="title"]',question_item).first().text(jsonData[_index].title)
                }
            }
            if(jsonData[_index].title_mp3==''){
                $('[name="mp3"]',question_item).hide();
            }else{
                $('[name="mp3"]',question_item).unbind('click').bind('click',(e)=>{
                    play('../material/'+jsonData[_index].title_mp3+'.mp3')
                })
                //自動撥放
                if(isActive){
                    play('../material/'+jsonData[_index].title_mp3+'.mp3')
                }
            }
            if(jsonData[_index].title_pic==''){
                $('[name="pic"]',question_item).hide();
            }else{
                $('[name="pic"] img',question_item).first().attr('src','../material/'+jsonData[_index].title_pic)
            }
            
            // console.log('88888',jsonData[_index].feedback)

 
            if(jsonData[_index].type!='fillin'){//填空題不需要選項  
                redrawAllItem(_index, isActive);
            }

            return question_item
    
            function redrawAllItem(_index, isActive) {
                
    
                let option_title_ary = jsonData[_index].option_title;
                let option_pic_ary = jsonData[_index].option_pic;
                let option_mp3_ary = jsonData[_index].option_mp3;
                let ans = jsonData[_index].ans;

                //陣列中比較長度，最長的長度保留下來 
                const longestLength = [option_title_ary, option_pic_ary, option_mp3_ary].reduce((acc, curr) => {
                    if (curr.length > acc) {
                        return curr.length;
                    } else {
                        return acc;
                    }
                }, 0);
    
              
                
    
                // 答案選項呈現
                if( jsonData[_index].type=='input'){
                    $('[name="tips_view"]',jq_dom).text(jsonData[_index].tiptext)
                    $('[name="tips_view"]',jq_dom).hide()
                    $('[name="input_view"] input',jq_dom).first().val(jsonData[_index].inputtext)
                    if (!isActive) {
                        $('[name="input_view"] input',jq_dom).prop('disabled', true);
                    }
                    $('[name="input_view"] input',jq_dom).unbind('keyup').bind('keyup', (e)=>{
                        if(e.keyCode == 13){
                            submit(e)
                        }else{
                            if($('[name="input_view"] input',jq_dom).val().length > 0){
                                $('[name="btn_submit"]',jq_dom).show();
                            }else{
                                $('[name="btn_submit"]',jq_dom).hide();
                            }
                        }
                    });
                }else{
                    for (let i = 0; i < longestLength; i++) {
                        let p = $(templete['option']);
                        $('[name="option_container"]', question_item).append(p);
                        p.i = i;
                        if (option_title_ary.length == 0) {
                            $('[name="title"]', p).hide();
                        }else{
                            $('[name="title"]', p).text(option_title_ary[i]);
                        }
        
                        if (option_pic_ary.length == 0) {
                            $('[name="image_title"]', p).hide();
                        }else{
                            $('[name="image_title"] img', p).first().attr("src", '../material/'+option_pic_ary[i]);
                        }
        
                        if (option_mp3_ary.length == 0) {
                            $('[name="voice_title"]', p).hide();
                        }else{
                            $('[name=voice_title]', p).click(function () {
                                play('../material/'+option_mp3_ary[i]+'.mp3');
                            });
                        }
        
                        itemary.push(p);
                        //判斷單複選擇題
                        if (isActive) {
                            p.click((e) => {
                                if (!btnIsOpen) {
                                    return;
                                }
                                if (ans.length > 1) {
                                    //複選題
                                    p.selected = !p.selected;
                                    toggleElements(p, option_selectStyle, true, true);
                                    $('[name="btn_submit"]',jq_dom).show();
                                    if (p.selected) {
                                        toggleElements(p, option_selected_style, false, true);
                                    } else {
                                        toggleElements(p, option_selected_style, false, false);
                                    }
                                } else {
                                    //單選題
                                    toggleback();
                                    p.selected = true;
                                    toggleElements(p, option_selectStyle, false, true);
                                    $('[name="btn_submit"]',jq_dom).show();
                                    if (jsonData[_index].submit.length > 0) {
                                        if (jsonData[_index].submit[0] != p.i) {
                                            let old_p = itemary[jsonData[_index].submit[0]]
                                            toggleElements(old_p, option_selected_style, false, false);
                                            jsonData[_index].submit[0] = p.i
                                            toggleElements(p, option_selected_style, false, true);
        
                                        }
                                    }
                                }
        
                            });
                        } else {
                            checkItemStatus(_index, p);
                        }
        
        
                    }
                }
    
                if (!isActive) {
                    if (AnsIsEqual(jsonData[_index].ans, jsonData[_index].submit)) {
                        showOX('O')
                    } else {
                        showOX('X')
                    }
                }
                return question_item;  

    
    
            }
            function aga_____in(e){
                // $('[name="btn_submit"]',jq_dom).hide();
                // $('[name="btn_next"]',jq_dom).hide();
                // $('[name="btn_again"]',jq_dom).hide();
                // $('[name="feedback_view"]',jq_dom).hide();
                // $('[name="btn_score"]',jq_dom).hide()
                btnIsOpen = true;
                if( jsonData[_index].type=='input' || jsonData[_index].type=='fillin'){
                    //輸入聊天
                    $('[name="input_view"] input',jq_dom).first().val('')
                    $('[name="input_view"] input',jq_dom).prop('disabled', false);
                    $('[name="btn_submit"]',jq_dom).hide();
                }else{
                    for (let i = 0; i < itemary.length; i++) {
                        let p = itemary[i];
                        p.selected == false;
                        toggleElements(p, option_selectStyle, false, false);
                        toggleElements(p, option_selectBingo, false, false);
                        toggleElements(p, option_selectWrong, false, false);
                    }
                }
                $('[name="ansox"]',question_item).first().empty();
                return;
            }
            function sub_____mit(e){
                // $('[name="btn_submit"]',jq_dom).hide();
                // $('[name="btn_next"]',jq_dom).hide();
                // $('[name="btn_again"]',jq_dom).hide();
                // $('[name="feedback_view"]',jq_dom).hide();
                // $('[name="btn_score"]',jq_dom).hide()
                if( jsonData[_index].type=='input' || jsonData[_index].type=='fillin'){
                    // console.log($('[name="input_view"] input',jq_dom),'====')
                    $('[name="input_view"] input',jq_dom).prop('disabled', true);
                    // $('[name="input_view"] input',jq_dom).unbind('keyup');
                    jsonData[_index].inputtext=$('[name="input_view"] input',jq_dom).first().val()
                    if($('[name="input_view"] input',jq_dom).first().val()==jsonData[_index].option_title[0]){
                        showOX('O')
                        jsonData[_index].submit=[0]
                        showNextAndFeedback()
                        return;
                    }else{
                        jsonData[_index].submit=[1]
                        showOX('X')
                    }
                
                }else{
                    btnIsOpen = false;
    
                    jsonData[_index].submit = []
                    for (let i = 0; i < itemary.length; i++) {
                        let p = itemary[i];
                        if (p.selected) {
                            jsonData[_index].submit.push(p.i);
                        }
                    }
    
                    // 作答時，顯示圈叉樣式
                    // console.log(jsonData[_index].ans, jsonData[_index].submit)
                    if (AnsIsEqual(jsonData[_index].ans, jsonData[_index].submit)) {
                        showOX('O')
                        showNextAndFeedback()
                        return true;
                    } else {
                        showOX('X')
                    }
                    // if (AnsIsEqual(jsonData[_index].ans, jsonData[_index].submit)) {
                    //     showNextAndFeedback()
                    //     return;
                    // }
                }



                // if (again_count < _again) {
                    
                //     $('[name="btn_again"]',jq_dom).show()
                //     if( jsonData[_index].type=='input'){
                //         $('[name="tips_view"]',jq_dom).show()
                //     }
                // } else {
                //     showNextAndFeedback()
                // }
                return false;
                function showNextAndFeedback() {
                    for (let i = 0; i < itemary.length; i++) {
                        let p = itemary[i]
                        checkItemStatus(_index, p)
                    }
                    if (_index >= jsonData.length - 1) {
                        $('[name="btn_score"]',jq_dom).show();
                    } else {
                        if(jsonData[_index].feedback!=''){
                            $('[name="feedback_view"]',jq_dom).show();
                        }
                        $('[name="btn_next"]',jq_dom).show();
                    }
                }

            }
            function showOX(OX='O'){
                $('[name="ansox"]', question_item).empty();
                let ox;
                if (OX=='O') {
                    ox=$(templete["ans_o"])
                } else {
                    ox=$(templete["ans_x"])
                }
                // $('[name="ansox"]', question_item).css({background:'red',width:'100px'})
                // console.log('0000',ox[0].outerHTML,$('[name="ansox"]', question_item))
                // console.log($('[name="ansox"]', question_item).length)
                $('[name="ansox"]', question_item).append(ox);
            }
            function checkItemStatus(_index, p) {//呈現正確與錯誤項目的提示
                if (jsonData[_index].submit.includes(p.i)) {
                    toggleElements(p, option_selected_style, false, true);
                    if (jsonData[_index].ans.includes(p.i)) {
                        toggleElements(p, option_selectBingo, false, true);
                    } else {
                        toggleElements(p, option_selectWrong, false, true);
                    }
                } else {
                    if (jsonData[_index].ans.includes(p.i)) {
                        toggleElements(p, option_selectBingo, false, true);
                    }
                }


            }
            //未被選取選項的樣式
            function toggleback() {
                for (let i = 0; i < itemary.length; i++) {
                    let p = itemary[i];
                    p.selected = false;
                    toggleElements(p, option_selectStyle, false, false);
                }
            }
    
        }
        function allViewHide(){
            $('[name="btn_allreset"]',jq_dom).hide()
            $('[name="btn_submit"]',jq_dom).hide()
            $('[name="btn_again"]',jq_dom).hide()
            $('[name="btn_next"]',jq_dom).hide()
            $('[name="btn_score"]',jq_dom).hide()
            $('[name="btn_wrong"]',jq_dom).hide()
            $('[name="btn_record"]',jq_dom).hide()
            $('[name="feedback_view"]',jq_dom).hide();
            $('[name="answer_record_view"]',jq_dom).hide();
            $('[name="score_view"]',jq_dom).hide();
        }
        function showScore(){
            allViewHide()
            bingo=0;
            wrong=0;
            // $('[name="feedback_view"]',jq_dom).hide();
            $('[name="question_container"]',jq_dom).hide();
            // $('[name="title"]',jq_dom).hide();

            $('[name="btn_allreset"]',jq_dom).show();
            $('[name="btn_record"]',jq_dom).show()
            $('[name="btn_score"]',jq_dom).hide();
            let total = jsonData.length;  //總題數
            $('[name="btn_wrong"]',jq_dom).hide();
            for (let i = 0; i < jsonData.length; i++) {
                let p = jsonData[i]
                console.log(p.type,p.ans, p.submit)
                if(p.type=='pair'){
                    let submit=p.submit;
                    let ans=p.ans;
                    let c=true;
                    for(let ii=0;ii<ans.length;ii++){
                        if(ans[ii]!=submit[ii]){
                            c=false
                            break;
                        }
                    }
                    if(c){
                        bingo++;
                    }else{
                        wrong++;
                    }
                }else{
                    if (AnsIsEqual(p.ans, p.submit)) {
                        bingo++;
                    } else {
                        wrong++;
                    }
                }
            }


            score = Math.floor(100 / jsonData.length * bingo); //計算分數
            let score_view= $('[name="score_view"]',jq_dom);
            score_view.show();

            $('[name="num"]',score_view).text(total);
            $('[name="bingo"]',score_view).text(bingo);
            $('[name="wrong"]',score_view).text(wrong);
            // $('[name="result"]',score_view).text(score);
            if (score < 100) {
                $('[name="btn_wrong"]',jq_dom).show();
            }
            let a=new PieChart(score);
            let pv=$('[name="PieChart"]',score_view)
            $(pv).empty()
            $(pv).append(a.view);
            ////data={score:50,right:5,wrong:5}
            EndCallback&&EndCallback({score:score,right:bingo,wrong:wrong})
            StatusChangeCallback&&StatusChangeCallback('呈現成績')
            
        }
    
        //播放音樂mp3函式
        function play(mp3) {
            audio.src = mp3;
            audio.play();
        }
    
        //全部題目做完重置，重新作答
        function reset() {
            index_now = 0;
            jsonData=Quiz.convertDataToExamJson(_data,_word,_type)
            // jsonData=JSON.parse(resetJsonData);
            $('[name="question_container"]',jq_dom).show();
            StatusChangeCallback&&StatusChangeCallback('重新做答')
            if(AllresetCallback){
                allViewHide()
                AllresetCallback&&AllresetCallback(()=>{
                    goNext();
                })
            }else{
                goNext();
            }
            
        }
    
    }
}

class PieChart{
    constructor(score,progressColor='#7752D8',totalColor='#ccc',bgColor='#eee'){
        let self=this;
        let css=`<style>
        .perCirc {
            position: relative;
            text-align: center;
            width: 200px;
            height: 200px;
            border-radius: 100%;
            background-color: ${progressColor};
            background-image: linear-gradient(91deg, transparent 50%, ${totalColor} 50%), linear-gradient(90deg, ${totalColor} 50%, transparent 50%);
        }
        .perCirc .perCircInner {
            position: relative;
            top: 20px;
            left: 20px;
            text-align: center;
            width: 160px;
            height: 160px;
            border-radius: 100%;
            background-color: ${bgColor};
            display:flex; justify-content:center; align-items:center;
        }
        .perCirc .perCircInner span{
            font-weight: bold;
            color:#444;
        }
        .perCirc .perCircStat {
            font-size: 5rem;
        }
        .perCirc .perCircInner span:nth-child(2) {
            font-size: 2.5rem;
        }
        </style>
        `
        let html=`
<div class='scoregroup'>

    <div id='sellPerCirc' class='perCirc'>
        <div class='perCircInner'>
            <span class='perCircStat'>0</span><span>%</span>
        </div>
    </div>

</div>
        `
        $('head').append($(css));
        self.view=$(html);
        perCirc($('#sellPerCirc',self.view), score);
        function perCirc($el, end, i) {
            if (end < 0)
                end = 0;
            else if (end > 100)
                end = 100;
            if (typeof i === 'undefined')
                i = 0;
            var curr = (100 * i) / 360;
            $el.find(".perCircStat").html(Math.round(curr));
            if (i <= 180) {
                $el.css('background-image', 'linear-gradient(' + (90 + i) + 'deg, transparent 50%, #ccc 50%),linear-gradient(90deg, #ccc 50%, transparent 50%)');
            } else {
                $el.css('background-image', 'linear-gradient(' + (i - 90) + 'deg, transparent 50%, #7752D8 50%),linear-gradient(90deg, #ccc 50%, transparent 50%)');
            }
            if (curr < end) {
                setTimeout(function () {
                    perCirc($el, end, ++i);
                }, 1);
            }else{
                // changeClass("is-hide", "is-show", document.querySelector(".end"));
            }
        }
    }
}