/**
 * 聽音辨字
 */
let templete = {}

window.onload = function () {
    data = reduce(data)
    // console.log(JSON.stringify(data))
    // return;
    word = reduce(word)
    $('[name="btn_play"]').hide();
    let audio = new Audio();
    // console.log('是否為陣列??',data.DocumentElement.Listen.listening.listening_listening instanceof Array)

    

    // listening_listening.listening_e_snd
    //播放音樂mp3函式
    let QuizUi = $('[name="Quiz"]')
    $('[name="QuizContainer"]').empty();

    $('[name="btn_play"]').show();


    $('[name="btn_quizStart"]').hide();
    console.log('=================')
    if (data.DocumentElement.Listen.listening.listening_listening instanceof Array){
        //多個mp3
        let mp3ary=[]
        for (let i=0;i<data.DocumentElement.Listen.listening.listening_listening.length;i++){
            mp3ary.push('../material/'+data.DocumentElement.Listen.listening.listening_listening[i].listening_e_snd+ '.mp3')
        }
        console.log(mp3ary)
        $('[name="btn_play"]').unbind('click')
        new MaxMediaPlayer(mp3ary,$('[name="btn_play"]'),$('<div/>'))
    }else{
        //單一mp3
        let mp3 = data.DocumentElement.Listen.listening.listening_listening.listening_e_snd;
        $('[name="btn_play"]').unbind('click').bind('click', (e) => {
            play('../material/' + mp3 + '.mp3')
        })
    }

    let QuizObj = new Quiz(data, word, QuizUi, 'listening_group', 1, _group = 4)
    QuizObj.addEventStatusChange((status = 'testing') => {
        //status=測驗進行中,呈現成績,呈現答題記錄,開始錯題重答
        console.log(status)
    })
    QuizObj.addEventEnd((data) => {
        //data={score:50,right:5,wrong:5}
        console.log(data)
    })
    $('[name="QuizContainer"]').append(QuizObj.view);

    $('[name="btn_quizStart"]').click((e) => {
    })
    
    function play(mp3) {
        audio.src = mp3;
        audio.play();
    }

}